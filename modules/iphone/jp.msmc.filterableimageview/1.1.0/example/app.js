// This is a test harness for your module
// You should do something interesting in this harness 
// to test out the module and to provide instructions 
// to users on how to use it by example.
Ti.include('optionPickerDialog.js');

var filterableimageview = require('jp.msmc.filterableimageview');
var presets = require('filters').presets;

var window = currentWindow = Ti.UI.createWindow({
    orientationModes: [
        Ti.UI.LANDSCAPE_LEFT,
        Ti.UI.LANDSCAPE_RIGHT,
        Ti.UI.PORTRAIT,
        Ti.UI.UPSIDE_PORTRAIT
    ],
   // backgroundImage:'images/transparent.png'
});

var imageView = filterableimageview.createFilterableImageView({
    image:'images/image.png',
    /*
    overlay:[
        {image:'images/overlay.png', blendMode:Ti.UI.iOS.BLEND_MODE_OVERLAY},
        {image:'images/screen.png', blendMode:Ti.UI.iOS.BLEND_MODE_SCREEN},
        {image:'images/frame.png'}
    ],
    */
    left:10,
    right:10,
    top:10,
    bottom:54
});

var flexSpace = Ti.UI.createButton({
    systemButton:Ti.UI.iPhone.SystemButton.FLEXIBLE_SPACE
});

var photoGallery = Ti.UI.createButton({
    image:'images/photo_gallery.png',
    backgroundImage:'none',
    width:32,
    height:32,
});

var filter = Ti.UI.createButton({
    image:'images/filter.png',
    backgroundImage:'none',
    width:32,
    height:32,
});

var tiltshift = Ti.UI.createButton({
    image:'images/tiltshift.png',
    backgroundImage:'none',
    width:32,
    height:32,
});

var save = Ti.UI.createButton({
    image:'images/save.png',
    backgroundImage:'none',
    width:32,
    height:32
});

var toolbar = Ti.UI.createToolbar({
    items:[photoGallery, flexSpace, filter, flexSpace, tiltshift, flexSpace, save],
    opacity:0.8,
    barColor:'#222222',
    bottom:0
});

window.add(imageView);
window.add(toolbar);

photoGallery.addEventListener('click', function(){
    Ti.Media.openPhotoGallery({
        allowEditing:true,
        success: function(e){
		  imageView.image = e.media;
        }
    });
});

filter.addEventListener('click', function(){
    var data = []
    for(var n in presets){
        data.push({title:n, value:presets[n]});
    }
    optionPickerDialog.setData(data);
	optionPickerDialog.open();
	optionPickerDialog.addEventListener('close', function(e){
		if (e.done==true && e.selectedRow){
			Ti.API.debug(e.selectedRow.value)
			imageView.filter = e.selectedRow.value;
		}
	});	
});

tiltshift.addEventListener('click', function(){
    var data = [
        { title:'No', value:filterableimageview.TILTSHIFT_OFF },
        { title:'Circle', value:filterableimageview.TILTSHIFT_CIRCLE },
        { title:'Line', value:filterableimageview.TILTSHIFT_LINE }, 
    ];
    optionPickerDialog.setData(data);
	optionPickerDialog.open();
	optionPickerDialog.addEventListener('close', function(e){
		if (e.done==true && e.selectedRow){
			imageView.tiltshift = e.selectedRow.value;
		}
	});	
});

var imageToSave;
save.addEventListener('click', function(){
    imageToSave = imageView.filteredImage();
    Ti.Media.saveToPhotoGallery(
        imageToSave,
        {
            success:function(){
                Ti.API.info("image was successfully saved.");
            },
            error:function(e){
                Ti.API.error("image was not saved. error:"+e.error);
            }
        } 
    );
});

imageView.addEventListener('filterChanged', function(){
    Ti.API.info("filter was changed.");
});

window.open();


