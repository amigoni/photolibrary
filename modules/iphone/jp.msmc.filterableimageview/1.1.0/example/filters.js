exports.presets = {
	/*
	 * Normal
	 */
	Normal:new filterableimageview.Filter(function(){
		// NO OPERATION
	}),
    /* 
     * Basic Filters 
     */
    FillColor:new filterableimageview.Filter(function(){
        this.fillColor({r:0.5, g:0.2, b:0.7});
    }),
    Brightness:new filterableimageview.Filter(function(){
        this.brightness(0.7);
    }),
    Saturation:new filterableimageview.Filter(function(){
        this.saturation(0.6);
    }),
    Vibrance:new filterableimageview.Filter(function(){
        this.vibrance(0.6);
    }),
    Contrast:new filterableimageview.Filter(function(){
        this.contrast(0.6);
    }),
    Hue:new filterableimageview.Filter(function(){
        this.hue(0.4);
    }),
    Colorize:new filterableimageview.Filter(function(){
        this.colorize({r:0.4,g:0.2,b:0.7}, 0.8);
    }),
    Invert:new filterableimageview.Filter(function(){
        this.invert();
    }),
	Gray:new filterableimageview.Filter(function(){ 
		this.grayscale(); 
	}),
    Sepia:new filterableimageview.Filter(function(){
        this.sepia(0.8);
    }),
    Gamma:new filterableimageview.Filter(function(){
        this.gamma(1.5);
    }),
    Noise:new filterableimageview.Filter(function(){
        this.noise(0.6);
    }),
    Clip:new filterableimageview.Filter(function(){
        this.clip(0.4);
    }),
    Channels:new filterableimageview.Filter(function(){
        this.channels({r:0.4,g:0.6,b:0.2});
    }),
    Curves:new filterableimageview.Filter(function(){
        this.curves({
            chan:'rgb',
            start:[0.0, 0.0],
            ctrl1:[0.2, 0.8],
            ctrl2:[0.8, 0.2],
            end:[1.0, 1.0]
        });
    }),
    Exposure:new filterableimageview.Filter(function(){
        this.exposure(0.6);
    }),
    Gradient:new filterableimageview.Filter(function(){
        this.gradient({
            type:'linear',
            opacity:0.1,           
            colors:[
                {position:0.00,color:'#1D6B93'},
                {position:0.40,color:'#AF2B13'},
                {position:0.50,color:'#FD2C21'},
                {position:0.60,color:'#AE6C12'},
                {position:1.00,color:'#BD7C16'}
            ]
        });
    }),
    Vignette:new filterableimageview.Filter(function(){
        this.vignette(0.6, 0.8);
    }),
    CircleBlur:new filterableimageview.Filter(function(){
        this.blur({
            style:'circle',
            mask:true,
            center:{x:0.5, y:0.5},
            radius:0.6
        });
    }),
    LineBlur:new filterableimageview.Filter(function(){
        this.blur({
            style:'line',
            mask:false,
            center:{x:0.5, y:0.5},
            angle:45.0*3.14/180.0,
            thickness:0.4
        });
    }),

	/*
	 * Vintage 
	 */
	Vintage:new filterableimageview.Filter(function(){ 
		this.sepia(0.8)
			.vignette(0.8, 0.5); 
	}),
	
	/*
	 * OrangePeel
	 */
	OrangePeel:new filterableimageview.Filter(function(){ 
		this.curves({chan:'rgb', start:[0.0, 0.0], ctrl1:[0.4, 0.19], ctrl2:[0.55, 0.78], end:[1.0, 1.0]})
			.vibrance(-0.3)
		    .saturation(-0.3)
		    .colorize('#ff9000', 0.3)
		    .contrast(-0.05)
		    .gamma(1.4);
	}),
    
	/*
	 * Lomo
	 */
	Lomo:new filterableimageview.Filter(function(){
		this.brightness(0.15)
		    .exposure(0.15)
		    .curves({chan:'rgb', start:[0.0, 0.0], ctrl1:[0.78, 0.0], ctrl2:[0.6, 1.0], end:[1.0, 1.0]})
		    .saturation(-0.2)
		    .gamma(1.8)
		    .vignette(0.5, 0.6)
		    .brightness(0.05);
	}),
	
	/*
	 * Love
	 */
	Love:new filterableimageview.Filter(function(){
		this.brightness(0.05)
		    .exposure(0.08)
		    .colorize('#c42007', 0.3)
		    .vibrance(0.5)
		    .gamma(1.3);
	})
};