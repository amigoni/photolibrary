exports.loadMe  = function (){
	var moment = require('lib/Moment');
	
	globals.isLoading = true;
	//globals.tutorialModeOn = true;
	
	//setLanguage();
	
	var DataPersons = require('model/DataPersons');
	initDataBase();
	
	
	//////////////////////////////
	//TestFlight
	
	//var testflight = require('com.0x82.testflight');
	//Ti.API.info("module is => " + testflight);
	
	// WARNING: ONLY USE THIS ON DEVELOPMENT! DON'T GO TO THE APP STORE WITH THIS LINE!!
	//testflight.setDeviceIdenifier(Ti.Platform.id);
	//testflight.takeOff('7d979545243ad638851dc8f69b99a19a_MTAzNzIyMDExLTEwLTE5IDAzOjM1OjA4Ljg5NjEwOA');
	
	/////////////////////////////
	
	//Flurry Analytics
	var Flurry = require('ti.flurry');
	Flurry.initialize('6Z6SRWRSWQMYY2PV8JXT');
	Flurry.debugLogEnabled = true;
	Flurry.eventLoggingEnabled = true;
	Flurry.reportOnClose = true;
	Flurry.sessionReportsOnPauseEnabled = true;
	Flurry.secureTransportEnabled = false;
	
	//Flurry.age = 24;
	//Flurry.userID = 'John Adams';
	//Flurry.gender = 'm';
	
	//Initialize UI
	//Start all the UI
	Titanium.UI.setBackgroundColor('#000'); // this sets the background color of the master UIView (when there are no windows/tab groups on it)
	
	var Font = require('ui/style/Font');
	
	//Give time to save the new properties
	setTimeout(function(){
		//var NavigationController = require('ui/NavigationController');	
		//NavigationController.loadMe();
		var Sidebar = require('ui/Sidebar');
		Sidebar.loadNav();
		/*
		if (globals.properties.hasBeenSetup === false){
			var PopUp = require('ui/components/PopUp');
			setTimeout(function(){
				var PopUp = require('ui/components/PopUp');
				
				var titleText = "Let's get started";
				var subTitleText = "Finally organize your pictures by person.\n\nPlease select yourself\nfrom the contact list.\n\nThis helps find pictures that include you.";
				var instructions = PopUp.instructionsPopUp(titleText,subTitleText);
		
				instructions.addEventListener('clickedOk', function(){
					Ti.App.fireEvent('addContact');
				});
				
				instructions.open();
			}, 1000);
		};
		*/
	},10);
	
	
	//Delay the rest to get the UI to start
	setTimeout(function (){
		var DataPersons = require('model/DataPersons');
		DataPersons.loadAllContactsFromAddressBook();
		
		
		////Notifications
		var Notifications = require('Notifications');
		Notifications.checkNotifications();
		
		globals.isLoading = false;
		
		//News
		var DataNews = require('model/DataNews');
		DataNews.checkForNews();
	},100);
};


function setLanguage(){
	if(Ti.App.Properties.getString('lang') == null){
        if(Ti.Locale.getCurrentLanguage() == 'fr')
        {
            Ti.App.Properties.setString('lang', 'fr');
        }
        else
        {
            Ti.App.Properties.setString('lang', 'en');
        }
    }
};

function initDataBase (){
	var mySK = 'mySecretKey'
	var jsondb = require("com.irlgaming.jsondb");
	jsondb.debug(false);
	
	globals.col = {};
	
	var supportDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationSupportDirectory);
	if(!supportDir.exists()){
		supportDir.createDirectory();
		supportDir.setRemoteBackup(true);
	}
	
	var dataDir = Ti.Filesystem.getFile(Ti.Filesystem.applicationSupportDirectory, 'dataDir');
	if (!dataDir.exists()) {
	    dataDir.createDirectory();
	   	dataDir.setRemoteBackup(true);
	};
	
	dataDir = dataDir.resolve();
	
	//creating or lading all the collections
	globals.col.properties = jsondb.factory('properties', mySK,dataDir);
	globals.col.persons = jsondb.factory('persons', mySK,dataDir);
	globals.col.labels = jsondb.factory('labels', mySK,dataDir);
	globals.col.media = jsondb.factory('media', mySK,dataDir);
	globals.col.bubbles = jsondb.factory('bubbles', mySK,dataDir);
	globals.col.pictureOfTheWeek = jsondb.factory('pictureOfTheWeek', mySK,dataDir);
	globals.col.news = jsondb.factory('news',mySK,dataDir);
	
	
	//Load App Properties
	var properties = globals.col.properties.getAll();
	if (!properties[0]){
		globals.col.properties.save({
			hasBeenSetup: false,
			firstWorkingDay: 1,
			sortPeopleBy: 'firstName',
			hasRatedApp: false,
			version: globals.version,
			openCount: 1
		});
		
		globals.col.properties.commit();
		properties = globals.col.properties.getAll();
		globals.properties = properties[0];
	}
	else{
		globals.properties = properties[0];
		
		if (globals.properties.version != globals.version){
			Ti.App.fireEvent('updatedApp');
			globals.col.properties.update({ },{$set:{version: globals.version}});
		};
		
		globals.col.properties.update({ },{$set:{openCount: properties[0].openCount+1}});
		globals.col.properties.commit();
		
		//globals.properties.firstWorkingDay = 1;
		//globals.properties.sortPeopleBy = 'firstName';
		//globals.properties = {hasBeenSetup: false}
	};
	
	
	//Thumbs Filesystem
	globals.thumbsDir = Titanium.Filesystem.getFile(Ti.Filesystem.applicationSupportDirectory,'thumbsDir');	
	globals.thumbsDir.createDirectory(); // this creates the directory if it doesn't exist'
	
	globals.pictureOfTheWeekDir = Titanium.Filesystem.getFile(Ti.Filesystem.applicationSupportDirectory,'pictureOfTheWeekDir');	
	globals.pictureOfTheWeekDir.createDirectory(); // this creates the directory if it doesn't exist'
	
	return true
};