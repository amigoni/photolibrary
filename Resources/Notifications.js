///Local notification 
exports.checkNotifications = function (){
	var notify = require('bencoding.localnotify');
	
	function localNotificationCallback(e){
		Ti.API.info("Let's how many local notifications we have scheduled'");
		Ti.API.info("Scheduled LocalNotification = " + e);	
		alert("You have " +  e.scheduledCount + " Scheduled LocalNotification");
	
		var test = JSON.stringify(e);
		Ti.API.info("results stringified" + test);
	};
	
	//notify.activeScheduledNotifications(localNotificationCallback);
	notify.cancelAllLocalNotifications();
	
	//Picture of The Week
	globals.properties.notifications = {};
	globals.properties.notifications.pictureOfTheWeekEnabled = true;
	
	var thisWeekFirstWorkingDay = moment().day(globals.properties.firstWorkingDay).startOf('day');
	var lastWeekFirstWorkingDay = moment(thisWeekFirstWorkingDay).day(globals.properties.firstWorkingDay-7);
	
	var lastWeekPictureOfTheWeek = globals.col.pictureOfTheWeek.find({date: lastWeekFirstWorkingDay.valueOf()});
	var haveAlreadyPickedLastWeek = lastWeekPictureOfTheWeek.length > 0 ? true : false;
	
	if (globals.properties.notifications.pictureOfTheWeekEnabled === true){
		var nextAlertDay;
		if (moment().day() > globals.properties.firstWorkingDay || haveAlreadyPickedLastWeek === true){
			nextAlertDay = globals.properties.firstWorkingDay + 7;
		}
		else{
			nextAlertDay =  globals.properties.firstWorkingDay;
		};
		
		
		var morningNotificationTime = 9;
		var morningPictureOfTheWeek = notify.scheduleLocalNotification({
			alertBody: "Choose your Picture of the week.\n You have till Midnight",
			alertAction: "Open",
			userInfo: {type:'pictureOfTheWeek'},
			//badge:11, //Then needs to be a number
			//sound:"pop.caf", //Copy file from KitchenSink example if you want to try this out
			//date: new Date(moment().valueOf()+5000),
			date: new Date(moment().day(nextAlertDay).hours(morningNotificationTime).minutes(0).seconds(0).valueOf()),
			repeat: 'weekly' 
		});
		
		
		var eveningNotificationTime = 20;
		var eveningPictureOfTheWeek = notify.scheduleLocalNotification({
			alertBody: "Don't Forget to pick the Picture of the week.\n You have till Midnight",
			alertAction: "Open",
			userInfo: {type:'pictureOfTheWeek'},
			//badge:11, //Then needs to be a number
			//sound:"pop.caf", //Copy file from KitchenSink example if you want to try this out
			//date: new Date(moment().valueOf()+10000),
			date: new Date( moment().day(nextAlertDay).hours(eveningNotificationTime).minutes(0).seconds(0).valueOf()),
			repeat: 'weekly' 
		});
		
		Ti.API.debug('Morning POW Alert : '+moment().day(nextAlertDay).hours(morningNotificationTime).minutes(0).seconds(0).format("dddd, MMMM Do YYYY, h:mm:ss a"))
		Ti.API.debug('Evening POW Alert : '+moment().day(nextAlertDay).hours(eveningNotificationTime).minutes(0).seconds(0).format("dddd, MMMM Do YYYY, h:mm:ss a"))
	};
};

//At Start of the App
/*
 * Check to see if notifications are enable
 * Cancell All notifications
 * If it's not Monday schedule notifications for next Monday on a weekly basis'
 * If it's Monday if I have picked the POW, schedule notifications for Next Week. 
 * 	if I have not picked. schedule notifications for today
 * 
 * When I pick the picture of the week. I need to cancel all notfications and reschedule them for next Monday. 
 */	
	