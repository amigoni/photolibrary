/*
 dragDropKit: Easy drag and drop implementation for Titanium!
 Made By Peter Willemsen from PixelhungryStudios.com

 For feedback and bug reports please mail Peter Willemsen <peterw.email@gmail.com>

 Are you tired of all those buggy drag and drop implementations found on the internet?
 Does it only work on iPhone and not on Android or vice versa?
 Do you want just a lightweight, fast and easy solution to just drag and drop?

 Then dragDropKit is the module for you! It just works and has been made by a very talented programmer.
 We had the same pain, which is why we spent a lot of effort and research building a good drag and drop solution.
 Now you can have it in your apps, because we distribute our work as a module!

 Features:

 - Extremely clean code, easy to implement - almost no change needed in your code.
 - Very flexible: You can throw almost every view at it, no matter how you positioned them.
 - Takes views that either have left & top or right and bottom properties.
 - Takes views that have percents, density independent pixels or just pixels as positions or sizes
   or a combination of them!
 - Fast: dragDropKit does only minimal calculation that is needed to be as stable yet as fast as possible.

 That's right, this is your turnkey solution to drag and drop!
*/
var d=null,e=!1,g=d,k=d,l=-1,m=[],n=10,o=10,p=e;;function q(a,b){return parseInt(b)*(parseInt(a)/100)}var r=Titanium.Platform.displayCaps.dpi/160;function t(a){return-1!=(a+"").indexOf("dp")?parseFloat(a)*r:parseFloat(a)}
function u(a,b,c,f){c={direction:c,pinchToZoom:f};"undefined"==typeof b.c&&(b.c=e);"undefined"==typeof b.b&&(b.b=e);"undefined"!=typeof b.size.width&&(-1==(b.width+"").indexOf("%")&&!b.c?c.width=t(b.size.width):(f=a.size.width,0>=a.size.width&&(f=Titanium.Platform.displayCaps.platformWidth),c.width=q((b.width+"").replace("%",""),f)));"undefined"!=typeof b.size.height&&(c.height=-1==(b.height+"").indexOf("%")&&!b.b?t(b.size.height):q((b.height+"").replace("%",""),a.size.height));"undefined"==typeof b.left&&
"undefined"==typeof b.right&&(f=a.size.width,0>=a.size.width&&(f=Titanium.Platform.displayCaps.platformWidth),c.left=f/2-c.width/2);"undefined"==typeof b.top&&"undefined"==typeof b.bottom&&(f=a.size.height,0>=a.size.height&&(f=Titanium.Platform.displayCaps.platformHeight),c.top=f/2-c.height/2);"undefined"!=typeof b.left&&(c.left=-1==(b.left+"").indexOf("%")?t(b.left):q(b.left.replace("%",""),a.size.width));"undefined"!=typeof b.right&&(c.right=-1==(b.right+"").indexOf("%")?t(b.right):q(b.right.replace("%",
""),a.size.width));"undefined"!=typeof b.top&&(c.top=-1==(b.top+"").indexOf("%")?t(b.top):q(b.top.replace("%",""),a.size.height));"undefined"!=typeof b.bottom&&(c.bottom=-1==(b.bottom+"").indexOf("%")?t(b.bottom):q(b.bottom.replace("%",""),a.size.height));c.view=b;c.d="undefined"!=typeof c.right;c.a="undefined"!=typeof c.bottom;return c}
function v(a){p=!0;for(var b in m){var c=m[b],f=a.x,j=a.y;n=f;o=j;var h=d,i=d;c.d?(f=k.size.width-a.x,h=f>c.right&&f<c.right+c.width):h=f>c.left&&f<c.left+c.width;c.a?(j=k.size.height-a.y,i=j>c.bottom&&j<c.bottom+c.height):i=j>c.top&&j<c.top+c.height;if(h&&i){n=a.x;o=a.y;l=parseInt(b);m[l].view.fireEvent("touchstart",{x:a.x,y:a.y,source:m[l].view});break}}}
function w(a){-1<l&&(m[l]=u(k,m[l].view,m[l].direction,m[l].pinchToZoom),m[l].view.fireEvent("touchend",{x:a.x,y:a.y,source:m[l].view}),p&&(m[l].view.fireEvent("click",{x:a.x,y:a.y,source:m[l].view}),p=e),l=-1)}
function x(a){p=e;-1<l&&(-1!=m[l].direction.indexOf("x")&&(m[l].d?m[l].view.right=m[l].right-a.x+n:m[l].view.left=m[l].left+a.x-n),-1!=m[l].direction.indexOf("y")&&(m[l].a?m[l].view.bottom=m[l].bottom-a.y+o:m[l].view.top=m[l].top+a.y-o),m[l].view.fireEvent("touchmove",{x:a.x,y:a.y,source:m[l].view}))}
exports.detach=function(a){for(var b=0;b<m.length;b++)if(m[b].view===a){m.splice(b,1);break}0==m.length&&(g!=d&&(g.removeEventListener("touchstart",v),g.removeEventListener("touchend",w),g.removeEventListener("touchmove",x),k.remove(g),g=d),l=-1,Ti.API.debug('Detached '+m.length))};exports.detachAll=function(){for(var a=0;a<m.length;a++)exports.detach(m[a].view)};
exports.attach=function(a){setTimeout(function(){k=a.container;var b="xy",c=e;"undefined"!=typeof a.direction&&(b=a.direction.toLowerCase());"undefined"!=typeof a.pinchToZoom&&(c=a.pinchToZoom);g==d&&(g="undefined"==typeof a.dragContainer?Ti.UI.createView({left:0,top:0,bottom:0,right:0,zIndex:9999}):a.dragContainer,a.container.add(g),g.addEventListener("touchend",w),g.addEventListener("touchstart",v),g.addEventListener("touchmove",x));if(c){var f=Ti.Platform.displayCaps.platformWidth/2,j=Ti.Platform.displayCaps.platformHeight/
2;g.addEventListener("pinch",function(a){for(var b in m){var c=m[b];if(c.pinchToZoom){var h=c.view;c.view.transform=Ti.UI.create2DMatrix().scale(a.scale);var i=f,s=j;f=Ti.Platform.displayCaps.platformWidth/2*a.scale;j=Ti.Platform.displayCaps.platformHeight/2*a.scale;i=f-i;s=j-s;h.left=h.left-i/2;h.top=h.top-s/2;c.width=c.width*a.scale;c.height=c.height*a.scale;c.left=c.left-i/2;c.top=c.top-s/2}}})}var h=e,i;for(i in m)if(m[i].view==a.view){h=!0;break}h||m.push(u(g,a.view,b,c));Ti.API.debug('Attached '+m.length)},500)};
