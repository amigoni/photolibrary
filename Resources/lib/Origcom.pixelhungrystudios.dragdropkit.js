/*
 dragDropKit: Easy drag and drop implementation for Titanium!
 Made By Peter Willemsen from PixelhungryStudios.com

 For feedback and bug reports please mail Peter Willemsen <peterw.email@gmail.com>

 Are you tired of all those buggy drag and drop implementations found on the internet?
 Does it only work on iPhone and not on Android or vice versa?
 Do you want just a lightweight, fast and easy solution to just drag and drop?

 Then dragDropKit is the module for you! It just works and has been made by a very talented programmer.
 We had the same pain, which is why we spent a lot of effort and research building a good drag and drop solution.
 Now you can have it in your apps, because we distribute our work as a module!

 Features:

 - Extremely clean code, easy to implement - almost no change needed in your code.
 - Very flexible: You can throw almost every view at it, no matter how you positioned them.
 - Takes views that either have left & top or right and bottom properties.
 - Takes views that have percents, density independent pixels or just pixels as positions or sizes
   or a combination of them!
 - Fast: dragDropKit does only minimal calculation that is needed to be as stable yet as fast as possible.

 That's right, this is your turnkey solution to drag and drop!
*/
var d=null,e=d,g=d,h=-1,i=[],j=10,l=10,win;function m(b,a){return parseInt(a)*(parseInt(b)/100)}var n=Titanium.Platform.displayCaps.dpi/160;function q(b){return-1!=(b+"").indexOf("dp")?parseFloat(b)*n:parseFloat(b)}
function r(b,a,c){c={direction:c};"undefined"==typeof a.c&&(a.c=!1);"undefined"==typeof a.b&&(a.b=!1);"undefined"!=typeof a.width&&(c.width=-1==(a.width+"").indexOf("%")&&!a.c?q(a.size.width):m((a.width+"").replace("%",""),b.size.width));"undefined"!=typeof a.height&&(c.height=-1==(a.height+"").indexOf("%")&&!a.b?q(a.size.height):m((a.height+"").replace("%",""),b.size.height));if("undefined"==typeof a.left&&"undefined"==typeof a.right){var f=b.size.width;0>=b.size.width&&(f=Titanium.Platform.displayCaps.platformWidth);
c.left=f/2-c.width/2}"undefined"==typeof a.top&&"undefined"==typeof a.bottom&&(f=b.size.height,0>=b.size.height&&(f=Titanium.Platform.displayCaps.platformHeight),c.top=f/2-c.height/2);"undefined"!=typeof a.left&&(c.left=-1==(a.left+"").indexOf("%")?q(a.left):m(a.left.replace("%",""),b.size.width));"undefined"!=typeof a.right&&(c.right=-1==(a.right+"").indexOf("%")?q(a.right):m(a.right.replace("%",""),b.size.width));"undefined"!=typeof a.top&&(c.top=-1==(a.top+"").indexOf("%")?q(a.top):m(a.top.replace("%",
""),b.size.height));"undefined"!=typeof a.bottom&&(c.bottom=-1==(a.bottom+"").indexOf("%")?q(a.bottom):m(a.bottom.replace("%",""),b.size.height));c.view=a;c.d="undefined"!=typeof c.right;c.a="undefined"!=typeof c.bottom;return c}
function s(b){for(var a in i){var c=i[a],f=b.x,k=b.y;j=f;l=k;var o=d,p=d;c.d?(f=g.size.width-b.x,o=f>c.right&&f<c.right+c.width):o=f>c.left&&f<c.left+c.width;c.a?(k=g.size.height-b.y,p=k>c.bottom&&k<c.bottom+c.height):p=k>c.top&&k<c.top+c.height;if(o&&p){j=b.x;l=b.y;h=parseInt(a);i[h].view.fireEvent("touchstart",{x:b.x,y:b.y,source:i[h].view});break}}}function t(b){if(win.moveModeOn === true){-1<h&&(i[h]=r(g,i[h].view,i[h].direction),i[h].view.fireEvent("touchend",{x:b.x,y:b.y,source:i[h].view}),h=-1)}}
function u(b){if(win.moveModeOn === true){-1<h&&(-1!=i[h].direction.indexOf("x")&&(i[h].d?i[h].view.right=i[h].right-b.x+j:i[h].view.left=i[h].left+b.x-j),-1!=i[h].direction.indexOf("y")&&(i[h].a?i[h].view.bottom=i[h].bottom-b.y+l:i[h].view.top=i[h].top+b.y-l),i[h].view.fireEvent("touchmove",{x:b.x,y:b.y,source:i[h].view}))}}
exports.detachAll=function(b){if(i.length>0){for(var a=0;a<i.length;a++){i.splice(a,1)};i=[];0==i.length&&(e.removeEventListener("touchstart",s),e.removeEventListener("touchend",t),e.removeEventListener("touchmove",u),g.remove(e),e=d,h=-1)}};
exports.attach=function(b){b.container.addEventListener("postlayout",function(){win=b.win;g=b.container;var a="xy";"undefined"!=typeof b.direction&&(a=b.direction.toLowerCase());e==d&&(e="undefined"==typeof b.dragContainer?Ti.UI.createView({left:0,top:0,bottom:0,right:0,zIndex:9999}):b.dragContainer,b.container.add(e),e.addEventListener("touchend",t),e.addEventListener("touchstart",s),e.addEventListener("touchmove",u));var c=!1,f;for(f in i)if(i[f].view==b.view){c=!0;break}c||i.push(r(e,b.view,a))})};