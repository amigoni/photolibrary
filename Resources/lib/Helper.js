exports.capitaliseFirstLetter = function (string){
    return string.charAt(0).toUpperCase() + string.slice(1);
};


exports.imageResize2 = function (screenWidth,screenHeight,picWidth,picHeight,scrollView){
	var w2HRatio = picWidth/picHeight;
	var h2WRatio = 1/w2HRatio;
	
	var newWidth;
	var newHeight;
	var initialScale;
	
	//Screen in Landscape 
	if(screenWidth > screenHeight){
		//Pic is Landscape
		if(picWidth > picHeight){
			newWidth = screenWidth;
			newHeight = screenWidth*picHeight/picWidth;
			
			//If the newHeight is too High need to shrink the picture
			//if(newHeight > screenHeight){
			//newHeight = screenHeight;
			//newWidth = screenHeight *picWidth/picHeight;
			//if (scrollView){scrollView.scrollEnabled = false};
			//}
		}
		//Pic is Portrait
		else{
			newHeight = screenHeight;
			newWidth = screenHeight*picWidth/picHeight;
		};
	}		
	else{ //Screen in Portrait
		//Pic is Portrait
		if(picWidth < picHeight){
			newHeight = screenHeight
			newWidth = screenHeight*picWidth/picHeight;
			initialScale = newWidth > screenWidth ? newWidth/screenWidth : 1;
			//newHeight = screenWidth*picHeight/picWidth;
			
			//If the newHeight is too High need to shrink the picture
			if(newHeight > screenHeight){
				newHeight = screenHeight;
				newWidth = screenHeight *picWidth/picHeight;
			};
		}
		//Pic is Landscape
		else{
			newWidth = screenWidth;
			newHeight = screenWidth*picHeight/picWidth;
		};
	};	
	//alert({width:newWidth, height:newHeight})
	return {width:newWidth, height:newHeight , initialScale: initialScale}
};


exports.imageResizeFill = function (screenWidth,screenHeight,picWidth,picHeight,scrollView){
	var w2HRatio = picWidth/picHeight;
	var h2WRatio = 1/w2HRatio;
	
	var newWidth;
	var newHeight;
	var initialScale;
	
	//Screen in Landscape 
	if(screenWidth > screenHeight){
		//Pic is Landscape
		if( picWidth > picHeight){
			newWidth = screenWidth;
			newHeight = screenWidth*picHeight/picWidth;
			
			//If the newHeight is too High need to shrink the picture
			if(newHeight > screenHeight){
				newHeight = screenHeight;
				newWidth = newHeight *picWidth/picHeight;
			};
		}
		//Pic is Portrait
		else{
			newHeight = screenHeight;
			newWidth = screenHeight*picWidth/picHeight;
		};
	}		
	else{ //Screen in Portrait
		//Pic is Portrait
		if(picWidth < picHeight){
			newHeight = screenHeight
			newWidth = screenHeight*picWidth/picHeight;
			initialScale = newWidth > screenWidth ? newWidth/screenWidth : 1;
			//newHeight = screenWidth*picHeight/picWidth;
			
			//If the newHeight is too High need to shrink the picture
			if(newWidth > screenWidth){
				newWidth = screenWidth;
				newHeight = newWidth *picHeight/picWidth;
			};
		}
		//Pic is Landscape
		else{
			newWidth = screenWidth;
			newHeight = screenWidth*picHeight/picWidth;
		};
	};	
	//alert({width:newWidth, height:newHeight})
	return {width:newWidth, height:newHeight , initialScale: initialScale}
};


exports.cleanMe = function(dirtyO){
	var children = dirtyO.children;
	
	for (var i =0; i < children.length; i++){
		dirtyO.remove(children[i]);
		children[i] = null;
	};
};

	



