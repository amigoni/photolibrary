exports.loadWin = function (imageID){
	var Window = require('ui/components/Window');
	var TopNavigation = require('ui/components/TopNavigation');
	var DataMedia = require('model/DataMedia');
	var DataPersons = require('model/DataPersons');
	
	//var win = Window.createWindow();
	var base = Titanium.UI.createWindow({
		opacity:0
	});
	
	var win = Ti.UI.createWindow({
		backgroundColor: 'transparent',
		barColor: '#000',
		//navBarHidden: false,
		//tabBarHidden: true,
		width: '100%',
		height: '100%',
		title: 'Tag People'
	});
	
	var nav = Titanium.UI.iPhone.createNavigationGroup({
	   window: win
	});
	
	base.add(nav);

	var doneButton = TopNavigation.doneButton();
	
	doneButton.addEventListener('click', function (){
		//Add tags to the media. 
		//DataMedia.addPeopleToMedia(imageID,tagged);
		base.close();
	});
	 
	win.rightNavButton = doneButton;

	
	//Data
	var data = DataMedia.loadPeoplePickerData(imageID);
	var taggedArray = data.taggedArray;
	var taggedDetailArray = data.taggedDetailArray;
	var fullArray = data.addressBookContacts;
	/*
	//1 Get the info about the media
	
	
	//Array of Tagged  to avoid double entry
	var tagged = media.addressBookIDs;
	media.addressBookIDs = null; //Avoid Pollution
	
	
	var addressBookContacts = DataPersons.loadAllContactsFromAddressBook();
	*/
	//
	
	var bg = Ti.UI.createView({
		backgroundColor:'black',
		opacity: .3
	});
	
	
	//Fotter Area with Bubbles
	var footerArea = Ti.UI.createView({
		backgroundImage: 'images/BottomPeopleListBG.png',
		//backgroundColor: 
		width:'100%',
		height: 80,
		//borderColor:'white',
		bottom:0,
		opactiy: .8
	});
	
	var footerIcon = Ti.UI.createView({
		backgroundImage:'images/People.png',
		width:38,
		height: 24,
		top:5,
		left:5,
		transform: Ti.UI.create2DMatrix().scale(.75)
	});
	
	var footerScrollView = Ti.UI.createScrollView({
		//backgroundColor: 'red',
		width: 320-40,
		height: 75,
		top:5,
		left: 40,
		contentHeight: 'auto',
		contentWidth: 320-40,
		showVerticalScrollIndicator: true
		//layout:'horizontal',
	});
	
	footerScrollView.contentContainer = Ti.UI.createView({
		top:0,
		width: 320-40,
		height: Ti.UI.SIZE,
		layout:'horizontal'
	});
	
	//Add Bubbles. Need person Name from AddressBookID
	for (var i=0; i < taggedDetailArray.length; i++){
		addTagBubble(taggedDetailArray[i].title,taggedDetailArray[i].id);
	};
	
	//Ti.API.debug(addressBookIDs)
	
	footerScrollView.add(footerScrollView.contentContainer);
	
	footerArea.add(footerIcon);
	footerArea.add(footerScrollView)
	
	//Favorites Tableview
	/*
	var favoritesTableView = Ti.UI.createTableView({
		backgroundColor:'transparent',
		width: '100%',
		height: 480-44-80-35,
		top: 35,
		//separatorStyle: false
		separatorColor: 'white',
		opacity: .8,
		visible: true
	});
	
	var favoritesTableData = [];
	
	setTimeout(function (){
	for (var i =0; i < favorites.length; i++){
		favorites[i].index = i;
		var row = addToTable( favorites[i]);
		favoritesTableData.push(row);
	};
	tableView.index = index;
	tableView.setData(tableData);
	},10);
	*/
	

	//All Contacts Tableview
	var tableView = Ti.UI.createTableView({
		backgroundColor:'transparent',
		width: '100%',
		height: 480-44-80-0,
		top: 0,
		//separatorStyle: false
		separatorColor: 'white',
		opacity: .8,
		visible: true
	});
	
	tableView.appendRow(Ti.UI.createTableViewRow({
		backgroundColor:'transparent',
			width: '100%',
			height: 40,
			color: 'white',
			font: {fontSize: 10},
			title:'Loading Contacts...',
			//className: 'PersonPickerPerson',
			//opacity:.3
		})
	);
	
	tableView.addEventListener('click', function(e){
		//Add or remove Checkmark
		//var row = e.rowData;
		if(e.rowData.selected == false){
			e.rowData.selected = true;
			e.rowData.checkMark.opacity = 1; //Used opacity because of bug making groove disappear. 
			addTagBubble(e.rowData.title,e.rowData.id);
			Ti.App.fireEvent('updatePersonsList');
			Ti.App.fireEvent('updatePicturesList');
		}
		else{
			e.rowData.selected = false;
			e.rowData.checkMark.opacity = 0;
			removeTagBubble(e.rowData.id);
			Ti.App.fireEvent('updatePersonsList');
			Ti.App.fireEvent('updatePicturesList');
		};	
	});
	
	function addTagBubble (title, id){
		//Check to see if the person id is already in the array if not add the person
		var view = Ti.UI.createView({
			//backgroundImage:'images/Tag_Bubble.png',
			//backgroundLeftCap: 30,
			backgroundColor: 'transparent',
			borderColor: 'white',
			borderRadius: 12,
			width:128,
			height: 24,
			top:2,
			left:7,
			title:title,
			id: id
			//layout:'horizontal'
		});
		
		var label = Ti.UI.createLabel({
			text: title+'   ',
			font: {fontSize:12,fontWeight: 'bold'},
			color: 'white',
			left: 10,
			width: 'auto',
			height:20
		});
		
		view.add(label);
		
		view.addEventListener('click', function (){
			//Make the bubble red the first time you tap it and delete it the second time
			
			if (view.readyToDelete == true){
				//Loop Through all the rows to remove the checkmark.
				for (var j= 0; j < tableView.data.length; j++){
					var sectionArray = tableView.data[j].rows;
					for (var i =0; i < sectionArray.length; i++){
						if (sectionArray[i].id === id){
							sectionArray[i].selected = false;
							sectionArray[i].checkMark.opacity = 0; 
						};
					};
				};
				
				
				footerScrollView.contentContainer.remove(view);
						
				DataMedia.removeTagFromMedia(data.media.URL,'person',id);
				
				Ti.App.fireEvent('updatePersonsList');
				Ti.App.fireEvent('updatePicturesList');
			}else{
				view.readyToDelete = true;
				view.backgroundColor = 'red';
				setTimeout(function(){
					if(view){
						view.backgroundColor = 'transparent';
						view.readyToDelete = false;
					};
				},2000);
			};
		});

		footerScrollView.contentContainer.add(view);
		
		DataMedia.addTagToMedia(data.media.URL,'person',id);
	};
	
	//Removes the Tag Bubble when you click on the row. 
	function removeTagBubble(id){
		var bubble;
		var bubbles = footerScrollView.contentContainer.children;
		//Ti.API.debug(parent);
		
		// Loop through all the tag bubbles to find the one to remove
		for (var i =0; i < bubbles.length; i++){
			Ti.API.debug(bubbles[i]);
			if (bubbles[i] && bubbles[i].id === id){
				bubble = bubbles[i];
				//Ti.API.debug(bubble);
				footerScrollView.contentContainer.remove(bubble);
			};
		};
				
		DataMedia.removeTagFromMedia(data.media.URL,'person',id);
	};
	
	var createRow = function (title,id,selected){
		var row = Ti.UI.createTableViewRow({
			backgroundColor:'transparent',
			width: '100%',
			height: 40,
			color: 'white',
			font: {fontSize: 10},
			title:title,
			id: id,
			selected: selected,
			className: 'PersonPickerPerson',
		});
		
		//var whiteLine = Ti.UI.createView({backgroundColor:'white',height:1,width:row.width,top:row.height-2, opacity: .2});
		//var blackLine = Ti.UI.createView({backgroundColor:'black',height:1,width:row.width,top:row.height-1, opacity:.4});
		
		/*
		var label  = Titanium.UI.createLabel({
			left: 15,
			text:contactData.fullName,
			font:{fontSize:15,fontWeight:'bold'},
			textAlign:'left',
			color:'white',
			height:20,
			//width:'auto',
			backgroundColor:'transparent',
			touchEnabled: false
		});	
		*/
		
		var checkMark = Ti.UI.createView({
			backgroundImage: 'images/Check_Mark.png',
			width: 29,
			height: 29,
			opacity: selected == false ? 0 :1
		});
		
		checkMark.right = 0;
		
		row.checkMark = checkMark;
		
		row.add(checkMark);
		//row.add(whiteLine);
		//row.add(blackLine);
		//row.add(label);
		
		return row;
	};
	
	var alphabetRow = function (title){
		var row = Ti.UI.createTableViewRow({
			backgroundColor:'transparent',
			width: '100%',
			height: 20,
			color: 'white',
			//font: {fontSize: 10},
			title:title,
			className: 'alphabetRow',
			opacity:.1
		});
		
		return row;
	};
	
	
	//Load TableData
	var tableData= [];
	
	//Small Function to add to table.
	function addToTable ( itemData){
		//Find if it's already tagged';
		var selected = (taggedArray.indexOf(itemData.id) == -1) ? false : true;
		var row = createRow(itemData.name, itemData.id, selected);
		var letter = itemData.name[0].toUpperCase();  
	        if(letter != cheader){
	            cheader = letter;
	           	row.header = cheader;
	           	//tableData.push(alphabetRow(contactData.fullName[0].toUpperCase()))
	            index.push({title:cheader ,index: itemData.index});
	        };  
		
		return row;
	};
	
	//Load Table Data
	var cheader = '';
	var index = [];
	
	//Optimized load. Add the first 20 rows then start appending the others. 
	if ( fullArray.length >= 2000){
		for (var i =0; i < 2; i++){
			fullArray[i].index = i;
			var row = addToTable( fullArray[i]);
			tableData.push(row);
		};
		
		tableView.setData(tableData);
		
		setTimeout(function (){
			for(var j= 2; j < fullArray.length; j++){
				fullArray[j].index = j;
				var row = addToTable( fullArray[j]);
				tableData.push(row);
			};
			
			tableView.index = index;
			tableView.setData(tableData);
		},50);
	}
	else{
		setTimeout(function (){
		for (var i =0; i < fullArray.length; i++){
			fullArray[i].index = i;
			var row = addToTable( fullArray[i]);
			tableData.push(row);
		};
		tableView.index = index;
		tableView.setData(tableData);
		},200);
	};
	
	
	///Filter Bar
	/*
	var tb1 = Titanium.UI.iOS.createTabbedBar({
		labels:['Favorites', 'All Contacts'],
		backgroundColor:'#black',
		top:5,
		//style:Titanium.UI.iPhone.SystemButtonStyle.BAR,
		height:25,
		width:300,
		index:0
	});
	
	tb1.addEventListener('click', function (e){
		if (e.index === 0 ){
			tableView.visible = false;
			favoritesTableView.visible = true;
		}
		else{
			favoritesTableView.visible = false;
			tableView.visible = true;
		};
	})
	*/
	
	//Assembleing View. 
	win.add(bg);
	win.add(footerArea);
	win.add(tableView);
	//win.add(tb1);
	
	base.addEventListener('open', function (){
		base.animate(Ti.UI.createAnimation({
			duration: 100,
			opacity: 1
		}));
	});
	
	base.open();	
	
};