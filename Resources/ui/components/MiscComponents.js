exports.persistantButton = function (dictionary){
	
	var unsel = dictionary.backgroundImage;
	var sel = dictionary.backgroundSelectedImage;
	if (!dictionary.selected){ dictionary.selected = false};
	
	var view = Ti.UI.createView({
		backgroundImage: dictionary.selected?sel : unsel,
		selected: dictionary.selected
	});
	
	view.reset = function (value){
		if (value === true){
			view.selected = true; 
			view.backgroundImage = sel;
		}
		else if (!value || value === false){
			view.selected = false; 
			view.backgroundImage = unsel;
		};
	};
	
	view.addEventListener('click', function (){
		if (view.selected === true){
			view.selected = false; 
			view.backgroundImage = unsel;
		}
		else if (view.selected === false){
			view.selected = true; 
			view.backgroundImage = sel;
		};
	});
	
	return view;
};


exports.standardTableView = function (){
	//Create Tableview
	var tableView = Ti.UI.createTableView({
		backgroundColor: 'transparent',
		width: '100%',
		height: Ti.Platform.displayCaps.platformHeight-42,
		top: 44,
		editable: true,
		editing: false,
		separatorStyle: false,
		selectionStyle: 0,
	});
	
	function reset(){
		if(tableView.rowSelected && tableView.rowSelected.selectionOverlay){
			tableView.rowSelected.selectionOverlay.opacity =.05;
			tableView.rowSelected.groove.visible = true;
			tableView.rowSelected = null;
		};
	};
	
	tableView.addEventListener('scroll', function(e){
		//Currently doesn't work need to put the identifier in the touchstart row listener'
		reset();
	});
	
	tableView.addEventListener('touchstart', function(e){
		tableView.rowSelected = e.row;
		tableView.rowSelected.index = e.index;
	});
	
	/*
	tableView.addEventListener('click', function (){
		tableView.clicked = true;
		setTimeout(function(){
			tableView.clicked === false; 
		},200);
		Ti.API.debug('Clicked')
	});
	*/
	
	tableView.reset = reset;
	
	return tableView;
};


exports.standarRowWithImage = function(title, number, image, rheader,editable){

	var Labels = require('ui/components/Labels');
	
	//var contactInfo = Ti.Contacts.getPersonByID(data.addressBookID);
	
	var row = Ti.UI.createTableViewRow({
		header: rheader? rheader: null,
		//backgroundColor:'transparent',
		backgroundSelectedImage:'images/row_selected.png',
		backgroundFocusedImage: 'images/row_selected.png',
		className: 'personRow',
		hasArrow: true,
		//data: data,
		height: 50,
		editable:editable?editable:false,
		//leftImage: image
		name:title
	});
	
	var bottomGroove = Ti.UI.createView({width:row.width,height:1, top:row.height-1,backgroundColor:'black',opacity:.05});
	
	var topGroove = Ti.UI.createView({width:row.width,height:1,top:0, backgroundColor:'white'});
	
	var img = Ti.UI.createView({
		backgroundImage: image,
		left: 10,
		top:4,
		height: 40,
		width: 40,
		touchEnabled:false
	});
	
	var nameLabel = Ti.UI.createLabel({
		text: title,
		color: globals.fontColors[2],
		font: {fontWeight: 'bold', fontSize: 17},
		shadowColor: '#fff',
		shadowOffset:{x:1,y:1},
		width: 200,
		left: 60,
		height:20,
		touchEnabled:false
	});
	
	var countLabel = Labels.label({
		text: '('+number+')',
		color: globals.fontColors[0],
		right: 15,
		height: 20,
		touchEnabled:false
	});
	
	var selectionOverlay = Ti.UI.createView({
		backgroundImage: 'images/row_selected.png',
		width:320,
		height:row.height,
		opacity: 0.02,
	});
	
	var longClick = false;
	selectionOverlay.addEventListener('touchstart', function (e){
		selectionOverlay.opacity = 1;
		//groove.visible = false;
		setTimeout(function(){
			longClick = true;
		},1500);
	});
	
	/*
	selectionOverlay.addEventListener('touchend', function(e){
		if (longClick === true) {
			selectionOverlay.opacity = .01;
		}
	});
	*/

	
	row.add(topGroove);
	row.add(bottomGroove);
	row.add(img);
	row.add(nameLabel);
	row.add(countLabel);
	row.add(selectionOverlay);
	
	row.selectionOverlay = selectionOverlay;
	row.groove = bottomGroove;
	row.countLabel = countLabel;
	
	return row;
};


exports.createAddPhotosRow = function (tagName){
	var row = Ti.UI.createTableViewRow({
		height: 50,
		editable: false,
		selectionStyle: false
	});
	
	var button = Ti.UI.createView({
		backgroundImage:'images/big_grey_button.png',
		width:Ti.UI.SIZE,
		backgroundLeftCap: 80,
		height: 44,
		top: 10,
		layout: 'horizontal'
	});
	
	
	button.add(Ti.UI.createLabel({
		//width: 160,
		height: 25,
		text: I('quick_tag'),
		textAlign: 'center',
		left: 55,
		top: 8,
		font:{fontSize: 20, fontWeight: 'bold'},
		shadowColor: 'black',
		shadowOffset:{x:-1,y:-1},
		color: '#FCFFF4'
	}));
	
	
	button.add(Ti.UI.createView({
		width: 20,
	}));
	
	/*
	var helpLabel = Ti.UI.createLabel({
		text: 'The quickest way to tag.\nSimply tap all the photos with '+tagName,
		width: 300,
		height: Ti.UI.SIZE,
		textAlign: 'center',
		top: 55,
		font: {fontSize: 14},
		color: globals.fontColors[2],
		shadowColor: 'white',
		shadowOffset:{x:0,y:1}
	});
	
	row.update = function (tagName1){
		helpLabel.text = 'The quickest way to tag.\nSimply tap all the photos with '+tagName1
	};
	
	*/
	function clean(){
		var children = row.children;
		for(var i = children.length-1 ; i >= 0;  i--){
			row.remove(children[i]);
			children[i] = null;
		};
		Ti.API.debug('Cleaned HeaderRow');
	};
	
	
	row.add(button);
	//row.add(helpLabel);
	
	row.clean = clean;
	
	return row;
};


exports.createThumbHeaderRow = function (type){
	var row = Ti.UI.createView({
		height: 230,
		opacity:1
	});
	
	//Limiting dimension is height of 210
	var whiteBorder = Ti.UI.createView({
		backgroundImage: 'images/picture_white_border.png',
		backgroundTopCap: 25,
		backgroundLeftCap: 25,
		width: Ti.UI.SIZE,	
		height: Ti.UI.SIZE
	});
	

	
	var picture = Ti.UI.createView({
		backgroundImage: type === 'person' ? 'images/cover_empty_big.png' :'images/cover_empty_label_big.png',
		backgroundColor: '#CCCCCC',
		width: 200,
		height: 200,
		top: 7,
		bottom: 10,
		left: 10,
		right: 10
	});
	//picture.grayscale();
	
	picture.addEventListener('postlayout', function (e){
		//whiteBorder.width = picture.size.width + 15;
		//whiteBorder.height = picture.size.height +15;
	});
	
	whiteBorder.add(picture);
	
	var thumbnailTypeLabel = Ti.UI.createLabel({
		text: '',
		//left: 0,
		//bottom:0,
		width: Ti.UI.SIZE,
		height: 20,
		textAlign: 'right',
		font: {fontSize: 8},
		color: globals.fontColors[2],
		shadowColor: 'white',
		shadowOffset: {x:0, y:1},
		opacity: 1,
		top: 205,
		right: 160 - 200/2
	});
	
	row.add(whiteBorder);
	row.add(thumbnailTypeLabel);
	
	function update (image){
		if(image){
			//row.opacity = 0;
			var Helper = require('lib/Helper');
			
			var newSize = Helper.imageResizeFill( 320-20, row.height-20, image.width, image.height);
		
			picture.backgroundImage = image;
			//picture.image = image;
			//picture.filter =[{name: 'grayscale'}]
			picture.width = newSize.width;
			picture.height = newSize.height;
			picture.left = 10;
			row.opacity = 1;
			thumbnailTypeLabel.opacity = 1;
			thumbnailTypeLabel.top = newSize.height;
			thumbnailTypeLabel.right = 160-newSize.width/2;
		}
		Ti.App.fireEvent('thumbHeaderRowLoaded');
	};
	
	
	function clean(){
		whiteBorder.remove(picture);
		picture = null;
		
		var children = row.children;
		for(var i = children.length-1 ; i >= 0;  i--){
			row.remove(children[i]);
			children[i] = null;
		};
		Ti.API.debug('Cleaned HeaderRow');
	};
	
	
	row.clean = clean;
	row.update = update;
	row.whiteBorder = whiteBorder;
	row.thumbnailTypeLabel = thumbnailTypeLabel;
	
	return row;
};



exports.createStandardThumbnail = function (index, data){
	var view = Ti.UI.createView({
		//backgroundImage: data.thumbnail? data.thumbnail : null,
		left: 5,
		width: 100,
		height: 100,//thumb.height,
		borderColor: '#E7E7E7',
		//index: index,
		visible: false,
		//URL: data.URL
		selected: false,
		kind: 'thumbnail'
	});
	
	function toggleOverlay (mode){
		mode = mode ? mode : 'single';
		if (mode === 'single'){
			toggleSingleSelectionOverlay();
		}
		else if( mode === 'multiple'){
			toggleMultipleSelectionOverlay();
		};
	};
	
	function toggleSingleSelectionOverlay(){
		if (!view.overlay){
			var overlay = Ti.UI.createView({
				backgroundColor:'black',
				width: 100-1,
				height: view.height-1,
				opacity: 0.35,
				touchEnabled: false,
				kind: 'darkOverlay'
			});
			
			view.add(overlay);
			
			view.overlay = overlay;
			
			setTimeout( function (){
				view.remove(view.overlay);
				view.overlay = null;
			}, 1000);
		};
	};

	
	function toggleMultipleSelectionOverlay (){
		if (view.selected === true){
			view.remove(view.overlayWhite);
			view.remove(view.checkMark);
			view.overlay = null;
			view.checkMark = null;
			view.selected = false;
		}
		else{
			var overlayWhite = Ti.UI.createView({
				backgroundColor:'white',
				width: 100-1,
				height: view.height-1,
				opacity:0.35,
				touchEnabled: false
			});
			
			var checkMark = Ti.UI.createView({
				backgroundImage: 'images/check_mark_yellow.png',
				width: 29,
				height: 29,
				right: 5,
				bottom: 5,
				touchEnabled: false
			});
			
			view.add(overlayWhite);
			view.add(checkMark);
			
			view.overlayWhite = overlayWhite;
			view.checkMark = checkMark;
			view.selected = true;
		}
	};
	
	view.toggleOverlay = toggleOverlay;
	
	
	return view;
};	

			
			

exports.createEmptyThumbnail = function  (index){
	//var thumb = leoImageResize(imageData.aspectRatioThumbnail.width, imageData.aspectRatioThumbnail.height,thumbWidth,null);
	var view = Ti.UI.createView({
		//backgroundImage: imageData.thumbnail,//globals.thumbsDir.nativePath+data.$id+'.jpg',
		backgroundColor: 'e6e6e6',
		left: 5,
		width:100,
		height:100,//thumb.height,
		borderColor: '#E7E7E7',
		index: index,
		visible: true
	});
	
	return view;
};	


var groove = function (top){
	var container = Ti.UI.createView({width:320,height:2, top:top,backgroundColor:'transparent',opacity:1});
	
	var top = Ti.UI.createView({width:container.width,height:1, top:0,backgroundColor:'black',opacity:.05});
	
	var bottom = Ti.UI.createView({width:container.width,height:1,top:1, backgroundColor:'white'});
	
	container.add(top);
	container.add(bottom);
	
	return container;
};

//This is a custome Activity Indicator, not using it for now. 
exports.loadingWheel = function (){
	var isLoading = true;
	var runAnim=function(){
	  // stop rotating the image if not loading
	  if (!isLoading) return;
	  // start rotating the image, 'recursively' call runAnim when rotation ends
	  imgView.animate({
	    duration : 1000,
	    anchorPoint : {x : 0.5, y : 0.5},
	    transform : Ti.UI.create2DMatrix().rotate(360)
	  }, runAnim); 
	};
	runAnim();
};


exports.optionDialog = function (){
	var win = Ti.UI.createWindow({
		//backgroundColor: '#000',
		width: '100%',
		height: Ti.UI.SIZE,
		bottom:-480
	});
	
	var bg = Ti.UI.createView({
		backgroundImage: 'images/bg_white.png',
		backgroundTopCap: 25,
		//backgroundColor: '#000',
		width: '100%',
		height: Ti.UI.SIZE,
		layout: 'vertical',
		//opacity: .8
	});
	
	bg.addEventListener('postlayout', function (){
		win.bottom = -bg.size.height;
	});
	
	
	win.add(bg);
	
	
	win.addEventListener('open', function (){
		win.animate(Ti.UI.createAnimation({
			duration: 250,
			bottom: 0,
		}));
	});
	
	win.addEventListener('close', function (){
		win.animate(Ti.UI.createAnimation({
			duration: 250,
			bottom: -bg.size.height,
		}));
	});
	
	return win;
};


exports.bigGrayButton = function (buttonName){
	var button = Ti.UI.createView({
		backgroundImage:'images/grey_button.png',
		width:Ti.UI.SIZE,
		backgroundLeftCap: 22,
		height: 44,
		//top: 10,
		layout: 'horizontal'
	});
	
	
	button.add(Ti.UI.createLabel({
		width: Ti.UI.SIZE,
		height: 25,
		text: buttonName,
		textAlign: 'center',
		left: 10,
		right: 10,
		top: 8,
		font:{fontSize: 20, fontWeight: 'bold'},
		shadowColor: 'black',
		shadowOffset:{x:-1,y:-1},
		color: '#FCFFF4'
	}));
	
	return button
}
