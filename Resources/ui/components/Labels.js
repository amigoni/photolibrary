exports.label = function (args){
	
	var label = Ti.UI.createLabel({
		text:args.text,
		textAlign: args.textAlign? args.textAlign : 'center',
		right: args.right? args.right : null,
		left: args.left? args.left: null,
		height: args.height? args.height : null,
		color: args.color? args.color: null
	});
	
	return label;
};
