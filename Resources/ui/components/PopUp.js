exports.bg = function (){
	var view = Ti.UI.createView({
		backgroundImage: 'images/popUp_bg.png',
		backgroundLeftCap: 50,
		backgroundTopCap: 50,
		width: 324,
		height: 480
	});
	
	return view;
};



exports.groove = function (top,width){
	if (!width){width = 290;}
	
	var container = Ti.UI.createView({width:width,height:2, top:top,backgroundColor:'transparent',opacity:.6});
	
	var top = Ti.UI.createView({width:width,height:1, top:0,backgroundColor:'black'});
	
	var bottom = Ti.UI.createView({width:width,height:1,top:1, backgroundColor:'white'});
	
	container.add(top);
	container.add(bottom);
	
	return container;
};

exports.alertDialog = function (titleText, subtitleText){
	var PopUp = require('ui/components/PopUp');
	var DataLabels = require('model/DataLabels');
	
	var win = Ti.UI.createWindow({
		tabBarHidden: true,
		navBarHidden:true,
		width:'100%',
		height: '100%',
		opacity:1
	});
	
	
	var bgShade = Ti.UI.createView({
		backgroundColor: 'black',
		width: '100%',
		height: '100%',
		opacity: 0
	});
	
	
	var bg = PopUp.bg();
	bg.height = Ti.UI.SIZE;
	bg.layout = 'vertical';
	bg.width = 300;
	//bg.top = 50;
	bg.transform = Ti.UI.create2DMatrix().scale(0);
	
	
	var titleLabel = Ti.UI.createLabel({
		color: 'white',
		textAlign: 'center',
		font: {fontSize: 20, fontWeight: 'bold'},
		width: 270,
		top: 10,
		text: titleText
	});
	
	
	var subtitleLabel = Ti.UI.createLabel({
		color: 'white',
		font:  {fontSize: 15, fontWeight: 'regular'},
		textAlign: 'center',
		//top: 5,
		width: 270,
		text: subtitleText
	});

	
	//Two Buttons Config
	var buttonContainer = Ti.UI.createView({
		width: 300,
		top: 5,
		bottom: 10,
		height: Ti.UI.SIZE
	});
	
	var okButton = Ti.UI.createLabel({
		//backgroundColor: 'transparent',
		text: 'OK',
		color: 'white',
		font: {fontSize: 30,fontWeight: 'bold'},
		//top: 130,
		left: 60
	});
	
	okButton.addEventListener('click', function (){
		win.fireEvent('ok');
		closeAnimation();
	});
	
	
	var cancelButton = Ti.UI.createLabel({
		//backgroundColor: 'transparent',
		text: I('Cancel'),
		color: 'white',
		font: {fontSize: 30, fontWeight: 'bold'},
		//top: okButton.top
		right: 60
	});
	
	cancelButton.addEventListener('click', function (){
		closeAnimation();
	});
	
	buttonContainer.add(okButton);
	buttonContainer.add(cancelButton);
	
	function closeAnimation(){
		bg.animate(Ti.UI.createAnimation({
			duration: 250,
			transform: Ti.UI.create2DMatrix().scale(0),
			opacity:0
		}));
		
		bgShade.animate(Ti.UI.createAnimation({
			duration: 250,
			//transform: Ti.UI.create2DMatrix().scale(0),
			opacity:0
		}));
		
		setTimeout(function (){
			win.close();
		}, 300);
	};

	//Assemble Everything
	bg.add(titleLabel);
	bg.add(subtitleLabel);
	bg.add(buttonContainer);

	win.add(bgShade);
	win.add(bg);
	
	win.addEventListener('close', function (){
		var Helper = require('lib/Helper');
		Helper.cleanMe(bg);
		
		win.remove(bgShade);
		win.remove(bg);
		bgShade = null;
		bg = null;
		win = null;
	});
	
	
	win.addEventListener ('open', function (){
		bg.animate(Ti.UI.createAnimation({
			delay: 100,
			duration: 200,
			transform: Ti.UI.create2DMatrix().scale(1),
			opacity:1
		}));
		
		bgShade.animate(Ti.UI.createAnimation({
			duration: 500,
			opacity:.4
		}));
	});
	
	return win;
};

exports.alertDialogOld = function (titleText,subTitleText,buttonsDictionary){
	var PopUp = require('ui/components/PopUp');
	
	var win = Ti.UI.createWindow({
		tabBarHidden: true,
		navBarHidden:true,
		width:'100%',
		height: '100%',
		opacity:1
	});
	
	
	var bgShade = Ti.UI.createView({
		backgroundColor: 'black',
		width: '100%',
		height: '100%',
		opacity: 0
	});
	
	
	var bg = PopUp.bg();
	bg.transform = Ti.UI.create2DMatrix().scale(0);
	bg.height = 230;
	
	/*
	var closeButton = Titanium.UI.createButton({
		backgroundImage:'images/nav_x.png',
		width:21,
		height:20,
		top:20,
		left:20
	});
	*/
	
	var closeAnimation = function (){
		bg.animate(Ti.UI.createAnimation({
			duration: 250,
			transform: Ti.UI.create2DMatrix().scale(0),
			opacity:0
		}));
		
		bgShade.animate(Ti.UI.createAnimation({
			duration: 250,
			//transform: Ti.UI.create2DMatrix().scale(0),
			opacity:0
		}));
		
		setTimeout(function (){
			win.close();
		}, 300);
	}
	
	/*
	closeButton.addEventListener('click', function (){
		closeAnimation();
	});
	*/
	
	
	var groove = PopUp.groove(54);
	
	
	var title = Ti.UI.createLabel({
		color: 'white',
		font: {fontSize: 20, fontWeight: 'bold'},
		top: 25,
		text: titleText
	});
	
	
	var subTitle1 = Ti.UI.createLabel({
		color: 'white',
		font:  {fontSize: 15, fontWeight: 'regular'},
		textAlign: 'center',
		top: 60,
		text: subTitleText
	});
	
	//One Button Config
	var singleButton = Ti.UI.createLabel({
		text: 'OK',
		color: 'white',
		font: {fontSize: 30,fontWeight: 'bold'},
		top: 130,
		//left: 60
	});
	
	singleButton.addEventListener('click', function (){
		closeAnimation();
	});
	
	
	//Two Buttons Config
	var yesButton = Ti.UI.createLabel({
		//backgroundColor: 'transparent',
		text: 'OK',
		color: 'white',
		font: {fontSize: 30,fontWeight: 'bold'},
		top: 130,
		left: 60
	});
	
	yesButton.addEventListener('click', function (){
		//Tag all the pictures from the url. 
		//var DataPersons = require('model/DataPersons');
		//DataPersons.checkFacesAlbumForPersonID(newPersonID);
		win.fireEvent('OKpressed');
	});
	
	
	var noButton = Ti.UI.createLabel({
		//backgroundColor: 'transparent',
		text: I('Cancel'),
		color: 'white',
		font: {fontSize: 30, fontWeight: 'bold'},
		top: yesButton.top,
		right: 60
	});
	
	noButton.addEventListener('click', function (){
		closeAnimation();
	});
	
	
	var footerLabel = Ti.UI.createLabel({
		color: 'white',
		font:  {fontSize: 13, fontWeight: 'regular'},
		textAlign: 'center',
		top: 170,
		width: 280,
		text: '( Pictures added to this Faces Album in iPhoto will automatically be added to this person )'
	});

	//Assemble Everything
	//bg.add(closeButton);
	bg.add(groove);
	bg.add(title);
	bg.add(subTitle1);
	
	if(!buttonsDictionary){
		bg.add(singleButton);
	}
	else{
		bg.add(yesButton);
		bg.add(noButton);
	};
	
	
	win.add(bgShade);
	win.add(bg);
	
	win.addEventListener('close', function (){
		var Helper = require('lib/Helper');
		Helper.cleanMe(bg);
		
		win.remove(bgShade);
		win.remove(bg);
		bgShade = null;
		bg = null;
		win = null;
	});
	
	win.open();
	
	bg.animate(Ti.UI.createAnimation({
		delay: 100,
		duration: 200,
		transform: Ti.UI.create2DMatrix().scale(1),
		opacity:1
	}));
	
	bgShade.animate(Ti.UI.createAnimation({
		duration: 500,
		opacity:.25
	}));
};


exports.loadingPopUp = function (titleText){
	var PopUp = require('ui/components/PopUp');
	
	
	var win = Ti.UI.createWindow({
		tabBarHidden: true,
		navBarHidden:true,
		width:'100%',
		height: '100%',
		opacity:1,
		opened: false
	});
	
	var bgShade = Ti.UI.createView({
		backgroundColor: 'black',
		width: '100%',
		height: '100%',
		opacity: 0
	});
	
	var bg = PopUp.bg();
	bg.transform = Ti.UI.create2DMatrix().scale(0);
	bg.height = 80;
	bg.width = Ti.UI.SIZE;
	
	var label = Ti.UI.createLabel({
		color: 'white',
		font: {fontSize: 20, fontWeight: 'bold'},
		//top: 25,
		textAlign: 'center',
		left: 20,
		right: 20,
		text: titleText
	})

	bg.add(label);
	
	win.add(bgShade);
	win.add(bg);
	
	
	var closeAnimation = function (){
		bg.animate(Ti.UI.createAnimation({
			duration: 150,
			transform: Ti.UI.create2DMatrix().scale(0),
			opacity: 0
		}));

		bgShade.animate(Ti.UI.createAnimation({
			duration: 150,
			//transform: Ti.UI.create2DMatrix().scale(0),
			opacity:0
		}));
		
		setTimeout(function (){
			win.close();
			Ti.API.debug('Closed Loading Popup');
		}, 200);
	};
	
	
	win.addEventListener('open', function (){
		Ti.API.debug('Opened Loading Popup');
		
		bg.animate(Ti.UI.createAnimation({
			delay: 100,
			duration: 200,
			transform: Ti.UI.create2DMatrix().scale(1),
			opacity:1
		}));
		
		bgShade.animate(Ti.UI.createAnimation({
			duration: 500,
			opacity:.25
		}));
	});
	
	
	win.addEventListener('close', function (){
		Ti.App.removeEventListener('closeLoadingPopUp', closeAnimation);
		var Helper = require('lib/Helper');
		Helper.cleanMe(bg);
		
		bg.remove(label);
		win.remove(bg);
		win.remove(bgShade);
		
		bgShade = null;
		label = null;
		bg = null;
		win = null;
	});
	
	Ti.App.addEventListener('closeLoadingPopUp', closeAnimation);
	
	win.open();
};


exports.facesMatch = function (personFullName,newPersonID,group){
	Ti.API.debug('facesmatch1')
	var PopUp = require('ui/components/PopUp');
	
	var win = Ti.UI.createWindow({
		tabBarHidden: true,
		navBarHidden:true,
		width:'100%',
		height: '100%',
		top:0,
	});
	
	
	var bgShade = Ti.UI.createView({
		backgroundColor: 'black',
		width: '100%',
		height: '100%',
		opacity: 0
	});
	
	
	var bg = PopUp.bg();
	bg.transform = Ti.UI.create2DMatrix().scale(0);
	bg.width = 300;
	bg.height = 230;
	
	
	var closeButton = Titanium.UI.createButton({
		backgroundImage:'images/nav_x.png',
		width:21,
		height:20,
		top:20,
		left:20
	});
	
	var closeAnimation = function (){
		bg.animate(Ti.UI.createAnimation({
			duration: 250,
			transform: Ti.UI.create2DMatrix().scale(0),
			opacity:0
		}));
		
		bgShade.animate(Ti.UI.createAnimation({
			duration: 250,
			opacity:0
		}));
		
		setTimeout(function (){
			win.close();
		}, 300);
	};
	
	
	closeButton.addEventListener('click', function (){
		closeAnimation();
	});
	
	
	var groove = PopUp.groove(54);
	
	
	var title = Ti.UI.createLabel({
		color: 'white',
		font: {fontSize: 20, fontWeight: 'bold'},
		top: 25,
		text: I('Faces_match_found')
	});
	
	
	var subTitle1 = Ti.UI.createLabel({
		color: 'white',
		font:  {fontSize: 15, fontWeight: 'regular'},
		textAlign: 'center',
		top: 60,
		text: I('We_found_a_Faces_Album_that_matches')
	});
	
	
	var subTitle2 = Ti.UI.createLabel({
		color: 'white',
		font:  {fontSize: 17, fontWeight: 'bold'},
		textAlign: 'center',
		top: 80,
		text: personFullName
	});
	
	
	var subTitle3 = Ti.UI.createLabel({
		color: 'white',
		font:  {fontSize: 15, fontWeight: 'regular'},
		textAlign: 'center',
		top: 100,
		text: I('Would_you_like_to_link_to_this_person')
	});
	
	
	var yesButton = Ti.UI.createLabel({
		//backgroundColor: 'transparent',
		text: I('Yes'),
		color: 'white',
		font: {fontSize: 30,fontWeight: 'bold'},
		top: 130,
		left: 60
	});
	
	yesButton.addEventListener('click', function (){
		//Tag all the pictures from the url. 
		var DataPersons = require('model/DataPersons');
		DataPersons.checkFacesAlbumForPersonID(newPersonID,'add', group);
		closeAnimation();
	});
	
	
	var noButton = Ti.UI.createLabel({
		//backgroundColor: 'transparent',
		text: I('No'),
		color: 'white',
		font: {fontSize: 30, fontWeight: 'bold'},
		top: yesButton.top,
		right: 60
	});
	
	noButton.addEventListener('click', function (){
		closeAnimation();
	});
	
	
	var footerLabel = Ti.UI.createLabel({
		color: 'white',
		font:  {fontSize: 13, fontWeight: 'regular'},
		textAlign: 'center',
		top: 170,
		width: 280,
		text: I('Pictures_added_to_this_Faces_Album')
	});

	//Assemble Everything
	bg.add(closeButton);
	bg.add(groove);
	bg.add(title);
	bg.add(subTitle1);
	bg.add(subTitle2);
	bg.add(subTitle3);
	bg.add(yesButton);
	bg.add(noButton);
	bg.add(footerLabel);
	
	win.add(bgShade);
	win.add(bg);
	
	
	win.addEventListener('close', function (){
		var bgChildren = bg.children;
		for(var i =0; i< bgChildren.length; i ++){
			bg.remove(bgChildren[i]);
			bgChildren[i] = null;
		};
		
		win.remove(bgShade);
		bgShade = null;
		win.remove(bg);
		bg = null;
		win = null;
	});
	
	win.open();
	
	bgShade.animate(Ti.UI.createAnimation({
		duration: 250,
		opacity:.3
	}));
	
	bg.animate(Ti.UI.createAnimation({
		duration: 150,
		transform: Ti.UI.create2DMatrix().scale(1),
		opacity:1
	}));
};




exports.instructionsPopUp = function (titleText,subTitleText){
	var PopUp = require('ui/components/PopUp');
	
	var win = Ti.UI.createWindow({
		tabBarHidden: true,
		navBarHidden:true,
		width:'100%',
		height: '100%',
		top:0,
		transform: Ti.UI.create2DMatrix().scale(1),
		opacity: 1
	});
	
	
	var bgShade = Ti.UI.createView({
		backgroundColor: 'black',
		width: '100%',
		height: '100%',
		opacity: 0
	});
	
	
	var bg = PopUp.bg();
	bg.opacity = 0;
	bg.width = 300;
	bg.height = Ti.UI.SIZE;
	bg.transform = Ti.UI.create2DMatrix().scale(.5);
	
	
	//Methods
	var closeAnimation = function (){
		win.okButton = null;
		win.bottomSpace = null;
		win.closeAnimation = null;
		win.mainView = null;
		
		bg.animate(Ti.UI.createAnimation({
			duration: 50,
			transform: Ti.UI.create2DMatrix().scale(.8),
			opacity: 0
		}));
		
		bgShade.animate(Ti.UI.createAnimation({
			duration: 350,
			//transform: Ti.UI.create2DMatrix().scale(0),
			opacity: 0
		}));
		
		
		setTimeout(function (){
			win.close();
		}, 300);
	};
	
	
	//ContentView
	var view = Ti.UI.createView({
		//backgroundColor: 'yellow',
		height: Ti.UI.SIZE,
		width: bg.width - 10 ,
		layout:'vertical'
	});
	
	var title = Ti.UI.createLabel({
		color: 'white',
		textAlign: 'center',
		font: {fontSize: 24, fontWeight: 'bold'},
		width: 280,
		top: 10,
		text: titleText
	});
	
	var subTitle1 = Ti.UI.createLabel({
		color: 'white',
		font:  {fontSize: 15, fontWeight: 'regular'},
		textAlign: 'center',
		verticalAlign:'center',
		top: 10,
		width: 260,
		text:subTitleText
	});
	

	var okButton = Ti.UI.createLabel({
		//backgroundColor: 'transparent',
		text: 'Ok',
		color: 'white',
		font: {fontSize: 30, fontWeight: 'bold'},
		top: 10,
		//right: 60
	});
	
	okButton.addEventListener('click', function (){
		closeAnimation();
		win.fireEvent('clickedOk');
	});
	
	
	var bottomSpace = Ti.UI.createView({height:10});
	
	view.add(title);
	view.add(subTitle1);
	view.add(okButton);
	view.add(bottomSpace);

	
	//Assemble Everything
	bg.add(view);
	
	
	win.add(bgShade);
	win.add(bg);
	
	win.addEventListener('close', function (){
		view.remove(title);
		view.remove(subTitle1);
		view.remove(okButton);
		bg.remove(view);
		win.remove(bg);
		win.remove(bgShade);
		
		bgShade = null;
		bg = null;
		win = null;
	});

	
	win.addEventListener('open', function (){
		bg.animate(Ti.UI.createAnimation({
			delay: 150,
			duration: 100,
			transform: Ti.UI.create2DMatrix().scale(1),
			opacity:1
		}));
	
		bgShade.animate(Ti.UI.createAnimation({
			duration: 500,
			opacity: .3
		}));
	});
	
	return win;
};


exports.rateMePopUp = function (){
	var PopUp = require('ui/components/PopUp');
	
	var win = Ti.UI.createWindow({
		tabBarHidden: true,
		navBarHidden:true,
		width:'100%',
		height: '100%',
		top:0,
		transform: Ti.UI.create2DMatrix().scale(1),
		opacity: 1
	});
	
	
	var bgShade = Ti.UI.createView({
		backgroundColor: 'black',
		width: '100%',
		height: '100%',
		opacity: 0
	});
	
	
	var bg = PopUp.bg();
	bg.opacity = 0;
	bg.width = 300;
	bg.height = Ti.UI.SIZE;
	bg.transform = Ti.UI.create2DMatrix().scale(.5);
	
	
	//Methods
	var closeAnimation = function (){
		win.okButton = null;
		win.bottomSpace = null;
		win.closeAnimation = null;
		win.mainView = null;
		
		bg.animate(Ti.UI.createAnimation({
			duration: 50,
			transform: Ti.UI.create2DMatrix().scale(.8),
			opacity: 0
		}));
		
		bgShade.animate(Ti.UI.createAnimation({
			duration: 350,
			//transform: Ti.UI.create2DMatrix().scale(0),
			opacity: 0
		}));
		
		
		setTimeout(function (){
			win.close();
		}, 300);
	};
	
	
	//ContentView
	var view = Ti.UI.createView({
		//backgroundColor: 'yellow',
		height: Ti.UI.SIZE,
		width: bg.width - 10 ,
		layout:'vertical'
	});
	
	var title = Ti.UI.createLabel({
		color: 'white',
		textAlign: 'center',
		font: {fontSize: 24, fontWeight: 'bold'},
		width: 280,
		top: 10,
		text: I('Rate_me')
	});
	
	var subTitle1 = Ti.UI.createLabel({
		color: 'white',
		font:  {fontSize: 15, fontWeight: 'regular'},
		textAlign: 'center',
		verticalAlign:'center',
		top: 10,
		width: 260,
		text: I('Please_rate')
	});
	
	
	var buttonContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 40,
		top:10,
		layout: 'horizontal'
	});

	var okButton = Ti.UI.createLabel({
		//backgroundColor: 'transparent',
		text: 'Ok',
		color: 'white',
		font: {fontSize: 30, fontWeight: 'bold'},
		//top: 10,
		//right: 60
	});
	
	okButton.addEventListener('click', function (){
		closeAnimation();
		
		var properties = globals.col.properties.getAll();
		globals.col.properties.update({$id: properties[0].$id}, {$set:{hasRatedApp: true}},{},true);
		globals.col.properties.commit();
		
		Ti.App.fireEvent('updateSideBar');
		
		var url = 'itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=577120882'
		Ti.Platform.openURL(url)
	});
	
	
	var cancelButton = Ti.UI.createLabel({
		//backgroundColor: 'transparent',
		text: I('Cancel'),
		color: 'white',
		font: {fontSize: 30, fontWeight: 'bold'},
		//top: 10,
		//right: 60
		left: 30
	});
	
	cancelButton.addEventListener('click', function (){
		closeAnimation();
	});
	
	buttonContainer.add(okButton);
	buttonContainer.add(cancelButton);
	
	
	var bottomSpace = Ti.UI.createView({height:10});
	
	view.add(title);
	view.add(subTitle1);
	view.add(buttonContainer);
	view.add(bottomSpace);

	
	//Assemble Everything
	bg.add(view);
	
	
	win.add(bgShade);
	win.add(bg);
	
	win.addEventListener('close', function (){
		view.remove(title);
		view.remove(subTitle1);
		view.remove(okButton);
		bg.remove(view);
		win.remove(bg);
		win.remove(bgShade);
		
		bgShade = null;
		bg = null;
		win = null;
	});

	
	win.addEventListener('open', function (){
		bg.animate(Ti.UI.createAnimation({
			delay: 150,
			duration: 100,
			transform: Ti.UI.create2DMatrix().scale(1),
			opacity:1
		}));
	
		bgShade.animate(Ti.UI.createAnimation({
			duration: 500,
			opacity: .3
		}));
	});
	
	return win;
};




exports.labelPopUp = function (name){
	var PopUp = require('ui/components/PopUp');
	var DataLabels = require('model/DataLabels');
	
	var win = Ti.UI.createWindow({
		tabBarHidden: true,
		navBarHidden:true,
		width:'100%',
		height: '100%',
		opacity:1
	});
	
	
	var bgShade = Ti.UI.createView({
		backgroundColor: 'black',
		width: '100%',
		height: '100%',
		opacity: 0
	});
	
	
	var bg = PopUp.bg();
	bg.height = 190;
	bg.width = 300;
	bg.top = 50;
	bg.transform = Ti.UI.create2DMatrix().scale(0);
	
	
	var title = Ti.UI.createLabel({
		color: 'white',
		font: {fontSize: 20, fontWeight: 'bold'},
		shadowColor:'black',
		shadowOffset:{x:1,y:1},
		top: 25,
		text: I('add_new_label')
	});
	
	
	var icon = Ti.UI.createView({
		backgroundImage:'images/tag_add.png',
		width: 26,
		height:28,
		top: title.top-5,
		left: 30
	});
	
	
	var textFieldBG = Ti.UI.createView({
		backgroundImage: 'images/textfield_bg.png',
		backgroundLeftCap: 25,
		backgroundTopCap: 15,
		width:240,
		height:30,
		top:75
	});
	
	
	var textField = Ti.UI.createTextField({
		//backgroundColor: 'white',//'transparent',
		//backgroundImage: 'images/textfield_bg.png',
		//backgroundLeftCap: 25,
		//backgroundTopCap: 15,
		//backgroundColor: 'red',
		hintText: I('enter_label_name'),
		width: 230,
		height: 20,
		left: 10,
		value: name,
		//top:75,
		//paddingLeft:10,
		//left:8,
		//color: 'white',
		autocorrect: false,
		clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ALWAYS,
		appearance: Titanium.UI.KEYBOARD_APPEARANCE_ALERT,
	});
	
	
	textField.addEventListener('return', function(e){
		closingAnimation();
		win.fireEvent('return',{value:textField.value});
	});
	
	
	textFieldBG.add(textField)
	
	
	//Two Buttons Config
	var okButton = Ti.UI.createLabel({
		//backgroundColor: 'transparent',
		text: 'OK',
		color: 'white',
		font: {fontSize: 30,fontWeight: 'bold'},
		top: 130,
		left: 60
	});
	
	okButton.addEventListener('click', function (){
		win.fireEvent('ok',{value:textField.value});
		closingAnimation();
	});
	
	
	var cancelButton = Ti.UI.createLabel({
		//backgroundColor: 'transparent',
		text: 'Cancel',
		color: 'white',
		font: {fontSize: 30, fontWeight: 'bold'},
		top: okButton.top,
		right: 60
	});
	
	cancelButton.addEventListener('click', function (){
		closingAnimation();
	});
	
	function closingAnimation(){
		bg.animate(Ti.UI.createAnimation({
			duration: 250,
			transform: Ti.UI.create2DMatrix().scale(0),
			opacity:0
		}));
		
		bgShade.animate(Ti.UI.createAnimation({
			duration: 250,
			//transform: Ti.UI.create2DMatrix().scale(0),
			opacity:0
		}));
		
		setTimeout(function (){
			win.close();
		}, 300);
	};

	//Assemble Everything
	bg.add(title);
	bg.add(icon);
	bg.add(textFieldBG);
	bg.add(okButton);
	bg.add(cancelButton);

	win.add(bgShade);
	win.add(bg);
	
	win.addEventListener('close', function (){
		textFieldBG.remove(textField);
		var bgChildren = bg.children;
		
		for(var i = bgChildren.length - 1 ; i >= 0;  i--){
			bg.remove(bgChildren[i]);
			bgChildren[i] = null;
		};
		
		win.remove(bgShade);
		win.remove(bg);
		bgShade = null;
		bg = null;
		win = null;
		
		Ti.API.debug('Cleaned Label Pop Up');
	});
	
	
	win.addEventListener ('open', function (){
		bg.animate(Ti.UI.createAnimation({
			delay: 100,
			duration: 200,
			transform: Ti.UI.create2DMatrix().scale(1),
			opacity:1
		}));
		
		bgShade.animate(Ti.UI.createAnimation({
			duration: 500,
			opacity:.4
		}));
		
		textField.focus();
	});
	
	return win;
};


exports.deletingPopUp = function (itemDeletingName){
	var PopUp = require('ui/components/PopUp');
	var DataLabels = require('model/DataLabels');
	
	var win = Ti.UI.createWindow({
		tabBarHidden: true,
		navBarHidden:true,
		width:'100%',
		height: '100%',
		opacity:1
	});
	
	
	var bgShade = Ti.UI.createView({
		backgroundColor: 'black',
		width: '100%',
		height: '100%',
		opacity: 0
	});
	
	
	var bg = PopUp.bg();
	bg.height = 70;
	bg.width = 300;
	//bg.top = 50;
	bg.transform = Ti.UI.create2DMatrix().scale(0);
	
	
	var title = Ti.UI.createLabel({
		color: 'white',
		font: {fontSize: 20, fontWeight: 'bold'},
		shadowColor:'black',
		shadowOffset:{x:1,y:1},
		width: 280,
		height:50,
		textAlign: 'center',
		//top: 25,
		text: I('Deleting')+itemDeletingName
	});
	
	bg.add(title);

	win.add(bgShade);
	win.add(bg);
	
	function closeMe(){
		win.close();
	};
	
	Ti.App.addEventListener('closePopUp', closeMe);
	
	win.addEventListener('close', function (){
		Ti.App.removeEventListener('closePopUp',closeMe);
		var Helper = require('lib/Helper');
		Helper.cleanMe(bg);
		
		win.remove(bgShade);
		win.remove(bg);
		bgShade = null;
		bg = null;
		win = null;
	});
	
	
	win.addEventListener ('open', function (){
		bg.animate(Ti.UI.createAnimation({
			delay: 100,
			duration: 100,
			transform: Ti.UI.create2DMatrix().scale(1),
			opacity:1
		}));
		
		bgShade.animate(Ti.UI.createAnimation({
			duration: 150,
			opacity:.4
		}));
	});
	
	win.open();
};


exports.awardCaptionPopUp = function (){
	var PopUp = require('ui/components/PopUp');
	var DataLabels = require('model/DataLabels');
	
	var win = Ti.UI.createWindow({
		tabBarHidden: true,
		navBarHidden:true,
		width:'100%',
		height: '100%',
		opacity:1
	});
	
	
	var bgShade = Ti.UI.createView({
		backgroundColor: 'black',
		width: '100%',
		height: '100%',
		opacity: 0
	});
	
	
	var cancelButton = Ti.UI.createView({
		backgroundImage:'images/nav_x.png',
		width: 23,
		height: 23,
		top: 16,
		left: 15
	}); //TopNavigation.backButton(win,'white');
	
	
	cancelButton.addEventListener('click', function (){
		closeAnimation();
	});
	
	
	var bg = Ti.UI.createView({
		backgroundColor: '#fcfff4',
		width: 300,
		height: 140,
		top: 70,
		borderRadius: 5,
		opacity: 0,
		transform: Ti.UI.create2DMatrix().scale(.5)
	});
	
	
	var hintText = Ti.UI.createLabel({
		text: I('Write_a_caption'),
		textAlign: 'left',
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		top: 9,
		left: 10,
		color: '#999999',
		//minimumFontSize: 12,
		font:{ fontFamily: 'Georgia', fontSize: 16}
	});
	
	var charLeft= Titanium.UI.createLabel({
		backgroundColor:'transparent',
		text: 120,
		//text:String(500 - content.value.length),
		font: {fontSize: 13},
		color: '#969696',
		width: 'auto',
		height: 15,
		bottom: -1,
		right: -1,
		textAlign:'right'
	});
	
	textArea = Ti.UI.createTextArea({
		backgroundColor: 'transparent',
		//borderColor: '#bbbbbb',
		verticalAlign: 'center',
		textAlign: 'left',
		width: 280,
		height: 70,
		top: 5,
		font:{ fontFamily: 'Georgia',  fontSize: 16},
		appearance:Titanium.UI.KEYBOARD_APPEARANCE_ALERT,
	});
	
	textArea.addEventListener('change', function (e){
		if (e.value.length > 0){ hintText.visible = false}
		else {hintText.visible = true};
		
		var leftChars = (120-e.value.length);
		charLeft.text = leftChars;
		if (leftChars >= 0){ charLeft.color = '#969696'}
		else{charLeft.color = 'red'}
	})
	
	textArea.add(hintText);
	textArea.add(charLeft);
	
	//Two Buttons Config
	var awardButton = Ti.UI.createView({
		backgroundImage:'images/grey_button.png',
		backgroundLeftCap: 10,
		width: Ti.UI.SIZE,
		height: 44,
		bottom: 5,
		layout: 'horizontal'
	});
	
	awardButton.add(Ti.UI.createView({
		backgroundImage:'/images/sidebar_pictureOfTheWeek_icon.png',
		width: 29,
		height: 30,
		left: 10
	}));
	awardButton.add(Ti.UI.createLabel({
		width: Ti.UI.SIZE,
		//height: 25,
		text: I('Award_Verb'),
		textAlign: 'right',
		left: 5,
		//top: 8,
		font:{fontSize: 25, fontWeight: 'bold'},
		shadowColor: 'black',
		shadowOffset:{x:-1,y:-1},
		color: '#FCFFF4'
	}));
	
	awardButton.add(Ti.UI.createView({width:15}));
	
	awardButton.addEventListener('click', function (){
		win.fireEvent('awarded',{value: textArea.value});
		closeAnimation();
	});
	
	

	//Assemble Everything
	bg.add(textArea);
	bg.add(Ti.UI.createView({
		backgroundColor:'#cccccc',
		width: 270,
		height: 1,
		top:  85
	}))
	bg.add(awardButton);



	win.add(bgShade);
	win.add(cancelButton);
	win.add(bg);
	
	
	//Methods
	function closeAnimation(){
		bg.animate(Ti.UI.createAnimation({
			duration: 250,
			transform: Ti.UI.create2DMatrix().scale(0),
			opacity: 0
		}));
		
		bgShade.animate(Ti.UI.createAnimation({
			duration: 250,
			//transform: Ti.UI.create2DMatrix().scale(0),
			opacity:0
		}));
		
		setTimeout(function (){
			win.close();
		}, 300);
	};
	
	
	win.addEventListener('close', function (){
		var Helper = require('lib/Helper');
		Helper.cleanMe(bg);
		
		win.remove(bgShade);
		win.remove(bg);
		bgShade = null;
		bg = null;
		win = null;
	});
	
	
	win.addEventListener ('open', function (){
		bg.animate(Ti.UI.createAnimation({
			delay: 100,
			duration: 200,
			transform: Ti.UI.create2DMatrix().scale(1),
			opacity:1
		}));
		
		bgShade.animate(Ti.UI.createAnimation({
			duration: 500,
			opacity: .8
		}));
		
		textArea.focus();
	});
	
	
	return win;
};


////////////////////////////////////////////////////////////////
//Not being Used
exports.pagesPopUp = function (){
	var PopUp = require('ui/components/PopUp');
	
	var win = Ti.UI.createWindow({
		tabBarHidden: true,
		navBarHidden:true,
		width:'100%',
		height: '100%',
		top:0,
		transform: Ti.UI.create2DMatrix().scale(1),
		opacity:1
	});
	
	
	var bgShade = Ti.UI.createView({
		backgroundColor: 'black',
		width: '100%',
		height: '100%',
		opacity: .3
	});
	
	
	var bg = PopUp.bg();
	bg.width = 300;
	bg.height = 230;
	bg.transform = Ti.UI.create2DMatrix().scale(.5);
	
	
	var closeAnimation = function (){
		bg.animate(Ti.UI.createAnimation({
			duration: 250,
			transform: Ti.UI.create2DMatrix().scale(.5),
			opacity:0
		}));
		
		bgShade.animate(Ti.UI.createAnimation({
			duration: 250,
			//transform: Ti.UI.create2DMatrix().scale(0),
			opacity:0
		}));
		
		setTimeout(function (){
			win.close();
		}, 300);
	};
	
	//Scrollable View
	var createPage = function (titleText,subTitleText){
		var view = Ti.UI.createView({
			//backgroundColor: 'red',
			height: bg.height - 30,
			width: bg.width - 10,
		});
		
		var title = Ti.UI.createLabel({
			color: 'white',
			font: {fontSize: 20, fontWeight: 'bold'},
			top: 10,
			width: 270,
			height: 25,
			text: titleText,
			textAlign: 'center'
		});
		
		
		var subTitle1 = Ti.UI.createLabel({
			color: 'white',
			font:  {fontSize: 15, fontWeight: 'regular'},
			textAlign: 'center',
			top: 40,
			width: 270,
			text:subTitleText
		});
		
		view.add(title);
		view.add(subTitle1);
		
		return view;
	};
	

	var okButton = Ti.UI.createLabel({
		//backgroundColor: 'transparent',
		text: 'Ok',
		color: 'white',
		font: {fontSize: 30, fontWeight: 'bold'},
		bottom: 10,
		//right: 60
	});
	
	okButton.addEventListener('click', function (){
		//globals.currentTabGroup.setActiveTab(2);
		closeAnimation();
		Ti.App.fireEvent('addContact');
	});
	
	
	
	//var page1 = createPage('Welcome', 
	//'Do you have hundreds or thousands of pictures and have a hard time finding someone in them?\n\nTag my Photos helps you organize\nyour pictures in the most intuitive way,\nby Person & Labels');
	//var page2 = createPage('People', "Organize your pictures by person.\n\nSimply tag each person in a photo they'll show up on the people list.\n\n( Hint )\nIf you have a pet, just add it to your contacts to tag pictures of them. ");
	//var page3 = createPage('Labels', "Skiing, Cooking, Funniest, Romantic...\n\nCreate Labels for what matters to you and group photos any way you like.\n\n( Hint )\nYou can apply multiple labels to a photo.");
	//var page4 = createPage("Let's get started", "Please select yourself\nfrom the contact list.\n\nThis helps find pictures that include you.");
	//page4.add(okButton);
	
	var page1 = createPage("Let's get started", "Finally organize your pictures by person.\n\nPlease select yourself\nfrom the contact list.\n\nThis helps find pictures that include you.");
	page1.add(okButton);
	
	var pages = [page1];
	
	var scrollableView = Ti.UI.createScrollableView({
		//backgroundColor: 'yellow',
		width: bg.width-10,
		height: bg.height-20,
		views: pages,
		showPagingControl:true,
		pagingControlColor: 'transparent',
		pagingControlHeight: 10	
	});
	

	//Assemble Everything
	bg.add(scrollableView);
	
	win.add(bgShade);
	win.add(bg);
	
	win.addEventListener('close', function (){
		page1 = null;
		page2 = null;
		pages = null;
		bg.remove(scrollableView);
		scrollableView = null;
		
		win.remove(bgShade);
		win.remove(bg);
		bgShade = null;
		bg = null;
		win = null;
	});
	
	
	win.open();
	
	bgShade.animate(Ti.UI.createAnimation({
		duration: 250,
		opacity:.3
	}));
	
	bg.animate(Ti.UI.createAnimation({
		duration: 150,
		transform: Ti.UI.create2DMatrix().scale(1),
		opacity:1
	}));
};




