module.exports = function (hasEvents){
	//Array of Buttons to be used to unselect the others. 
	var buttonsArray = [];
	
	var view = Ti.UI.createView({
		backgroundImage: 'images/tabbed_bar_bg.png',
		width: 324,
		height: 54,
		bottom: -2
	});
	
	var buttonsContainer = Ti.UI.createView({
		height: 54,
		bottom:0,
		width: Ti.UI.SIZE,
		//left:0,
		layout: 'horizontal'
	});
	Ti.API.debug('Warning From SIZE of Tabbebar will be in a couple of lines');
	
	
	//Have to use this method because each icon has a different size. I need to figure out how to make that indiependent. 
	var createButton = function (selected,text,unselectedImage,selectedImage){
		//var selectedImage = null;//'images/tab_sel.png';
		//var unselectedImage =  null;
		
		var button = Ti.UI.createView({
			//backgroundImage: selected == true? selectedImage: unselectedImage,
			width: 67,
			height: 50,
			bottom:-3,
			style: 1,
			selected: selected,
			selectedImage: 'images/'+selectedImage,
			unselectedImage: 'images/'+unselectedImage
		});
		
		var label = Ti.UI.createLabel({
			text: text,
			font: {fontSize: 12, fontWeight: 'bold'},
			color: selected === true? globals.fontColors[2] : globals.fontColors[0],//'white',
			shadowColor: 'white',
			shadowOffset:{x:1,y:1},
			bottom: 0,
			textAlign: 'center'
		});
		
		button.addEventListener('click', function (){
			//Deselect all other buttons
			for (var i =0; i < buttonsArray.length; i++){
				if(buttonsArray[i]== button){
					buttonsArray[i].icon.backgroundImage =  buttonsArray[i].selectedImage;
					buttonsArray[i].label.color = globals.fontColors[2];
					buttonsArray[i].icon.selected = true;
				}
				else{
					buttonsArray[i].icon.backgroundImage =  buttonsArray[i].unselectedImage;
					buttonsArray[i].label.color = globals.fontColors[0];
					buttonsArray[i].icon.selected = false;
				}
			};
		
		});
		
		button.add(label);
		
		button.label = label;
		
		return button;
	};
	
	
	//album Button
	var albumsButton = createButton(true,'Albums','albums_grey.png','albums_grey_sel.png');
	//albumsButton.left = 15;
	albumsButton.index = 0;
	
	albumsButton.icon = Ti.UI.createView({
		backgroundImage:albumsButton.selected === false ? 'images/albums_grey.png':  'images/albums_grey_sel.png',
		width:	26,
		height: 33,
		bottom: 13,
		transform:Ti.UI.create2DMatrix().scale(.9)
	});
	
	albumsButton.add(albumsButton.icon);
	
	albumsButton.addEventListener('click', function (){
		//Select Tab
		globals.currentTabGroup.setActiveTab(0);
	});
	
	
	//events Button
	var eventsButton = createButton(false,'Events','events_grey.png','events_grey_sel.png');
	eventsButton.index = 1;
	
	eventsButton.icon = Ti.UI.createView({
		backgroundImage: eventsButton.selected === false ? 'images/events_grey.png' :  'images/events_grey_sel.png',
		width:	35,
		height: 34,
		bottom: 12,
		transform: Ti.UI.create2DMatrix().scale(.9)
	});
	
	eventsButton.add(eventsButton.icon);
	
	eventsButton.addEventListener('click', function (){
		//Select Tab
		globals.currentTabGroup.setActiveTab(1);
	});
	

	//People Button
	var peopleButton = createButton(false,'People','people_grey.png','people_grey_sel.png');
	peopleButton.index = 1;
	peopleButton.icon = Ti.UI.createView({
		backgroundImage:peopleButton.selected === false ? 'images/people_grey.png': 'images/people_grey_sel.png',
		width:	40,
		height: 25,
		bottom: 16,
		transform: Ti.UI.create2DMatrix().scale(1.2)
	})
	
	peopleButton.add(peopleButton.icon);
	
	peopleButton.addEventListener('click', function (){
		//Select Tab
		globals.tabGroup.setActiveTab(2);
	});
	
	buttonsArray.push(peopleButton);
	
	
	//Labels Button
	var labelsButton = createButton(false,'Labels','tag_grey.png','tag_grey_sel.png');
	//labelsButton.left = 15;
	labelsButton.index = 0;
	
	labelsButton.icon = Ti.UI.createView({
		backgroundImage:labelsButton.selected === false ? 'images/tag_grey.png':'images/tag_grey_sel.png' ,
		width:	30,
		height: 30,
		bottom: 14,
		transform: Ti.UI.create2DMatrix().scale(.9)
	});
	
	labelsButton.add(labelsButton.icon);
	
	labelsButton.addEventListener('click', function (){
		//Select Tab
		globals.tabGroup.setActiveTab(3);
	});
	
	
	//Assemble 	
	buttonsArray.push(albumsButton);
	buttonsArray.push(eventsButton);
	buttonsArray.push(labelsButton);
	
	buttonsContainer.add(peopleButton);
	buttonsContainer.add(labelsButton);
	buttonsContainer.add(albumsButton);
	if (hasEvents === true){ buttonsContainer.add(eventsButton);};
	
	
	view.add(buttonsContainer);
	
	var appear = function (){
		view.animate(Ti.UI.createAnimation({
			duration:250,
			transform: Ti.UI.create2DMatrix().translate(0,0)
		}));
	};
	
	var disappear = function (){
		view.animate(Ti.UI.createAnimation({
			duration:250,
			transform: Ti.UI.create2DMatrix().translate(0,56)
		}))
	};
	
	view.appear = appear;
	view.disappear = disappear;
	
	return view;
};