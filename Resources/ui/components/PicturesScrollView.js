module.exports = function (numberOfAssets,createThumbnailFunction,win,isLibraryPicker){
	isLibraryPicker = isLibraryPicker ? isLibraryPicker : false;
	var MiscComponents = require('ui/components/MiscComponents');
	var DataMedia = require('model/DataMedia');
	
	/*
	var scrollView = Ti.UI.createScrollView({
		width: 320,
		height: 480-44,
		top:44,
		contentHeight: 460-42+1,
		contentWidth: 320,
		showVerticalScrollIndicator: true,
		win:win
	});*/
	
	var scrollView = Ti.UI.createTableView({
		backgroundColor: 'transparent',
		width: 320,
		height: Ti.Platform.displayCaps.platformHeight-44,
		top: 43,
		win: win,
		separatorStyle: false,
		selectionStyle: false,
		visible: false,
		loadingComplete: false,
		numberOfAssets: numberOfAssets,
		selectionEnabled: true //Switch for letting it click once. 
	});
	
	//Footer
	var footerView = Ti.UI.createView({
		width:scrollView.width,
		height: 30,
		left:0
	});
	
	var activityIndicator = Ti.UI.createActivityIndicator({
      color: globals.fontColors[2],
      font: {fontFamily:'Helvetica Neue', fontSize:26, fontWeight:'bold'},
      message: '',
      style: isLibraryPicker === false ? Titanium.UI.iPhone.ActivityIndicatorStyle.DARK : Titanium.UI.iPhone.ActivityIndicatorStyle.PLAIN ,
      //top:10,
      //left:10,
      height:25,
      width: 25
    });
	
	var footerViewLabel = Ti.UI.createLabel({
		text:'Loading...',
		color: isLibraryPicker === false? globals.fontColors[2] : 'white',
		visible:false
	});
	
	footerView.add(activityIndicator);
	footerView.add(footerViewLabel);
	scrollView.footerView = footerView;
	
	scrollView.setContentInsets({top: 5},{animated:false});
	
	//Cleans up the mess. Safeguard.
	var clean = function (){
		tableData = null;
		activityIndicator.hide();
		footerView.remove(activityIndicator);
		activityIndicator = null;
		
		if(scrollView.data[0]){
			for (var i = 0; i < scrollView.data[0].rows.length; i++){
				var row = scrollView.data[0].rows[i];
				var children = row.children;
				for (var j=0; j < children.length; j++){
					row.remove(children[j]);
					children[j]= null;
				};
			};
		};
	};

	
	var lastDistance = 0;
	var updating = false;
	scrollView.lastPartFilledRow = 0;
	scrollView.lastFilledItem = 0;
	scrollView.nextToFillThumb = 0; //Set the first thumb to be the one to fill at start. 
	
	scrollView.addEventListener('scroll', function(e){
		//If you are getting close to the last filled row. 
		//Then fill more rows. 
		var offset = e.contentOffset.y;
		var height = e.size.height;
		var total = offset + height;
		var theEnd = e.contentSize.height;
		var distance = theEnd - total;

		// going down is the only time we dynamically load,
		// going up we can safely ignore -- note here that
		// the values will be negative so we do the opposite
		if (distance < lastDistance){
			// adjust the % of rows scrolled before we decide to start fetching
			var currentFilledHeight =scrollView.lastPartFilledRow *105;
			
			var triggerPoint = (isLibraryPicker === false) ? theEnd*.5 : (theEnd-1);
			
			if (!updating && total >= triggerPoint){ //This is to Not slow the selection in the Library Picker
				if(scrollView.nextToFillThumb < scrollView.numberOfAssets){
					//BEGIN UPDATE
					Ti.API.info(scrollView.nextToFillThumb+' Filling from this thumb');
					updating = true;
					scrollView.fireEvent('loadMoreData',{startIndex: scrollView.nextToFillThumb, endIndex: scrollView.nextToFillThumb+99})
				}
				else if (scrollView.loadingComplete === false){
					//activityIndicator.hide();
					//footerViewLabel.text = numberOfAssets+' Photos';
					//footerViewLabel.visible = true;
					scrollView.loadingComplete = true;
				};
			};
		};
		
		lastDistance = distance;
	});
	
	var selectedThumbsArray = [];
	
	scrollView.addEventListener('touchstart', function(e){
		if (win.selectionMode === 'multiple'){
			if(!e.source.selected || e.source.selected === false ){
				selectedThumbsArray.push(e.source);
			}
			else {
				selectedThumbsArray.splice(selectedThumbsArray.indexOf(e.source),1);
			};
			scrollView.selectedThumbsArray = selectedThumbsArray; //Must associate after each change. 
			
			Ti.App.fireEvent('selectedMultiple', {value:scrollView.selectedThumbsArray.length});
		};
		
		scrollView.selectedThumb = e.source;
		scrollView.selectionEnabled = false; 
		scrollView.selectedThumb.toggleOverlay(win.selectionMode);
	});
	
	scrollView.addEventListener('scroll', function(e){
		if (scrollView.selectedThumb && win.selectionMode != 'multiple'){
			scrollView.selectedThumb.toggleOverlay(win.selectionMode);
			scrollView.selectedThumb = null;
			scrollView.selectionEnabled = true; 
		};
	});
	
	
	
	var thumbsArray = [];
	
	function createRow (index){
		var row = Ti.UI.createTableViewRow({
			height: 105,
			width: 320,
			layout: 'horizontal',
			numberOfThumbs: 0,
			className: 'pictures',
			index: index,
			selected: false
		});
		
		//Optimize this. Don't use horizontal layout.
		
		var thumb1 = createThumbnailFunction(index);
		var thumb2 = createThumbnailFunction(index+1);
		var thumb3 = createThumbnailFunction(index+2)
		row.add(thumb1);
		row.add(thumb2);
		row.add(thumb3);
		
		thumbsArray.push(thumb1);
		thumbsArray.push(thumb2);
		thumbsArray.push(thumb3);
		
		return row; 
	};
	
	
	
	
	var tableData = [];
	scrollView.imagesDataArray = [];
	scrollView.lastRowIndex = 0;
	scrollView.lastPartFilledRow = 0;
	scrollView.rowToFill = 0;
	
	function loadData (imagesData){
		//If I get 0 images in return it means that some pictures where removed from the library.
		//In which case my numberOfAssets is wrong and needs to be updated. 
		//alert(imagesData.length)
		if(imagesData.length === 0 ){
			scrollView.numberOfAssets = scrollView.imagesDataArray.length;
		};
		scrollView.imagesDataArray = scrollView.imagesDataArray.concat(imagesData);
		
		var startTime = new Date().getTime();
		
		var nextToFillThumb =  scrollView.nextToFillThumb; //Grab a copy of this number before it's changed.
		
		footerViewLabel.visible = false;
		activityIndicator.show();
		
		//1.CreateRows
		var rowsToCreate;
		if(scrollView.nextToFillThumb % 3 === 0){
			rowsToCreate = Math.ceil(imagesData.length/3);
		}
		else{
			var emptySpotsInLastRow = 3-(scrollView.nextToFillThumb%3)
			rowsToCreate = Math.ceil((imagesData.length-emptySpotsInLastRow)/3);
		};
		//Ti.API.debug('Rows to create: '+rowsToCreate);
		
		for(var i = 0; i < rowsToCreate ; i++){
			var row = createRow(scrollView.lastRowIndex);
			tableData.push(row);
			scrollView.lastRowIndex++;	
		};
		
		scrollView.data = tableData;
		
		//2. Fill The thumbs with data
		for (var i = 0 ; i < imagesData.length; i++){
			//1. Create a row every 3.
			/*
			if((i+nextToFillThumb)%3 === 0){
				var row = createRow();
				tableData.push(row);
			};
			*/
			
			//2. Create thumbnail.
			//var thumb = createThumbnailFunction(i+nextToFillThumb,imagesData[i]);
			
			//2. Fill thumbNail.
			//Ti.API.debug(scrollView.rowToFill);
			//Ti.API.debug(tableData[scrollView.rowToFill]);
			//Ti.API.debug(tableData[scrollView.rowToFill].children);
			
			var thumb = tableData[scrollView.rowToFill].children[(i+nextToFillThumb)%3];
			thumb.index = i+nextToFillThumb;
			thumb.backgroundImage = imagesData[i].thumbnail;
			
			thumb.data = imagesData[i];
			thumb.URL = imagesData[i].URL;
			//thumb.selected = imagesData[i].selected;
			if(imagesData[i].selected === true){
				thumb.toggleOverlay('multiple');
			};
			thumb.visible = true;
			
			
			//3. Add Thumbnail to last row.
			//tableData[tableData.length-1].add(thumb);
			
			//4. Increment parameters
			if(scrollView.nextToFillThumb < scrollView.numberOfAssets){
				scrollView.nextToFillThumb++;
			};
			scrollView.rowToFill = Math.floor(scrollView.nextToFillThumb/3);
			//scrollView.lastPartFilledRow = Math.floor((scrollView.nextToFillThumb-1)/3);
			
			//TODO Need to revise this for when thumbnails are deleted from library. 
			//The numberOfAssets is no longer a valid number so need to find a different 
			//comparison.
			checkIfStillLoading();
		};
		
		//scrollView.data = tableData;
		
		var endTime = new Date().getTime();
		Ti.API.info('Loaded  '+imagesData.length+' in Picture List in: ' + (endTime - startTime) + 'ms');
		
		globals.popUpDelaySwitch = false; 
		if(updating === true){updating = false};
		Ti.App.fireEvent('closeLoadingPopUp');
		Ti.App.fireEvent('pictureListReady');
	};
	
	
	function checkIfStillLoading(){
		if(scrollView.nextToFillThumb === scrollView.numberOfAssets){
			activityIndicator.hide();
			footerViewLabel.text = scrollView.numberOfAssets+" "+I('Photos');
			footerViewLabel.visible = true;
			scrollView.loadingComplete = true;
		};
	};
	
	function hideMe (){
		setTimeout(function (){scrollView.opacity = 0;}, 500)
	};
	
	var refreshScrollView = function(imagesData){
		thumbsArray = [];
		selectedThumbsArray = [];
		tableData = [];
		scrollView.imagesDataArray = [];
		scrollView.lastRowIndex = 0;
		scrollView.lastPartFilledRow = 0;
		scrollView.rowToFill = 0;
		scrollView.nextToFillThumb = 0;
		scrollView.data = tableData;
		//numberOfAssets = imagesData.length;
		//scrollView.numberOfAssets = numberOfAssets;
		//Load Initial 
		loadData(imagesData);	
		scrollView.visible = true;
		scrollView.animate(Ti.UI.createAnimation({
			opacity: 1,
			duration: 150
		}));
	};
	
	function clearSelectedThumbs (){
		for (var i = 0; i < selectedThumbsArray.length; i++){
			//if (thumbsArray[i].selected === true){
				selectedThumbsArray[i].toggleOverlay('multiple');
			//};
		};
		selectedThumbsArray = [];
		scrollView.selectedThumbsArray = selectedThumbsArray;
	};
	
	
	//Expose Properties and Functions
	scrollView.refreshScrollView  = refreshScrollView;
	scrollView.loadData = loadData;
	scrollView.clean = clean;
	scrollView.hideMe = hideMe; 
	scrollView.thumbsArray = thumbsArray;
	scrollView.selectedThumbsArray = selectedThumbsArray;
	scrollView.clearSelectedThumbs = clearSelectedThumbs;
	scrollView.checkIfStillLoading = checkIfStillLoading;
	
	return scrollView;	
};