exports.shareButton = function (shareOptionsArray, personAddressBookID, win){
	var button = Ti.UI.createButton({
		//systemButton:Titanium.UI.iPhone.SystemButton.ACTION,
		backgroundImage: 'images/share_button.png',
		backgroundSelectedImage: 'images/share_button_sel.png',
		width: 35,
		height: 29,
		transform:Ti.UI.create2DMatrix().scale(1.2)
	});
	
	button.clickable = true;
	
	button.addEventListener('click', function (e){
		if (button.clickable === true){
			button.clickFunction(e);
			button.clickable = false; 
			var clickTimeout = setTimeout(function(){
				button.clickable = true; 
			},
			2000);
		};
	});
	
	
	return button;
};


var shareKitInit = function (){
	var sharekit = require('com.0x82.sharekit');
	// See documentation/configuration.html for reference!
	// The values shown here DO NOT WORK. Please change them before testing!
	sharekit.configure({
	  // Required: set your app name and url
	  my_app_name: 'sharekit',
	  my_app_url: 'http://example.com',
	
	  // OPTIONAL: Setting this to true will show the list in Alphabetical Order, setting to false will follow the implicit order
	  share_menu_alphabetical_order: false,
	  // OPTIONAL: set to true to hide the "More" button on the Share actionsheet
	  // hide_more_button: true,
	  // OPTIONAL: Append 'Shared With ' signature to email (and related forms)
	  shared_with_signature: false,
	
	  // See documentation/configuration.html to configure the twitter module
	  twitter_consumer_key: 'J6F57WzjhVHneb7RBVj7w',
	  twitter_consumer_secret: 'Pp6zSsbbcrYL1cfKNgL8Ns0VSRDkx2P6VZjodKtS3I',
	  twitter_callback_url: 'http://0x82.com/auth/twitter/callback',
	  // twitter_use_xauth: true
	  // twitter_username: 'rubenfonseca'
	
	  // For shortening URLs on Twitter
	  // See the documentation/configuration.html
	  bit_ly_login: 'rubenfonseca',
	  bit_ly_key: 'R_09b5fe9a6b6b52c46a4b81243607b1a8',
	
	  // See documentation/configuration.html
	  facebook_key: '391689264232611',
	  // facebook_local_key: 'lite',
	  facebook_secret: 'a1a5443b1201bb98065fce50003a836a',
	  facebook_use_session_proxy: false,
	  facebook_session_proxy_url: '',
	
	  // See documentation/configuration.html
	  readitlater_key: '52cA0Cf1d71f5Q0Z28p490xr02T1x540',
	
	  vkontakte_app_id: '2733819',
	
	  evernote_user_store_url: 'https://sandbox.evernote.com/edam/user',
	  evernote_net_store_url: 'http://sandbox.evernote.com/edam/note/',
	  evernote_consumer_key: 'rubenfonseca',
	  evernote_secret: '87682e715027225f',
	  
	  flickr_consumer_key: '1b5ff0520b9cdac5a2818abc8e210ce2',
	  flickr_secret_key: 'c6f6c62863799bc0',
	  flickr_callback_url: 'myapp://flickr',
	
	  linked_in_consumer_key: '5Bv-FiC0fDb-hW4BqI15nCEXqdVaddX56HQ1rFtWOY8ItV_LeiH7gw23FviiY1tc',
	  linked_in_secret: 'XPe-qC-vwExw5W0nvKatVD5_PB6TvPCpspXXnyxpnDW6Kwz_W9ehLLviT_JdxkzY',
	  linked_in_callback_url: 'http://linkedin.com',
	
	  foursquare_client_id: 'OO14A3X5RLEPB34Z0GLWUIBBWK30RTJBMD4KPSHNOXIWX4BB',
	  foursquare_redirect_uri: 'http://foursquare.com',
	
	  // Optional. See: http://developer.apple.com/iphone/library/documentation/UIKit/Reference/UIKitDataTypesReference/Reference/reference.html#//apple_ref/c/econst/UIBarStyleDefault
	  bar_style: "UIBarStyleDefault",
	
	  // Optional. Value between 0-255, set all to -1 to default
	  form_font_color_red: -1,
	  form_font_color_green: -1,
	  form_font_color_blue: -1,
	  form_bg_color_red: -1,
	  form_bg_color_green: -1,
	  form_bg_color_blue: -1,
	
	  // Optional. See: http://developer.apple.com/iphone/library/documentation/UIKit/Reference/UIViewController_Class/Reference/Reference.html#//apple_ref/occ/instp/UIViewController/modalPresentationStyle
	  ipad_modal_presentation_style: "UIModalPresentationFormSheet",
	  // Optional. See: http://developer.apple.com/iphone/library/documentation/UIKit/Reference/UIViewController_Class/Reference/Reference.html#//apple_ref/occ/instp/UIViewController/modalTransitionStyle
	  ipad_modal_transition_style: "UIModalTransitionStyleCoverVertical",
	
	  use_placeholders: true,
	
	  max_fav_count: 3,
	  allow_offline: true,
	  allow_auto_share: true
	});
};


exports.createPopUp = function (image){
	var PopUp = require('ui/components/PopUp');
	var ShareButton = require('ui/components/ShareButton');
	
	var win = Ti.UI.createWindow({
		tabBarHidden: true,
		navBarHidden: true,
		width:'100%',
		height: '100%',
		opacity: 1
	});
	
	win.addEventListener('click', function (e){
		if (e.source.kind != 'button'){
			closeAnimation();
		};	
	});
	
	
	var bgShade = Ti.UI.createView({
		backgroundColor: 'black',
		width: '100%',
		height: '100%',
		opacity: 0
	});
	
	
	var bg = PopUp.bg();
	bg.transform = Ti.UI.create2DMatrix().scale(.8);
	bg.height =  150;
	bg.width = 300;
	bg.opacity = 0;
	
	
	var closeButton = Ti.UI.createView({
		backgroundImage:'images/nav_x.png',
		width:23,
		height:23,
		top: 15,
		left: 20,
		kind: 'button'
	});
	
	closeButton.addEventListener('click', function (){
		closeAnimation();
	});
	
	
	var facebookButton = ShareButton.facebookButton(image);
	facebookButton.bottom = 30;
	
	facebookButton.addEventListener('click', function (){
		
		if (image.width > 960){
			var ratio = image.height/image.width;
			image.width = 960;
			image.height = ratio*960;
		};
		
		ShareButton.postToFacebookPhoto(image, facebookButton);
	});
	
	
	
	var twitterButton = ShareButton.twitterButton(image);
	twitterButton.bottom = 30;
	
	twitterButton.addEventListener('click', function(e) {
		var Twitter = require('com.0x82.twitter');
		//var Twitter = Ti.UI.currentWindow.Twitter;
		if (image.width > 960){
			var ratio = image.height/image.width;
			image.width = 960;
			image.height = ratio * 960;
		};
	  	
	  	var composer = Twitter.createTweetComposerView();
	  	composer.addImage(image); // must be a TiBlob
	  	composer.open();
	});
	
	
	var emailButton = ShareButton.emailButton(image);
	emailButton.bottom = 30;
	
	emailButton.addEventListener('click',function (){
		if (image.width > 960){
			var ratio = image.height/image.width;
			image.width = 960;
			image.height = ratio * 960;
		};

		ShareButton.emailPhoto(image);
	});
	
	
	//Assemble
	bg.add(closeButton);
	bg.add(facebookButton);
	bg.add(emailButton);
	bg.add(twitterButton);
	
	win.add(bgShade);
	win.add(bg);
	
	
	//Methods
	var closeAnimation = function (){
		bg.animate(Ti.UI.createAnimation({
			duration: 50,
			transform: Ti.UI.create2DMatrix().scale(.8),
			opacity: 0
		}));
		
		bgShade.animate(Ti.UI.createAnimation({
			duration: 350,
			//transform: Ti.UI.create2DMatrix().scale(0),
			opacity: 0
		}));
		
		
		setTimeout(function (){
			win.close();
		}, 300);
	};
	
	
	function clean (){
		cleanItem(facebookButton);
		cleanItem(emailButton);
		cleanItem(twitterButton);
		cleanItem(bg);
		cleanItem(win);
		win = null
	};
	
	
	//Listeners
	win.addEventListener('open', function (){
		bg.animate(Ti.UI.createAnimation({
			delay: 150,
			duration: 100,
			transform: Ti.UI.create2DMatrix().scale(1),
			opacity:1
		}));
	
		bgShade.animate(Ti.UI.createAnimation({
			duration: 500,
			opacity:.7
		}));
	});
	
	
	win.addEventListener('close', function (){
		clean();
	});
	
	
	return win;
};




exports.emailButton = function (image){
	var view = Ti.UI.createView({
		//backgroundColor: 'red',
		width: 80,
		height: 80,
		kind: 'button'
	});
	
	
	var icon = Ti.UI.createButton({
		backgroundImage:'images/email_white.png',
		width: 63,
		height: 40,
		bottom: 23,
		kind: 'button'
	});
	
	
	var label = Ti.UI.createLabel({
		text: 'e-mail',
		font: {fontSize: 14, fontWeight: 'bold'},
		bottom: 0,
		textAlign: 'center',
		color: 'white',
		touchEnabled: false
	});
	
	view.add(icon);
	view.add(label);
	
	
	function clean(){
		view.remove(icon);
		view.remove(label);
		icon = null;
		label = null;
	};
	
	view.label = label;
	view.clean = clean;
	
	return view;
};


exports.twitterButton = function (image){
	var view = Ti.UI.createView({
		//backgroundColor: 'red',
		width: 80,
		height: 80,
		right: 10,
		kind: 'button'
	});
	
	
	var icon = Ti.UI.createButton({
		backgroundImage:'images/twitter_white.png',
		width: 58,
		height: 47,
		bottom: 23,
		kind: 'button'
	});

	
	var label = Ti.UI.createLabel({
		text: 'twitter',
		font: {fontSize: 14, fontWeight: 'bold'},
		color: 'white',
		bottom: 0,
		textAlign: 'center',
		touchEnabled: false
	});
	
	view.add(icon);
	view.add(label);
	
	
	function clean(){
		view.remove(icon);
		view.remove(label);
		icon = null;
		label = null;
	};
	
	view.label = label;
	view.clean = clean;
	
	return view
};


exports.facebookButton = function (image){
	var view = Ti.UI.createView({
		//backgroundColor: 'red',
		width: 105,
		height: 80,
		left: 5,
		kind: 'button'
	});
	
	var icon = Ti.UI.createButton({
		backgroundImage:'images/facebook_white.png',
		width: 25,
		height: 52,
		bottom: 23,
		kind: 'button'
	});
	
	
	var label = Ti.UI.createLabel({
		text: 'facebook',
		font: {fontSize: 14, fontWeight: 'bold'},
		bottom: 0,
		textAlign: 'center',
		color: 'white',
		touchEnabled: false
	});
	
	view.add(icon);
	view.add(label);
	
	
	function clean(){
		view.remove(icon);
		view.remove(label);
		icon = null;
		label = null;
	};
	
	
	
	view.label = label;
	view.clean = clean;
	
	return view;
};


var createOptionDialog = function (shareOptionsArray,image,personAddressBookID){
	var win = Ti.UI.createWindow({
		//backgroundColor: '#000',
		width: '100%',
		height: Ti.UI.SIZE,
		bottom:-480
	});
	
	var bg = Ti.UI.createView({
		backgroundImage: 'images/bg_white.png',
		backgroundTopCap: 25,
		
		//backgroundColor: '#000',
		width: '100%',
		height: Ti.UI.SIZE,
		layout: 'vertical',
		//opacity: .8
	});
	
	function createButton (title,callback){
		var button = Ti.UI.createButton({
			//backgroundColor: 'white',
			top: 10,
			width: 280,
			height: 40,
			title: title
		});
		
		button.addEventListener('click', function (){
			if (callback) {
				callback(image,button,personAddressBookID);
			};
			
			win.animate(Ti.UI.createAnimation({
				duration: 250,
				bottom: -win.size.height,
			}));
			
			setTimeout(function (){
				win.close();	
			}, 300);
		});
		
		
		return button;
	};
	
	
	bg.add(Ti.UI.createView({height:20}));
	
	for( var i = 0; i < shareOptionsArray.length; i++){
		var item = shareOptionsArray[i];
		if(item === 'email'){
			bg.add(createButton('Email Photo', emailPhoto));
		}
		else if (item === 'twitter'){
			bg.add(createButton('Tweet', tweetPhoto));
		}
		else if (item === 'facebook'){
			bg.add(createButton('Post on Facebook', ShareButton.postToFacebookPhoto));
		}
		else if (item === 'contact'){
			bg.add(createButton('Assign to Contact', assignToContactPhoto));
		};
	};
	
	bg.add(createButton('Cancel', null));
	bg.add(Ti.UI.createView({height:20}));
	
	win.add(bg);
	win.bottom = -290;
	
	win.addEventListener('open', function (){
		win.animate(Ti.UI.createAnimation({
			duration: 250,
			bottom: 0,
		}));
	});
	
	return win;
};


var assignToContactPhoto = function (image,button,personAddressBookID){
	var person = Titanium.Contacts.getPersonByID(personAddressBookID);
	var image1 = Ti.UI.createImageView({
		image: image
	});
	person.setImage(image1.toBlob());
	
};

exports.sharePhotos = function (shareMethod, scrollView){
	var DataMedia = require('model/DataMedia');
	
	var PopUp = require('ui/components/PopUp');
	PopUp.loadingPopUp('Preparing photos');
	
	var imagesArray = [];
	function successCb (e){
		imagesArray.push(e.asset.defaultRepresentation.fullScreenImage);
	
		if(imagesArray.length === scrollView.selectedThumbsArray.length){
			if (shareMethod === 'email'){
				emailMultiplePhotos(imagesArray);
			}
			else if ( shareMethod === 'twitter'){
				tweetMultiplePhotos(imagesArray);
			}
			else if (shareMethod === 'facebook'){
				facebookMultiplePhotos(imagesArray);	
			}
		};
		
		setTimeout(function (){
			Ti.App.fireEvent('closeLoadingPopUp');
		},100);
	};
	
	function errorCb(){
		alert('There was an error in getting this photo')
	};
	
	for (var i = 0; i < scrollView.selectedThumbsArray.length; i++){
		DataMedia.getSingleAssetFromURL(scrollView.selectedThumbsArray[i].URL, successCb, errorCb)
	};
};


var tweetPhoto = function (image,button,personID){
	var sharekit = require('com.0x82.sharekit');
	sharekit.share({
	    title: "",
	    view: button,
	    image: image,
	    sharer: 'Twitter'
	 });
};

var tweetMultiplePhotos = function (imagesArray){
	var Twitter = require('com.0x82.twitter');
		//var Twitter = Ti.UI.currentWindow.Twitter;
	var composer = Twitter.createTweetComposerView();	
		
	for (var i = 0 ; i < imagesArray.length; i ++){
			var image = imagesArray[i];
			if (image.width > 960){
				var ratio = image.height/image.width;
				image.width = 960;
				image.height = ratio * 960;
			};
	
			composer.addImage(image); // must be a TiBlob
	};
	
	composer.open();
};




exports.postToFacebookPhoto= function (image,button,personID){
	
	var osVersion = 5; 
	if (osVersion != 6){
		var ShareButton = require('ui/components/ShareButton');
		var win = facebookPopUP(image,'');
		
		win.addEventListener('post', function (e){
			shareIt(e.value);
		});
		
		win.open();
	};
	
	
	function shareIt (text){
		/*
		var sharekit = require('com.0x82.sharekit');
		sharekit.share({
		    title: text,
		    view: button,
		    image: image,
		    sharer: 'Facebook'
		});
		*/
		
		Ti.Facebook.appid = '391689264232611';
		Titanium.Facebook.permissions = ['publish_actions'];	
		Ti.Facebook.authorize();
		
		var data = {
		    message: text,
		    picture: image
		};
	
		Ti.Facebook.requestWithGraphPath('me/photos', data, 'POST', function(e){
		    if (e.success) {
		        alert("Success! :)\nPosted on Facebook ");
		    } else {
		        if (e.error) {
		            alert("There was an error :( !\nPlease try again");
		        } else {
		            alert("Something didn't work :( !\nPlease try again");
		        }
		    }
		});
	};
};



var facebookMultiplePhotos = function (imagesArray){
	var osVersion = 5; 
	
	if (osVersion != 6){
		var ShareButton = require('ui/components/ShareButton');
		var win = facebookPopUP( imagesArray[0] , '', imagesArray.length);
		
		win.addEventListener('post', function (e){
			Ti.Facebook.appid = '391689264232611';
			Titanium.Facebook.permissions = ['publish_actions'];	
			Ti.Facebook.authorize();
			
			for (var i = 0 ; i < imagesArray.length; i ++){
				var image = imagesArray[i];
				
				if (image.width > 960){
					var ratio = image.height/image.width;
					image.width = 960;
					image.height = ratio * 960;
				};
			
				var data = {
				    message: e.value,
				    picture: image
				};
		
				Ti.Facebook.requestWithGraphPath('me/photos', data, 'POST', function(e){
				    if (e.success) {
				        Ti.API.debug("Success!  From FB: " + e.result);
				    } 
				    else {
				        if (e.error) {
				            Ti.API.debug(e);
				        } else {
				            Ti.API.debug("Unkown result");
				        }
				    };
				});
			};	
		});
		
		win.open();
	};
};




exports.emailPhoto = function (image,button,personID){
	var emailDialog = Ti.UI.createEmailDialog({
		barColor:'#000',
	});

	emailDialog.addAttachment(image);
	emailDialog.open();
};


var emailMultiplePhotos = function(imagesArray){
	var emailDialog = Ti.UI.createEmailDialog({
		barColor:'#000',
	});
	
	for (var i = 0 ; i < imagesArray.length; i ++){
		emailDialog.addAttachment(imagesArray[i]);
	};
	
	emailDialog.open();
};


var facebookPopUP = function (image, text, count){
	var win = Ti.UI.createWindow({
		tabBarHidden: true,
		navBarHidden: true,
		width:'100%',
		height: '100%',
	});
	
	var bgShade = Ti.UI.createView({
		backgroundColor: 'black',
		width: '100%',
		height: '100%',
		opacity: 0
	});
	
	var bg = Ti.UI.createView({
		backgroundColor: '#FCFFF4',
		width: 310,
		height: 240,
		borderRadius: 10,
		top: 14,
		opacity: 0	
	});
	
	
	//Navbar Area
	var bgNavBar = Ti.UI.createView({
		backgroundImage: '/images/navbar_facebook.png',
		width: 315,
		height: 48,
		//borderRadius: 10,
		top: -1,
	});
	
	
	var TopNavigation = require('ui/components/TopNavigation');
	var rightButton = TopNavigation.editButton();
	rightButton.top = 5;
	rightButton.label.text = 'Post';
	
	rightButton.addEventListener('click', function (){
		win.fireEvent('post', {value:textArea.value}); 
		closeAnimation();
	});
	
	
	var leftButton =  TopNavigation.editButton();
	leftButton.left = 10;
	leftButton.right = null;
	leftButton.top = 5;
	leftButton.label.text = 'Cancel';
	
	leftButton.addEventListener('click', function (){
		closeAnimation();
	});
	////
	
	
	
	var textArea = Ti.UI.createTextArea({
		backgroundColor: 'transparent',
		//appearance:Titanium.UI.KEYBOARD_APPEARANCE_ALERT,
		font: {fontSize: 15},
		width: 200,
		height: bg.height-50,
		left: 5,
		top: 45,
		suppressReturn: false
	});
	
	
	var Helper = require('lib/Helper');
	var newSize = Helper.imageResize2(75, 90,image.width, image.height);

	var thumbImage = Ti.UI.createImageView({
		image: image,
		borderRadius: 5,
		borderWidth: 1,
		borderColor: '#bbbbbb',
		width: newSize.width,
		height: newSize.height,
		top: 50,
		right: 5,
		//transform: Ti.UI.create2DMatrix().rotate(2)
	});
	
	if (count){
		var countImage = Ti.UI.createView({
			backgroundColor: '#3B5998',
			width: 26,
			height: 26,
			borderRadius: 13,
			borderWidth: 2,
			borderColor: 'white',
			bottom: 4,
			right: 4
		});
		
		var countLabel = Ti.UI.createLabel({
			text: count ? count : '',
			color: globals.fontColors[0],
			//top: 50,
			//right: 15,
			//height: 20,
			color: 'white',
			touchEnabled: false,
			font: {fontSize: 14, fontWeight: 'bold'}
		});
		
		countImage.add(countLabel);
		thumbImage.add(countImage);
	};
	
	
	//Assemble
	bg.add(bgNavBar);
	bg.add(leftButton);
	bg.add(rightButton);
	bg.add(textArea);
	
	bg.add(thumbImage);
	
	
	win.add(bgShade);
	win.add(bg);
	
	
	//Methods
	function openAnimation(){
		bgShade.animate(Ti.UI.createAnimation({
			duration: 250,
			opacity: .8
		}));	
		
		bg.opacity = 1;
		textArea.focus();
	};	
	
	function closeAnimation(){
		win.animate(Ti.UI.createAnimation({
			duration: 250,
			transform: Ti.UI.create2DMatrix().translate(0,480)
		}));
		
		setTimeout(function (){
			bg.remove(bgNavBar);
			bg.remove(leftButton);
			bg.remove(rightButton);
			bg.remove(textArea);
			
			if(count){
				countImage.remove(countLabel);
				thumbImage(countImage);
			};
			
			bg.remove(thumbImage);
			
			win.remove(bg);
			win.remove(bgShade);
			
			//win.close();
			win = null;
		},400);
	};
	
	
	win.closeAnimation = closeAnimation;
	
	win.addEventListener('open', function (){
		openAnimation();
	});
	
	return win;
};


function cleanItem(item){
	var children = item.children;
	for(var i = children.length-1 ; i >= 0;  i--){
		item.remove(children[i]);
		children[i] = null;
	};
};



exports.shareManyButton = function (win){
	var button = Ti.UI.createButton({
		//systemButton:Titanium.UI.iPhone.SystemButton.ACTION,
		backgroundImage: 'images/share_button.png',
		backgroundSelectedImage: 'images/share_button_sel.png',
		width: 35,
		height: 29,
		transform:Ti.UI.create2DMatrix().scale(1.2)
	});
	
	button.clickable = true;
	
	//Share options: 
		//Email
		//Assign to Contact
		//Facebook
		//Twitter
		//Mobile
	
	var buttonClick = function(e){
		//1. Change the scrollView to select multiple mode
			// a. display bottom nav bar ( facebook, twitter, email)
			// b. cover top nav bar with the one of sharing: Text: Select Pictures, Cancel Button
		// 2. Bottom Bar buttons disable as go over the limit. 
		// 3. Facebook sharing	
					
	
	};	
				
	button.addEventListener('click', function (e){
		if (button.clickable === true){
			buttonClick(e);
			button.clickable = false; 
			var clickTimeout = setTimeout(function(){
				button.clickable = true; 
			},
			2000);
		};
	});
	
	
	return button;
};