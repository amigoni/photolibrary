exports.createWindow = function (args){
	var _ = require('/lib/underscore');
	var win = Ti.UI.createWindow(_.extend({
		barImage:'images/navbar_grey.png',
		//barColor: 'transparent',
		//backgroundImage: 'images/bg_white.png',
		//backgroundColor: 'white',
		//barColor: '#000',
		navBarHidden: false,
		tabBarHidden: true,
		width: '100%',
		height: Ti.Platform.displayCaps.platformHeight,
		translucent : true,
		orientationModes:[Titanium.UI.PORTRAIT]
	},
	args||{})
	);
	
	
	var navBarShadow = Ti.UI.createView({
		backgroundImage: 'images/navbar_shadow.png',
		width: 320,
		height: 5,
		top: 43,
		zIndex: 99
	});
	
	win.add(navBarShadow);
	
	//Listener to update the navigation bar
	win.addEventListener('focus', function (){
		//The setTimeout is here because the tabbed bar looses it's location on visible = true.
		//This with the reset of the bottom parameter seems to fix the issue. 
		//globals.tabGroup.containerWindow.backToPortrait();
		//globals.tabGroup.navBar.update(win);
	});
	
	win.addEventListener('open', function (){
		//globals.tabGroup.navBar.update(win);
	});
	
	win.addEventListener('close', function(){
		clean();
	});
	
	
	
	function clean (){
		if(win.preClean){win.preClean()};

		var children = win.children;
		
		for(var i = children.length - 1 ; i >= 0;  i--){
			win.remove(children[i]);
			children[i] = null;
		};
		
		Ti.API.debug('Cleaned window');
	};
	
	win.clean = clean;
	
	return win;
};