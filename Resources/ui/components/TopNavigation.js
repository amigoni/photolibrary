
//Creates and determines the behaviour of the NavBar.
//The navbar is above the tabGroup.Wanted it to be on each tab but 
//It doesn't seem possible
exports.createNavBar = function (){
	var view = Ti.UI.createView({
		//backgroundImage:'images/navbar_grey.png',
		backgroundColor: 'yellow',
		width:  320,
		height: 44, //46
		top: 0
	});
	
	
	function removeOld (button){
		/*button.animate(Ti.UI.createAnimation({
			duration:150,
			opacity:0
		}));*/
		
		button.opacity = 0;
		
		setTimeout(function(){
			view.remove(button);
			button = null;
		}, 10)
	};
	
	function addNew (button){
		view.add(button);
		
		button.opacity = 1; 
		/*
		button.animate(Ti.UI.createAnimation({
			duration:10,
			opacity:1
		}));
		*/
	};
	
	var changeLeftButton = function (leftButton){
		if (leftButton){
			leftButton.left = 0;
			leftButton.opacity = 0;
			addNew(leftButton);
		};
		
		if (view.leftButton){
			removeOld(view.leftButton);
		};
		
		setTimeout( function(){
			view.leftButton = leftButton;
		}, 60);
	};
	
	var changeRightButton = function (rightButton){
		if (rightButton){
			rightButton.right= 0;
			rightButton.opacity = 0;
			addNew(rightButton);
		};
		
		if (view.rightButton){
			removeOld(view.rightButton);
		};
		
		setTimeout( function(){
			view.rightButton = rightButton;
		}, 60);
	};
	
	var changeTitle = function (centerTitle){
		if (centerTitle){
			centerTitle.opacity = 0;
			addNew(centerTitle);
		};
		
		if (view.centerTitle){
			removeOld(view.centerTitle);
		};
		
		setTimeout( function(){
			view.centerTitle = centerTitle;
		}, 60);
	};
	
	var pastWin;
	var update = function (win){
		
		//Got the Tabbed bar in here too.
		if (win.tabbedBarHidden === true){
			//globals.tabGroup.tabbedBar.visible = false;
			//globals.tabGroup.tabbedBar.disappear();
		}
		else{
			//globals.tabGroup.tabbedBar.appear();
		};
		
		if(win != pastWin){
			//alert('Updat')
			if(!win.navBarTransparent){win.navBarTransparent = false};
			if (win.navBarTransparent === true){
				
				//disappear();
				setTimeout(function(){
					view.backgroundImage = null;
					changeLeftButton(win.leftNavButton);
					changeRightButton(win.rightNavButton);
					changeTitle(win.centerTitle);
				},200)
				
				//setTimeout(appear, 400);
			}
			else if (win.navBarTransparent === false) {
				view.backgroundImage = view.currentBG;
				changeLeftButton(win.leftNavButton);
				changeRightButton(win.rightNavButton);
				changeTitle(win.centerTitle);
			};
			
		};
		pastWin = win;
	};
	
	var rotate = function (width , height){
		view.width = width + 4;
	};
	
	var disappear = function (win){
		/*
		view.animate(
			Ti.UI.createAnimation({
				duration: 250,
				opacity: 0,
				transform: Ti.UI.create2DMatrix().translate(0,-50)
			})
		);	
		*/
		view.opacity = 0;
	};
	
	var appear = function (win){
		/*
		view.animate(
			Ti.UI.createAnimation({
				duration: 250,
				opacity: 1,
				transform: Ti.UI.create2DMatrix().translate(0,0)
			})
		);	
		*/
		view.opacity = 1;
	};
	
	var goUp = function (win){
		
		view.animate(
			Ti.UI.createAnimation({
				duration: 250,
				opacity: 0,
				transform: Ti.UI.create2DMatrix().translate(0,-50)
			})
		);	
		
	};
	
	var goDown = function (win){
		setTimeout(function(){
			view.animate(
				Ti.UI.createAnimation({
					duration: 250,
					opacity: 1,
					transform: Ti.UI.create2DMatrix().translate(0,0)
				})
			);	
		},250);
	};
	
	function clean(){
		var children = view.children;
		for(var i = children.length-1 ; i >= 0;  i--){
			view.remove(children[i]);
			children[i] = null;
		};
		Ti.API.debug('NavBar Cleaned');
	};
	
	/*
	view.add(Ti.UI.createView({
		backgroundColor: 'black',
		width: view.width,
		height: view.height,
		opacity:0
	}));
	*/
	
	view.update = update;
	view.rotate = rotate;
	view.disappear = disappear; 
	view.appear = appear; 
	view.goUp = goUp;
	view.goDown = goDown;
	
	return view;
};


exports.addButton = function (color){
	if(!color){color = 'grey'};
	
	var button = Ti.UI.createView({
		//backgroundImage: 'images/navbar_button.png',
		//backgroundSelectedImage: 'images/navbar_button_sel.png',
		width: 40,
		height: 40,
		right:10
		//backgroundLeftCap: 15,
	});
	
	if (color === 'white'){
		button.add(Ti.UI.createView({
			backgroundImage: 'images/nav_add.png',
			width:21,
			height: 21
		}));
	}
	else{
		button.add(Ti.UI.createView({
			backgroundImage: 'images/nav_add_grey.png',
			width:21,
			height: 21,
			//transform:Ti.UI.create2DMatrix().scale(1.2)
		}));
	};	
	
	button.width = 40;
	
	return button;
};

exports.addButtonFull = function (){
	var button = Ti.UI.createButton({
		backgroundImage:'images/navbar_add_button.png',
		backgroundSelectedImage:'images/navbar_add_button_sel.png',
		backgroundFocusedImage:'images/navbar_add_button_sel.png',
		width: 47,
		height: 31,
		right: 5,
		opacity: 1,
		style:Titanium.UI.iPhone.SystemButtonStyle.PLAIN
	});

	return button;
};


exports.menuButton = function (){
	var button = Ti.UI.createButton({
		backgroundImage:'images/navbar_menu_button.png',
		backgroundSelectedImage:'images/navbar_menu_button_sel.png',
		backgroundFocusedImage:'images/navbar_menu_button_sel.png',
		width: 47,
		height: 31,
		left: 5,
		opacity: 1,
		style:Titanium.UI.iPhone.SystemButtonStyle.PLAIN
	});
	
	button.addEventListener('click', function (){
		
		if(globals.tutorialModeOn === true){
			var PopUp = require('ui/components/PopUp');
			var alertDialog = PopUp.alertDialog(I('Are_you_sure?'),I('This_will_end_the_tutorial'));
			alertDialog.open();
			alertDialog.addEventListener('ok', function (){
				setTimeout(function(){
					Ti.App.fireEvent('toggleSideBar')
				}, 250);
				
				globals.tutorialModeOn = false;
			});
		}
		else{
			Ti.App.fireEvent('toggleSideBar')
		};
	
	});

	return button;
};


exports.shareButton = function (){
	var button = Ti.UI.createButton({
		backgroundImage:'images/navbar_share_button.png',
		backgroundSelectedImage:'images/navbar_share_button_sel.png',
		backgroundFocusedImage:'images/navbar_share_button_sel.png',
		width: 47,
		height: 31,
		left: 5,
		opacity: 1,
		style:Titanium.UI.iPhone.SystemButtonStyle.PLAIN
	});

	return button;
};


exports.backButton = function (win,color){
	if(!color){color = 'grey'};
	
	var button;
	var image;
	if(color === 'white'){
		button = Ti.UI.createButton({
			backgroundColor:'transparent',
			width:40,
			height: 40,
			left: 5,
			opacity: 1,
			style:Titanium.UI.iPhone.SystemButtonStyle.PLAIN
		});
		
		
		var image = Ti.UI.createView({
			backgroundImage: color === 'white' ? 'images/nav_back_white.png': 'images/nav_back_grey.png',
			width: 25,
			height: 23,
			//transform:Ti.UI.create2DMatrix().scale(1.2)
		});
		
		
		button.add(image);
	}
	else{
		button = Ti.UI.createButton({
			backgroundImage:'images/navbar_back_full.png',
			backgroundSelectedImage:'images/navbar_back_full_sel.png',
			backgroundFocusedImage:'images/navbar_back_full_sel.png',
			width: 47,
			height: 31,
			left: 5,
			opacity: 1,
			style:Titanium.UI.iPhone.SystemButtonStyle.PLAIN
		});
	};
	
	button.addEventListener('touchstart', function (e){
		if(color === 'white'){
			image.backgroundImage = 'images/nav_back_white_sel.png'
		}
		else{
			//image.backgroundImage = 'images/nav_back_grey_sel.png'
		};
	});
	
	button.addEventListener('click', function (){
		if(color != 'white'){
			globals.currentStack.navGroup.close(win);
		};
	});
	
	button.addEventListener('postlayout', function (){
		//button.opacity = 1; 
	});
	
	function clean (){
		if (image != null){
			button.remove(image);
			image = null;
		};
	};
	
	button.clean = clean;

	return button;
};


exports.closeButton = function (){
	var button = Ti.UI.createView({
		backgroundImage: 'images/nav_x_grey.png',
		width:20,
		height: 20,
		//backgroundLeftCap: 15,
		left:5,
		opacity: 0
	});

	return button;
};


exports.doneButton = function (){
	var button = Ti.UI.createButton({
		title: I('Done')
	});
	
	return button;
};


exports.cancelButton = function (){
	var button = Ti.UI.createButton({
		title: I('Cancel')
	});
	
	return button;
};


exports.editButton = function (text,width){
	var button = Ti.UI.createButton({
		backgroundImage:'images/navbar_empty_button.png',
		backgroundSelectedImage:'images/navbar_empty_button_sel.png',
		backgroundFocusedImage:'images/navbar_empty_button_sel.png',
		backgroundLeftCap: 25,
		width: Ti.UI.SIZE ,
		height: 30,
		right: 5,
		opacity: 1,
		style:Titanium.UI.iPhone.SystemButtonStyle.PLAIN
	});
	
	
	var nameLabel = Ti.UI.createLabel({
		text: text ? text : 'edit',
		color: globals.fontColors[2],
		font: {fontWeight: 'bold', fontSize: 15},
		//shadowColor: 'black',
		//shadowOffset:{x:0,y:-1},
		width: width ? width : Ti.UI.SIZE,
		minimumFontSize: 13,
		textAlign: 'center',
		left: 5,
		right: 5,
		height: 20
	});
	
	
	nameLabel.addEventListener('postlayout', function (){
		Ti.API.debug('Post');
	});
	
	button.add(nameLabel);
	
	function clean (){
		button.remove(nameLabel);
		nameLabel = null;
	};
	
	button.label = nameLabel;
	button.clean = clean;
	
	
	return button;
};


exports.topNavigationBG = function (){
	var view = Ti.UI.createView({
		//backgroundImage:'images/navbar_bg.png',
		backgroundColor: 'black',
		width: 320,
		height: 44,
		opacity: .3
	});
	
	return view;
};


exports.centerSingleLabel = function (text, color){
	if(!color){color = 'grey'};
	
	var view = Ti.UI.createView({
		width: 190,
		height: 44,
		opacity: 0
	});

	
	var label = Ti.UI.createLabel({
		text: text,
		textAlign: 'center',
		width: Ti.UI.SIZE,
		height: 20,
		top: 8,
		//left: 10,
		//right: 10,
		color: color === 'white'? '#FCFFF4': globals.fontColors[2],
		font:{fontWeight: 'bold', fontSize: 20},
		minimumFontSize:14,
		shadowColor: color === 'white' ? 'black': '#FCFFF4',
		shadowOffset:{x:1,y:1}
	});
	
	
	view.add(label);
	
	
	function clean(){
		var children = view.children;
		for(var i = children.length-1 ; i >= 0;  i--){
			view.remove(children[i]);
			children[i] = null;
		};
		Ti.API.debug('Cleaned CenterSingle');
	};
	
	
	view.label = label;
	view.clean = clean;
	
	
	return view;
};


exports.centerDoubleLabel = function (bigText, smallText,color){
	if(!color){color = 'grey'};
	
	var view = Ti.UI.createView({
		width: 220,
		height: 44,
		opacity: 0
		//visible: false
	});
	
	view.addEventListener('postlayout', function(){
		if (view.size.width > 240){
			view.width = 240;
		};
	});
	
	var bigLabel = Ti.UI.createLabel({
		text: bigText,
		textAlign: 'center',
		width: 'auto',
		height: 23,
		top: 5,
		color: color === 'white'? '#FCFFF4': globals.fontColors[2],
		font:{fontWeight: 'bold', fontSize: 20},
		minimumFontSize:12,
		shadowColor: color === 'white' ? 'black': '#FCFFF4',
		shadowOffset:{x:1,y:1}
	});
	
	bigLabel.addEventListener('postlayout', function (e){
		//reposition();
	});
	
	
	var smallLabel = Ti.UI.createLabel({
		text: smallText,
		textAlign: 'center',
		width: Ti.UI.SIZE,
		height: 15,
		top: 23,
		//right: 35,
		//left: 30,
		color: color === 'white'? '#FCFFF4': globals.fontColors[2],
		font:{fontWeight: 'bold', fontSize: 13},
		minimumFontSize:12,
		shadowColor: color === 'white' ? 'black': '#FCFFF4',
		shadowOffset:{x:1,y:1}
	});
	
	
	
	function reposition (){
		//If they are numbers put them in the middle.
		if(smallLabel.text.indexOf('(')=== 1){
			
			smallLabel.left = null;
		}
		else {
			//smallLabel.left = 200/2 - (bigLabel.size.width/2 - 10);
			smallLabel.right = 35;
		};
		//view.visible = true;
	};
	
	
	view.update = function (bigLabelText,smallLabelText){
		smallLabel.text = smallLabelText;
		bigLabel.text = bigLabelText;
		
		//reposition();
	};
	
	
	view.add(bigLabel);
	view.add(smallLabel);
	
	function clean(){
		var children = view.children;
		for(var i = children.length-1 ; i >= 0;  i--){
			view.remove(children[i]);
			children[i] = null;
		};
		Ti.API.debug('Cleaned CenterDouble');
	};
	
	view.clean = clean;
	view.smallLabel = smallLabel;
	view.bigLabel = bigLabel;
	
	view.animate(Ti.UI.createAnimation({
		opacity: 1,
		duration: 100
	}));
	
	return view;
};

exports.centerSingleLabelWithBubble = function (text,color){
	if(!color){color = 'grey'};
	
	var view = Ti.UI.createView({
		width: 190,
		height: 44,
		//visible: false
		opacity: 0
	});

	
	var label = Ti.UI.createLabel({
		text: text,
		textAlign: 'center',
		width: Ti.UI.SIZE,
		height: 20,
		top: 8,
		//left: 10,
		//right: 10,
		color: color === 'white'? '#FCFFF4': globals.fontColors[2],
		font:{fontWeight: 'bold', fontSize: 20},
		minimumFontSize:14,
		shadowColor: color === 'white' ? 'black': '#FCFFF4',
		shadowOffset:{x:1,y:1}
	});
	
	label.addEventListener('postlayout', function (){
		Ti.API.debug('Post')
		if(label.size.width > 170){
			label.width = 170;
			label.left = 0;
			tagSymbol.right = 0;
		}
		else{
			tagSymbol.right = (view.width - label.size.width)/2-15;
		};
		
		view.animate(Ti.UI.createAnimation({
			opacity: 1,
			duration: 100
		}));
		
	});
	
	var tagSymbol = Ti.UI.createView({
		backgroundImage: 'images/navbar_tag_symbol.png',
		width: 31,
		height: 19,
		top: 23,
		//right: -3
	});
	
	view.add(label);
	view.add(tagSymbol);
	
	
	function clean(){
		var children = view.children;
		for(var i = children.length-1 ; i >= 0;  i--){
			view.remove(children[i]);
			children[i] = null;
		};
		Ti.API.debug('Cleaned CenterSingle');
	};
	
	
	view.label = label;
	view.clean = clean;
	
	
	return view;
};


exports.centerDoubleLabelWithBubble = function (bigText, smallText,color){
	if(!color){color = 'grey'};
	
	var view = Ti.UI.createView({
		width: 220,
		height: 44,
		opacity: 0
		//visible: false
	});
	
	view.addEventListener('postlayout', function(){
		if (view.size.width > 240){
			view.width = 240;
		};
	});
	
	var bigLabel = Ti.UI.createLabel({
		text: bigText,
		textAlign: 'center',
		width: 'auto',
		height: 23,
		top: 5,
		color: color === 'white'? '#FCFFF4': globals.fontColors[2],
		font:{fontWeight: 'bold', fontSize: 20},
		minimumFontSize:12,
		shadowColor: color === 'white' ? 'black': '#FCFFF4',
		shadowOffset:{x:1,y:1}
	});
	
	bigLabel.addEventListener('postlayout', function (e){
		reposition();
	});
	
	
	var smallLabel = Ti.UI.createLabel({
		text: smallText,
		textAlign: 'center',
		width: Ti.UI.SIZE,
		height: 15,
		top: 23,
		right: 35,
		//left: 30,
		color: color === 'white'? '#FCFFF4': globals.fontColors[2],
		font:{fontWeight: 'bold', fontSize: 13},
		minimumFontSize:12,
		shadowColor: color === 'white' ? 'black': '#FCFFF4',
		shadowOffset:{x:1,y:1}
	});
	
	var tagSymbol = Ti.UI.createView({
		backgroundImage: 'images/navbar_tag_symbol.png',
		width: 31,
		height: 19,
		top: 22,
		right: 7
	});
	
	function reposition (){
		//If they are numbers put them in the middle.
		if(smallLabel.text.indexOf('(')=== 1){
			
			smallLabel.left = null;
		}
		else {
			//smallLabel.left = 200/2 - (bigLabel.size.width/2 - 10);
			smallLabel.right = 35;
		};
		//view.visible = true;
	};
	
	
	view.update = function (bigLabelText,smallLabelText){
		smallLabel.text = smallLabelText;
		bigLabel.text = bigLabelText;
		
		//reposition();
	};
	
	
	view.add(bigLabel);
	view.add(smallLabel);
	view.add(tagSymbol);
	
	function clean(){
		var children = view.children;
		for(var i = children.length-1 ; i >= 0;  i--){
			view.remove(children[i]);
			children[i] = null;
		};
		Ti.API.debug('Cleaned CenterDouble');
	};
	
	view.clean = clean;
	view.smallLabel = smallLabel;
	view.bigLabel = bigLabel;
	
	view.animate(Ti.UI.createAnimation({
		opacity: 1,
		duration: 100
	}));
	
	return view;
};


exports.centerSingleLabelPhotoLibrary = function (text,color){
	if(!color){color = 'grey'};
	
	var view = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 40,
	});
	
	view.addEventListener('postlayout', function(){
		if (view.size.width > 240){
			view.width = 240;
		};
	});
	
	var icon = Ti.UI.createView({
		backgroundImage:'images/photos_app.png',
		width:	32,
		height: 25,
		//bottom: 20,
		left:0
	})
	
	var label = Ti.UI.createLabel({
		text: text,
		textAlign: 'left',
		width: Ti.UI.SIZE,
		height: 20,
		left: 40,
		color: color === 'white'? 'white': globals.fontColors[1],
		font:{fontWeight: 'bold', fontSize: 20},
		minimumFontSize:12,
		shadowColor: color === 'white' ? 'black': 'white',
		shadowOffset:{x:1,y:1}
	});
	
	label.addEventListener('postlayout', function (){
		if (label.size.width > 200){
			label.width = 200;
		};
	});
	
	view.add(icon);
	view.add(label);
	
	return view;
};