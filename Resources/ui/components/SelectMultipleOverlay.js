exports.appear = function (win){
	win.selectionMode = 'multiple';
		
	var topBar =  Ti.UI.createWindow({
		backgroundImage:'images/navbar_grey.png',
		width:  320,
		height: 43, //46
		top: 0
	});
	
	var TopNavigation = require('ui/components/TopNavigation');
	var cancelButton = TopNavigation.editButton('Cancel');
	
	cancelButton.addEventListener('click', function (){
		win.toggleSelectMode();
		closeAnimation();
	});
	
	var centerTitle =Ti.UI.createLabel({
		text: I("Select_Photos"),
		textAlign: 'center',
		width: Ti.UI.SIZE,
		height: 20,
		top: 8,
		//left: 10,
		//right: 10,
		color: '#FCFFF4',
		font:{fontWeight: 'bold', fontSize: 20},
		minimumFontSize:14,
		shadowColor: 'black',
		shadowOffset:{x:1,y:1}
	});// TopNavigation.centerSingleLabel(I("Select_Photos"), 'white');
	
	topBar.add(centerTitle);
	topBar.add(cancelButton);
	
	var bottomBar =  Ti.UI.createView({
		//backgroundImage:'images/navbar_grey.png',
		backgroundColor: 'black',
		width:  324,
		height: 60, //46
		bottom: 0,
		opacity: .8,
		transform: Ti.UI.create2DMatrix().translate(0,60)
	});
	
	var ShareButton = require('ui/components/ShareButton');
	
	var facebookButton = ShareButton.facebookButton();
	facebookButton.transform = Ti.UI.create2DMatrix().scale(.7);
	facebookButton.enabled = true;
	
	facebookButton.addEventListener('click', function (){
		if (facebookButton.enabled === true){
			ShareButton.sharePhotos('facebook', win.scrollView);
		}
	});
	
	
	
	var emailButton = ShareButton.emailButton();
	emailButton.transform = Ti.UI.create2DMatrix().scale(.7);
	emailButton.enabled = true;
	
	emailButton.addEventListener('click', function (){
		if (emailButton.enabled === true){
			ShareButton.sharePhotos('email', win.scrollView);
		};	
	});
	
	
	
	var twitterButton = ShareButton.twitterButton();
	twitterButton.transform = Ti.UI.create2DMatrix().scale(.7);
	twitterButton.enabled = true;
	
	twitterButton.addEventListener('click', function (){
		if (twitterButton.enabled === true){
			ShareButton.sharePhotos('twitter', win.scrollView);
		};
	});
	
	
	bottomBar.add(facebookButton);
	bottomBar.add(emailButton);
	bottomBar.add(twitterButton);
	
	//Methods
	function openAnimation (){
		bottomBar.animate(Ti.UI.createAnimation({
			duration: 250,
			transform: Ti.UI.create2DMatrix().translate(0,0)
		}));
	};
	
	function closeAnimation (){
		bottomBar.animate(Ti.UI.createAnimation({
			duration: 250,
			transform: Ti.UI.create2DMatrix().translate(0,60)
		}));
		
		win.navBarHidden = false;
		
		Ti.App.removeEventListener('selectedMultiple', update);
		
		setTimeout(function(){
			topBar.remove(cancelButton);
			topBar.close();
			topBar = null;
			win.remove(bottomBar);
		},250);
	};
	
	function update(e){
		var selectedCount = e.value;
		if (selectedCount === 1){
			twitterButton.enabled = true;
			twitterButton.opacity = 1;
			
		}
		else{
			twitterButton.enabled = false;
			twitterButton.opacity = .5;
		};
		
		if (selectedCount <= 20 && selectedCount > 0){
			emailButton.enabled = true;
			emailButton.opacity = 1;
			facebookButton.enabled = true;
			facebookButton.opacity = 1;
		}
		else{
			emailButton.enabled = false;
			emailButton.opacity = .5;
			facebookButton.enabled = false;
			facebookButton.opacity = .5;
		};
		
		twitterButton.label.text ='twitter '+selectedCount+'/1';
		emailButton.label.text = 'email '+selectedCount+'/20' ;
		facebookButton.label.text = 'facebook '+selectedCount+'/20' ;
	};
	
	update({value:0});
	///
	
	Ti.App.addEventListener('selectedMultiple', update);
	
	topBar.open();
	openAnimation();
	win.add(bottomBar);
};	