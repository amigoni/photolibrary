exports.loadWin = function (dictionary, tagType, whatSpecialKind){''
	whatSpecialKind = whatSpecialKind ? whatSpecialKind : 'normal';
	
	var Helper = require ('lib/Helper');
	var DataMedia = require('model/DataMedia');
	
	var win = Ti.UI.createWindow({
		tabbedBarHidden: true,
		navBarHidden: true,
		backgroundColor : '#000',
		barColor: '#000',
		opacity: 0,
		//transform:Ti.UI.create2DMatrix().scale(1.3),
		//title : (imageId + 1) + I(dictionary.scrollableGallery.i18nOfKey, ' of ') + dictionary.images.length,
		translucent : true,
		orientationModes: whatSpecialKind === 'normal' ? [
			Titanium.UI.PORTRAIT,
			Titanium.UI.UPSIDE_PORTRAIT,
			Titanium.UI.LANDSCAPE_LEFT,
			Titanium.UI.LANDSCAPE_RIGHT,
		] : [Titanium.UI.PORTRAIT],
		kind: 'pictureGallery'
	});
	
	globals.currentModal = win;
	
	//Gets the CurrentOrientation without the event being Fired.
	function getCurrentOrientation (){
		win.screenWidth = Ti.Platform.displayCaps.platformWidth;
		win.screenHeight = Ti.Platform.displayCaps.platformHeight;
		var orientation; 
		if (win.screenWidth > win.screenHeight) {
		    orientation = 3;
		} else {
		    orientation = 1;    
		};
		
		return orientation;
	};
	
	var currentOrientation = getCurrentOrientation();
	
	//win.dictionary = dictionary;
	win.isUiHidden = false;
	win.keyboardUp = false;
	win.touchedBubble = false; //This is for the toggle to catch when a bubble is touched. 
	win.moveModeOn = false;
	win.deleteBubbleMode = false;
	win.loadingMoreData = false; //Filter to prevent from loading data while already loading;
	win.navBarTransparent = true; 
	win.imagesDataArray = dictionary.imagesDataArray;
	
	
	
	///// NavBar
	var TopNavigation = require('ui/components/TopNavigation');
	var navBar = createNavBar();
	
	var backButton = Ti.UI.createView({
		//backgroundColor: 'red',
		width: 72,
		height: 72,
		top: 0,
		left: 0
	}); //TopNavigation.backButton(win,'white');
	
	backButton.add(Ti.UI.createView({
		backgroundImage:'images/nav_x.png',
		width: 23,
		height: 23,
		top: 15,
		left: 15
	})); //TopNavigation.backButton(win,'white');
	
	backButton.addEventListener('click', function (){
		//NOTE: Can't seem to get the win.close event to fire. so I am putting it here instead. 
		//globals.currentStack.navBar.rotate(320, 480);
		//globals.currentStack.navBar.goUp();
		//globals.currentStack.navBar.goDown();
		
		winCloseAnimation();
		//globals.tabGroup.comeUpfront();
	});
	
	
	var centerTitle = TopNavigation.centerDoubleLabel(dictionary.winTitle, dictionary.winSubtitle+' ( '+(dictionary.currentPageIndex+1)+' of '+dictionary.numberOfAssets+' )','white');
	centerTitle.opacity = 1;
	win.centerTitle = centerTitle;
	
	navBar.add(centerTitle);
	navBar.add(backButton);
	/////
	
	
	//Bottom Bar leave above PageScrollView needs to be refreshed when Page Scrollview loads current image;
	var BottomBar = require('ui/windows/galleryWindow/BottomBar');
	var bottomBar;
	if (whatSpecialKind && whatSpecialKind != 'normal'){
		bottomBar = BottomBar.pictureOfTheWeek(dictionary, win, whatSpecialKind);
	}
	else {
		bottomBar = BottomBar.normalBottomBar(dictionary, win);
	}
	bottomBar.zIndex= 99;
	win.bottomBar = bottomBar;
	
	///// Page ScrollView
		// viewWith Image Create function
	var PageScrollView = require('ui/windows/galleryWindow/PageScrollView');
	var scrollableGalleryView = new PageScrollView(dictionary.numberOfAssets, dictionary.currentPageIndex ,win, dictionary);
	scrollableGalleryView.anchorPoint = {x: .5, y: .5};
	//scrollableGalleryView.transform = Ti.UI.create2DMatrix().scale(0),
	/*
	scrollableGalleryView.addEventListener('scroll', function(e){
		if (instructions){
			instructions.closeAnimation()
			instructions = null;
		};
	});
	*/
	
	scrollableGalleryView.addEventListener('touchstart', function(e){
		if (instructions){
			instructions.closeAnimation()
			win.remove(instructions);
			instructions = null;
		};
	});
	
	
	function winCloseAnimation (){	
		globals.tabGroup.appear();
		
		Ti.Gesture.removeEventListener('orientationchange', scrollableGalleryView.orientationChange);
		
		//Close the Keyboard and move everything down
		if (win.keyboardUp === true){
				win.keyboardUp = false;
				//win.closeAnimation();
				win.selectionView.closing();
		};
		
		win.animate(Ti.UI.createAnimation({
			delay: 400,
			duration: 250,
			opacity: 0,
			//transform: Titanium.UI.create2DMatrix().scale(1.5)
		}));
		
		if (instructions){ instructions.visible = false;}
		navBar.visible = false;
		bottomBar.visible = false;
		
		scrollableGalleryView.closingAnimation();
		
		setTimeout(function(){
			clean();
			win.close();
			win = null;
		},650);
		
		if (tagType === 'person'){
			//Ti.App.fireEvent('updateLabelsList');
			Ti.App.fireEvent('updatePersonDetail');
			Ti.App.fireEvent('updatePersonsList');
		}
		else if (tagType === 'label'){
			//Ti.App.fireEvent('updatePersonsList');
			Ti.App.fireEvent('updateLabelDetail');	
			Ti.App.fireEvent('updateLabelsList');
		};	
	};


	function closeAnimation (){	
		//Close the Keyboard and move everything down
		scrollableGalleryView.animate(Ti.UI.createAnimation({
			duration: 150,
			transform: Titanium.UI.create2DMatrix().translate(0, 0)
		}));
		
		win.keyboardUp = false;
		win.selectionView.closing();
		
		if(win.isUiHidden === true){
			toggleUI();
		};
	};
	
	
	function upAnimation (moveY){	
		//Calculate and move the Scrollable View accordingly to leave room for the table
		scrollableGalleryView.animate(Ti.UI.createAnimation({
			duration: 200,
			transform: Titanium.UI.create2DMatrix().translate(0, -moveY)
		}));
	};
	
	
	function toggleMoveMode (){
		if (win.moveModeOn === false){
			scrollableGalleryView.scrollEnabled = false;
			win.currentImageScrollView.scrollEnabled = false;
			//globals.currentStack.navBar.disappear();
			navBar.disappear();				
			bottomBar.visible = false;
			win.moveModeOn = true;
			
			Ti.API.debug('MoveMode ON')
		}
		else if (win.moveModeOn === true){
			win.scrollableGalleryView.scrollEnabled = true;
			win.currentImageScrollView.scrollEnabled = false;
			//globals.currentStack.navBar.appear();
			navBar.appear();
			bottomBar.visible = true;
			win.moveModeOn = false;
			
			Ti.API.debug('MoveMode OFF')
		};
	};
	

	//Toogle Bubbles, BottomBar and NavBar to Leave all clean. 
	
	function UIAppear (){
		win.currentImage.dragLayer.visible = true;
		win.bottomBar.visible = true;
		//globals.currentStack.navBar.appear();
		if(win.instructions){
			win.instructions.visible = true;
		};
		
		navBar.appear();
	};
	
	function UIDisappear(){
		win.currentImage.dragLayer.visible = false;
		win.bottomBar.visible = false;
		if(win.instructions){
			win.instructions.visible = false;
		};
		navBar.disappear();
		//globals.currentStack.navBar.disappear();
	};
	
	var toggleUI = function() {
		if (win.isUiHidden == true) {
			UIAppear();
		} 
		else{
			UIDisappear();
		}
		win.isUiHidden = !win.isUiHidden;
	};
	
	
	
	//Rotation
	var rotationCover = Ti.UI.createView({
		width: 320,
		height: 480,
		visible: false,
		anchorPoint: {x:0.5, y: 0.5}
	});
	
	
	var currentOrientation = getCurrentOrientation();
	var lastOrientation = currentOrientation;
	
	function rotateWindow(e){
	
		currentOrientation = e ? e.orientation : currentOrientation;
		
		if (currentOrientation != 5 && currentOrientation != 6 && currentOrientation != 0 && currentOrientation != lastOrientation ){
			//Ti.API.debug('Current Orientation '+ currentOrientation);
			lastOrientation = currentOrientation;
			
			//Close the Person Selection if it' open
			if(win.selectionView){win.selectionView.orientationClosing()};
			
			//Get the old size of the image to use for the rotation cover 
			var image = win.scrollableGalleryView.currentView.imageView.backgroundImage;		
			var oldSize = Helper.imageResize2(win.screenWidth, win.screenHeight, image.width, image.height);
			
			win.screenWidth = Ti.Platform.displayCaps.platformWidth;
			win.screenHeight = Ti.Platform.displayCaps.platformHeight;
			//globals.tabGroup.containerWindow.width = win.screenWidth;
			//globals.tabGroup.containerWindow.height = win.screenHeight;
			win.width = win.screenWidth;
			win.height = win.screenHeight;
			
			//alert(win.screenWidth)
			
			//Rotation Cover Stuff
			var newSize = Helper.imageResize2(win.screenWidth, win.screenHeight, image.width, image.height);
			var scaleFactor = newSize.width/oldSize.width;
			
			rotationCover.backgroundImage = image;
			rotationCover.width = oldSize.width;
			rotationCover.height = oldSize.height;
			
			var animation = Ti.UI.createAnimation({
				duration: 250,
				transform: Ti.UI.create2DMatrix().scale(scaleFactor)
			})
			
			var animationHandler = function() {
				win.scrollableGalleryView.opacity = 1;
				rotationCover.visible = false;
				rotationCover.transform = Ti.UI.create2DMatrix().scale(1)
		
			  	animation.removeEventListener('complete',animationHandler);
			};
			animation.addEventListener('complete',animationHandler);
			
			rotationCover.animate(animation);
			rotationCover.visible = true;
			///
			
			
			scrollableGalleryView.orientationChange(e);
			if(win.labelPickerPopUp){win.labelPickerPopUp = win.labelPickerPopUp.closeAnimation()};
			//globals.currentStack.navBar.rotate(win.screenWidth, win.screenHeight);
			navBar.rotate();
		};
	};

	Ti.Gesture.addEventListener('orientationchange', rotateWindow);
	///

	
	
	function clean(){
		Ti.App.removeEventListener('pause', closeOnSuspend);
		
		win.closeAnimation = null;
		win.upAnimation = null;
		win.currentBubbles = null;
		win.scrollableGalleryView = null;
		win.bottomBar = null;
		win.currentImageScrollView = null;
		win.currentImage = null;
		win.currentPage =  null;
		//win.dictionary = null;
		win.createClickDot = null;
		win.toggleUI = null;
		win.toggleMoveMode = null;
		win.getCurrentOrientation = null;
		win.lastPage = null;
		images = null;
		win.images = null;
		
		
		//backButton.clean();
		centerTitle.clean();
		navBar.remove(backButton);
		navBar.remove(centerTitle);
		backButton = null;
		centerTitle = null;
		win.remove(navBar);
		navBar = null;
		
		
		if (instructions){
			instructions.clean();
			win.remove(instructions);
			instructions = null;
		};
		
		win.remove(rotationCover);
		rotationCover = null;
		
		scrollableGalleryView.clean();
		win.remove(scrollableGalleryView);
		scrollableGalleryView = null;
		
		bottomBar.clean();
		win.remove(bottomBar);
		bottomBar = null;
		
		//win = null;
		Ti.API.debug('Cleaned Gallery')
	};

	
	var instructions;
	if (whatSpecialKind === 'normal'){
		instructions = createInstructions(win);
	};
	
	//Listeners
	
	win.addEventListener('open', function (){
		//globals.tabGroup.goBehind();
		
		win.animate(Ti.UI.createAnimation({
			//delay: 100,
			duration: 150,
			opacity: 1,
			//transform: Ti.UI.create2DMatrix().scale(1),
			curve: Ti.UI.iOS.ANIMATION_CURVE_EASE_OUT
		}));
		
		scrollableGalleryView.animate(Ti.UI.createAnimation({
			delay: 150,
			duration: 150,
			//width: 320,
			//height: 480
			//transform: Ti.UI.create2DMatrix().scale(1),
		}));
		
		//Assemble Window
		win.add(scrollableGalleryView);
		win.add(bottomBar);
		win.add(rotationCover);
		if (instructions){win.add(instructions);}
		
	
		win.add(navBar);
		
		setTimeout( function (){
			navBar.appear();
			if(instructions){instructions.openAnimation()};
			bottomBar.openAnimation();
			globals.tabGroup.disappear();
		},150);
	});
	
	
	function closeOnSuspend (){
		winCloseAnimation();
	};
	
	Ti.App.addEventListener('pause', closeOnSuspend);
	
	win.closeAnimation = closeAnimation;
	win.winCloseAnimation = winCloseAnimation;
	win.upAnimation = upAnimation;
	win.clean = clean;
	win.rotationCover = rotationCover;
	//win.currentBubbles = currentBubbles;
	win.bottomBar = bottomBar;
	win.toggleUI = toggleUI;
	win.UIAppear = UIAppear;
	win.UIDisappear = UIDisappear;
	win.toggleMoveMode = toggleMoveMode;
	win.getCurrentOrientation = getCurrentOrientation;
	win.scrollableGalleryView = scrollableGalleryView;
	win.lastPage = scrollableGalleryView.currentPage;
	win.instructions = instructions;
	

	return win;
};


var createInstructions = function (win){
	var view = Ti.UI.createView({
		//backgroundImage: 'images/tag_bubble.png',
		backgroundLeftCap: 20,
		backgroundTopCap: 12,
		//backgroundColor: 'black',
		//borderRadius: 5,
		//borderColor: 'white',
		width: Ti.UI.SIZE,
		height: 50,
		top: 50,
		touchEnabled: false,	
		opacity:0
	});
	
	view.add(Ti.UI.createView({
		backgroundColor: 'black',
		borderRadius: 5,
		opacity:.6,
		width:160,
		height:45,
	}))
	
	var label1 = Ti.UI.createLabel({
		text: I('Double_tap_to_add'),
		textAlign:'center',
		font: {fontWeight: 'bold', fontSize: 14},
		width:Ti.UI.SIZE,
		color: 'white',
		top: 5,
		left:10,
		right:10
	})
	
	var label2 = Ti.UI.createLabel({
		text: I('Tap_and_hold_to_move'),
		textAlign:'center',
		font: {fontWeight: 'bold', fontSize: 14},
		width:Ti.UI.SIZE,
		color: 'white',
		top: 25,
		left:10,
		right:10
	});
	
	
	function openAnimation (){
		view.animate(Ti.UI.createAnimation({
			//delay:500,
			duration: 250,
			opacity: 1,
			//transform: Titanium.UI.create2DMatrix().scale(1)
		}));
	};
	
	function closeAnimation (){
		view.animate(Ti.UI.createAnimation({
			duration: 250,
			//transform: Titanium.UI.create2DMatrix().scale(0)
			opacity: 0
		}));
		
		setTimeout(function(){
				clean();
		},250);
	};
	
	
	function clean(){
		var children = view.children;
		for(var i = children.length-1 ; i >= 0;  i--){
			view.remove(children[i]);
			children[i] = null;
		};
		Ti.API.debug('Cleaned Instructions');
	};
	
	view.add(label1);
	view.add(label2);
	
	
	//Expose
	view.clean = clean;
	view.closeAnimation = closeAnimation;
	view.openAnimation = openAnimation;
	
	
	return view
};


//Functions that gets initial Orientation
function getCurrentOrientation (){
	var screenWidth = Ti.Platform.displayCaps.platformWidth;
	var screenHeight = Ti.Platform.displayCaps.platformHeight;
	var orientation; 
	if (screenWidth > screenHeight) {
	    orientation = 3;
	} else {
	    orientation = 1;    
	};
	
	return orientation;
};


function createNavBar (){
	var view = Ti.UI.createView({
		//backgroundColor: 'yellow',
		width: '100%',
		height: 44,
		top: 1,
		//transform: Ti.UI.create2DMatrix().translate(0,-44)
	});
	
	
	function disappear (){
		view.animate(Ti.UI.createAnimation({
			duration : 250,
			opacity: 0,
			transform: Ti.UI.create2DMatrix().translate(0,-44)
		}))
	};
	
	function appear(){
		view.animate(Ti.UI.createAnimation({
			duration : 250,
			opacity: 1,
			transform: Ti.UI.create2DMatrix().translate(0,0)
		}));
	};	
	
	function rotate(){
		view.width = Ti.Platform.displayCaps.platformWidth;
	};
	
	view.disappear = disappear;
	view.appear = appear;
	view.rotate = rotate;
	
	
	return view;
};