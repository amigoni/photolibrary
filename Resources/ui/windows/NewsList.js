exports.loadWin = function (personID){
	var Window = require('ui/components/Window');
	var TopNavigation = require('ui/components/TopNavigation');
	var MiscComponents = require('ui/components/MiscComponents');
	
	var DataNews = require('model/DataNews');
	
	var news = globals.col.news.find({},{$sort:{guid:-1}});
	
	//Create Window
	var win = Window.createWindow({
		tabBarHidden: true,
		tabbedBarHidden: true
	});
	win.clickable = true;
	
	
	//Set Navigation
	var menuButton = TopNavigation.menuButton();
	win.leftNavButton = menuButton;

	var centerTitle = TopNavigation.centerSingleLabel(I('News'), 'white');// TopNavigation.centerDoubleLabel('', '', 'white');
	win.centerTitle = centerTitle;
	win.titleControl = centerTitle;
	
	
	//Create Tableview
	var tableData = [];
	
	for (var i =0; i < news.length; i++){
		tableData.push(createRow(news[i]))
	};
	
	
	var tableView = MiscComponents.standardTableView();
	tableView.separatorStyle = 1;
	tableView.showVerticalScrollIndicator = false;
	tableView.data = tableData;
	
	tableView.addEventListener('click', function (e){
		Ti.Platform.openURL(e.row.link);
		//Ti.App.fireEvent('updateSideBar'); 
	});
	
	
	//Assemble Window
	win.add(tableView);
	
	setTimeout( function (){
		globals.col.news.update({},{$set:{opened:true}});
		globals.col.news.commit();
		Ti.App.fireEvent('updateSideBar'); 
	},500);

	
	return win;
};


function createRow (data){

	var Labels = require('ui/components/Labels');
	
	var row = Ti.UI.createTableViewRow({
		backgroundSelectedImage:'images/row_selected.png',
		backgroundFocusedImage: 'images/row_selected.png',
		className: 'newsRow',
		hasArrow: true,
		height: Ti.UI.SIZE,
		editable:false,
		link: data.link,
		guid: data.guid,
		layout: 'vertical'
	});
	
	var bottomGroove = Ti.UI.createView({width:row.width,height:1, top:row.height-1,backgroundColor:'black',opacity:.05});
	
	var topGroove = Ti.UI.createView({width:row.width,height:1,top:0, backgroundColor:'white'});
	
	var titleLabel = Ti.UI.createLabel({
		text: data.title,
		color: globals.fontColors[2],
		font: {fontWeight: 'bold', fontSize: 17},
		shadowColor: '#fff',
		shadowOffset:{x:1,y:1},
		width: 300,
		left: 10,
		height: 20,
		top: 5,
		touchEnabled: false
	});
	
	//var rtext = require('ti.richtext'); 
	var descriptionLabel = Ti.UI.createLabel({ 
		text: data.description,
		//html: data.description,
		color: globals.fontColors[0],
		font: {fontSize: 14},
		shadowColor: '#fff',
		shadowOffset:{x:1,y:1},
		width: 300,
		left: 10,
		bottom:5,
		height:Ti.UI.SIZE,
		touchEnabled: false
	});
	
	var selectionOverlay = Ti.UI.createView({
		backgroundImage: 'images/row_selected.png',
		width:320,
		height:row.height,
		opacity: 0.02,
	});
	
	var longClick = false;
	selectionOverlay.addEventListener('touchstart', function (e){
		selectionOverlay.opacity = 1;
		//groove.visible = false;
		setTimeout(function(){
			longClick = true;
		},1500);
	});
	
	/*
	selectionOverlay.addEventListener('touchend', function(e){
		if (longClick === true) {
			selectionOverlay.opacity = .01;
		}
	});
	*/

	
	row.add(topGroove);
	row.add(bottomGroove);
	row.add(titleLabel);
	row.add(descriptionLabel);
	row.add(selectionOverlay);
	
	row.selectionOverlay = selectionOverlay;
	row.groove = bottomGroove;
	
	return row;
};