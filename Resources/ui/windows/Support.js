//This and EventsLists are identical. 
exports.loadWin = function (){
	var Window = require('ui/components/Window');
	var TopNavigation = require('ui/components/TopNavigation');
	
	//Create Window
	var win = Window.createWindow({
		tabBarHidden:true,
		tabbedBarHidden:false,
		kind: 'support'
	});
		
	var backButton = TopNavigation.backButton(win);
	
	var centerTitle = TopNavigation.centerSingleLabel(I('Support'),'white');
	
	win.leftNavButton = backButton;
	win.centerTitle = centerTitle;
	win.titleControl = centerTitle;
	
	
	var webView = Ti.UI.createWebView({
		top: 44,
		left: 0,
		height: Ti.Platform.displayCaps.platformHeight-42,
		width: Ti.Platform.displayCaps.platformWidth,
		//url:'https://getSatisfaction.com/mozzarello'	
		url:'https://mozzarello.uservoice.com/clients/widgets/6jzizawi7g4jWFZ70Z9g.html'
	});
	
	
	win.add(webView)
	
	function preClean (){
		Ti.API.debug('Cleaned Album List');
	};
	
	
	//Expose
	win.preClean = preClean;
	
	//Listeners
	
	return win;
};