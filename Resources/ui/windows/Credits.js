//This and EventsLists are identical. 
exports.loadWin = function (){
	var Window = require('ui/components/Window');
	var TopNavigation = require('ui/components/TopNavigation');
	
	//Create Window
	var win = Window.createWindow({
		tabBarHidden:true,
		tabbedBarHidden:false,
		kind: 'support'
	});
		
	var backButton = TopNavigation.backButton(win);
	
	var centerTitle = TopNavigation.centerSingleLabel(I('Credits'),'white');
	
	win.leftNavButton = backButton;
	win.centerTitle = centerTitle;
	win.titleControl = centerTitle;
	
	
	var file = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, "ui/windows/Credits.html");
	var blob = file.read();
	
	var rtext = require('ti.richtext'); 
	
	var scrollView = Ti.UI.createScrollView({
		top: 44,
		//backgroundColor: 'yellow',
		width: 320,
		height: 480-44,
		contentWidth: 320,
		contentHeight: Ti.UI.SIZE
	});
	
	var richLabel = rtext.createView({ 
		//backgroundColor: 'red',
		top: 0,
		width: 300,
		height: Ti.UI.SIZE, 
		//font: , 
		html: blob.toString(),
	});
	
	scrollView.add(richLabel);
	
	win.add(scrollView)
	
	function preClean (){
		Ti.API.debug('Cleaned Album List');
	};
	
	
	//Expose
	win.preClean = preClean;
	
	//Listeners
	
	return win;
};