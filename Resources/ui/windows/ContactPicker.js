exports.loadWin = function (pickingYourself){
	pickingYourself = pickingYourself? pickingYourself : false;
	
	var Window = require('ui/components/Window');
	var DataPersons = require('model/DataPersons');
	
	var baseWin = Ti.UI.createWindow({
		modal: true,
		navBarHidden: true
	});
	
	var win = Ti.UI.createWindow({
		title: I('New_Contact'),
		width: '100%',
		height: '100%',
		modal: true,
		tabBarHidden: true,
		navBarHidden: false
	});
	
	var cancelButton = Ti.UI.createButton({
		title: 'Cancel'
	});
	
	cancelButton.addEventListener('click', function (){
		baseWin.close();
		setTimeout(function (){
			baseWin.clean()},
		250);
	})
	
	win.leftNavButton = cancelButton;

	
	
	var addButton = Ti.UI.createButton({
		title: 'add new'
	});
	
	var addContactWin;
	addButton.addEventListener('click', function (){
		var addContactForm = require('ui/windows/addContactForm');
		addContactWin = addContactForm.loadWin(null,baseWin);
		addContactWin.addEventListener('addedNewContact', function (e){
			pickedContact(e.value.id);
			baseWin.close();
			setTimeout(function (){
				baseWin.clean()},
			250);
		});
		nav.open(addContactWin);
	});
	
	win.rightNavButton = addButton;
	
	
	
	///TableView
	var tableData = []
	//Create Row Dividers 
    var cheader = '';//alphabet header variable 
    var index = [];  //array for the right index of the table. Alphabet to scroll. 
	
	for (var i = 0; i < globals.addressBookContacts.length; i++){
		var letter = globals.addressBookContacts[i].name[0].toUpperCase();
		
		var row;
		if (letter != cheader){
			cheader = letter;
			index.push({title: letter, index: i});
				
			row = Ti.UI.createTableViewRow({
				className: 'standard',
				title: globals.addressBookContacts[i].name,
				id: globals.addressBookContacts[i].id,
				header: cheader
			});
		}
		else{
			row = Ti.UI.createTableViewRow({
				className: 'standard',
				title: globals.addressBookContacts[i].name,
				id: globals.addressBookContacts[i].id
			});
		};
		
		tableData.push(row);
	};
	
	
	var tableFooter = Ti.UI.createLabel({
		text: I('Please_enter_First_and_Last_name'),
		textAlign: 'center',
		shadowColor: 'white',
		width: 290,
		top:0,
		shadowOffset: {x:1,y:1},
		font: {fontSize: 15},
		color: '#54637C',
		
	});
	
	//var firstNameRow = createRow('First',textTyped);
	//var lastNameRow = createRow('Last');
	
	var tableView = Ti.UI.createTableView({
		//style: Titanium.UI.iPhone.TableViewStyle.GROUPED,
		width: '100%',
		height: Ti.Platform.displayCaps.platformHeight-44,
		top: 0,
		//footerView: tableHeader,
		data: tableData,
		index: index
	});
	
	
	function pickedContact (addressBookID){
		var person = Ti.Contacts.getPersonByID(addressBookID);
		var you = globals.col.persons.find({isMyself: true});
		
		var info = {};
		info.firstName = person.firstName;
		info.lastName = person.lastName;
		info.addressBookID = person.recordId	
		if (pickingYourself === true){
			info.isMyself = true;
		};
		
		
		//Check to see if the person already exists. 
		if(!DataPersons.loadPersonForAddressBookID(info.addressBookID)){
			//Match with faces Callback. 
			var addPersonCB = function (e1){
				if(e1){
					//var PopUp = require('ui/components/PopUp');
					//PopUp.facesMatch(e.name, newPersonID, e.group);
				};
			};
			
			//Fire this before adding person. otherwise it will be setup already
			
			if (pickingYourself === true){
				Ti.App.fireEvent('pickedYourself');
			};
			
			var newPersonID = DataPersons.addPerson(info).$id;
			
			//var win1 = PicturesList.loadWin('person',personID);
			var PersonDetail = require('ui/windows/PersonDetail');
			globals.currentStack.open(PersonDetail.loadWin(newPersonID),{animated:true});
			
			
			Ti.App.fireEvent('updatePersonsList');
			baseWin.close();
		}
		else{
			if (pickingYourself === true){
				var you = globals.col.persons.find({addressBookID: info.addressBookID});	
				if (you[0]){
					DataPersons.changeMyselfPersonStatus(you[0].$id);
					alert("Set this person as yourself");
					baseWin.close();
				}
			}
			else{
				alert(I("This_person_is_already_in_your_list"));
			}
		};
	};
	
	tableView.addEventListener('click', function (e){
		pickedContact(e.rowData.id);
	})

	
	win.add(tableView);
	
	var nav = Ti.UI.iPhone.createNavigationGroup({
		window: win
	});
	
	baseWin.add(nav);
	
	
	//Methods
	if (pickingYourself === true){
		setTimeout(function(){
			var PopUp = require('ui/components/PopUp');
			
			var titleText =I('Select_Yourself');
			var subTitleText = I('Please_select_yourself_from');
			var instructions = PopUp.instructionsPopUp(titleText,subTitleText);
			
			instructions.open();
		}, 250);	
	};
	
	function clean (){
		if (addContactWin){
			addContactWin.clean();
			addContactWin.close();
		};
		
		var children = win.children;
		
		for(var i = children.length - 1 ; i >= 0;  i--){
			win.remove(children[i]);
			children[i] = null;
		};
		
		Ti.API.debug('Cleaned Contact Picker window');
	};
	
	baseWin.clean = clean;
	
	
	
	return baseWin; 
};
