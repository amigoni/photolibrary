//This and AlbumList are identical. 
exports.loadWin = function (groupList){
	var Window = require('ui/components/Window');
	var TopNavigation = require('ui/components/TopNavigation');
	var PicturesList = require('ui/windows/PicturesList');
	
	//Load Data
	var dataIn;

	//Create Window
	var win = Window.createWindow({
		tabBarHidden: true,
		tabbedBarHidden: false,
		kind: 'events'
	});
	
	
	//Set Navigation
	//globals.tabGroup.navBar.backgroundImage = 'images/navbar_red.png';
	//globals.tabGroup.navBar.currentBG = 'images/navbar_red.png';
			
	var menuButton = TopNavigation.menuButton();
	
	var centerTitle = TopNavigation.centerSingleLabel('Events','white');
	
	win.leftNavButton = menuButton;
	win.centerTitle = centerTitle;
	win.titleControl = centerTitle;
	
	var emptyListLabel = Ti.UI.createLabel({
		text: I('There_are_no_Events_in_this_list'),
		width: 300,
		height: Ti.UI.SIZE,
		top: 55,
		font: {fontSize: 14},
		textAlign: 'center',
		color: globals.fontColors[2],
		shadowColor: 'white',
		shadowOffset: {x:0,y:1},
		visible: groupList.length > 0 ? false : true
	});	
	
	//Create Tableview
	var tableData = [];
	
	for (var i = 0; i < groupList.length; i++){	
		var MiscComponents = require('ui/components/MiscComponents');
		var row = MiscComponents.standarRowWithImage(groupList[i].name,groupList[i].numberOfAssets,groupList[i].posterImage);
		tableData.push(row);
	};
	
	
	var tableView = MiscComponents.standardTableView();
	tableView.clickable = true;
	tableView.data = tableData;
	
	var tableViewClick = _.throttle(function(e){
		var PicturesListLibrary = require('ui/windows/PicturesListLibrary');
		var win1 = PicturesListLibrary.loadWin(groupList[e.index]);
		globals.currentStack.open(win1,{animated:true});
	},2000);	
	
	tableView.addEventListener('click', function (e){
		if (tableView.clickable === true){
			tableViewClick(e);
			tableView.clickable = false; 
			var clickTimeout = setTimeout(function(){
				tableView.clickable = true; 
			},
			2000);
		};
	});
	
	
	//Assemble Window
	win.add(emptyListLabel);
	win.add(tableView);
	
	
	//Methods
	function preClean (){
		Ti.API.debug('Cleaned Event List');
	};
	
	
	//Expose
	win.preClean = preClean;
	
	
	//Listeners
	win.addEventListener('focus', function (){
		tableView.reset();
	});
	
	
	return win;
};