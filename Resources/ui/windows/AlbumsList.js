//This and EventsLists are identical. 
exports.loadWin = function (groupList){
	var Window = require('ui/components/Window');
	var TopNavigation = require('ui/components/TopNavigation');
	var PicturesList = require('ui/windows/PicturesList');
	var MiscComponents = require('ui/components/MiscComponents');
	
	//Load Data
	var dataIn;

	//Create Window
	var win = Window.createWindow({
		tabBarHidden:true,
		tabbedBarHidden:false,
		kind: 'albums'
	});
	
	
	
	//Set Navigation
	//globals.tabGroup.navBar.backgroundImage = 'images/navbar_brown.png';
	//globals.tabGroup.navBar.currentBG = 'images/navbar_brown.png';
		
	var menuButton = TopNavigation.menuButton();
	
	var centerTitle = TopNavigation.centerSingleLabel('Albums','white');
	
	win.leftNavButton = menuButton;
	win.centerTitle = centerTitle;
	win.titleControl = centerTitle;
	
	
	var emptyListLabel = Ti.UI.createLabel({
		text: I('There_are_no_Albums_in_this_list'),
		width: 300,
		height: Ti.UI.SIZE,
		top: 55,
		font: {fontSize: 14},
		textAlign: 'center',
		color: globals.fontColors[2],
		shadowColor: 'white',
		shadowOffset: {x:0,y:1},
		//visible: dataIn.length > 0 ? false : true
	});	
	
	
	//Tableview
	var tableView = MiscComponents.standardTableView();
	tableView.clickable = true;
	
	var tableViewClick = _.throttle(function(e){
		var PicturesListLibrary = require('ui/windows/PicturesListLibrary');
		globals.currentStack.open(PicturesListLibrary.loadWin(groupList[e.index]),{animated:true});
	}, 2000);	
	
	tableView.addEventListener('click', function (e){
		if (tableView.clickable === true){
			tableViewClick(e);
			tableView.clickable = false; 
			var clickTimeout = setTimeout(function(){
				tableView.clickable = true; 
			},
			2000);
		};
	});
	
	
	//Assemble Window
	win.add(emptyListLabel);
	win.add(tableView);
	
	
	//Methods
	function setData (){
		tableView.data = [];
		var tableData = [];
		
		for (var i = 0; i < groupList.length; i++){
			var row = MiscComponents.standarRowWithImage(groupList[i].name,groupList[i].numberOfAssets,groupList[i].posterImage);
			tableData.push(row);
		};
		
		tableView.data = tableData;
		emptyListLabel.visible = groupList.length > 0 ? false : true;
	};
	
	function preClean (){
		Ti.API.debug('Cleaned Album List');
	};
	
	
	//Expose
	win.preClean = preClean;
	
	//Listeners
	win.addEventListener('focus', function (){
		tableView.reset();
	});
	
	win.addEventListener('open',function (){
		setData();
	});
	
	
	return win;
};