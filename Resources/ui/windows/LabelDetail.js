exports.loadWin = function (labelID){
	var assetslibrary = require('ti.assetslibrary');
	var Window = require('ui/components/Window');
	var TopNavigation = require('ui/components/TopNavigation');
	var PicturesList = require('ui/windows/PicturesList');
	var DataLabels = require('model/DataLabels');
	var MiscComponents = require('ui/components/MiscComponents')
	
	//Get initial Data
	var dataIn;
	
	//Create Window
	var win = Window.createWindow({
		tabBarHidden:true,
		tabbedBarHidden:true
	});
	win.clickable = true;
	
	//Set Navigation
	win.leftNavButton = TopNavigation.backButton(win);
	
	var editButton = TopNavigation.editButton('edit', 40);
	editButton.left = null;
	editButton.right = 5;
	editButton.label.text = 'edit';
	
	editButton.addEventListener('click', function (){
		var PopUp = require('ui/components/PopUp');
		
		var newLabelPopUp = PopUp.labelPopUp(centerTitle.label.text);
		
		newLabelPopUp.addEventListener('ok', function(e){
			var newLabel = DataLabels.updateLabelName(labelID, e.value);
		});
		
		newLabelPopUp.addEventListener('return', function (e){
			var newLabel = DataLabels.updateLabelName(labelID, e.value);
		});
		
		newLabelPopUp.open();
	});
	
	win.rightNavButton = editButton;
	
	var centerTitle = TopNavigation.centerSingleLabel('-','white');
	win.centerTitle = centerTitle;
	win.titleControl = centerTitle;
	
	//Create Tableview
	var tableData = [];
	
	//Thumb Picture Header
	var thumbHeaderRow = MiscComponents.createThumbHeaderRow('label');
	thumbHeaderRow.clickable = true;
	
	var thumbHeaderRowClick = function(e){
		if(dataIn.allPictures.length > 0){
			var opts = {
			  cancel: 2,
			  options: [I('Pick_a_specific_photo'), 
			  I('Randomly_rotate_photo'), 
			  //'Latest Photo',
			  'Cancel'],
			  selectedIndex: 2,
			  //destructive: 0,
			  title: I('Select_Cover_Photo')
			};
			
			var dialog = Ti.UI.createOptionDialog(opts)
			
			dialog.addEventListener('click', function (e){
				if (e.index === 0){
					globals.currentStack.open(PicturesList.loadWin('allPictures','label',labelID,true),{animated:true});
				}
				else if (e.index === 1){
					var DataLabels = require('model/DataLabels');
					DataLabels.setThumbForLabel(labelID,null,'random');
				}
				//else if (e.index === 2){
					//alert('St')
					//DataPersons.setThumbForPerson(personID,null,'latest');
				//};
			});
			
			dialog.show();
		}
		else{
			alert(I('Please_tag_some_pictures_before')+centerTitle.label.text)
		};
	};	
	
	thumbHeaderRow.addEventListener('touchend', function (e){
		if (win.clickable === true){
			thumbHeaderRowClick(e);
			win.clickable = false; 
			var clickTimeout = setTimeout(function(){
				win.clickable = true; 
			},
			1500);
		};
	});
	
	
	
	//Favorites Row
	var favoritesRow = createRow(I('Favorites'),'-');
	favoritesRow.clickable = true;
	
	favoritesRow.add(Ti.UI.createView({
		backgroundImage: 'images/favorites_detail.png',
		width: 30,
		height: 28,
		left: 17
	}));
	
	var favoritesRowClick =  function(e){
		var win1 = PicturesList.loadWin('favorites','label',labelID);
		globals.currentStack.open(win1,{animated:true});
	};
	
	favoritesRow.addEventListener('click', function (e){
		if (win.clickable === true){
			if(favoritesRow.enabled === true){
				favoritesRowClick(e);
				win.clickable = false; 
				var clickTimeout = setTimeout(function(){
					win.clickable = true; 
				},
				1500);
			}
			else{
				alert(I('Press_the_star_button_while'));
			};
		};
	});
	
	
	
	//All Pictures Row
	var allPicturesRow = createRow(I('All_Pictures'),'-');
	allPicturesRow.clickable = true; 
	
	allPicturesRow.add(Ti.UI.createView({
		backgroundImage: 'images/polaroids.png',
		width: 31,
		height: 31,
		left: 15
	}));
	
	var allPicturesRowClick = function(e){
		var win1 = PicturesList.loadWin('allPictures','label',labelID);
		globals.currentStack.open(win1,{animated:true});
	};	
	
	allPicturesRow.addEventListener('click', function (e){
		if (win.clickable === true){
			if (allPicturesRow.enabled === true){
				allPicturesRowClick(e);
				win.clickable = false; 
				var clickTimeout = setTimeout(function(){
					win.clickable = true; 
				},
				1500);
			}
			else{
				//tagMoreRowClick(e);
				alert(I('Tag_photos_of_this_Label_to_add_them_to_this_list'));
				win.clickable = true;
			};
		};	
	});
	
	
	
	//Add Photos Row
	var tagMoreRow = MiscComponents.createAddPhotosRow();
	tagMoreRow.clickable = true;
	
	var tagMoreRowClick = function(e){
		var LibraryPicturesPicker = require('/ui/windows/LibraryPicturesPicker');
		LibraryPicturesPicker.loadWin('label',labelID);
	};	
	
	tagMoreRow.addEventListener('click', function (e){
		if (win.clickable === true){
			tagMoreRowClick(e);
			win.clickable = false; 
			var clickTimeout = setTimeout(function(){
				win.clickable = true; 
			},
			1500);
		};
	});
	
	
	
	var tableView = MiscComponents.standardTableView();
	tableView.showVerticalScrollIndicator = false;
	tableView.data = tableData;
	
	
	//Assemble Window
	win.add(tableView);
	
	
	//Methods
	var updateLabelDetail = _.throttle(function (e1){
		var getAlsoThumbNail = false;
		if (e1 && e1.getAlsoThumbNail){
			getAlsoThumbNail = e1.getAlsoThumbNail;
		};
		
		function callback (e){
			if (getAlsoThumbNail === true){
				thumbHeaderRow.update(e);
				Ti.App.fireEvent('updateLabelsList');
			};	
		};
		

		dataIn = DataLabels.loadLabelDetail(labelID,callback);
		
		centerTitle.label.text = dataIn.label.name;
		//tagMoreRow.update(centerTitle.text);
		thumbHeaderRow.thumbnailTypeLabel.text = dataIn.label.thumbnailType === 'normal'? '' : dataIn.label.thumbnailType;
		favoritesRow.update(dataIn.favorites.length);
		allPicturesRow.update(dataIn.allPictures.length);
	
		tableView.headerView = thumbHeaderRow;
		tableData = [];
		tableView.data = [];
		
		tableData.push(allPicturesRow);
		tableData.push(favoritesRow);
		tableData.push(tagMoreRow);
		
		tableView.data = tableData;
	},250);
	
	
	function preClean(){
		allPicturesRow.clean();
		favoritesRow.clean();
		tagMoreRow.clean();
		thumbHeaderRow.clean();
		//editButton.clean();
		//backButton.clean();
		//centerTitle.clean();
		/*
		editButton = null;
		backButton = null;
		centerTitle = null;
		
		allPicturesRow = null;
		togetherRow = null;
		favoritesRow = null;
		tagMoreRow = null;
		thumbHeaderRow = null;
		*/
		Ti.API.debug("Cleaned LabelDetail");
	};
	
	//Expose
	win.preClean = preClean;
	
	//Listeners
	Ti.App.addEventListener('updateLabelDetail', updateLabelDetail);
	
	win.addEventListener('close', function (){
		Ti.App.removeEventListener('updateLabelDetail', updateLabelDetail);
	});
	
	win.addEventListener('focus', function (){
		tableView.reset();
	});
	
	
	//Initialize
	updateLabelDetail({getAlsoThumbNail: true});
	
	return win;
};


var createRow = function (titleText,count){
	var Labels = require('ui/components/Labels');
	
	var row = Ti.UI.createTableViewRow({
		backgroundColor:'transparent',
		className: 'personDetailRow',
		height: 50,
		editable: false
		//leftImage: data.image
	});
	
	var groove = Ti.UI.createView({
		backgroundImage: 'images/groove_white.png',
		width: 320,
		height:2,
		top:row.height-2
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: titleText,
		color: globals.fontColors[2],
		font: {fontWeight: 'bold', fontSize: 17},
		minimumFontSize: 14,
		shadowColor: '#fff',
		shadowOffset:{x:1,y:1},
		width: 175,
		left: 75,
		height:20
	});
	
	var countLabel = Ti.UI.createLabel({
		text: '('+count+')',
		color: globals.fontColors[0],
		right: 15,
		textAlign: 'right',
		width: 50,
		height:20
	});
	
	var selectionOverlay = Ti.UI.createView({
		backgroundImage: 'images/row_selected.png',
		width:320,
		height:row.height,
		opacity: 0.02,
	});
	
	selectionOverlay.addEventListener('touchstart', function (e){
		if(row.enabled === true){
			selectionOverlay.opacity = 1;
			groove.visible = false;
		}
	});
	
	function update(newCount){
		countLabel.text = '('+newCount+')';
		if(newCount === 0){
			titleLabel.color = globals.fontColors[0];
			row.enabled = false;
		}
		else{
			titleLabel.color = globals.fontColors[2];
			row.enabled = true;
		};
	};
	
	function clean(){
		var children = row.children;
		for(var i = children.length-1 ; i >= 0;  i--){
			row.remove(children[i]);
			children[i] = null;
		};
		Ti.API.debug('Cleaned Row');
	};
	
	update(count);
	
	
	row.add(groove);
	row.add(titleLabel);
	row.add(countLabel);
	row.add(selectionOverlay);
	
	row.titleLabel = titleLabel;
	row.countLabel = countLabel;
	row.selectionOverlay = selectionOverlay;
	row.groove = groove;
	row.update = update;
	row.clean = clean;
	
	return row;
};


var labelEmptyThumb = function (){
	var view = Ti.UI.createView({
		backgroundImage: 'images/cover_empty_label.png',
		//top:2,
		width:80,
		height:80
	}).toImage();
	
	return view;
};