exports.loadWin = function (tagType,tagID){
	
	var tag;
	var titleText;
	var subTitleText;
	if(tagType === 'person'){
		var DataPersons = require('model/DataPersons');
		tag = DataPersons.loadPerson(tagID);
		titleText = (tag.firstName? tag.firstName: '')+" "+(tag.lastName? tag.lastName:'');
		subTitleText = I('Select_Photos')+" "+titleText
	}
	else if (tagType === 'label'){
		var DataLabels = require('model/DataLabels');
		tag = DataLabels.loadLabel(tagID);
		titleText = (tag.name);
		subTitleText = I('Select_Photos')+" "+titleText
	};
	
	var popBg = Ti.UI.createView({
		backgroundColor: 'black',
		borderColor: 'white',
		borderWidth:4,
		borderRadius:5,
		width: 320,
		height: Ti.Platform.displayCaps.platformHeight-40,
		//top: 20,
		//left: 10,
		opacity: .8,
		//zIndex:9999
	});
	
	var tabGroup = Ti.UI.createTabGroup({
		width: 320,
		height: Ti.Platform.displayCaps.platformHeight,
		backgroundImage: popBg.toImage(),
		transform: Ti.UI.create2DMatrix().scale(0),
		opacity: 0
	});
	
	tabGroup.addEventListener('close', function(e){
		
		var copyStack = windowStack;
		for (var i = copyStack.length-1; i >= 0; i--){
			tab.close(windowStack[i]);
			windowStack.pop();
			//Ti.API.debug(windowStack);
		};
		
		windowStack = null;
		copyStack = null;
		
		tabGroup.removeTab(tab);
		tab = win = null;
		
		var startTime = new Date().getTime();
		
		var children = tabGroup.children;
		if (children){
			for (var i =0; i < children.length; i++){
				tabGroup.remove(children[i]);
				children[i]=null;
			};
		};
		
		var endTime = new Date().getTime();
		Ti.API.info('Cleaned TabGroup ' + (endTime - startTime) + 'ms');
	});
	
	//Keet track of open windows to clean them.
	var windowStack = [];
	
	var win = loadGroupList(tagType, tagID, titleText, subTitleText, tabGroup, windowStack);
	
	var tab = Ti.UI.createTab({
		window: win
	});
	
	
	tabGroup.addTab(tab);
	
	globals.tabGroup.darken();
	
	tabGroup.open();
	tabGroup.animate(Ti.UI.createAnimation({
		duration: 150,
		transform: Ti.UI.create2DMatrix().scale(1),
		opacity:1
	}));
};


var loadGroupList = function (tagType, tagID, titleText,subTitleText, tabGroup, windowStack){
	var assetslibrary = require('ti.assetslibrary');
	
	var win = createPopUpWindow(titleText, subTitleText, tabGroup, 'closeButton', null, tagType);
	
	//TableView
	var tableData = [];
	
	var tableView = Ti.UI.createTableView({
		backgroundColor: 'transparent',
		width: 312,
		height: Ti.Platform.displayCaps.platformHeight-51,
		top:46
	});
	tableView.clickable = true; 
	
	function reset(){
		if(tableView.rowSelected){
			tableView.rowSelected.backgroundColor = 'transparent';
			tableView.rowSelected = null;
		};
	};
	
	
	var tableViewClick = function(e){
		e.row.backgroundColor = 'blue';
		tableView.rowSelected = e.row;
		
		if (e.rowData.groupType != assetslibrary.AssetsGroupTypeSavedPhotos && e.rowData.groupType != assetslibrary.AssetsGroupTypeLibrary && e.rowData.groupType != assetslibrary.AssetsGroupTypePhotoStream ){
			var groupWin = loadGroupWindow(e.rowData.groupType, titleText,subTitleText,e.rowData.groupName,tagType,tagID,tabGroup);
			tabGroup.tabs[0].open(groupWin, {animated: true});
			windowStack.push(groupWin);
		}
		else if (e.rowData.groupType === assetslibrary.AssetsGroupTypeSavedPhotos || e.rowData.groupType === assetslibrary.AssetsGroupTypeLibrary || e.rowData.groupType === assetslibrary.AssetsGroupTypePhotoStream){
			var cameraSuccessCb = function(e) {
				var assetsWin = loadPicturePicker(subTitleText,tabGroup,e.groups[0],tagType,tagID);
				tabGroup.tabs[0].open(assetsWin, {animated: true});
				windowStack.push(assetsWin);
			};
			
			var cameraErrorCb = function(e) {
				Ti.API.error('error: ' + e.error);
			};
			
			var groups = assetslibrary.getGroups([e.rowData.groupType], cameraSuccessCb, cameraErrorCb);	
		};
	};
	
	
	tableView.addEventListener('click', function (e){
		if (tableView.clickable === true){
			tableViewClick(e);
			tableView.clickable = false; 
			var clickTimeout = setTimeout(function(){
				tableView.clickable = true; 
			},
			2000);
		};
	});
	

	var createRow = function (groupName,groupType,groupPicture,count){
		var row = Ti.UI.createTableViewRow({
			backgroundColor: 'transparent',
			height: 50,
			groupType: groupType,
			groupName: groupName,
			numberOfAssets: count,
			editable:false
		});
		
		var img = Ti.UI.createImageView({
			left: 5,
			height: 40,
			width: 40,
			image: groupPicture,
			//borderRadius: 5,
			hires: true
		});
		
		var title = Ti.UI.createLabel({
			color: 'white',
			font: {fontSize: 17,fontWeight: 'bold'},
			height: 20,
			width: 300-60-30,
			left: 60,
			textAlign: 'left',
			text: groupName
		});
		
		var number = Ti.UI.createLabel({
			color: 'white',
			font: {fontSize: 15,fontWeight: 'regular'},
			height:20,
			width: 45,
			right: 10,
			textAlign: 'right',
			text: '('+count+')'
		});
		
		row.add(title);
		row.add(img);
		row.add(number);
		
		return row;
	};
	
	
	var successCb = function(e) {
		if (e.groups[0]){
			var groupName;
			var count; 
			if (e.groups[0].type === assetslibrary.AssetsGroupTypeFaces){
				groupName = I('Faces');
				count = e.groups.length
			}
			else if (e.groups[0].type === assetslibrary.AssetsGroupTypeAlbum){
				groupName = I('Albums');
				count = e.groups.length;
			}
			else if (e.groups[0].type === assetslibrary.AssetsGroupTypeEvent){
				groupName = I('Events');
				count = e.groups.length;
			}
			else {
				groupName = e.groups[0].name
				count = e.groups[0].numberOfAssets;
			};
			
			if(count > 0){
				tableData.push(createRow(groupName,e.groups[0].type,e.groups[0].posterImage,count));
				tableView.setData(tableData);
			};
		};
	};
	
	var errorCb = function(e) {
		Ti.API.error('error: ' + e.error);
	};
	
	//Checking what groups are present and making a row for each. 
	//cameraRoll
	assetslibrary.getGroups([assetslibrary.AssetsGroupTypeSavedPhotos], successCb, errorCb);
	//photoLibrary
	assetslibrary.getGroups([assetslibrary.AssetsGroupTypeLibrary], successCb, errorCb);
	//photoStream
	assetslibrary.getGroups([assetslibrary.AssetsGroupTypePhotoStream], successCb, errorCb);
	//faces
	assetslibrary.getGroups([assetslibrary.AssetsGroupTypeFaces], successCb, errorCb);
	//albums
	assetslibrary.getGroups([assetslibrary.AssetsGroupTypeAlbum], successCb, errorCb);
	//events
	assetslibrary.getGroups([assetslibrary.AssetsGroupTypeEvent], successCb, errorCb);
	
	
	win.add(tableView);
	
	win.addEventListener('focus', function (){
		reset();
	});
	
	function preClean (){
		for(var i = 0; i < tableView.data[0].rows.length; i++){
			var row = tableView.data[0].rows[i];
			var children = row.children;
			for (var j = 0; j < children.length; j++){
				row.remove(children[j]);
				children[j]= null;
			};
		};
		
		tableView.data = null;
	};
	
	win.preClean = preClean;
	
	
	return win; 
};


var loadGroupWindow = function (groupType, titleText, subTitleText,groupName,tagType,tagID, tabGroup, windowStack){
	var assetslibrary = require('ti.assetslibrary');
	Ti.API.info("module is => " + assetslibrary);
	
	//Create Window
	var win = createPopUpWindow(groupName, subTitleText, tabGroup,'backButton', null,tagType);
	
	
	//TableView
	var tableView = Ti.UI.createTableView({
		backgroundColor: 'transparent',
		width: 312,
		height: Ti.Platform.displayCaps.platformHeight-51,
		top:46
	});
	tableView.clickable = true;
	
	function reset(){
		if(tableView.rowSelected){
			tableView.rowSelected.backgroundColor = 'transparent';
			tableView.rowSelected = null;
		};
	};
	
	
	//Assemble
	win.add(tableView);
	
	
	
	//Load Data;
	var successCb = function(e) {
		var groups = e.groups.reverse();
		
		var createRow = function(group) {
			//Ti.API.info('Group: ' + group.name + ' n assets: ' + group.numberOfAssets);
			var row = Ti.UI.createTableViewRow({height: 50});
			
			var img = Ti.UI.createImageView({
				left: 5,
				height: 40,
				width: 40,
				image: group.posterImage,
				//borderRadius: 5,
				hires: true
			});
		
			var title = Ti.UI.createLabel({
				color: 'white',
				font: {fontSize: 17,fontWeight: 'bold'},
				height: 20,
				width: 300-60-30,
				left: 60,
				textAlign: 'left',
				text: group.name
			});
			
			
			var number = Ti.UI.createLabel({
				color: 'white',
				font: {fontSize: 15,fontWeight: 'regular'},
				height:20,
				width: 45,
				right: 10,
				textAlign: 'right',
				text: '('+group.numberOfAssets+')'
			});
		
			row.add(img);
			row.add(title);
			row.add(number);
			
			return row;
		};
		
		var tableData = [];
		
		//Sort Faces alphabetically
		if(groupType === assetslibrary.AssetsGroupTypeFaces){
			 function sortAZ(ob1,ob2) {
		    	var n1 = ob1.name.toLowerCase()
		    	var n2 = ob2.name.toLowerCase()
		    	if (n1 > n2) {return 1}
				else if (n1 < n2){return -1}
			    else { return 0}//nothing to split
			};
			
			groups.sort(sortAZ);
		};
		
		for (var i = 0; i < groups.length; i++){	
			var row = createRow(groups[i]);
			tableData.push(row);
		};
		
		tableView.data = tableData;
		
		var tableViewClick = function (e){
			var assetsWin = loadPicturePicker(subTitleText,tabGroup,groups[e.index],tagType,tagID);
			tabGroup.tabs[0].open(assetsWin, {animated: true});
			windowStack.push(assetsWin);
		};
		
		tableView.addEventListener('click', function(e) {
			if (tableView.clickable === true){
				tableViewClick(e);
				tableView.clickable = false; 
				var clickTimeout = setTimeout(function(){
					tableView.clickable = true; 
				},
				1000);
			};
		});
	};
	
	var errorCb = function(e) {
		Ti.API.error('error: ' + e.error);
	};
	
	//Get data to create the rows. 
	var groups = assetslibrary.getGroups([groupType], successCb, errorCb);	
	
	win.addEventListener('focus', function (){
		reset();
	});
	
	function preClean (){
		//scrollView.clean();
		//win.remove(scrollView);
		//scrollView = null;
	};

	return win; 
};




var loadPicturePicker = function (subTitleText,tabGroup,group,tagType,tagID){
	var DataMedia = require('/model/DataMedia');
	var DataPerson = require('model/DataPersons');
	var PicturesScrollView = require('ui/components/PicturesScrollView');
	var MiscComponents = require('ui/components/MiscComponents');
	
	//Data Placeholder
	var numberOfAssets = group.numberOfAssets;
	
	
	//Create Window
	var win =  createPopUpWindow(group.name, subTitleText,tabGroup, 'backButton', null,tagType);
	win.selectionMode = 'multiple';

	
	var scrollView = new PicturesScrollView(numberOfAssets,MiscComponents.createStandardThumbnail,win,true);
	scrollView.top = 40;
	scrollView.height = Ti.Platform.displayCaps.platformHeight-38;
	scrollView.transform = Ti.UI.create2DMatrix().scale(310/320);
	
	scrollView.addEventListener('loadMoreData', function(e){
		DataMedia.getGroupAssetsQuickTag(group,scrollView.loadData,e.startIndex,e.endIndex,tagType,tagID);
	});
	
	scrollView.addEventListener('click', function(e){
		//*******WARNING********//
		//This event happens after the touch start so the selected state has already happened.
		var view = e.source;
		if(view.selected === false){	
			//Mark in Data for the person
			DataMedia.removeTagFromMedia(view.URL, tagType, tagID, false);
		}
		else if (view.selected === true){
			if(tagType === 'person'){
				//Needs an addressBook input
				var newTagID = DataPerson.loadPerson(tagID).addressBookID;
				DataMedia.addTagToMediaFromLibraryPicker(view,tagType,newTagID)
			}
			else if (tagType === 'label'){
				DataMedia.addTagToMediaFromLibraryPicker(view, tagType, tagID);
			};
		};
	});	
	
	
	
	//Callback for the loading data function
	var update = function (data){
		scrollView.refreshScrollView(data);
		win.add(scrollView);
	};
		
	DataMedia.getGroupAssetsQuickTag(group,update,0,59,tagType,tagID);
	
	
	function preClean (){
		scrollView.clean();
		win.remove(scrollView);
		scrollView = null;
	};
	
	
	return win;
};


var createPopUpWindow = function (title, subTitle, tabGroup, leftButton, rightButton,tagType){
	//Create Window
	var win = Ti.UI.createWindow({
		tabBarHidden: true,
		navBarHidden:true,
		width:320,
		height: Ti.Platform.displayCaps.platformHeight-40,
		top: 0
	});
	
	var titleLabel = Ti.UI.createLabel({
		width: 230,
		height:20,
		minimumFontSize:10,
		textAlign:'center',
		color: 'white',
		font: {fontSize: 17, fontWeight: 'bold'},
		top: 7,
		text: title
	});
	
	
	var subTitleLabel = Ti.UI.createLabel({
		color: 'white',
		font:  {fontSize: 13, fontWeight: 'bold'},
		top: 25,
		width:200,
		height:20,
		minimumFontSize:12,
		textAlign: 'center',
		text: subTitle
	});
	
	
	var closeButton = Titanium.UI.createView({
		backgroundColor: 'transparent',
		borderRadius:5,
		borderColor: 'white',
		style:Titanium.UI.iPhone.SystemButtonStyle.PLAIN,
		width: Ti.UI.SIZE,
		height: 28,
		top: 12,
		right:10
	});
	
	
	closeButton.add(Titanium.UI.createLabel({
		//backgroundImage:'images/nav_x.png',
		text: I('Done'),
		width: Ti.UI.SIZE,
		font:{fontSize:13, fontWeight: 'bold'},
		color:'white',
		//width:21,
		//height:20,
		//borderColor:'white',
		//borderRadius: 5,
		//top:15,
		left:7,
		right: 7
	}));

	
	closeButton.addEventListener('click', function (){
		if (tagType === 'person'){
			Ti.App.fireEvent('updatePersonDetail', {getAlsoThumbNail: true});
			Ti.App.fireEvent('updatePersonsList');
		}
		else if (tagType === 'label'){
			Ti.App.fireEvent('updateLabelDetail',  {getAlsoThumbNail: true});	
			Ti.App.fireEvent('updateLabelsList');
		};
		
		Ti.App.fireEvent('updatePicturesList');
		
		tabGroup.animate(Ti.UI.createAnimation({
			duration: 250,
			transform: Ti.UI.create2DMatrix().scale(0),
			opacity:0
		}));
	
		
		globals.tabGroup.lighten();
		
		setTimeout(function (){
			tabGroup.close();
			
			if (globals.tutorialModeOn === true){
				var PopUp = require('ui/components/PopUp');
				var instructions = PopUp.instructionsPopUp(I("Finished"),I("Good_job"));	
				instructions.open();
				globals.tutorialModeOn = false;
			}
		}, 300);
		
	});
	
	
	
	backButton = Titanium.UI.createView({
		//backgroundImage:'images/nav_x.png',
		width:50,
		height:50,
		top: 0,
		left:0
	});
	
	backButton.add(Titanium.UI.createButton({
		backgroundImage:'images/nav_back.png',
		width:24,
		height:21,
		//top:15,
		//left:15
	}));
	
	backButton.addEventListener('click', function (){
		tabGroup.tabs[0].close(win);
	});

	var groove = popGroove(44);
	
	win.add(titleLabel);
	win.add(subTitleLabel);
	win.add(groove);
	
	if (leftButton === 'backButton'){
		win.add(backButton);
	};
	
	win.add(closeButton);
	
	
	function generalClean (item){
		var children = item.children;
		if (children){
			for (var i = children.length-1; i >= 0 ; i--){
				item.remove(children[i]);
				children[i] = null;
			};
		};
	};
	
	
	function clean (){
		if(win.preClean){win.preClean()};
		
		generalClean(groove);
		generalClean(closeButton);
		generalClean(backButton);
		generalClean(win);
	
		Ti.API.debug('Cleaned Window');
	};
	
	
	win.addEventListener('close', function(){
		clean();
	});
	
	
	
	win.clean = clean;
	win.closeButton = closeButton; 
	
	
	return win;
};



var popGroove = function (top){
	var container = Ti.UI.createView({width:320-9,height:2, top:top,backgroundColor:'transparent',opacity:.6});
	
	var top = Ti.UI.createView({width:320-9,height:1, top:0,backgroundColor:'black'});
	
	var bottom = Ti.UI.createView({width:320-9,height:1,top:1, backgroundColor:'white'});
	
	container.add(top);
	container.add(bottom);
	
	return container;
};
