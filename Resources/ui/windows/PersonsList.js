exports.loadWin = function (){
	var Window = require('ui/components/Window');
	var TopNavigation = require('ui/components/TopNavigation');
	var PicturesList = require('ui/windows/PicturesList');
	var NavigationController = require('ui/NavigationController');
	var DataPersons = require('model/DataPersons');
	var MiscComponents = require('ui/components/MiscComponents');
	
	//Load Data
	var dataIn;

	//Create Window
	var win = Window.createWindow({
		tabBarHidden: true,
		tabbedBarHiddend:true,
		kind: 'people'
	});
	
	
	
	var menuButton = TopNavigation.menuButton();

	var addButton = TopNavigation.addButtonFull();
	
	function addContact (e){
		var ContactPicker = require ('ui/windows/ContactPicker');
		var win1 = ContactPicker.loadWin(e && e.pickingYourself? e.pickingYourself : false);
		win1.open();
	};
	
	function addContact1 (){
		//Open Contact  Picker
		Titanium.Contacts.showContacts({
			selectedPerson: function(e){
				// *** ADD FILTER FOR CONTACTS KIND TO ALLOW ONLY PERSON.	
				var person = Ti.Contacts.getPersonByID(e.person.recordId);
	
				var info = {};
				info.firstName = person.firstName;
				info.lastName = person.lastName;
				info.addressBookID = person.recordId	
				if(globals.properties.hasBeenSetup === false){	
					info.isMyself = true;
				};
				
				//Check to see if the person already exists. 
				if(!DataPersons.loadPersonForAddressBookID(info.addressBookID)){
					//Match with faces Callback. 
					var addPersonCB = function (e){
						if(e){
							//var PopUp = require('ui/components/PopUp');
							//PopUp.facesMatch(e.name, newPersonID, e.group);
						};
					};
					
					//Fire this before adding person. otherwise it will be setup already
					if(globals.properties.hasBeenSetup === false){
						Ti.App.fireEvent('pickedYourself');
					};
					
					var newPersonID = DataPersons.addPerson(info).$id;
					
					
					//var win1 = PicturesList.loadWin('person',personID);
					var PersonDetail = require('ui/windows/PersonDetail');
					globals.currentStack.open(PersonDetail.loadWin(newPersonID),{animated:true});
					
					
					refreshData();
				}
				else{
					alert("This person is already in your list");
				};
			},
			cancel: function(){
				//alert('Ciao')
				//globals.tabGroup.navBar.update(win);
			}
		});	
	};
	
	addButton.addEventListener('click', function (){
		addContact();
	});
	
	
	var centerTitle = TopNavigation.centerSingleLabel(I('People'),'white');//TopNavigation.centerDoubleLabel('People', '', 'white');
	
	win.leftNavButton = menuButton;
	win.rightNavButton = addButton;
	win.centerTitle = centerTitle;
	win.titleControl = centerTitle;
	
	

	//HintText
	var hintText = Ti.UI.createLabel({
		text: I('Swipe_to_delete_a_person'),
		width: 300,
		height: Ti.UI.SIZE,
		bottom: 10,
		font: {fontSize: 14},
		textAlign: 'center',
		color: globals.fontColors[2],
		shadowColor: 'white',
		shadowOffset: {x:0,y:1},
		visible: false
	});	
	
	var emptyListLabel = Ti.UI.createLabel({
		text: I('There_are_no_people_in_this_list'),
		width: 300,
		height: Ti.UI.SIZE,
		top: 55,
		font: {fontSize: 14},
		textAlign: 'center',
		color: globals.fontColors[2],
		shadowColor: 'white',
		shadowOffset: {x:0,y:1},
		//visible: dataIn.length > 0 ? false : true
	});	
	
	
	//Create Tableview
	var tableView = MiscComponents.standardTableView();
	tableView.clickable = true;
	
	var tableViewClick = function (e){
		var personID = e.rowData.data.$id;
	
		Ti.App.addEventListener('thumbHeaderRowLoaded', openPersonDetail);
		var PersonDetail = require('ui/windows/PersonDetail');
		var win1 = PersonDetail.loadWin(e.rowData.data.$id);
		
		function openPersonDetail (){
			globals.currentStack.open(win1,{animated:true});
			Ti.App.removeEventListener('thumbHeaderRowLoaded', openPersonDetail);
		};
	};
	
	
	tableView.addEventListener('touchend', function (e){
		if (tableView.clickable === true){
			tableViewClick(e);
			tableView.clickable = false; 
			var clickTimeout = setTimeout(function(){
				tableView.clickable = true; 
			},
			1500);
		};
	});
	
	
	tableView.addEventListener('delete', function (e){
		var PopUp = require('ui/components/PopUp');
		PopUp.deletingPopUp(e.row.name);
		
		//Delay to let the animation start
		setTimeout(function (){
			DataPersons.deletePerson(e.rowData.data.$id);
		},150);
	});
	
	var refreshData = _.throttle(function (){
		Ti.API.debug("REFRESHING PERSON LIST")
		dataIn = DataPersons.loadPersonsList();

		tableData = [];
		
		//Right Index Variables
		var sheader = '';
		var rheader;
		var tableIndex = [];
		
		for (var i = 0; i < dataIn.length; i++ ){
			var data = dataIn[i];
			var title1;
			if (data.firstName && !data.lastName){
				title1 = data.firstName;
			}
			else if (data.lastName && !data.firstName){
				title1 = data.lastName;
			}
			else{
				title1 = data.firstName+' '+data.lastName;
			};
			
			//Right Index Work
			if(dataIn.length > 30){
				var letter = title1[0].toUpperCase();  
		      	if(letter != sheader){
		       		sheader = letter;
		            rheader = sheader;
		         	tableIndex.push({title: sheader, index:i});
		       }
		       else{
		       		rheader = null;
		       };
			};
			
			
			
			//NOTE have to do it this way not to pollute the DB with image. 
			var row; 
		 	if(data.thumbnail){ 
				row = MiscComponents.standarRowWithImage(title1,data.media.length,data.thumbnail,rheader,true);
			}
			else{
				row = MiscComponents.standarRowWithImage(title1,data.media.length,personEmptyThumb(),rheader,true);
			}
		 	
         	row.data = data;
         	tableData.push(row);
		};
		
		tableView.setData(tableData);
		
		if (dataIn.length > 30){
			tableView.index = tableIndex;
		};
		
		if (dataIn.length > 8){
			hintText.visible = false;
		}
		else{
			hintText.visible = true;
		};
		
		emptyListLabel.visible = dataIn.length > 0 ? false : true;
	},500);
	
	refreshData();
	
	//First Start Opening
	var pickedYourself = function (){
		var PopUp = require('ui/components/PopUp');
		var titleText = I('Finished');
		var subTitleText = '\n'+I('all_set');
		var instructions = PopUp.instructionsPopUp(titleText,subTitleText);
		instructions.open();
		
		win.tabbedBarHidden = false;
	};
	
	
	if (globals.properties.hasBeenSetup === false){
		win.tabbedBarHidden = true;
	};
	
	
	
	
	//Assemble Window
	win.add(emptyListLabel);
	win.add(hintText);
	win.add(tableView);
	
	
	//Methods
	function preClean (){
	
	};
	
	//Expose
	globals.addContact = addContact;
	win.preClean = preClean;
	
	
	//Listener
	Ti.App.addEventListener('updatePersonsList', refreshData);
	//Ti.App.addEventListener('pickedYourself', pickedYourself);
	Ti.App.addEventListener('addContact', addContact);
	
	win.addEventListener('close', function (){
		Ti.App.removeEventListener('updatePersonsList', refreshData);
		//Ti.App.removeEventListener('pickedYourself', pickedYourself);
		Ti.App.removeEventListener('addContact', addContact);
	});
	
	win.addEventListener('focus', function (){
		tableView.reset();
		//Tutorial 
		//if (dataIn.length === 0  && globals.isLoading != true){
		if (dataIn.length === 0){
			globals.tutorialModeOn = true;
		};	
			
		if (globals.tutorialModeOn === true){	
			setTimeout(function (){
				/*
				var PopUp = require('ui/components/PopUp');
				var titleText = I('People');
				var subTitleText = I('Tag_a_person_in_any_photo');
				var instructions = PopUp.instructionsPopUp(titleText,subTitleText);	
				instructions.open();
				*/
				var PopUp = require('ui/components/PopUp');
				var instructions = PopUp.instructionsPopUp( I('Tutorial'),I("Let's_start_by"));	
				instructions.open();
			},500);
		}
	});
	
	return win;
};


var personEmptyThumb = function (){
	var view = Ti.UI.createView({
		backgroundImage: 'images/cover_empty.png',
		//top:2,
		width:80,
		height:80
	}).toImage();
	
	return view;
};