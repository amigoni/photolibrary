//Creates the ScrollView wiht pages element. Optimized to use only two items at the time. 

var _ = require('lib/underscore');

module.exports = function pageScrollView(assetsCount,currentIndex,win,dictionary){
	//Notes: assetsCount is given in case the whole array has not loaded yet.
	//win is necessary as a reference for the the functions outside the scrollView. Mainly to indentify
	//...the current image and the current scrollview that holds the image
	//dictionary holds the imagesData parameters
	
	var visibleViews = []; //Array to hold  visible views;
	var recycledPages = []; //Array to hold Views to Recycle; 
	
	/*
	//Implementation with Scrollable View.
	//Would be great but can't seem to unload views so the memory runs out.
	
	var viewWithImage = require('ui/windows/galleryWindow/viewWithImage');
	var views =[];	
		
	for (var i = currentIndex ; i < currentIndex +3; i++){
		var view = new viewWithImage(dictionary.imagesData[i],i,dictionary,win);
		views.push(view);
	};
	
	
	var scrollView = Ti.UI.createScrollableView({
		//top : 0,
		views : views,
		showPagingControl : false,
		maxZoomScale : 2.0,
		minZoomScale : 1.0,
		//backgroundColor: 'white',
		width: 340,
		height: 480,
		currentPage: 3,
		cacheSize:5
	});
	
	win.scrollableGalleryView = scrollView;
	
	scrollView.addEventListener('scroll', function(e){
		var i = e.currentPage+3
		
		if (!views[i] && scrollView.currentPage < assetsCount-3 ){
			var view = new viewWithImage(dictionary.imagesData[i],i,dictionary,win);//recycledPages[0];
			//scrollView.removeView(e.currentPage-1);
			scrollView.addView(view);
			
			//win.scrollableGalleryView.views = views;
		};		
		Ti.API.debug(views.length)
	});
	
	function checkViewsVisibilityScrollable (e){
		
		var firstNeededIndexS = e.currentPage-2;
		var lastNeededIndexS = e.currentPage+2;
		//firstNeededIndexS = firstNeededIndexS >= 0 ? firstNeededIndexS : 0;
		
		Ti.API.debug(firstNeededIndexS+' indexes: '+lastNeededIndexS+' views'+views.length);
		
		//Find if there is that view in the array. 
		var i;
		if(lastNeededIndexS > views.length-1){
			i = lastNeededIndexS-3+currentIndex;
			if (i < assetsCount){
				var view = new viewWithImage(dictionary.imagesData[i],i,dictionary,win);//recycledPages[0];
				views.push(view);
				win.scrollableGalleryView.views = views;
			}
		}	
		else if (firstNeededIndexS < currentIndex-2){
			i = firstNeededIndexS + currentIndex-2;
			alert(i)
			if (i >= 0){
				var view = new viewWithImage(dictionary.imagesData[i],i,dictionary,win);//recycledPages[0];
				views.splice(0,0,view);
				win.scrollableGalleryView.views = views;
			}
		};	
		
		//Ti.API.debug('Recycled Views: '+recycledPages.length+' Views: '+views.length);
	};
	*/
	
	var scrollView = Ti.UI.createScrollView({
		//backgroundColor:'white',
		width:340,
		height:480,
		contentWidth: 340*assetsCount,
		contentHeight: 480,
		pagingEnabled: true,
		scrollType: 'horizontal',
		zoomScale:1,
		maxZoomScale:1,
		minZoomScale:1,
		anchorPoint: {x:0.5,y:0.5},
		hasBeenLayedOut: false
	});
	
	win.scrollableGalleryView = scrollView;
	
	scrollView.addEventListener('postlayout', function (){
		//Used after the first layout to determine the firtst currentView. 
		if(scrollView.hasBeenLayedOut === false){
			checkViewsVisibility({x:currentIndex*scrollView.width});
			setCurrentView();
			scrollView.hasBeenLayedOut = true;
		};
	});
	
	scrollView.setContentOffset({x: currentIndex*(scrollView.width),y:0},{animated:false});	
	

	//Function that does array difference.
	function ArrayDiff(a,b) {
	    return b.filter(function(i) {return !(a.indexOf(i) > -1);});
	};
	
	function checkViewsVisibility (e){
		Ti.API.info('CHECK VIEWS VISIBILITY');
		//Avoid scrolling outside of boundaries	
		if (e.x < 0){ e.x = 0};
		if (e.x > assetsCount * scrollView.width-scrollView.width){e.x = assetsCount *scrollView.width-scrollView.width}
		
		//Find the indexes to be displayed
		var firstNeededIndex = Math.floor((e.x)/scrollView.width) - 1;
		var lastNeededIndex = firstNeededIndex + 3; // Math.floor((e.x+scrollView.width-1)/scrollView.width);
		Ti.API.debug('FirstNeededIndex: '+firstNeededIndex+' LastNeededIndex: '+lastNeededIndex);
		firstNeededIndex = firstNeededIndex > 0 ? firstNeededIndex : 0;
		lastNeededIndex = lastNeededIndex < (assetsCount-1) ? lastNeededIndex : assetsCount-1;
		
		Ti.API.debug('FirstNeededIndex: '+firstNeededIndex+' LastNeededIndex: '+lastNeededIndex);
		
		//Recycle Pages
		var visibleViewsCount = visibleViews.length;
		for (var i = visibleViewsCount-1; i >=0; i--){
			visibleViews[i].image.clearBubbles();
			if (visibleViews[i].index < firstNeededIndex || visibleViews[i].index > lastNeededIndex){
				Ti.API.debug('removing view index: ' + i);
				visibleViews[i].visible = true;
				recycledPages.push(visibleViews[i]);
				scrollView.remove(visibleViews[i]);
			};
		};
		
		visibleViews = ArrayDiff(recycledPages,visibleViews);
		Ti.API.info('visible views: ' + JSON.stringify(_.map(visibleViews, function(v) {
			return v.index;
		})));
		
		//Add Missing Pages
		for (var i = firstNeededIndex; i <= lastNeededIndex; i++){
			if (!isDisplayingPageForIndex(i)){
				var view;
				var recycled = recycledPages.shift();
				if(recycled){
					view = recycled;
					view.configureView(i);
				}
				else{
					var viewWithImage = require('ui/windows/galleryWindow/viewWithImage');
					view = new viewWithImage(dictionary.imagesData[i],i,dictionary,win);
				};
				
				scrollView.add(view);
				visibleViews.push(view);
			};
		};
		Ti.API.info('visible views now: ' + JSON.stringify(_.map(visibleViews, function(v) {
			return v.index;
		})));
		
		//Ti.API.debug('Recycled Views: '+recycledPages.length+' Visible Views: '+visibleViews.length);
	};
	
	function isDisplayingPageForIndex (index){
		var isDisplaying = false; 
		
		for (var i =0; i < visibleViews.length; i ++){
			if (visibleViews[i].index === index){
				isDisplaying = true;
				break
			};
		};
		return isDisplaying;
	};


	var scrollStartX = 0;


	scrollView.addEventListener('scroll', function(e){
		if (e.source != scrollView) {
			return;
		}

		var delta = Math.sqrt(Math.pow(e.x - scrollStartX, 2));

		if (delta >= 340) {
			//Ti.API.info('watch out! delta=' + delta);
			scrollView.beganScrolling = false;
		}

		//Ti.API.debug("scroll outer: " + e.x);
		//Ti.API.info('contentOffset: ' + scrollView.contentOffset.x);
		if (scrollView.beganScrolling === false){
			scrollStartX = e.x;
			checkViewsVisibility(e);
		}
		//alert('scroll')
		//checkViewsVisibilityScrollable(e)
		scrollView.beganScrolling = true;
	});
	
	
	scrollView.addEventListener('scrollEnd', function(e){
		scrollView.beganScrolling = false;
		
		//Find the current View;
		setCurrentView();
		
		
		// Update Title 
		win.centerTitle.update(dictionary.winTitle, dictionary.winSubtitle+' ( '+(win.currentImageScrollView.index+1)+' of '+assetsCount+' )');
		
		Ti.API.debug('Recycled Views: '+recycledPages.length+' Visible Views: '+visibleViews.length);
	});
	
	
	//Rotates each image
	function rotatePages(orientation){
		for (var i =0; i < visibleViews.length; i++){
			if(orientation === 'landscape'){
				visibleViews[i].rotateImage(480,320);	
			}
			else{
				visibleViews[i].rotateImage(320,480);
			}
		};
		var width = orientation === 'landscape'? 500 : 340;
		
		
		for (var i =0; i < recycledPages.length; i++){
			if(orientation === 'landscape'){
				recycledPages[i].rotateImage(480,320);	
			}
			else{
				recycledPages[i].rotateImage(320,480);
			}
		};
	};
	
	var currentOrientation = getCurrentOrientation();
	var lastOrientation = currentOrientation;
		
	//Handles rotation of the view. 
	function orientationChange (e){
		currentOrientation = e? e.orientation: currentOrientation;
		//Exclude undefined, faceup and facedown orientations.
		if (currentOrientation != 5 && currentOrientation != 6 && currentOrientation != 0 && currentOrientation != lastOrientation ){
			Ti.API.debug(currentOrientation+'Current Orientation');
			lastOrientation = currentOrientation;
				
			if(currentOrientation === 3 || currentOrientation === 4){
				//rotatePages('landscape');
				if(win.selectionView){win.selectionView.orientationClosing()};
				win.currentImageScrollView.rotateImage(480,320);
				scrollView.width = 500;
				//scrollView.rect.width = 500;
				scrollView.height= 320;
				scrollView.contentHeight = 320;
				scrollView.contentWidth = assetsCount*500;
				scrollView.setContentOffset({x: (win.currentImageScrollView.index)*500,y:0},{animated:false});
				
				globals.currentTabGroup.navBar.rotate('landscape');
			}
			else if (currentOrientation === 1 || currentOrientation === 2){ 
				//rotatePages('portrait');
				if(win.selectionView){win.selectionView.orientationClosing()};
				win.currentImageScrollView.rotateImage(320,480);
				scrollView.width = 340;
				//scrollView.rect.width = 340;
				scrollView.height= 480;
				scrollView.contentHeight = 480;
				scrollView.contentWidth = assetsCount*340;
				scrollView.setContentOffset({x: (win.currentImageScrollView.index)*340,y:0},{animated:false});
				globals.currentTabGroup.navBar.rotate('portrait');
			};
		};		
	};
	
	
	function setCurrentView (){
		for(var i =0; i < visibleViews.length; i++){
			if(visibleViews[i].left-10 === scrollView.contentOffset.x){
				win.currentImageScrollView = visibleViews[i];
				win.currentImage = win.currentImageScrollView.image;
				win.currentImage.resetImage(true,true);
				Ti.API.debug(win.currentImageScrollView.index+' Current Index');
			};
		};
	};
	
	function clean(){
		var children = scrollView.children;
		for(var i =0; i < children.length; i++){
			children[i].clean();
			scrollView.remove(children[i]);
			children[i] = null;
		};
	};
	
	scrollView.orientationChange = orientationChange;
	scrollView.clean = clean;

	return scrollView;
};


//Functions that gets initial Orientation
function getCurrentOrientation (){
	var screenWidth = Ti.Platform.displayCaps.platformWidth;
	var screenHeight = Ti.Platform.displayCaps.platformHeight;
	var orientation; 
	if (screenWidth > screenHeight) {
	    orientation = 3;
	} else {
	    orientation = 1;    
	};
	
	return orientation;
};