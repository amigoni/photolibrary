exports.loadWin = function(xPosition,yPosition,xGlobal,yGlobal,returnCallBack,selectCallBack,win,parentImage){
	
	var view = Ti.UI.createView({
		width: 220,
		height: 46,//win.screenWidth === 320 ? 186: 140,
		left: xGlobal,
		top: yGlobal ,
		visible:false,
		transform:Titanium.UI.create2DMatrix().scale(0)
	}); 
	
	//Adjust the horizontal position to stay inside the screen
	if(xGlobal < (.5 * view.width+5)){
		view.left = 5;
	}
	else if (xGlobal > win.screenWidth-(view.width + 5)){
		view.left = win.screenWidth -(view.width + 5);
	}
	else {
		view.left = xGlobal - 10;
	};
	
	
	
	var bg = Ti.UI.createView({
		backgroundColor: 'black',
		width: view.width,
		height: view.height-10,
		left: 0,
		top: 8,
		opacity:.6,
		borderRadius: 5,
		borderColor:'white'
	});
	
	
	var textField = Ti.UI.createTextField({
		backgroundColor: 'transparent',
		hintText: 'Who is this?',
		width: view.width-10,
		height: 35,
		top:8,
		left:8,
		color: 'white',
		appearance:Titanium.UI.KEYBOARD_APPEARANCE_ALERT,
		autocorrect: false
	});
	
	textField.addEventListener('change', function(e){
		populateTable(e);
	});
	
	//Could move this inside the element. 
	textField.addEventListener('return', function(e){
		win.closeAnimation();
		//globals.tabGroup.navBar.appear();
	});
	
	
	
	var whiteLine = Ti.UI.createView({
		backgroundColor: 'white',
		width: 219,
		height: 1,
		top: 8 + 35,
		visible: false
	});
	
	
	
	//TableView
	var createTableViewRow = function (rowDataIn){
		var row = Ti.UI.createTableViewRow({
			height: win.screenWidth === 320 ? 35 : 30,
			title: rowDataIn.name,
			font: {fontSize:18, fontWeight: 'bold'},
			color: 'white',
			data: rowDataIn,
			className: 'personTable',
			editable:false,
		});
		
		return row;
	};
	
	
	var tableView = Ti.UI.createTableView({
		backgroundColor: 'transparent',
		width: view.width,
		height: bg.height - textField.height-2,
		top:textField.height+8,
		left: 0
	});
	
	
	//Could move this inside the element. 
	tableView.addEventListener('click', function(e){
		if (e.row.kind != 'addNewContact'){
			tagWithContact(e.rowData.data);
		}
		else{
			var addContactForm = require('ui/windows/addContactForm');
			var win1 = addContactForm.loadWin(textField.value);
			
			win1.addEventListener('addedNewContact', function(e2){
				tagWithContact(e2.value);
			});
			
			win1.open();
		};
	});	
			
	var topArrow = Ti.UI.createView({
		backgroundImage:'images/top_arrow.png',
		width: 17,
		height: 9,
		left: xGlobal-view.left-7, // Places the Arrow in the right spot
		top: 0,
		opacity:.6,
	});
	
	if(topArrow.left < 5){ 
		topArrow.left = 5;
	}
	else if (topArrow.left > view.width -20 ){
		topArrow.left = view.width -20;
	};
	
	//Reset anchorPoint to match the click point. 
	view.anchorPoint = {x:topArrow.left/view.width, y: topArrow.top/view.height}
	
	
	
	//Methods
	var tagWithContact = function (personAddressBookData){
		//1. Close the Keyboard and move everything down
		win.closeAnimation();
		//globals.tabGroup.navBar.appear();
		var startTime = new Date().getTime();
		var dataForBubble = personAddressBookData//e.rowData.data;//tableView.data[0].rows[e.index].data

		//Prepare Bubble Data 
		var bubbleData = {};
		bubbleData.x = xPosition/parentImage.width;
		bubbleData.y = (yPosition-10)/parentImage.height; //-10 is for the top margin?
		bubbleData.title = dataForBubble.name;
		bubbleData.mediaID = win.currentImage.$id;
		bubbleData.tagType = 'person';
		bubbleData.hasSpot = true; 
		
		//2. Select Callback: i.e. enter data in the database
		var dataToSend = {};
		dataToSend.tagType = 'person';
		dataToSend.tagID = dataForBubble.id;//This is the AddressBookID for person.
		dataToSend.mediaURL = win.currentImage.URL;
		dataToSend.bubbleData = bubbleData;
		dataToSend.pictureData = {
			URL: dataToSend.mediaURL, 
			backgroundImage: win.imagesDataArray[win.currentIndex].thumbnail
		}
		
		var DataMedia = require('model/DataMedia');
		//Need to check here if you need to add a new one or not. Before adding the tag. 
		DataMedia.addTagToMedia(dataToSend.pictureData, dataToSend.tagType, dataToSend.tagID, dataToSend.bubbleData, false);
	};
	
	
	function populateTable(e){
		var DataPersons = require('model/DataPersons');
		var requestedData = DataPersons.queryAddressBookContacts(e.value);
		
		var tableData = [];
		tableView.setData(tableData);
		
		if (requestedData){
			for (var i = 0; i < requestedData.length; i++){
				var row = createTableViewRow(requestedData[i]);
				tableData.push(row); 
			};
			
			var addNewContactRow =  Ti.UI.createTableViewRow({
				height: win.screenWidth === 320 ? 35 : 30,
				title: I('Add_new_contact'),
				font: {fontSize:18, fontWeight: 'bold'},
				color: 'white',
				className: 'personTable',
				editable: false,
				kind: 'addNewContact'
			});
			
			tableData.push(addNewContactRow);
		};	
	
		tableView.setData(tableData);
		
		var rowCountDisplayed; 
		if (win.screenWidth === 320){
			rowCountDisplayed = tableData.length <= 4 ? tableData.length : 4;
			tableView.height = 35 * rowCountDisplayed - 1
			bg.height=  36 + tableView.height;
		}
		else{
			rowCountDisplayed = tableData.length <= 3 ? tableData.length : 3;
			tableView.height = 30 * rowCountDisplayed-1
			bg.height= 36 + tableView.height;
		};
		
		if(rowCountDisplayed > 0){
			whiteLine.visible = true;
		}
		else{
			whiteLine.visible = false;
		};
		
		view.height = bg.height + 10;
	};
	
	
	var open = function (){
		view.visible = true;
		view.animate(Ti.UI.createAnimation({
			duration: 150,
			transform: Titanium.UI.create2DMatrix().scale(1)
		}));
	};
	
	
	function orientationClosing (){
		win.closeAnimation();
	};
	
	 
	var closing = function (){
		textField.blur();
		view.animate(Ti.UI.createAnimation({
			duration: 50,
			transform: Titanium.UI.create2DMatrix().scale(0)
		}));
		
		setTimeout(function(){
			view.remove(bg);
			view.remove(textField);
			view.remove(tableView);
			view.remove(whiteLine);
			view.add(topArrow);
			
			view.visible = false;
			win.selectionView = null;
			win.remove(view);
			view = null;
		},270);
	};
	
	
	
	//Assemble
	view.add(bg);
	view.add(textField);
	view.add(tableView);
	view.add(whiteLine);
	view.add(topArrow);
	
	
	//Expose elements
	view.xPosition = xPosition;
	view.yPosition = yPosition;
	view.textField = textField;
	view.tableView = tableView;
	view.closing = closing;
	view.open = open;
	
	
	return view;
};