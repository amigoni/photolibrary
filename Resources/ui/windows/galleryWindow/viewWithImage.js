//Contains the component witht the image, scrollview, bubbles and all the other fun stuff. 
module.exports = function (imageData,index,dictionary,win,leftPosition){
	var Helper = require('lib/Helper');
	var DataMedia = require('model/DataMedia');
	var newSize = Helper.imageResize2(win.screenWidth,win.screenHeight, imageData.aspectRatioThumbnail.width,imageData.aspectRatioThumbnail.height, scrollView);
	
	
	var scrollView = Ti.UI.createScrollView({
		//backgroundColor: 'yellow',
		width: 320,
		height: 480,
		contentWidth:'auto',
		contentHeight:'auto',
		showHorizontalScrollIndicator: false,
		showVerticalScrollIndicator:false,
		minZoomScale:1,  
        maxZoomScale:2, 
        zoomScale:1,
        oldZoom:1,
        index:index,
		left: 0,
		kind: 'scrollView',
		anchorPoint: {x:0.5,y:0.5},
		top:0,
		bottom:0,
		scrollEnabled: false
	});
	

	scrollView.addEventListener('scale', function(e){
		//Ti.API.debug("scale inner "+e.scale);
		if (e.source != scrollView) {
			return;
		};
		
		//Resize Bubbles at scale
		if (e.scale !=1 && scrollView.image.dragLayer){ // e.scale !=1 to prevent the event the fires at creation. 
			win.UIDisappear();
			
			/*
			for(var i = 0; i < scrollView.image.dragLayer.children.length; i++){
				var bubble = scrollView.image.dragLayer.children[i];
				if (bubble.kind === 'bubbleMainView'){
					bubble.animate(Ti.UI.createAnimation({
			        	duration:100,
			        	transform: Ti.UI.create2DMatrix().scale(1/scrollView.zoomScale)
			        }));
				};
			};
			*/     
		}
		else if (e.scale === 1){
			if (win.isUiHidden === false){
				win.UIAppear();
			};
		};
		
		
		//Disables scrolling if less or equal to 1;
		if (scrollView.zoomScale === 1){ 
			scrollView.scrollEnabled = false;
		}
		else if (scrollView.zoomScale  > 1){
			scrollView.scrollEnabled = true;
			//win.scrollableGalleryView.currentView.visible = false;
		}
		else if (scrollView.zoomScale  < 1){
			scrollView.scrollEnabled = false;
		};
		
		//Make it visible here othewise when you scroll sometimes you get black.
		//win.scrollableGalleryView.currentView.visible = true;
    });
    
    
    scrollView.addEventListener('scroll', function (e){
		if (e.source != scrollView) {
			return;
		};
		
		//Ti.API.debug("scroll inner: " + e.x);
		
		if (win.keyboardUp === true && e.source.kind != 'bubble' && win.selectionView){
			win.closeAnimation();
			win.keyboardUp = false;
		};
		
		//Hide image in the back. Had to put it here. Only event that fires all the time. 
		//I couldn't do it after the load it would flash black somehow. 
		//if(win.scrollableGalleryView.currentView && win.scrollableGalleryView.currentView.visible === true){
			//win.scrollableGalleryView.currentView.visible = false;
		//};	
	});
	
	var filterableimageview = require('jp.msmc.filterableimageview');
	
	//var image =  filterableimageview.createFilterableImageView({	
	var image = Ti.UI.createView({
		backgroundImage: imageData.aspectRatioThumbnail,
		//image: imageData.aspectRatioThumbnail,
		//filter:[{name: 'grayscale'}],
		hasFullImage: false, //Parameter to know which image is loaded. For optimization.
		width: 320,
		height: newSize.height,
		parent: scrollView,
		//parentScrollView: scrollView,
		bubblesData: imageData.bubblesData? imageData.bubblesData : [],
		kind: 'image',
		URL: imageData.URL,
		data: imageData,
		labelsData:[],
		pinchingGesture:true,
		anchorPoint:{x:0.5,x:0.5}
	});
	
	
	var dragLayer = Ti.UI.createView({
		//backgroundColor: 'red',
		top:0,
		bottom:0,
		left:0,
		right:0,
		opacity:1
	});		
		
	image.add(dragLayer);
	image.dragLayer = dragLayer;	
	scrollView.image = image;
	win.currentImage = image;
	
	
	image.addEventListener('singletap', function (e){
		//When there is no Person Selection or Tag Selection and you tap on the screen. 
		if (win.keyboardUp === false && win.touchedBubble === false && win.moveModeOn === false && win.deleteBubbleMode === false) {
		 	win.toggleUI();
		}
		//When the Person Selection is up and you click outside of it
		else if (win.keyboardUp === true && win.selectionView){
			win.closeAnimation();
			//globals.tabGroup.navBar.appear();
			win.keyboardUp = false;
		};
	});
	
	
	image.addEventListener( 'doubletap', function (e){
		//DeleteMode is when you first tap a bubble to delete it. 
		if(win.deleteBubbleMode === false){
			if(win.keyboardUp == false && e.source.kind != 'bubble'){
				//globals.tabGroup.navBar.disappear();
				win.keyboardUp = true;
			
				createClickDot(e.x, e.y,image,scrollView);	
				
				var gPoint = image.convertPointToView({x: e.x, y: e.y}, win);
				
				var moveY; 
				var orientation; 
				var keyboardHeight;
				var tableHeight;
				if (win.screenWidth > win.screenHeight) {
				    orientation = 3;
				    keyboardHeight = 160;
					tableHeight = 140;
				} else {
				    orientation = 1; 
				    keyboardHeight = 220;
					tableHeight = 180;   
				};
					
				moveY = tableHeight - (Ti.Platform.displayCaps.platformHeight-gPoint.y-keyboardHeight);
				moveY = moveY > 0 ? moveY : 0;
				
				win.upAnimation(moveY);
				//Open the selectionView with the text field. At the window level, Incase it is below the height of the scrollableView. 
				//There is an issue with the animation up and down though. 
				var PersonGalleryPopUp = require('ui/windows/galleryWindow/PersonGalleryPopUp');
				win.selectionView = PersonGalleryPopUp.loadWin(e.x,e.y,gPoint.x,gPoint.y-moveY, dictionary.returnCallBack,dictionary.selectCallBack,win,image);
				win.add(win.selectionView);
				win.selectionView.textField.focus();
				win.selectionView.open();
			}
			else if (win.keyboardUp === true){
				win.keyboardUp = false;
				win.closeAnimation();
			};
		};
	});
	
	
	image.addEventListener('touchend', function(){
		if( image.currentBubble ){
			image.currentBubble.stoppedTouchingBubble();
		};
	});
	
	scrollView.add(image);
	
	//Initialize with Image if it's the current one
	if (index === dictionary.currentImageIndex){
		DataMedia.loadMediaAssetImage(image);
	};
	
	
	
	//Bubbles Management
	var Bubble = require('ui/windows/galleryWindow/Bubble');
	var dragDropKit = require("lib/com.pixelhungrystudios.dragdropkit");
	var visibleBubbles = [];
	var recycledBubbles = [];
	var bubblesArray = [];
	var freeBubbleIndex = 0; //Allows to place the free Bubbles properly. 
	
	
	function addBubble (bubblesData){
		if (bubblesData.hasSpot === false){
			bubblesData.x = .5;
			bubblesData.y =  .15+ (40/image.height)+ freeBubbleIndex * (40/image.height);
			if (bubblesData.y > .9){
				freeBubbleIndex = -1;
			};
			freeBubbleIndex++;
		};
		
		var bubble  = new Bubble(bubblesData,dictionary,image,scrollView,win);
		
		dragDropKit.attach({
			view: bubble,
			container: dragLayer,
			win:win
		}); 
		
		dragLayer.add(bubble);
		bubblesArray.push(bubble);
	};
	
	
	function removeBubble(bubble){
		dragDropKit.detach(bubble);
		dragLayer.remove(bubble);
	};
	
	
	function detachBubble(bubble){
		dragDropKit.detach(bubble);
	};
	
	
	function attachBubble(bubble){
		dragDropKit.attach({
			view: bubble,
			container: dragLayer,
			win:win
		});
	};
	
	
	function loadBubbles(){
		//This is a fix for the Touch Lock after going back while timer for configuring an image.
		//This assures that everything is completely detached. 
		dragDropKit.detachAll();
	
		var DataBubbles = require('model/DataBubbles');
		var bubblesData = DataBubbles.getBubblesForMedia(image.URL);
		
		for (var i =0; i < bubblesData.length; i++){
			addBubble(bubblesData[i]);
		};
	};

	
	function clearBubbles (){
		freeBubbleIndex = 0;
		
		for(var i = bubblesArray.length-1  ; i >= 0; i--){
			dragDropKit.detach(bubblesArray[i]);
			bubblesArray[i].clean();	
			dragLayer.remove(bubblesArray[i]);
		};
		
		dragDropKit.detachAll();
		
		Ti.API.debug('Bubbles Array is:')
		Ti.API.debug(bubblesArray);
		
		bubblesArray =[];
	};
	
	
	function checkIfBubbleExists (bubbleID){
		var bubbleExists = false;
		if(bubbleID){
			for (var i = 0; i < bubblesArray.length; i++){
				//Ti.API.debug(bubblesArray[i].data.$id)
				if(bubbleID === bubblesArray[i].data.$id){
					bubbleExists = true;
				};
			};
			//Ti.API.debug(bubbleID)
		};
		
		return bubbleExists;
	};
	
	
	//Function that does array difference.
	function ArrayDiff(a,b) {
	    return b.filter(function(i) {return !(a.indexOf(i) > -1);});
	};
	///
	
	
	
	function loadLabelsData (URL){
		var DataLabels = require('model/DataLabels');
		image.labelsData = DataLabels.loadLabelsForMediaURL(URL);
	};
	
	
	function resetView (){
		clearBubbles();
		Ti.API.debug('ViewWithImage has been reset');
	};
	
	function setFullSizeImage (e){
		if (e.index === scrollView.index){
			Ti.API.debug('SETTING IT '+e.index+' '+scrollView.index)
			image.backgroundImage = win.scrollableGalleryView.currentView.imageView.backgroundImage;
			//image.image = win.scrollableGalleryView.currentView.imageView.backgroundImage;
		}
	};
	
	
	//Reconfigures the View according to the new page Parameters. 
	function configureView (newIndex){
		var startTime = new Date().getTime();

		var currentImage = win.imagesDataArray[newIndex].aspectRatioThumbnail//win.scrollableGalleryView.currentView.imageView;
		
		clearBubbles();
		dragLayer.opacity = 1;
		scrollView.configured = false;
		scrollView.visible = false;
		scrollView.zoomScale = 1;
		
		
		//THIS MUST STAY BEFORE Resizing the scrollView. 
		var newSize = Helper.imageResize2(win.screenWidth, win.screenHeight, currentImage.width, currentImage.height);
		image.width = newSize.width;
		image.height = newSize.height;
		
		scrollView.width = win.screenWidth;
		scrollView.height = win.screenHeight;
		scrollView.index = newIndex;
		scrollView.scrollEnabled = false;
		image.index = newIndex;
		image.URL = win.imagesDataArray[newIndex].URL;
		var hash =Ti.Utils.md5HexDigest(win.imagesDataArray[win.currentIndex].thumbnail)
		image.hash = hash;
		
		
		
		//Center the image inside
		if(image.height < win.screenHeight){
			image.top = (win.screenHeight - image.height)/2;
		}
		else{
			image.top = null;
		};
		
		var xOffset = (image.width > win.screenWidth) ? (image.width - win.screenWidth)/2 : 0;
		var yOffset = (image.height > win.screenHeight) ? (image.height - win.screenHeight)/2 : 0;
		scrollView.setContentOffset({x: xOffset, y: yOffset},{animated:false});
		
		scrollView.left = newIndex*(win.screenWidth + 20) + 10;
		
		//Load Image	
		if(win.scrollableGalleryView.currentView){
			image.backgroundImage = win.scrollableGalleryView.currentView.imageView.backgroundImage;
			//image.image = win.scrollableGalleryView.currentView.imageView.backgroundImage;
			scrollView.visible = true;
			scrollView.configured = true;	
			win.scrollableGalleryView.currentView.imageView.visible = false;
		};
		
		
		//BottomBar update
		//Load image Data for Favorite Data;
		
		var mediaDetail = DataMedia.loadMediaFromURL(image.URL, hash);
		
		loadBubbles();
		
		loadLabelsData(image.URL);
		
		win.bottomBar.update((mediaDetail? mediaDetail.isFavorite : false),image.labelsData);
		
		var endTime = new Date().getTime();
		Ti.API.info('CONFIGURE VIEW IN: ' + (endTime - startTime) + 'ms');
	};
	
	
	//Rotating Function 
	function rotateImage (){
		scrollView.zoomScale = 1;
		if(win.isUiHidden === false){
			clearBubbles();
		};
		//Resize the Image
		var newSize = Helper.imageResize2(win.screenWidth, win.screenHeight, image.width, image.height, scrollView);
		image.width = newSize.width;
		image.height = newSize.height;
		
		//Center the image inside
		if (image.width > win.screenWidth){image.left = -(image.width-win.screenWidth)/2}
		else {image.left = null};
		if (image.height > win.screenHeight){image.top = -(image.height-win.screenHeight)/2}
		else {image.top = null};
		
		//Resized the ScrollView
		scrollView.width = win.screenWidth;//newSize.width;
		scrollView.height = win.screenHeight;
		scrollView.left = scrollView.index*(win.screenWidth+20)+10;
		
		
		scrollView.scrollingEnabled = false;
		
		if(win.isUiHidden === false){
			loadBubbles()
		};
	};
	
	function closeAnimation (){
		var white = Ti.UI.createView({
			backgroundColor: 'white',
			width: scrollView.widht,
			height: scrollView.height,
			opacity: 0
		});
		scrollView.add(white);
		
		white.animate(Ti.UI.createAnimation({
			opacity: 1,
			duration: 250
		}));
	};
	
	
	function clean (){
		clearBubbles();
				
		var imageChildren = image.children;
		for(var i = imageChildren.length - 1 ; i >= 0;  i--){
			image.remove(imageChildren[i]);
			imageChildren[i] = null;
		};
		
		Ti.API.debug('Cleaned ViewWithImage Image '+image.children);
		
		var scrollViewChildren = scrollView.children;
		for(var i = scrollViewChildren.length - 1 ; i >= 0;  i--){
			scrollView.remove(scrollViewChildren[i]);
			scrollViewChildren[i]= null;
		};
		
		Ti.API.debug('Cleaned ViewWithImage ScrollView '+scrollView.children);
		
		//Clears everything after close incase you press close window while loading.
		setTimeout(function(){
			dragDropKit.detachAll();
			Ti.API.debug("DELAYED DETACHMENT");
		},1000);
		
		Ti.App.removeEventListener('addedNewBubble', addBubble);
		Ti.App.removeEventListener('pageScrollViewLoadedFullImage', setFullSizeImage )
	};
	
	
	//Listeners
	Ti.App.addEventListener('addedNewBubble', addBubble);
	Ti.App.addEventListener('pageScrollViewLoadedFullImage', setFullSizeImage )
	
	
	//Expose
	image.addBubble = addBubble;
	image.removeBubble = removeBubble;
	image.loadBubbles = loadBubbles;
	image.clearBubbles = clearBubbles; 
	image.attachBubble = attachBubble;
	image.detachBubble = detachBubble;
	image.visibleBubbles = visibleBubbles;
	image.checkIfBubbleExists = checkIfBubbleExists;
	image.dragLayer = dragLayer; 
	
	image.loadLabelsData = loadLabelsData;
	
	scrollView.rotateImage = rotateImage; 
	scrollView.configureView = configureView;
	scrollView.resetView = resetView;
	scrollView.image = image;
	scrollView.clean = clean;
	scrollView.closeAnimation = closeAnimation;
	
	win.currentImageScrollView = scrollView;
	win.currentImage = scrollView.image; 
	

	return scrollView;
};


var createClickDot = function (xPosition, yPosition,image,imageScrollView){
	//NOTE: Put it here because it's a child of the window, not the image. 
	var view = Ti.UI.createView({
		backgroundImage: 'images/click_dot.png',
		//backgroundColor: 'white',
		width: 17,
		height: 17,
		top: yPosition-16*0.5,
		left: xPosition-16*0.5,
		opacity: 1,
		transform:Titanium.UI.create2DMatrix().scale(1/imageScrollView.zoomScale)
	});
	
	//Appear Animation
	view.animate(Ti.UI.createAnimation({
		duration: 150,
		opacity: 1
	}));
	
	//Disappear Animation
	setTimeout(function (){
		view.animate(Ti.UI.createAnimation({
			duration: 600,
			opacity: 0
		}));
	}, 150);
	
	//Remove item;
	setTimeout(function(){
		image.remove(view);
		view = null;
	}, 900);
	
	image.add(view);
};