
//Bottom Bar 
exports.normalBottomBar = function (dictionary,win){
	var MiscComponents = require('ui/components/MiscComponents');
	
	var bottomBar = Ti.UI.createView({
	   // backgroundImage: 'images/blackBar.png',
	    bottom:0,
	    height:44,
	    width:'100%',
	    transform: Ti.UI.create2DMatrix().translate(0, 44),
	    //opacity: .7
	});


	//Label Button
	var labelsButton = Ti.UI.createView({
		//backgroundColor: 'red',
		width: 200,
		height: 44,
		left:0
	});
	labelsButton.clickable = true; 
	
	var labelsButtonClick = function(e){
		//var PeoplePicker = require('ui/components/PeoplePicker');
		//var peoplePicker = PeoplePicker.loadWin(currentImage.$id);
		//bottomBar.visible = false;
		//tagDisplayArea.visible = false;
		win.toggleUI();
		var LabelGalleryPopUp = require('ui/windows/galleryWindow/LabelGalleryPopUp');
		LabelGalleryPopUp.loadWin(dictionary.requestLabelDataFunction,win);
	};
	
	labelsButton.addEventListener('click', function (e){
		if (labelsButton.clickable === true){
			labelsButtonClick(e);
			labelsButton.clickable = false; 
			var clickTimeout = setTimeout(function(){
				labelsButton.clickable = true; 
			},
			2000);
		};
	});
	
	var labelIcon = Ti.UI.createButton({
		//systemButton:Titanium.UI.iPhone.SystemButton.ACTION,
		backgroundImage: 'images/tag_add.png',
		width: 29,
		height: 31,
		left: 10
	});
	
	var labelsText = Ti.UI.createLabel({
		text: '',
		//backgroundColor: 'red',
		width: 155,
		font:{fontWeight: 'bold', fontSize: 13},
		color: 'white',
		shadowColor: 'black',
		shadowOffset:{x:1,y:1},
		height: 40,
		left: 45,
		touchEnabled: false
	});
	
	labelsButton.add(labelIcon);
	labelsButton.add(labelsText);



	//Share Button
	var ShareButton = require('ui/components/ShareButton');

	var shareButton = ShareButton.shareButton(['email','facebook','twitter','contact'],null,win);
	shareButton.right = 10;
	
	shareButton.clickFunction = function(e){
		win.currentImage.dragLayer.visible = false;
		
		var DataMedia = require('model/DataMedia');
		var successCb = function (e){
			//var optionDialog = createOptionDialog(shareOptionsArray,imageToSend,personAddressBookID);
			var optionDialog = ShareButton.createPopUp(e.asset.defaultRepresentation.fullScreenImage);
			optionDialog.open();
		};
		
		function errorCb (){
			alert("Sorry couldn't get this photo")
		};
		
		DataMedia.getSingleAssetFromURL(win.currentImage.URL,successCb, errorCb)
	};
		
		
	//Favorite Button
	var favoriteButton = MiscComponents.persistantButton({
		backgroundImage: 'images/favorites.png',
		backgroundSelectedImage: 'images/favorites_white.png',
		selected: false
	});
	favoriteButton.clickable = true; 
	
	favoriteButton.width = 30;
	favoriteButton.height = 29;
	favoriteButton.right = 70;
	favoriteButton.transform = Ti.UI.create2DMatrix().scale(1.2);

	var favoritesButtonClick = function(e){
		var DataMedia = require('model/DataMedia');
		
		var pictureData = {
			URL: win.currentImage.URL, 
			backgroundImage: win.imagesDataArray[win.currentIndex].thumbnail
		};
		
		DataMedia.changeFavoriteStatusForMedia(pictureData);
	};	
	
	favoriteButton.addEventListener('click', function (e){
		if (favoriteButton.clickable === true){
			favoritesButtonClick(e);
			favoriteButton.clickable = false; 
			var clickTimeout = setTimeout(function(){
				favoriteButton.clickable = true; 
			},
			500);
		};
	});
	
	
	//Favorite Button
	var posterButton = MiscComponents.persistantButton({
		backgroundImage: 'images/favorites.png',
		backgroundSelectedImage: 'images/favorites_white.png',
		//selected: false
	});
	
	//posterButton.clickable = true; 
	posterButton.width = 30;
	posterButton.height = 29;
	posterButton.right = 120;
	//posterButton.transform = Ti.UI.create2DMatrix().scale(1.2);

	posterButton.addEventListener('click', function (e){
		var PosterDetail = require('/ui/windows/PosterDetail');
		var win1 = PosterDetail.loadWin({image: win.currentImage.backgroundImage});
		win1.open();
	});
	
	
	
	//Assemble
	bottomBar.add(Ti.UI.createView({
		backgroundColor: 'black',
		width: bottomBar.width,
		height: bottomBar.height,
		opacity:0
	}));
	
	bottomBar.add(shareButton);
	bottomBar.add(labelsButton);
	bottomBar.add(labelsText);
	bottomBar.add(favoriteButton);
	//bottomBar.add(posterButton);
	
	
	
	//Methods
	function updateLabelsText (labelsData){
		var labelsTextString = '';
		for (var i =0; i < labelsData.length; i ++){
			labelsTextString += (labelsData[i].name);
			if (i < labelsData.length-1){
				labelsTextString += ', ';
			};
		};
		
		if(labelsData.length === 0){
			labelsTextString = I('add_label');
			labelsText.color = '#CCCCCC';
		}
		else{
			labelsText.color = 'white';
		}
		labelsText.text = labelsTextString;
	};
	
	function update(isFavorite,labelsData){
		favoriteButton.reset(isFavorite);
		updateLabelsText(labelsData);	
	};
	
	function clear (){
		favoriteButton.reset(false);
		labelsText.text = '';
	};
	
	function openAnimation(){
		bottomBar.animate(Ti.UI.createAnimation({
			duration: 250,
			transform: Ti.UI.create2DMatrix().translate(0, 0),
		}));
	};
	
	function clean (){
		labelsButton.remove(labelsText);
		labelsButton.remove(labelIcon);
		labelsText = null;
		labelIcon = null;
		
		var children = bottomBar.children;
		for(var i = children.length-1 ; i >= 0;  i--){
			bottomBar.remove(children[i]);
			children[i] = null;
		};
		Ti.API.debug('Cleaned BottomBar');
	};
	
	
	
	//Expose 
	bottomBar.favoriteButton = favoriteButton;
	bottomBar.update = update;
	bottomBar.clear = clear;
	bottomBar.updateLabelsText  = updateLabelsText;
	bottomBar.clean = clean;
	bottomBar.openAnimation = openAnimation;
	
	
	return bottomBar;
};	


exports.pictureOfTheWeek = function (dictionary, win, whatSpecialKind){
	var MiscComponents = require('ui/components/MiscComponents');
	
	var bottomBar = Ti.UI.createView({
	   // backgroundImage: 'images/blackBar.png',
	    bottom:0,
	    height: 44,
	    width:'100%',
	    //opacity: .7
	    transform: Ti.UI.create2DMatrix().translate(0, 44),
	});
	
	
	var leftLabel = Ti.UI.createLabel({
		text: whatSpecialKind === 'thisWeekRow' ? I("able_to_pick_on_Monday") :I('Please_pick_a_picture'),
		textAlign: whatSpecialKind != 'thisWeekRow' ? 'left': 'center',
		//backgroundColor: 'red',
		width: whatSpecialKind != 'thisWeekRow' ? 220 : 310,
		font:{fontWeight: 'bold', fontSize: 17},
		color: 'white',
		shadowColor: 'black',
		shadowOffset:{x:1,y:1},
		height: 40,
		left: whatSpecialKind != 'thisWeekRow' ? 10 : null
	});
	
		
		
	//Favorite Button
	var awardButton = MiscComponents.persistantButton({
		backgroundImage: 'images/sidebar_pictureOfTheWeek_icon.png',
		backgroundSelectedImage: 'images/sidebar_pictureOfTheWeek_icon.png',
		selected: false
	});
	awardButton.clickable = true; 
	
	awardButton.width = 29;
	awardButton.height = 30;
	awardButton.right = 10;
	awardButton.transform = Ti.UI.create2DMatrix().scale(1.2);

	var awardButtonClick = function(e){
		var PopUp = require('ui/components/PopUp');
		var win1 = PopUp.awardCaptionPopUp();
		
		win1.addEventListener('awarded', function (e){
			var PopUp = require('ui/components/PopUp');
			var alertDialog = PopUp.alertDialog(I('Award_Verb'),I( "Are_you_sure _no_change_choice"))
			
			alertDialog.addEventListener('ok', function (){
				var DataPictureOfTheWeek = require('model/DataPictureOfTheWeek');
				DataPictureOfTheWeek.addPictureOfTheWeek(win.currentImage.URL, e.value);
				
				//Gives Time to save the picture
				setTimeout(function (){
					var powDetailData = DataPictureOfTheWeek.loadPictureOfTheWeekDetail(win.currentImage.URL);
					var PictureOfTheWeekDetail = require('ui/windows/PictureOfTheWeekDetail');
					var win2 = PictureOfTheWeekDetail.loadWin(powDetailData);
					win2.open();
					win.winCloseAnimation();
				},1000)
			});
			
			alertDialog.open();
		});
		
		win1.open();
	};	
	
	awardButton.addEventListener('click', function (e){
		if (awardButton.clickable === true){
			awardButtonClick(e);
			awardButton.clickable = false; 
			var clickTimeout = setTimeout(function(){
				awardButton.clickable = true; 
			},
			500);
		};
	});
	
	
	//Assemble
	bottomBar.add(leftLabel);
	if (whatSpecialKind != 'thisWeekRow'){
		bottomBar.add(awardButton);
	};
	
	
	
	function clean (){
		var children = bottomBar.children;
		for(var i = children.length-1 ; i >= 0;  i--){
			bottomBar.remove(children[i]);
			children[i] = null;
		};
		Ti.API.debug('Cleaned BottomBar');
	};
	
	function clear (){
		
	}
	
	function update(){
		
	}
	
	function openAnimation(){
		bottomBar.animate(Ti.UI.createAnimation({
			duration: 250,
			transform: Ti.UI.create2DMatrix().translate(0, 0),
		}));
	};
	
	
	
	//Expose 
	bottomBar.awardButton = awardButton;
	bottomBar.clean = clean;
	bottomBar.clear = clear;
	bottomBar.update = update;
	bottomBar.openAnimation = openAnimation;
	
	
	return bottomBar;
};