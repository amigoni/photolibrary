exports.loadWin = function (requestLabelDataFunction,winGallery){
	var PopUp = require('ui/components/PopUp');
	var DataLabels = require('model/DataLabels');
	var DataMedia = require('model/DataMedia');
	
	var screenW =  Ti.Platform.displayCaps.platformWidth;
	
	var win = Ti.UI.createView({
		tabBarHidden: true,
		navBarHidden:true,
		width:'100%',
		height: '100%',
		opacity:1
	});
	
	var bgShade = Ti.UI.createView({
		backgroundColor: 'black',
		width: '100%',
		height: '100%',
		opacity: 0
	});
	
	var bg = PopUp.bg();
	bg.transform = Ti.UI.create2DMatrix().scale(0);
	bg.width = '95%';
	bg.top = 10;
	bg.height = screenW === 320 ? 252-150+35*4 :252-150-32+30*2+9;
	
	
	var openAnimation = function (){
		//Delayed to wait for layout. Need to do it here after it has been opened and layed out. 
		setTimeout(function (){
			//bubblesScrollView.scrollToBottom();
		},50);
		
		bg.animate(Ti.UI.createAnimation({
			delay: 100,
			duration: 200,
			transform: Ti.UI.create2DMatrix().scale(1),
			opacity:1
		}));
		
		bgShade.animate(Ti.UI.createAnimation({
			duration: 500,
			opacity:0
		}));
		
		textField.focus();
		//globals.tabGroup.navBar.disappear();
	};
	
	var closeAnimation = function (){
		bg.animate(Ti.UI.createAnimation({
			duration: 250,
			transform: Ti.UI.create2DMatrix().scale(0),
			opacity:0
		}));
		
		bgShade.animate(Ti.UI.createAnimation({
			duration: 250,
			//transform: Ti.UI.create2DMatrix().scale(0),
			opacity:0
		}));
		
		setTimeout(function (){
			//win.closeAnimation();
			winGallery.remove(win);
			winGallery.labelPickerPopUp = null;
			winGallery.toggleUI();
			win = null;
			
		}, 300);
		
		textField.blur();
		//globals.tabGroup.navBar.appear();
	};
	
	//Navigation
	var closeButton = Ti.UI.createView({
		//backgroundImage:'images/nav_x.png',
		//backgroundColor: 'red',
		width: 72,
		height: 46,
		top: 0,
		left: 0
	});
	
	closeButton.add(Ti.UI.createView({
		backgroundImage:'images/nav_x.png',
		width: 23,
		height: 23,
		top: 15,
		left: 20
	}));
	
	closeButton.addEventListener('click', function (){
		closeAnimation();
	});
	
	var centerTitle = Ti.UI.createView({
		width:Ti.UI.SIZE,
		height: 44,
		top:0,
		layout:'horizontal'
	});
	
	var title = Ti.UI.createLabel({
		color: 'white',
		font: {fontSize: 20, fontWeight: 'bold'},
		top: 14,
		left:4,
		text: 'Labels',
	});
	
	var icon = Ti.UI.createView({
		backgroundImage:'images/tag_add.png',
		width: 22,
		height:24,
		top: title.top-1,
		//left: title.left-35
	});
	
	centerTitle.add(icon);
	centerTitle.add(title);
	
	//TextField
	var textField = Ti.UI.createTextField({
		value:' ',
		width: 70,//'94%',
		height: 20,
		color: 'white',
		//top:46,
		appearance:Titanium.UI.KEYBOARD_APPEARANCE_ALERT,
		autocorrect:false,
		suppressReturn: false,
		//zIndex: 999,
		//hintText: 'add new label',
		kind: 'textfield'
	});
	
	var hintText = Ti.UI.createLabel({
		text: I('add_label'),
		textAlign: 'left',
		left: 5,
		font: {fontSize: 15},
		color: '#CCCCCC'
	});
	
	hintText.toggleVisible = function (){
		if(bubblesContainer.children && bubblesContainer.children.length < 1){
			hintText.opacity = 1;
		}else{
			hintText.opacity = 0;
		};
	};
	
	textField.add(hintText);
	
	
	function returnFunction(e){
		textField.value = ' ';//Using space because of delete funciton. 
		
		hintText.toggleVisible();
		
		var dataToStore = {};
		dataToStore.name = e.value.slice(1);//remove space.
		
		if (dataToStore.name.length > 0){
			function capitaliseFirstLetter(string){
			    return string.charAt(0).toUpperCase() + string.slice(1);
			};
			dataToStore.name = capitaliseFirstLetter(dataToStore.name);
			
			//Create Label
			var label = DataLabels.addLabel(dataToStore);
			
			var pictureData = {
				URL: winGallery.currentImage.URL, 
				backgroundImage: winGallery.imagesDataArray[winGallery.currentIndex].thumbnail
			};
			
			//Add it to the Media
			DataMedia.addTagToMedia(pictureData,'label',label.$id,null,true);
			
			winGallery.currentImage.labelsData = DataLabels.loadLabelsForMediaURL(winGallery.currentImage.URL);
			
			winGallery.bottomBar.updateLabelsText(winGallery.currentImage.labelsData);
			//CreateThe new Bubble
			bubblesContainer.remove(textField);//Remove and then add it after. Zindex doesn't work in horizontal'
			var newBubble = bubblesContainer.addBubble(label);
			bubblesContainer.add(textField);//Remove and then add it after. Zindex doesn't work in horizontal'
			textField.focus();
			
			//Delayed to wait for layout
			setTimeout(function (){
				//bubblesScrollView.scrollToBottom();
			},50);
			
			tableView.update('');
		}
		else{
			closeAnimation();
		};	
	}
	
	textField.addEventListener('return', function(e){
		returnFunction(e);
	});
	
	//This is used to eventually copy the behaviour on the email addresses. When you delete and it selects the last bubble. 
	var prevRecip = false;
	
	textField.addEventListener('change', function(e){
		if (e.value === "") { 
			if(bubblesContainer.selectedBubble){
				deleteSelectedBubble();
			}
			else{
				if(bubblesContainer.children.length > 1){
					bubblesContainer.children[bubblesContainer.children.length-2].selectBubble();
				};
			};
		};
		
		//Have to use this otherwise you can't detect Delete when there is no characters in the TextField. '
		if( e.value === '' && !prevRecip ){ 
		 	//setPrevRecip(); 
			//prevRecip = true;
			textField.value = " ";
		}
		else if ( e.value === '' && prevRecip ) {
			//removePrevRecip();
			//prevRecip = false;
			textField.value = " ";
		};
		
		
		//Need to remove the leading space to get a proper search value. 
		tableView.update(e.value.slice(1))
		
		
		if(textField.value === '' || textField.value === ' '){
			hintText.toggleVisible();
		}
		else{
			hintText.opacity = 0;
		}
	});
	
	//ScrollView
	//To give a grey shade to the scrollview. 
	var scrollBg = Ti.UI.createView({
		backgroundColor:'white',
		width:'94%',
		height:screenW === 320 ? 48 :24,
		top:46,
		left:10,
		opacity: .2,
	});
	
	var bubblesScrollView = Ti.UI.createScrollView({
		//backgroundColor: 'yellow',
		width: screenW-30,
		height:scrollBg.height,
		top:48,
		scrollType:'vertical',
		contentWidth: 300,
		contentHeight:'auto'
	});
	
	
	var bubblesContainer = Ti.UI.createView({
		//backgroundColor: 'red',
		width: bubblesScrollView.width-1,
		height: Ti.UI.SIZE,
		layout:'horizontal',
		childPositionX: 5,
		childPositionY:22,
		top:0
	});
	
	
	bubblesContainer.addBubble = function (data){
		var bubble;
		var bubbleExists = false; 
		
		if(bubblesContainer.children){
			for (var i =0; i < bubblesContainer.children.length;i++){
				if (bubblesContainer.children[i].kind != 'textfield' && bubblesContainer.children[i].label.text === data.name){
					bubbleExists = true;
				};
			};
		};
		
		if (bubbleExists === false){
			bubble = createBubble(data);
			bubblesContainer.add(bubble);	
		};
		
		return bubble;
	};
	
	var currentZindex = 1;
	
	var createBubble = function (data){
		var view = Ti.UI.createView({
			backgroundImage:'images/round_bubble_white.png',
			backgroundLeftCap:22,
			backgroundTopCap:11,
			width: Ti.UI.SIZE,
			height: 20,
			left: 2,
			top: 2,
			kind: 'bubble',
			zIndex:currentZindex,
			data:data,
			selected: false
		});
		currentZindex++;
		
		var label = Ti.UI.createLabel({
			text: data.name,
			font:{fontWeight: 'bold',fontSize:15},
			width: Ti.UI.SIZE,
			height: 16,
			left:10,
			right:10,
			top:2,
			touchEnabled:false
		});
		
		label.addEventListener('postlayout', function (){
			//label.width = label.size.width;
			//view.width = view.size.width;
		});
		
		
		view.addEventListener('click', function (e){
			if (view.selected === false){
				var bubbles = bubblesContainer.children;
			
				for(var i =0; i < bubbles.length; i++){
					if(bubbles[i].kind === 'bubble'){
						if (bubbles[i]!= view){
							bubbles[i].backgroundImage = 'images/round_bubble_white.png';
							bubbles[i].label.color = 'black';
							bubbles[i].selected = false;
						}
						else{
							bubbles[i].backgroundImage = 'images/round_bubble_red.png';
							bubbles[i].label.color = 'white';
							bubbles[i].selected = true;
							bubblesContainer.selectedBubble = bubbles[i];
						};
					};
				};
			}
			else{
				if(bubblesContainer.selectedBubble){
					deleteSelectedBubble();
				};
			};
		});
		
		function selectBubble(){
			view.backgroundImage = 'images/round_bubble_red.png';
			view.label.color = 'white';
			view.selected = true;
			bubblesContainer.selectedBubble = view;
		};
		
		
		bubblesContainer.childPositionX += 50;
		
		view.add(label);
		
		view.label = label;	
		view.selectBubble = selectBubble;
		
		return view;
	};
	
	bubblesScrollView.add(bubblesContainer);
	
	
	function deleteSelectedBubble(){
		bubblesContainer.remove(bubblesContainer.selectedBubble);
		DataMedia.removeTagFromMedia(winGallery.currentImage.URL,'label',bubblesContainer.selectedBubble.data.$id)
		winGallery.currentImage.labelsData = DataLabels.loadLabelsForMediaURL(winGallery.currentImage.URL);
		winGallery.bottomBar.updateLabelsText(winGallery.currentImage.labelsData);
		bubblesContainer.selectedBubble = null;
	};
	
	//Load currentBubbles
	for (var i=0; i < winGallery.currentImage.labelsData.length; i++){
		bubblesContainer.addBubble(winGallery.currentImage.labelsData[i]);
	};
	
	bubblesContainer.add(textField);
	textField.focus();
	
	
	//TableView
	var createTableViewRow = function (rowDataIn){
		var row = Ti.UI.createTableViewRow({
			height: screenW === 320 ? 35: 30,
			width:bubblesScrollView.width,
			title: rowDataIn.name,
			color: 'white',
			data: rowDataIn
		});
		
		return row;
	};
	
	var tableView = Ti.UI.createTableView({
		backgroundColor: 'transparent',
		width: bubblesScrollView.width ,
		height: winGallery.screenWidth === 320 ? (35*4-1): 30*2-1,////bg.height - 48-48,
		top:winGallery.screenWidth === 320 ? (46+51):(46+51-23),
	});
	
	//Could move this inside the element. 
	tableView.addEventListener('click', function(e){
		if(e.row.isEmptyRow == true){
			returnFunction({value: textField.value});
			return;
		};
		
		textField.value = ' ';
		
		hintText.toggleVisible();
		
		bubblesContainer.remove(textField);//Remove and then add it after. Zindex doesn't work in horizontal'
		
		var newBubble =bubblesContainer.addBubble(e.rowData.data);	
		
		bubblesContainer.add(textField);
		textField.focus();
		
		//Delayed to wait for layout
		setTimeout(function (){
			//bubblesScrollView.scrollToBottom();
		},50);
		
		var labelID = e.rowData.data.$id;
		
		var pictureData = {
			URL: winGallery.currentImage.URL, 
			backgroundImage: winGallery.imagesDataArray[winGallery.currentIndex].thumbnail
		};
		
		DataMedia.addTagToMedia(pictureData,'label',labelID,null,true);
		
		winGallery.currentImage.labelsData = DataLabels.loadLabelsForMediaURL(winGallery.currentImage.URL);
	
		winGallery.bottomBar.updateLabelsText(winGallery.currentImage.labelsData);
		
	});	
	
	tableView.update = function (value){
		//Need to remove the leading space to get a proper search value. 
		var requestedData = requestLabelDataFunction(value);
		
		var tableData = [];
		tableView.setData(tableData); //Clears the table;
		
		if (requestedData){
			for (var i = 0; i < requestedData.length; i++){
				var row = createTableViewRow(requestedData[i]);
				tableData.push(row); 
			};
			
			if (requestedData.length === 0){
				var row = Ti.UI.createTableViewRow({
					title: I('Add_new_Label')+value,
					color: 'white',
					font: {fontSize: 16},
					height: winGallery.screenWidth === 320 ? 35 : 30,
					isEmptyRow: true
				});
				
				tableData.push(row);
			};
			
			tableView.setData(tableData);
			
			//Change the Table Height according to results. 
			/*
			if(winGallery.screenWidth === 320){
				var rowsToDisplay = tableData.length < 4 ? tableData.length : 4;
				tableView.height = 35*rowsToDisplay -1;
				bg.height = 252-150+35*4;
			}
			else{
				var rowsToDisplay = tableData.length < 2 ?tableData.length : 2;
				tableView.height = 30*rowsToDisplay-1;
				bg.height = 252-150-32+30*2+9;
			};
			*/
		};	
	};
	
	tableView.update('');
	
	
	

	//Assemble Everything
	bg.add(closeButton);
	bg.add(centerTitle);
	bg.add(PopUp.groove(45,screenW === 320 ? screenW-25 : screenW-34));
	//bg.add(textField);
	//bg.add(scrollBg);
	bg.add(bubblesScrollView);
	
	bg.add(PopUp.groove(screenW === 320 ? 96 : (96-24),screenW === 320 ? screenW-25 : screenW-34));
	bg.add(tableView);
	
	win.add(bgShade);
	win.add(bg);
	
	
	//Expose
	win.closeAnimation = closeAnimation;
	winGallery.labelPickerPopUp = win; 
	
	winGallery.add(win);
	
	openAnimation();
};