//Creates the ScrollView wiht pages element. Optimized to use only two items at the time. 

//Notes: assetsCount is given in case the whole array has not loaded yet.
//win is necessary as a reference for the the functions outside the scrollView. Mainly to indentify
//...the current image and the current scrollview that holds the image
//dictionary holds the imagesData parameters


module.exports = function pageScrollView(assetsCount, currentIndex, win, dictionary){
	var Helper = require('lib/Helper');
	var _ = require('lib/underscore');
	var DataMedia = require('model/DataMedia');

	win.currentIndex = currentIndex;
	var visibleViews = []; //Array to hold  visible views;

	var scrollPadding = 2;
	var scrollViewWidth = win.screenWidth+20;
	var initialOffset = {x: currentIndex * scrollViewWidth, y: 0};
	var scrollDelta = 0;
	var scrollStartX = initialOffset.x;


	var scrollView = Ti.UI.createScrollView({
		//backgroundColor:'white',
		width: scrollViewWidth,
		height: win.screenHeight,
		contentWidth: scrollViewWidth * assetsCount+1,
		contentHeight: win.screenHeight,
		pagingEnabled: true,
		scrollType: 'horizontal',
		zoomScale:1,
		maxZoomScale:1,
		minZoomScale:1,
		anchorPoint: {x:0.5,y:0.5},
		hasBeenLayedOut: false
	});
	
	scrollView.setContentOffset(initialOffset, {animated: false}); //Must do it after definition to work. 
	
	win.scrollableGalleryView = scrollView;
	createViewPool(currentIndex);
	updateViewImage(currentIndex);
	setTimeout(function (){
		forEachVisibleView(currentIndex, updateViewImage);
	},250)
	
	
	var scrollCounter = 0
	scrollView.addEventListener('scroll', function(e){
		
		if (e.source != scrollView) {
			return;
		};
		scrollCounter++
		
		if(scrollCounter >10){
			scrollCounter = 0;
				e = scrollView.contentOffset;
	
			//Avoid scrolling outside of boundaries
			if (e.x < 0){
				e.x = 0;
			};
			
			if (e.x >= (win.imagesDataArray.length - 1) * (win.screenWidth+20)) {
				e.x = (win.imagesDataArray.length-1) * (win.screenWidth+20);
			};
		
			//Location Trigger. Regulates where in the scroll to trigger the next index.
			scrollDelta = e.x - scrollStartX;
			scrollStartX = e.x;	
			var index = e.x / (win.screenWidth+20);
			
			var currentIndexOffset = currentIndex * (win.screenWidth+20)+10;
			
			//if(scrollDelta > 0 && e.x >= currentIndexOffset+(win.screenWidth-20)){
			if(scrollDelta > 0 && e.x >= currentIndexOffset+20){	
				index = Math.ceil(index);
			}
			//else if (scrollDelta < 0 && e.x <= currentIndexOffset -(win.screenWidth-20)){
			else if (scrollDelta < 0 && e.x <= currentIndexOffset -20){
				index = Math.floor(index);
			}
			else{
				index = currentIndex;
			};
			
			
			//Filter to avoid multiple loads because of bounce. 
			if(index > currentIndex +1){
				index = currentIndex+1
			}
			else if (index < currentIndex-1){
				index = currentIndex-1
			};
			//Ti.API.info('index: ' + index + ' current: ' + currentIndex);
			
			if (scrollView.currentView.imageView.visible === false){
				scrollView.currentView.imageView.visible = true;
			};
			
			if (index != currentIndex) {
				Ti.API.info('PAGESCROLLVIEW  NEWINDEX: '+index);
				
				updateViews(index);
				
				win.bottomBar.clear();
				win.centerTitle.update(dictionary.winTitle, dictionary.winSubtitle+' ( ' + (currentIndex + 1) + ' of ' + assetsCount + ' )');
				
				if(loadViewWithImageTimeout){
					clearTimeout(loadViewWithImageTimeout);
				};
				
				
				//Load more data to the array if necessary. 
				setTimeout(function(){
					var currentLength = win.imagesDataArray.length;
					//Ti.API.debug('CurrentLength: '+currentLength+' assetsCount:'+assetsCount )
					if(currentLength < assetsCount && currentIndex >= currentLength - 10 && win.loadingMoreData === false){
						win.loadingMoreData = true;
						win.fireEvent('loadMoreData',{startIndex: currentLength, endIndex:currentLength+10});	
					};
				},550);
				
				scrollView.currentView = visibleViews[currentIndex];
				
				if (win.instructions){
					win.instructions.closeAnimation();
					win.instructions = null;
				};
			};
			
			scrollView.beganScrolling = true;
		};
	});
	
	
	
	var loadViewWithImageTimeout;
	
	scrollView.addEventListener('scrollEnd', function(e){
		scrollCounter = 0;
		//Ti.API.info('scrollEnd');
		scrollView.beganScrolling = false;
	
		loadViewWithImageTimeout = setTimeout(function(){
			if(scrollView.viewWithImage.index != currentIndex){
				scrollView.viewWithImage.configureView(currentIndex);
			}
			else{
				if (scrollView.currentView.imageView.visible === true){
					scrollView.currentView.imageView.visible = false;
				};
			}
			
			Ti.API.debug('Loading Full Images array')
			Ti.API.debug(loadingFullImageIndexArray)
			if (visibleViews[currentIndex].imageView.hasFullImage === false && loadingFullImageIndexArray.indexOf(currentIndex)=== -1){
				//scrollView.loadingFullImagesCount++
				visibleViews[currentIndex].loadFullImage(currentIndex);
			};
		},300);
	});
	
	
	
	var viewWithImage = require('ui/windows/galleryWindow/viewWithImage');
	scrollView.viewWithImage = new viewWithImage(win.imagesDataArray[currentIndex],currentIndex,dictionary,win);
	
	scrollView.add(scrollView.viewWithImage);
	
	
	//Scrolling Methods
	var loadingFullImageIndexArray =[];//Array of indexes that are loading
	
	
	
	function firstVisibleIndex(index) {
		return index - scrollPadding;
	};
	
	function lastVisibleIndex(index) {
		return index + scrollPadding;
	};
	
	function isDisplayingPageForIndex (index){
		var isDisplaying = false;
		
		for (var i =0; i < visibleViews.length; i ++){
			if (visibleViews[i].index === index){
				isDisplaying = true;
				break;
			};
		};
		return isDisplaying;
	};

	function forEachVisibleView(index, doThis) {
		if (!doThis) {
			return;
		};
		
		
		for (var i = index; i <=lastVisibleIndex(index); i++){
			doThis.call(null, i);
		};
		
		var index2 = index > 0 ? index : 0;
		for (var i = index2-1; i >= firstVisibleIndex(index); i--){
			doThis.call(null, i);
		};
		
		
		/*
		for (var i = firstVisibleIndex(index); i <= lastVisibleIndex(index); i++) {
			doThis.call(null, i);
		}
		*/
	};
	
	function setViewIndex(view, index) {
		view.left = (win.screenWidth + 20) * index +10;
		
		if (index < 0 || index >= assetsCount) {
			view.visible = false;
		}
		else {
			view.visible = true;
		};
	};
	
	function moveViewToIndex(from, to) {
		view = visibleViews[from];
		visibleViews[to] = view;
		setViewIndex(view, to);
		updateViewImage(to);
		delete visibleViews[from];
	};

	function moveViewsToRight(count) {
		for (var i = 0; i < count; i++) {
			var firstVisible = firstVisibleIndex(currentIndex);
			var lastVisible = lastVisibleIndex(currentIndex);
			moveViewToIndex(firstVisible, lastVisible + 1);
			currentIndex++;
			win.currentIndex = currentIndex;
		};
	};

	function moveViewsToLeft(count) {
		for (var i = 0; i < count; i++) {
			var firstVisible = firstVisibleIndex(currentIndex);
			var lastVisible = lastVisibleIndex(currentIndex);
			moveViewToIndex(lastVisible, firstVisible - 1);
			currentIndex--;
			win.currentIndex = currentIndex;
		};
	};
	
	
	function createViewPool(index) {
		forEachVisibleView(index, function(i){
			var view = createView(index);
			visibleViews[i] = view;
			scrollView.add(view);
			setViewIndex(view, i);
		});
	};
	
	scrollView.loadingFullImagesCount = 0;
	function createView (index){
		var view = Ti.UI.createView({
				width: win.screenWidth,
				height: win.screenHeight,
				//backgroundColor: 'white',
				index: index,
				kind:'containerView'
			});
			
			
			var imageView = Ti.UI.createView({
				width: win.screenWidth,
				height: win.screenHeight,
				index: index,
				hasFullImage: false,
				//filter:[{name:'grayscale'}]
			});
					
			view.add(imageView);
			
			//Methods
			var loadFullImage = _.debounce(function(newIndex){
				function callback(backIndex, asset){
					//This limits the back log
					//scrollView.loadingFullImagesCount--
					loadingFullImageIndexArray.splice(loadingFullImageIndexArray.indexOf(backIndex),1);
					
					
					if (backIndex === imageView.index){
						imageView.backgroundImage = asset;
						//imageView.image = asset;
						imageView.hasFullImage = true;
						Ti.App.fireEvent('pageScrollViewLoadedFullImage',{index: backIndex});
					};
				};
				
				DataMedia.loadMediaAssetImage(imageView, newIndex, callback);
			}, 100, true);
			
			
			function clean(){
				view.remove(imageView);
				imageView = null;
				loadFullImage = null;
				Ti.API.debug('Cleaned Scrolling View')
			};
			
			//Expose
			view.imageView = imageView;
			view.loadFullImage = loadFullImage;
			view.clean = clean;
			
			return view;
	};
	
	//var fullLoadImageEnabled = true;
	
	function loadFullImage (imageView, newIndex){
		function callback(backIndex, asset){
			//fullLoadImageEnabled = true;
			//This limits the back log
			scrollView.loadingFullImagesCount--
			loadingFullImageIndexArray.splice(loadingFullImageIndexArray.indexOf(backIndex),1);
			
			if (backIndex === imageView.index){
				imageView.backgroundImage = asset;
				//imageView.image = asset;
				imageView.hasFullImage = true;
				Ti.App.fireEvent('pageScrollViewLoadedFullImage',{index: backIndex});
			};
		};
		
		Ti.API.debug('Loading Image Count: '+scrollView.loadingFullImagesCount);
		if (scrollView.loadingFullImagesCount <= 1){
			loadingFullImageIndexArray.push(newIndex);
			scrollView.loadingFullImagesCount++
			DataMedia.loadMediaAssetImage(imageView, newIndex, callback);
		};
	};
	
	
	function updateViewImage(index) {
		if (index < 0 || index >= assetsCount || (index === visibleViews[index].imageView.index && visibleViews[index].imageView.hasFullImage === true)) {
			return;
		};
		
		var imageData = win.imagesDataArray[index];
		var imageView = visibleViews[index].imageView;
		imageView.backgroundImage = imageData.aspectRatioThumbnail;
		imageView.hasFullImage = false;
		imageView.URL = imageData.URL;
		imageView.index = index;
		imageView.width = imageData.aspectRatioThumbnail.width;
		imageView.height = imageData.aspectRatioThumbnail.height;
		setTimeout(function (){
			imageView.visible = true; //Might have been turned off by scroll of viewWithImage
		},10);
		
		
		resizeImage(visibleViews[index]);
		/*
		if (imageView.hasFullImage === false && scrollView.loadingFullImagesCount <= 1){	
			scrollView.loadingFullImagesCount++
			Ti.API.debug('LOADING FULL IMAGE COUNT: '+scrollView.loadingFullImagesCount);
			loadingFullImageIndexArray.push(index);
			visibleViews[index].loadFullImage(index);
		};
		*/
		if (imageView.hasFullImage === false ){	
			loadFullImage(imageView,index);
		};
	};
	
	
	function updateViews (index){
		//Ti.API.debug('index: '+index+'Current Index: '+currentIndex)
		//Ti.API.info('updateViews index:' + index);
		
		//var view;
		//var newViewIndex;
	
		//var firstVisible = firstVisibleIndex(index);
		//var lastVisible = lastVisibleIndex(index);

		var nViewsToMove = index - currentIndex;
		//nViewsToMove = nViewsToMove < 0 ? -nViewsToMove : nViewsToMove;
		//Ti.API.info('nviews: ' + nViewsToMove);

		if (nViewsToMove < 0) {
			nViewsToMove = -nViewsToMove;
			moveViewsToLeft(nViewsToMove);
		}
		else {
			moveViewsToRight(nViewsToMove);
		};
	};
	

	function resizeImage (view1){
		var newSize = Helper.imageResize2(win.screenWidth, win.screenHeight, view1.imageView.width, view1.imageView.height);
		view1.imageView.width = newSize.width;
		view1.imageView.height = newSize.height;
		
		if (((view1.imageView.width-win.screenWidth)/2) >=0){
			view1.imageView.left = -(view1.imageView.width-win.screenWidth)/2;
		}
		else{
			view1.imageView.left = null;
		};
		
		
		if ((view1.imageView.height - win.screenHeight)/2 >= 0) {
			view1.imageView.top = -(view1.imageView.height - win.screenHeight)/2
		}
		else{
			view1.imageView.top = null;
		};
	
		//Ti.API.debug(view1.imageView.top);
		view1.width = win.screenWidth;
		view1.height = win.screenHeight;
	};
	
	
	
	//Rotation
	function rotatePages(){
		for (var i =0; i < visibleViews.length; i++){
			if(visibleViews[i]!= null){
				resizeImage(visibleViews[i]);
				visibleViews[i].left = visibleViews[i].imageView.index * (win.screenWidth+20)+10;
			};
		};
	};
	
		
	//Handles rotation of the view. 
	function orientationChange (e){
		scrollView.opacity = 0;
		scrollView.zoomScale = 1;
		scrollView.width = win.screenWidth+20;
		scrollView.height= win.screenHeight;
		scrollView.contentHeight = win.screenHeight;
		scrollView.contentWidth = (win.screenWidth+20)*win.imagesDataArray.length+1;
		scrollView.viewWithImage.rotateImage();
		rotatePages();
		scrollView.setContentOffset({x: currentIndex*(win.screenWidth+20),y:0},{animated:false});
	};
	///
	
	function closingAnimation (){
		scrollView.viewWithImage.closeAnimation();
		
		scrollView.animate(Ti.UI.createAnimation({
			delay: 0,
			//color: 'white',
			duration: 200,
			//transform: Ti.UI.create2DMatrix().scale(0),
			width: 320,
			height: 1,
			curve: Ti.UI.iOS.ANIMATION_CURVE_EASE_IN_OUT
		}));
	};
	
	
	function clean(){
		var children = scrollView.children;
		for(var i = children.length-1 ; i >= 0;  i--){
			if(children[i].clean){children[i].clean()}
			scrollView.remove(children[i]);
			children[i] = null;
		};
		Ti.API.debug('Cleaned PageScrollView');
	};
	
	
	//Expose
	scrollView.orientationChange = orientationChange;
	scrollView.clean = clean;
	scrollView.closingAnimation = closingAnimation;
	
	
	//Initialize
	win.scrollableGalleryView.currentView = visibleViews[currentIndex];
	scrollView.viewWithImage.configureView(currentIndex);
	
	win.addEventListener('finishedLoadingMoreData', function (){
		scrollView.contentWidth = (win.screenWidth+20) * win.imagesDataArray.length+1;
	});


	return scrollView;
};