//Creates the ScrollView wiht pages element. Optimized to use only two items at the time. 

module.exports = function pageScrollView(assetsCount,currentIndex,win,dictionary){
	
	var Helper = require('lib/Helper');
	var _ = require('lib/underscore');
	var DataMedia = require('model/DataMedia');

	//Notes: assetsCount is given in case the whole array has not loaded yet.
	//win is necessary as a reference for the the functions outside the scrollView. Mainly to indentify
	//...the current image and the current scrollview that holds the image
	//dictionary holds the imagesData parameters
	
	var visibleViews = []; //Array to hold  visible views;

	var scrollPadding = 2;
	
	var scrollViewWidth = 340;
	var initialOffset = {x: currentIndex * scrollViewWidth, y: 0};

	var scrollDelta = 0;
	
	var userIsFastScrolling = false;

	var scrollView = Ti.UI.createScrollView({
		//backgroundColor:'yellow',
		width: win.screenWidth+20,
		height:480,
		contentWidth: (win.screenWidth+20) * win.imagesDataArray.length,
		contentHeight: 480,
		pagingEnabled: true,
		scrollType: 'horizontal',
		zoomScale:1,
		maxZoomScale:1,
		minZoomScale:1,
		anchorPoint: {x:0.5,y:0.5},
		hasBeenLayedOut: false,
		isRotating: false
	});
	
	win.scrollableGalleryView = scrollView;
	
	createViewPool(currentIndex);

	scrollView.setContentOffset(initialOffset);

	forEachVisibleView(currentIndex, updateViewImage);
	
	scrollView.currentImage = visibleViews[currentIndex];
	
	
	//Updates the ScrollView content when more data is loaded. Couldn't do it with just a reference for some reason.
	win.addEventListener('finishedLoadingMoreData', function (){
		scrollView.contentWidth = (win.screenWidth+20) * win.imagesDataArray.length;
	});


	function firstVisibleIndex(index) {
		return index - scrollPadding;
	};


	function lastVisibleIndex(index) {
		return index + scrollPadding;
	};


	function forEachVisibleView(index, doThis) {
		if (!doThis) {
			return;
		}
		for (var i = firstVisibleIndex(index); i <= lastVisibleIndex(index); i++) {
			doThis.call(null, i);
		};
	};


	function createViewPool(index) {
		forEachVisibleView(index, function(i) {
			var view = Ti.UI.createView({
				width: win.screenWidth,
				height: win.screenHeight,
				//backgroundColor: 'white',
				index:index,
				kind:'containerView'
			});
			
			var imageView = Ti.UI.createView({
				width: win.screenWidth,
				height: win.screenHeight,
			});
			
			view.add(imageView);
			view.imageView = imageView;
			
			visibleViews[i] = view;
			scrollView.add(view);
			setViewIndex(view, i);
		});
	};
	
	win.scrollableGalleryView.currentView = visibleViews[currentIndex];


	function setViewIndex(view, index) {
		view.left = (win.screenWidth+20) * index +10;
		if (index < 0 || index >= win.imagesDataArray.length) {
			view.visible = false;
		}
		else {
			view.visible = true;
		};
	};


	function updateViewImage(index) {
		if (index < 0 || index >= win.imagesDataArray.length) {
			return;
		};
		
		//visibleViews[index].width = win.screenWidth;
		//visibleViews[index].height = win.screenHeight;
		
		var imageData = win.imagesDataArray[index];
		var newSize = Helper.imageResize2(win.screenWidth, win.screenHeight, imageData.aspectRatioThumbnail.width, imageData.aspectRatioThumbnail.height);
		
		var image = visibleViews[index].imageView;
		image.backgroundImage = imageData.aspectRatioThumbnail;
		image.hasFullImage = false;
		image.width = newSize.width;
		image.height = newSize.height;
		
		var top; 
		if ((image.height - win.screenHeight)/2 > 0) {
			top = -(image.height - win.screenHeight)/2
		};
		//Ti.API.debug('TOP: '+top);
		image.left = -(image.width-win.screenWidth)/2;
		image.top = top;
		//Ti.API.debug('yOffset Image: '+image.top);
		image.URL = imageData.URL;
		image.index = index;
		image.visible = true;
		
		
		if (userIsFastScrolling === false){
			DataMedia.loadMediaAssetImage(image, index);
		};
	};

	
	function moveViewToIndex(from, to) {
		//Ti.API.debug('Visible Views Length: '+visibleViews.length)
		view = visibleViews[from];
		visibleViews[to] = view;
		setViewIndex(view, to);
		updateViewImage(to);
		delete visibleViews[from];
	};


	function moveViewsToRight(count) {
		for (var i = 0; i < count; i++) {
			var firstVisible = firstVisibleIndex(currentIndex);
			var lastVisible = lastVisibleIndex(currentIndex);
			moveViewToIndex(firstVisible, lastVisible + 1);
			currentIndex++;
		};
	};


	function moveViewsToLeft(count) {
		for (var i = 0; i < count; i++) {
			var firstVisible = firstVisibleIndex(currentIndex);
			var lastVisible = lastVisibleIndex(currentIndex);
			moveViewToIndex(lastVisible, firstVisible - 1);
			currentIndex--;
		};
	};
	
	
	function updateViews (index){
		//var view;
		//var newViewIndex;
	
		var firstVisible = firstVisibleIndex(currentIndex);
		var lastVisible = lastVisibleIndex(currentIndex);

		var nViewsToMove = index - currentIndex;
		//Ti.API.debug('Updating '+nViewsToMove+' Views')
		
		if (nViewsToMove < 0) {
			nViewsToMove = -nViewsToMove;
			moveViewsToLeft(nViewsToMove);
		}
		else {
			moveViewsToRight(nViewsToMove);
		};
	};
	
	
	function isDisplayingPageForIndex (index){
		var isDisplaying = false;
		
		for (var i =0; i < visibleViews.length; i ++){
			if (visibleViews[i] != null && visibleViews[i].index === index){
				isDisplaying = true;
				break;
			};
		};
		return isDisplaying;
	};

	//Loading initial viewWithImage
	var viewWithImage = require('ui/windows/galleryWindow/viewWithImage');
	scrollView.viewWithImage = new viewWithImage(win.imagesDataArray[currentIndex],currentIndex,dictionary,win);
	scrollView.add(scrollView.viewWithImage);


	var scrollStartX = initialOffset.x;
	var scrollDirection = 0;
	
	//Timeouts For Delayed loading
	var loadViewWithImageTimeout;
	var loadTimeouts = [];
	var lastScrollTime = 0;
	
	scrollView.addEventListener('scroll', function(e){
		if (e.source != scrollView) {
			return;
		};
		
		if (scrollView.isRotating === true){
			return;
		};

		e = scrollView.contentOffset;
		//Avoid scrolling outside of boundaries
		if (e.x < 0){
			e.x = 0;
		};
		
		if (e.x > (win.imagesDataArray.length - 1) * (win.screenWidth+20)) {
			e.x = (win.imagesDataArray.length - 1) * (win.screenWidth+20);
		};

		scrollDelta = e.x - scrollStartX;
		scrollStartX = e.x;
		//Ti.API.debug(scrollDelta+' ScrollDelta')
/*
		if (scrollDelta >= 340 || scrollDelta <= -340) {
			//Ti.API.info('watch out! scrollDelta=' + scrollDelta);
			scrollView.beganScrolling = false;
		}

*/
		var index = e.x / (win.screenWidth+20);
		
		if(scrollDelta >0 && e.x >= currentIndex * (win.screenWidth+20)+10+(win.screenWidth-10)){
			index = Math.ceil(index);
		}
		else if (scrollDelta < 0 && e.x <= currentIndex * (win.screenWidth+20)+10-(win.screenWidth-10)){
			index = Math.floor(index);
		}
		else{
			index = currentIndex;
		};
		
		//Filter to avoid multiple low because of bounce. 
		if(index > currentIndex +1){
			index = currentIndex+1
		}
		else if (index < currentIndex-1){
			index = currentIndex-1
		};
		

		
		if (index != currentIndex) {	
			//Determine if the user is Fastscrolling.
			//This helps avoid during extra work for no reason. 
			//Like loading full assets. 
			var thisScrollTime = new Date().getTime();
			if(thisScrollTime - lastScrollTime < 700){
				userIsFastScrolling = true;
				Ti.API.info('USER IS FASTSCROLLING GALLERY WINDOW '+(thisScrollTime - lastScrollTime));
			}
			else{
				userIsFastScrolling = false;
			};
			
			lastScrollTime = new Date().getTime();
			
			
			//visibleViews[currentIndex].visible = true; //It's still the old currentIndex before updateViews'
			//scrollView.viewWithImage.visible = false
			
			//Show the current View;
			updateViews(index);
			Ti.API.info('New index '+currentIndex);
			
			win.bottomBar.clear();
			
			//Update Title
			win.centerTitle.update(dictionary.winTitle, dictionary.winSubtitle+' ( ' + (currentIndex + 1) + ' of ' + assetsCount + ' )');
			
			//Clear Timeouts
			//clearTimeout(loadViewWithImageTimeout);
			//Ti.API.debug('Number of Timeouts '+loadTimeouts.length);
			for (var i = 0; i < loadTimeouts.length; i++ ){
				clearTimeout(loadTimeouts[i]);
			};
			
			loadTimeouts = [];
			
			
			var loadViewWithImageTimeout = setTimeout(function(){
				//Ti.API.debug("Configuring View");
				scrollView.viewWithImage.configureView(currentIndex);
			},500);
			
			loadTimeouts.push(loadViewWithImageTimeout);
			
			
			//Load more data to the array if necessary. 
			setTimeout(function(){
				var currentLength = win.imagesDataArray.length;
				
				if(currentLength < assetsCount && currentIndex >= currentLength - 10 && win.loadingMoreData === false){
					win.loadingMoreData = true;
					win.fireEvent('loadMoreData',{startIndex: currentLength, endIndex:currentLength+10});	
				};
			},550);
			
			scrollView.currentIndex = currentIndex;
			scrollView.currentView = visibleViews[currentIndex];
			scrollView.currentImage = visibleViews[currentIndex].imageView;
		}
		/*
		else if (scrollView.viewWithImage.hasBeenReset === false){
			scrollView.viewWithImage.resetView();
		};
		*/
		
		
		
	});
	
	
	function rotateImage (view1){
		//Ti.API.debug('I am '+view1.kind)
		var newSize = Helper.imageResize2(win.screenWidth, win.screenHeight, view1.imageView.width, view1.imageView.height);
		view1.width = win.screenWidth;
		view1.height = win.screenHeight;
		view1.imageView.width = newSize.width;
		view1.imageView.height = newSize.height;
		view1.imageView.left = -(view1.imageView.width-win.screenWidth)/2;
		var top; 
		if ((view1.imageView.height - win.screenHeight)/2 > 0) {
			top = -(view1.imageView.height - win.screenHeight)/2
		};
		view1.imageView.top = top;
		//Ti.API.debug(view1.imageView.top);
		view1.left = view1.imageView.index * (win.screenWidth+20)+10;
	};
	
	
	//Rotates each image
	function rotatePages(){
		Ti.API.debug(visibleViews)
		for (var i =0; i < visibleViews.length; i++){
			if(visibleViews[i]!= null){
				rotateImage(visibleViews[i]);
			};
		};
	};
	
	var currentOrientation = getCurrentOrientation();
	var lastOrientation = currentOrientation;
		
	//Handles rotation of the view. 
	function orientationChange (e){
		scrollView.isRotating = true;
		currentOrientation = e? e.orientation: currentOrientation;
		//Exclude undefined, faceup and facedown orientations.
		if (currentOrientation != 5 && currentOrientation != 6 && currentOrientation != 0 && currentOrientation != lastOrientation ){
			Ti.API.debug('Current Orientation '+ currentOrientation);
			lastOrientation = currentOrientation;
				
			if(currentOrientation === 3 || currentOrientation === 4){
				win.screenWidth = 480;
				win.screenHeight = 320;	
				globals.currentTabGroup.navBar.rotate('landscape');
			}
			else if (currentOrientation === 1 || currentOrientation === 2){ 
				win.screenWidth = 320;
				win.screenHeight = 480;
				globals.currentTabGroup.navBar.rotate('portrait');
			};
			
			if(win.selectionView){win.selectionView.orientationClosing()};
				
			scrollView.width = win.screenWidth+20;
			scrollView.height= win.screenHeight;
			scrollView.contentHeight = win.screenHeight;
			scrollView.contentWidth = (win.screenWidth+20)*win.imagesDataArray.length;
			
			scrollView.viewWithImage.rotateImage();
			rotatePages();
			scrollView.setContentOffset({x: currentIndex*(win.screenWidth+20),y:0},{animated:false});
			Ti.API.debug(scrollView.contentOffset.x);
		};	
		scrollView.isRotating = false;	
	};
	
	
	function clean(){
		clearTimeout(loadViewWithImageTimeout);
		scrollView.viewWithImage.clean();
		
		var children = scrollView.children;
		for(var i =0; i < children.length; i++){
			//children[i].clean();
			scrollView.remove(children[i]);
			children[i] = null;
		};
		
		Ti.API.debug('Cleaned PageScrollView')
	};
	
	scrollView.orientationChange = orientationChange;
	scrollView.clean = clean;

	return scrollView;
};


//Functions that gets initial Orientation
function getCurrentOrientation (){
	var screenWidth = Ti.Platform.displayCaps.platformWidth;
	var screenHeight = Ti.Platform.displayCaps.platformHeight;
	var orientation; 
	if (screenWidth > screenHeight) {
	    orientation = 3;
	} else {
	    orientation = 1;    
	};
	
	return orientation;
};