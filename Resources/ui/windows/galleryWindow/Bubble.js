module.exports = function (bubbleData, dictionary, image,parentScrollView,win){
	
	var view = Ti.UI.createView({
		backgroundColor: 'transparent',
		width: Ti.UI.SIZE,
		height: 40,
		left: 160,//Should be bubbleData.x, but doing this otherwise the label gets shrunk. Look in Postlayout.
		top: bubbleData.y,
		realPosition:{x: bubbleData.x, y: bubbleData.y}, //The real position before shifted by exepctions. 
		kind: 'bubbleMainView',
		readyToDelete: false,
		dragEnabled: false,
		data: bubbleData,
		justTouched: false, //Here to emulate longpress for dragDropKit,
		parent: parentScrollView,
		opacity: 0,
		positioned: false,
		anchorPoint:{x:.5,y:1}
	});
	
	
	var bg = Ti.UI.createView({
		backgroundImage: 'images/tag_bubble.png',
		width: Ti.UI.SIZE,
		height: 22,
		backgroundLeftCap: 20,
		top: 6,
		kind: 'bubble',
		touchEnabled: false,	
	});
	
	
	var label = Ti.UI.createLabel({
		text: bubbleData.title,
		textAlign: 'center',
		color: 'white',
		height: 15,
		font:{fontSize: 12, fontWeight: 'bold'},
		width: Ti.UI.SIZE,
		top:2,//14,
		left: 5,
		right:5,
		kind: 'bubble',
		touchEnabled: false,
		zIndex:1
	});
	
	label.addEventListener('postlayout', function(){
		//Ti.API.debug('label postlayout');
		//This is to Freeze the dimensions.
		//otherwise if you drag the bubble to the right of the screen.
		// it shirnks when it gets close to the edge.
		label.width = label.size.width;
		bg.width = bg.size.width;
		view.width = view.size.width;
		
		repositionBubble(bubbleData.x, bubbleData.y-30/parentScrollView.image.height);
		appearAnimation();
	});
	
	bg.add(label);
	
	
	var topArrow = Ti.UI.createView({
		backgroundImage:'images/top_arrow.png',
		width: 18,
		height: 10,
		left: 0,
		top: bg.top+bg.height-1,
		touchEnabled: false,
		transform: Ti.UI.create2DMatrix().rotate(180),
		kind: 'bubble'
	});
	
	
	var deleteButton = Ti.UI.createView({
		backgroundImage:'images/delete_circle.png',
		width: 15,
		height: 15,
		left: 0,
		top: 0,
		touchEnabled: true,
		kind: 'bubble Delete',
		visible: false,
		transform: Ti.UI.create2DMatrix().scale(1),
		opacity: 0
	});
	
	
	view.addEventListener('touchstart', function (e){
		parentScrollView.scrollEnabled = false;
		win.touchedBubble = true;
		parentScrollView.image.currentBubble = view;
		
		
		//Brings the view to the front. 
		for (var i = 0; i < parentScrollView.image.children.length; i++){
			var bubble = parentScrollView.image.children[i];
			
			if (bubble.kind && bubble.kind === 'bubbleMainView' && bubble!= view){
				bubble.zIndex = 9999-i;
			}
			else if (bubble.kind === 'bubbleMainView' && bubble == view){
				bubble.zIndex = 9999;
			}			
		};
		
		
		//This is a workaround for the dragDropKit. 
		//It is also good cause I am not limited to the small delete area button
		view.justTouched = true;
		if (deleteButton.visible === true){
			deleteView();
		}
		else {
			//dragDropKit workaround for longpress. 
			setTimeout(function (){
				if(win.moveModeOn === false && view.justTouched === true){
					win.currentBubble = view;
					
					view.animate(Ti.UI.createAnimation({
						transform: Ti.UI.create2DMatrix().scale(1.2*1/parentScrollView.zoomScale),
						opacity: .9,
						duration: 150
					}));
					view.scale = 1.2;
					
					topArrow.left = view.size.width/2 - topArrow.width/2;
					view.anchorPoint= {x:((topArrow.left+topArrow.width/2)/view.size.width),y:1};
					/*
					topArrow.animate({
						transform:Ti.UI.create2DMatrix().translate((view.size.width/2-(topArrow.left+topArrow.width/2)),0),
						duration: 150
					});
					*/
					win.toggleMoveMode();
				};
			}, 250);
		};
	});
	
	
	view.addEventListener('touchend', function (e){
		view.justTouched = false; //This is to fake the long press. Not present in drag kit.
	});
	
	
	///Methods
	
	function deleteView (){
		var DataBubbles = require('model/DataBubbles');
		DataBubbles.deleteBubbleWithX(bubbleData.$id);
		
		disappearAnimation();
		
		setTimeout(function (){
			win.currentImage.removeBubble(view);
		}, 200);
	};
	
	
	function deleteDisappear (){
	 	deleteButton.animate({
	 		transform: Ti.UI.create2DMatrix().scale(0),
	 		opacity: 0,
	 		duration: 100
	 	});
	 	
	 	setTimeout( function (){
	 		deleteButton.visible = false;
	 	}, 150);
	};
	
	
	function deleteAppear  (){
	 	deleteButton.visible = true;
	 	deleteButton.animate({
	 		transform: Ti.UI.create2DMatrix().scale(1),
	 		opacity: 1,
	 		duration: 100
	 	});
	
	 	setTimeout(deleteDisappear, 2000);
	};
	 
	
	var appearAnimation = function (){
		//view.opacity = 1;
	
		view.animate(Ti.UI.createAnimation({
			duration: 200,
			opacity: 1,
			curve: Ti.UI.iOS.ANIMATION_CURVE_EASE_IN_OUT,
			//transform: Titanium.UI.create2DMatrix().scale(1/parentScrollView.zoomScale)
		}));
		
	};
	
	
	var disappearAnimation = function (){
		view.opacity = 0;
		
		/*
		view.animate(Ti.UI.createAnimation({
			opacity:0,
			duration: 100,
			transform: Titanium.UI.create2DMatrix().scale(0)
		}));
		*/
	};
	
	
	function recycleBubble (){
		view.left = 0;
		view.top = 0;
		view.realPosition = {x: 0, y: 0};
		view.readyToDelete = false;
		view.dragEnabled = false;
		view.data = null;
		view.justTouched =  false;
		view.opacity = 0;
		view.positioned = true;
	};
	
	
	function configureBubble(newBubbleData){
		//view.top = newBubbleData.y-5;
		//view.left = newBubbleData.x;
		view.realPosition = {x: newBubbleData.x, y: newBubbleData.y}; //The real position before shifted by exepctions. 
		view.data = newBubbleData;
		
		label.width = Ti.UI.SIZE;//Must do this otherwise postlayout won't fire;
		bg.width = Ti.UI.SIZE;
		view.width = Ti.UI.SIZE;
		
		label.text = newBubbleData.title;		
		repositionBubble(newBubbleData.x, newBubbleData.y);
		appearAnimation();
	};
	
	
	function repositionBubble(x, y){ //Repositions bubble and top arrow if close to the edges. 
		parentScrollView.image.detachBubble(view);
		
		var realX = x*parentScrollView.image.width;
		var realY = y*parentScrollView.image.height;
		
		//Ti.API.debug('Real X Left: '+(realX -view.size.width/2)+' Real X Right: '+(realX +view.size.width/2));
		
		
		//Horizontal Border Behaviour
		var leftEdge = (parentScrollView.image.width-win.screenWidth)/2;
		var rightEdge = parentScrollView.image.width-(parentScrollView.image.width-win.screenWidth)/2;
	
		if (realX - view.size.width/2 < leftEdge){
			view.left = leftEdge;
			topArrow.left = realX -topArrow.width/2+5-leftEdge;
		}
		else if (realX + view.size.width/2 > rightEdge ){
			view.left  = rightEdge-view.size.width;
			topArrow.left = realX - view.left -topArrow.width/2-10+leftEdge;
		}
		else { //Normal position
			view.left = realX -view.size.width/2; //Using the bubbleData here to avoid label shrinking. 	
			topArrow.left = view.size.width/2-topArrow.width/2;	
		};
		
		
		//Vertical Border Behaviour
		var topEdge = (win.screenHeight - parentScrollView.image.height) < 40 ? 40 : 0;
		var bottomEdge = (win.screenHeight - parentScrollView.image.height) < 40 ? (parentScrollView.image.height -(35+40)) : (parentScrollView.image.height -35);
		
		if (realY < topEdge ){
			view.top = topEdge;
		}
		else if (realY > bottomEdge){
			//To Avoid bottom bar
			view.top = bottomEdge;
		}
		else {
			view.top = realY;
		};
		
		
		parentScrollView.image.attachBubble(view);
		
		view.anchorPoint = {x:((topArrow.left+topArrow.width/2)/view.size.width),y:1};
	};
	
	
	//Splitting this function. Need to be able to call it when by mistake you are not touching the bubble
	//anymore but you are still touching the screen.
	function stoppedTouchingBubble (){
		win.currentImage.currentBubble = null;
		parentScrollView.scrollingEnabled = true;
		parentScrollView.image.currentBubble = null;
		//Setting timer to prevent the image to get the touchEnabled = true parameter in time
		//For ToggleUI to be enabled. 
		setTimeout(function (){
			win.touchedBubble = false;
		}, 600);
		
		view.justTouched = false; //dragDropKit workaround for longpress. 
		
		if(win.moveModeOn === true && view == win.currentBubble){	
			
			view.realX = (view.left + topArrow.left + topArrow.width/2); 
			
			if (view.realX > (win.screenWidth-10)){view.realX = win.screenWidth-10}
			else if(view.realX < 10){view.realX = 10}
			
			if (view.top > parentScrollView.image.size.height  - 40){
				view.realY = view.top - 40;
			}
			else{
				view.realY = view.top
			}
			repositionBubble(view.realX/parentScrollView.image.width,view.realY/parentScrollView.image.height);
			
			view.animate(Ti.UI.createAnimation({
				transform: Ti.UI.create2DMatrix().scale(1*1/parentScrollView.zoomScale),
				opacity: 1,
				duration: 50
			}));
			
			var dataToSend = {};
			dataToSend.$id = view.data.$id;
			dataToSend.x = view.realX/parentScrollView.image.width;
			dataToSend.y = (view.realY+30)/parentScrollView.image.height; //The +5 is to accomodate for everything being down in the view 5px. 
			
			var DataBubbles = require('model/DataBubbles');
			DataBubbles.updateBubble(dataToSend);
			
			/*
			parentScrollView.dragObject.detach({
				view: view,
				container: parentScrollView.image.dragLayer
			});
			*/
			
			win.toggleMoveMode();
		}
		else{
			deleteAppear();
			win.deleteBubbleMode=true;
			
			//This blocks from tapping quickly while deleting and making new one comes up.
			//Also make sure you don't have to wait 2 seconds to add a new one. 
			setTimeout(function (){
				win.deleteBubbleMode = false;
			}, 1500);
		};	
	};
	
	
	function clean(){
		bg.remove(label);
		label = null;
		view.remove(bg);
		bg = null;
		view.remove(topArrow);	
		topArrow = null;
		view.remove(deleteButton);	
		deleteButton = null;
	};
	
	
	//Assemble
	view.add(bg);
	view.add(topArrow);	
	view.add(deleteButton);	
	
	
	
	//Exposing 
	//view.deleteButton = deleteButton;
	view.appearAnimation = appearAnimation;
	view.disappearAnimation = disappearAnimation;
	view.stoppedTouchingBubble = stoppedTouchingBubble;
	view.recycleBubble = recycleBubble;
	view.configureBubble = configureBubble;
	//view.toggleComeUP = toggleComeUP;
	view.clean = clean;
	
	
	return view;
};
