exports.loadWin = function (personID){
	var Window = require('ui/components/Window');
	var TopNavigation = require('ui/components/TopNavigation');
	var MiscComponents = require('ui/components/MiscComponents');
	
	//Create Window
	var win = Window.createWindow({
		tabBarHidden: true,
		tabbedBarHidden: true
	});
	win.clickable = true;
	
	
	//Set Navigation
	var menuButton = TopNavigation.menuButton();
	win.leftNavButton = menuButton;

	var centerTitle = TopNavigation.centerSingleLabel(I('Feedback'), 'white');// TopNavigation.centerDoubleLabel('', '', 'white');
	win.centerTitle = centerTitle;
	win.titleControl = centerTitle;
	
	
	//Create Tableview
	var tableData = [];
	
	var magazineRow = createRow(I('Magazines_Covers'), I('Turn_your_pictures'), '/images/magazine_icon_grey.png', '99c', false);
	
	magazineRow.addEventListener('tryButton', function (e){
		magazineTry();
	});
	
	magazineRow.addEventListener('buyButton', function (e){
		magazineBuy();
	});
	
	tableData.push(magazineRow);
	
	var tableView = MiscComponents.standardTableView();
	tableView.separatorStyle = 1;
	tableView.showVerticalScrollIndicator = false;
	tableView.data = tableData;
	
	
	//Assemble Window
	win.add(tableView);

	
	return win;
};


var createRow = function (titleText, descriptionText, iconImage, priceText,hasBeenPurchased){
	var Labels = require('ui/components/Labels');
	var MiscComponents = require('ui/components/MiscComponents');
	
	var row = Ti.UI.createTableViewRow({
		backgroundColor:'transparent',
		className: 'shopRow',
		height: Ti.UI.SIZE,
		editable: false,
		enabled: false,
		//leftImage: data.image
		//layout: 'vertical'
	});
	
	var icon = Ti.UI. createView({
		backgroundImage: iconImage,
		//backgroundColor: 'red',
		left: 15,
		width: 27,
		height: 34,
		top: 10
	});
	
	var rightSideView = Ti.UI.createView({
		//backgroundColor: 'yellow',
		left: 50,
		top: 10,
		width: 320-60,
		height: Ti.UI.SIZE,
		layout: 'vertical'
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: titleText,
		color: globals.fontColors[2],
		font: {fontWeight: 'bold', fontSize: 20},
		minimumFontSize: 18,
		shadowColor: '#fff',
		shadowOffset:{x:1,y:1},
		width: 260,
		left: 0,
		//top: 10,
		height:20
	});
	
	var descriptionLabel = Ti.UI.createLabel({
		text: descriptionText,
		font:{fontSize:15},
		color: globals.fontColors[2],
		left: 0,
		//top: 30,
		textAlign: 'left',
		width: 260,
		height: Ti.UI.SIZE
	});
	
	
	var buttonContainer;
	var purchasedLabel;
	
	function changeStatus (status){
		if (status === 'unpurchased'){
			if (purchasedLabel){rightSideView.remove(purchasedLabel)};
			
			buttonContainer = Ti.UI.createView({
				//backgroundColor: 'red',
				width: 260,
				left: 0,
				top:10,
				bottom:10,
				height: 44
			});
			
			var tryButton = MiscComponents.bigGrayButton(I('Try'));
			tryButton.left = 0;
			
			tryButton.addEventListener('click', function (){
				row.fireEvent('tryButton');
			});
			
			var buyButton = MiscComponents.bigGrayButton(I('Buy')+' '+priceText);
			buyButton.right = 15;
			
			buyButton.addEventListener('click', function (){
				row.fireEvent('buyButton');
			});
			
			buttonContainer.add(tryButton)
			buttonContainer.add(buyButton);
			
			rightSideView.add(buttonContainer);
		}	
		else if (status === 'purchased'){
			if(buttonContainer){rightSideView.remove(buttonContainer)};
			
			purchasedLabel = Ti.UI.createLabel({
				text: I('Purchased'),
				color: globals.fontColors[2],
				font: {fontWeight: 'bold', fontSize: 20},
				minimumFontSize: 14,
				shadowColor: '#fff',
				shadowOffset:{x:1,y:1},
				width: 260,
				left: 0,
				//top: 10,
				height:20
			});
			
			rightSideView.add(purchasedLabel);
		}
	};
	
	
	rightSideView.add(titleLabel);
	rightSideView.add(descriptionLabel);
	changeStatus('unpurchased');
	
	row.add(icon);
	row.add(rightSideView);
	
	function clean(){
		var children = row.children;
		for(var i = children.length-1 ; i >= 0;  i--){
			row.remove(children[i]);
			children[i] = null;
		};
		Ti.API.debug('Cleaned Row');
	};
	
	row.titleLabel = titleLabel;
	row.descriptionLabel = descriptionLabel;
	row.clean = clean;
	
	
	return row;
};



function magazineTry (){
	var assetslibrary = require('ti.assetslibrary');
		
	var successCb = function(e) {
		var ModalPictureSelector = require('/ui/windows/ModalPictureSelector');
		var win1 = ModalPictureSelector.loadWin(e.groups[0]);
		
		win1.addEventListener('pickedPhoto', function (e){
			var PosterDetail = require('ui/windows/PosterDetail');
			
			function successAsset (e){
				var MagazineWin = PosterDetail.loadWin({image:e.asset.defaultRepresentation.fullScreenImage}, true);
				MagazineWin.open();
			};
			
			function errorAsset(e){
				alert(e)
			};
			
			var DataMedia = require('model/DataMedia');
			DataMedia.getSingleAssetFromURL(e.URL,successAsset, errorAsset)
			
			win1.closingAnimation();
		});
		
		win1.open();
	};	
	
	var errorCb = function(e) {
		Ti.API.error('error: ' + e.error);
	};
	
	//Create the data for the rows. 
	var groupT = assetslibrary.getGroups([assetslibrary.AssetsGroupTypeSavedPhotos], successCb, errorCb);
};

function magazineBuy (){
	alert('Magazine Buy');
};
