exports.loadWin = function (group){
	var DataMedia = require('/model/DataMedia');
	var DataPersons = require('model/DataPersons');
	var TopNavigation = require('ui/components/TopNavigation');
	var MiscComponents = require('ui/components/MiscComponents');
	var Window = require('ui/components/Window');
	var PicturesScrollView = require('ui/components/PicturesScrollView');
	
	//Create Window
	var win = Ti.UI.createWindow({
		width: '100%',
		height: '100%',
		tabbedBarHidden: true,
		navBarHidden: true,
		tabBarHidden: true,
		//backgroundColor : '#3A3D3B',
		//backgroundGradient : { type:'linear', colors:['#535353', '#232323'], startPoint:{ x:0, y:0 }, endPoint:{ x:0, y:480 }, backFillStart:false },
		//barColor: '#000',
		opacity: 1,
		//transform:Ti.UI.create2DMatrix().scale(1.2),
		//translucent : true,
		orientationModes: [Titanium.UI.PORTRAIT],
		transform: Titanium.UI.create2DMatrix().translate(0,480),
		kind: 'modalPicturePicker',
		selectionMode: 'single'
	});
	
	var popBg = Ti.UI.createView({
		backgroundColor: 'black',
		borderColor: 'white',
		borderWidth:4,
		borderRadius:5,
		width: 320,
		height: Ti.Platform.displayCaps.platformHeight,
		//top: 20,
		//left: 10,
		opacity: .8,
		//zIndex:9999
	});
	
	
	globals.currentModal = win;
	
	///// NavBar
	var navBar = Ti.UI.createView({
		width: 320,
		height: 44,
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundImage:'images/nav_x.png',
		//backgroundColor: 'red',
		width: 72,
		height: 44,
		top: 0,
		left: 0
	}); //TopNavigation.backButton(win,'white');
	
	closeButton.add(Ti.UI.createView({
		backgroundImage:'images/nav_x.png',
		width: 23,
		height: 23,
		top: 10,
		left: 10
	})); //TopNavigation.backButton(win,'white');
	
	closeButton.addEventListener('click', function (){
		closingAnimation();
	});
	
	var titleLabel = TopNavigation.centerSingleLabel(I('Select_Photo'),'white');
	titleLabel.opacity = 1;
	
	navBar.add(closeButton);
	navBar.add(titleLabel);
		
	win.add(popBg);
	win.add(navBar);
	
	
	var emptyListLabel = Ti.UI.createLabel({
		text: 'There are no pictures in this list',
		width: 300,
		height: Ti.UI.SIZE,
		top: 55,
		font: {fontSize: 14},
		textAlign: 'center',
		color: globals.fontColors[2],
		shadowColor: 'white',
		shadowOffset: {x:0,y:1},
		visible: false//numberOfAssets > 0 ? false : true
	});	
	
	
	var scrollView = new PicturesScrollView(group.numberOfAssets, MiscComponents.createStandardThumbnail,win);
	scrollView.clickable = true;
	
	scrollView.addEventListener('loadMoreData', function(e){
		DataMedia.getGroupAssets(group, scrollView.loadData, e.startIndex, e.endIndex);
	});
	
	var scrollViewClick = function(e){
		var thumb = e.source; 
		if (win.selectionMode === 'single'){
			win.fireEvent('pickedPhoto', {URL: e.source.URL});
		};
	};
	
	scrollView.addEventListener('click', function(e){
		if (scrollView.clickable === true){
			scrollViewClick(e);
			scrollView.clickable = false; 
			var clickTimeout = setTimeout(function(){
				scrollView.clickable = true; 
			},
			2000);
		};
	});	
	
	
	win.add(emptyListLabel);
	
	//Callback for the loading data function
	var update = function (data, assetsCount){
		scrollView.numberOfAssets = assetsCount;
		scrollView.refreshScrollView(data);
		win.add(scrollView);
		scrollView.selectionEnabled = true; 
		scrollView.selectedThumb = null;
		if(scrollView.numberOfAssets === 0){emptyListLabel.visible = true}
		else{emptyListLabel.visible = false}
	};
	
	function refreshData (){
		DataMedia.getGroupAssets(group,update,0,29);
	};
	
	refreshData();
	
	function toggleSelectMode (){
		if (win.selectionMode === 'multiple'){
			win.selectionMode = 'single';
			scrollView.clearSelectedThumbs();
		}
		else {
			var SelectMultipleOverlay = require('ui/components/SelectMultipleOverlay');
			SelectMultipleOverlay.appear(win, scrollView);
		};
	};
	
	function closingAnimation (){	//WARNING Don't name closeAnimation. it causes crashes'		
		win.animate(Ti.UI.createAnimation({
			duration: 250,
			//opacity: 0,
			transform: Titanium.UI.create2DMatrix().translate(0,480)
		}));
		
		setTimeout(function(){
			//clean();
			globals.currentModal = null;
			win.close();
		},250);
	};

	function openAnimation (){
		win.animate(Ti.UI.createAnimation({
			duration:  250,
			//opacity: 1,
			transform: Titanium.UI.create2DMatrix().translate(0,0)
		}));
	};
	
	function preClean (){
		scrollView.clean();
		win.remove(scrollView);
		scrollView = null;
		Ti.App.removeEventListener('updatePicturesList', refreshData);
	};
	
	
	//Expose
	win.preClean = preClean;
	win.scrollView = scrollView;
	win.toggleSelectMode = toggleSelectMode;
	win.closingAnimation = closingAnimation;
	
	//Event listeners and Cleanup 
	Ti.App.addEventListener('updatePicturesList', refreshData);
	
	win.addEventListener('focus', function(){
		if (scrollView.selectedThumb){
			scrollView.selectedThumb.toggleOverlay();
			scrollView.selectedThumb = null;
			scrollView.selectionEnabled = true; 
		};
	});
	
	win.addEventListener('open', function (){
		openAnimation();
	});
	
	
	return win;
};