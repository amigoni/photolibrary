exports.loadWin = function (){
	var Window = require('ui/components/Window');
	var TopNavigation = require('ui/components/TopNavigation');
	var PicturesList = require('ui/windows/PicturesList');
	var DataLabels = require('model/DataLabels');
	var MiscComponents = require('ui/components/MiscComponents');
	
	
	//Load Data
	var dataIn;

	//Create Window
	var win = Window.createWindow({
		tabBarHidden:true,
		tabbedBarHidden:false,
		kind: 'labels'
	});
	
	
	
	//Set Navigations
	//globals.tabGroup.navBar.backgroundImage = 'images/navbar_orange.png';
	//globals.tabGroup.navBar.currentBG = 'images/navbar_orange.png';
	
	var menuButton = TopNavigation.menuButton();
	/*
	var editButton = TopNavigation.editButton();
	
	editButton.addEventListener( 'click', function (){
		if (tableView.editing === true){
			for(var i = 0; i < tableView.data.length; i++){
				for(var j = 0; j < tableView.data[i].rows.length; j++){
					tableView.data[i].rows[j].countLabel.visible = true;
				};
			};
			tableView.editing = false;	
			editButton.label.text = 'edit';
		}
		else{
			for(var i = 0; i < tableView.data.length; i++){
				for(var j = 0; j < tableView.data[i].rows.length; j++){
					tableView.data[i].rows[j].countLabel.visible = false;
				};	
			};
			tableView.editing = true;	
			editButton.label.text = 'Done'
		};
	});
	*/
	
	var addButton = TopNavigation.addButtonFull();
	
	addButton.addEventListener('click', function (){
		tableView.editing = false;	
		var PopUp = require ('ui/components/PopUp');
		
		var newLabelPopUp = PopUp.labelPopUp();
		
		newLabelPopUp.addEventListener('ok', function(e){
			var newLabel = DataLabels.addLabel({name:e.value});

			//Open the new label detail window
			if (newLabel){
				var LabelDetail = require('ui/windows/LabelDetail'); 
				globals.currentStack.open(LabelDetail.loadWin(newLabel.$id),{animated:true});
				
				refreshData();
			};
		});
		
		newLabelPopUp.addEventListener('return', function (e){
			DataLabels.addLabel({name: e.value});
		});
		
		newLabelPopUp.open();
	});
	
	var centerTitle = TopNavigation.centerSingleLabel('Labels','white');
	
	win.leftNavButton = menuButton;
	win.rightNavButton = addButton;
	win.centerTitle = centerTitle;
	win.titleControl = centerTitle;
	
	
	
	//HintText
	var hintText = Ti.UI.createLabel({
		text: I('Swipe_to_delete_a_Label'),
		width: 300,
		height: Ti.UI.SIZE,
		bottom: 10,
		font: {fontSize: 14},
		textAlign: 'center',
		color: globals.fontColors[2],
		shadowColor: 'white',
		shadowOffset: {x:0,y:1},
		visible: false
	});	
	
	var emptyListLabel = Ti.UI.createLabel({
		text: I('There_are_no_labels_in_this_list'),
		width: 300,
		height: Ti.UI.SIZE,
		top: 55,
		font: {fontSize: 14},
		textAlign: 'center',
		color: globals.fontColors[2],
		shadowColor: 'white',
		shadowOffset: {x:0,y:1},
		//visible: dataIn.length > 0 ? false : true
	});	
	
	
	//Create Tableview
	var tableView = MiscComponents.standardTableView();
	tableView.clickable = true; 
	//tableView.data = tableData;
	
	var tableViewClick = function (e){
		var labelID = e.rowData.data.$id;
	
		Ti.App.addEventListener('thumbHeaderRowLoaded', openLabelDetail);
		var LabelDetail = require('ui/windows/LabelDetail');
		var win1 = LabelDetail.loadWin(e.rowData.data.$id);
		
		function openLabelDetail (){
			globals.currentStack.open(win1,{animated:true});
			Ti.App.removeEventListener('thumbHeaderRowLoaded', openLabelDetail);
		};
	};
	
	tableView.addEventListener('touchend', function (e){
		if (tableView.clickable === true){
			tableViewClick(e);
			tableView.clickable = false; 
			var clickTimeout = setTimeout(function(){
				tableView.clickable = true; 
			},
			1500);
		};
	});
	
	tableView.addEventListener('delete', function (e){
		DataLabels.deleteLabel(e.rowData.data.$id);
	});
	
	var refreshData = _.throttle(function (){
		Ti.API.debug("REFRESHING LABEL LIST")
		//Fetch Data
		dataIn = DataLabels.loadLabelsList();
		
		tableData = [];
		
		//Right Index Variables
		var sheader = '';
		var tableIndex = [];
		
		for (var i =0; i < dataIn.length; i++ ){
			var data = dataIn[i];
			var title1 = data.name
			
			///Right Index Work
			if(dataIn.length > 30){
				var letter = title1[0].toUpperCase();  
		      	if(letter != sheader){
		            sheader = letter;
		         	tableIndex.push({title: sheader, index:i});
		        }else{
		        	sheader = null
		        };	 
			}
			else{
				sheader = null
			}; 
			
			
			//NOTE have to do it this way not to pollute the DB with image. 
			var row; 
		 	if(data.thumbnail){ 
				row = MiscComponents.standarRowWithImage(title1,data.media.length,data.thumbnail,sheader,true);
			}
			else{
				row = MiscComponents.standarRowWithImage(title1,data.media.length,labelEmptyThumb(),sheader,true);
			}
         	row.data = data;
         	tableData.push(row);
		};
		
		tableView.setData(tableData);
		
		if (dataIn.length > 30){
			tableView.index = tableIndex;
		};
		
		if (dataIn.length > 8){
			hintText.visible = false;
		}
		else{
			hintText.visible = true;
		};
		
		emptyListLabel.visible = dataIn.length > 0 ? false : true;
	},500);
	
	refreshData();
	
	
	
	//Assemble Window
	win.add(emptyListLabel);
	win.add(hintText);
	win.add(tableView);
	
	
	///Methods
	function preClean (){
	
	};
	
	
	//Expose
	win.preClean = preClean;
	
	
	//Listener
	Ti.App.addEventListener('updateLabelsList', refreshData);
	
	win.addEventListener('close', function (){
		Ti.App.addEventListener('updateLabelsList', refreshData);
	});
	
	win.addEventListener('focus', function (){
		tableView.reset();
		//Instructions 
		if (dataIn.length <= 0){
			setTimeout(function (){
				var PopUp = require('ui/components/PopUp');
				var titleText = I('Labels');
				var subTitleText = I("Skiing_Cooking_Funny");
				var instructions = PopUp.instructionsPopUp(titleText,subTitleText);
				instructions.open();	
			},500)
		};
	});
	
	return win;
};


var labelEmptyThumb = function (){
	var view = Ti.UI.createView({
		backgroundImage: 'images/cover_empty_label.png',
		//top:2,
		width:80,
		height:80
	}).toImage();
	
	return view;
};


var loadNewLabelWin = function (refreshData){
	var win = Ti.UI.createWindow({
		height: 480,
		width: 320
	});
	
	var bg = Ti.UI.createView({
		backgroundColor: 'black',
		borderRadius: 15,
		width: 300,
		height: 200,
		opacity: .8
	});
	
	var label = Ti.UI.createLabel({
		top: 480/2-200/2+20,
		text: 'New label',
		color: 'white'
	});
	
	var textField = Ti.UI.createTextField({
		backgroundColor: 'white',
		borderRadius:5,
		hintText:'enter_label_name',
		width: 160,
		top:label.top + 30
	});
	
	
	var cancelButton = Ti.UI.createButton({
		width: 30,
		height: 30,
		top: label.top,
		left: 320/2-300/2+ 10
	});
	
	cancelButton.addEventListener('click', function (){
		win.close();
	});
	
	var doneButton = Ti.UI.createButton({
		title: 'done',
		width: 30,
		height: 30,
		top: label.top+ 50,
		left: 150+ 10
	});
	
	doneButton.addEventListener('click', function (){
		var DataLabels = require('model/DataLabels');
		
		var dataToStore = {};
		dataToStore.name = textField.value;
		
		DataLabels.addLabel(dataToStore);
		
		refreshData();
		
		win.close();
	});
	
	win.add(bg);
	win.add(label);
	win.add(textField);
	win.add(cancelButton);
	win.add(doneButton);
	
	win.open();
};
