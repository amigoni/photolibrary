exports.loadWin = function (personID){
	var Window = require('ui/components/Window');
	var TopNavigation = require('ui/components/TopNavigation');
	var MiscComponents = require('ui/components/MiscComponents');
	
	//Get initial Data
	var dataIn;
	
	//Create Window
	var win = Window.createWindow({
		tabBarHidden: true,
		tabbedBarHidden: true
	});
	win.clickable = true;
	
	
	//Set Navigation
	var menuButton = TopNavigation.menuButton();
	win.leftNavButton = menuButton;


	var centerTitle = TopNavigation.centerSingleLabel(I('Feedback'), 'white');// TopNavigation.centerDoubleLabel('', '', 'white');
	win.centerTitle = centerTitle;
	win.titleControl = centerTitle;
	
	
	//Create Tableview
	var tableData = [];
	
	
	var feedbackRow = createRow(I('Feedback'), I('Have_any_ideas'));
	
	feedbackRow.addEventListener('click', function (e){
		var Ideas = require('ui/windows/Ideas');
		globals.currentStack.open(Ideas.loadWin(),{animated:true});
	});
	
	
	var supportRow = createRow(I('Support'), I('Help_me'));
	
	supportRow.addEventListener('click', function (){
		var Support = require('ui/windows/Support');
		globals.currentStack.open(Support.loadWin(),{animated:true});
	});
	
	var creditsRow = createRow(I('Credits'), I('Thank_you'));
	
	creditsRow.addEventListener('click', function (){
		var Credits = require('ui/windows/Credits');
		globals.currentStack.open(Credits.loadWin(),{animated:true});
	});
	
	tableData.push(feedbackRow);
	tableData.push(supportRow);
	tableData.push(creditsRow);
	
	var tableView = MiscComponents.standardTableView();
	tableView.showVerticalScrollIndicator = false;
	tableView.data = tableData;
	
	
	//Assemble Window
	win.add(tableView);

	
	return win;
};


var createRow = function (titleText, subtitleText){
	var Labels = require('ui/components/Labels');
	
	var row = Ti.UI.createTableViewRow({
		backgroundColor:'transparent',
		className: 'personDetailRow',
		height: 70,
		editable: false,
		enabled: false
		//leftImage: data.image
	});
	
	var groove = Ti.UI.createView({
		backgroundImage: 'images/groove_white.png',
		width: 320,
		height:2,
		top:row.height-2,
		opacity:1
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: titleText,
		color: globals.fontColors[2],
		font: {fontWeight: 'bold', fontSize: 17},
		minimumFontSize: 14,
		shadowColor: '#fff',
		shadowOffset:{x:1,y:1},
		width: 300,
		left: 10,
		top: 10,
		height:20
	});
	
	var subtitleLabel = Ti.UI.createLabel({
		text: subtitleText,
		font:{fontSize:13},
		color: globals.fontColors[0],
		left: 10,
		top: 30,
		textAlign: 'left',
		width: 300,
		height: Ti.UI.SIZE
	});
	
	subtitleLabel.addEventListener('postlayout', function (){
		groove.top = row.size.height-2
	});
	
	var selectionOverlay = Ti.UI.createView({
		backgroundImage: 'images/row_selected.png',
		width: 320,
		height: row.height,
		opacity: 0.02,
	});
	
	selectionOverlay.addEventListener('touchstart', function (e){
		if(row.enabled === true){
			selectionOverlay.opacity = 1;
			groove.visible = false;
		}
	});
	
	function clean(){
		var children = row.children;
		for(var i = children.length-1 ; i >= 0;  i--){
			row.remove(children[i]);
			children[i] = null;
		};
		Ti.API.debug('Cleaned Row');
	};
	
	row.add(groove);
	row.add(titleLabel);
	row.add(subtitleLabel);
	row.add(selectionOverlay);
	
	row.titleLabel = titleLabel;
	row.subtitleLabel = subtitleLabel;
	row.selectionOverlay = selectionOverlay;
	row.groove = groove;
	row.clean = clean;
	
	return row;
};



var infoFromContact = function (id){
	var person = Ti.Contacts.getPersonByID(id);
	
	var info = {};
	info.firstName = person.firstName;
	info.lastName = person.lastName;
	//info.created = person.created;
	//info.modified = person.modified;

	info.addressBookID = person.recordId	
	
	return info;
};




var personEmptyThumb = function (){
	var view = Ti.UI.createView({
		backgroundImage: 'images/cover_empty.png',
		//top:2,
		width:80,
		height:80
	}).toImage();
	
	return view;
};