exports.loadWin = function (currentFacesAlbumID,dataArray,person){
	var PopUp = require('ui/components/PopUp');
	
	var win = Ti.UI.createWindow({
		tabBarHidden: true,
		navBarHidden:true,
		width:'100%',
		height: '100%',
		top:0,
		transform: Ti.UI.create2DMatrix().scale(1),
		opacity:1
	});
	
	var bgShade = Ti.UI.createView({
		backgroundColor: 'black',
		width: '100%',
		height: '100%',
		opacity: 0
	});
	
	
	var bg = PopUp.bg();
	bg.transform = Ti.UI.create2DMatrix().scale(0);
	bg.height = 460;
	
	
	var closeButton = Titanium.UI.createButton({
		backgroundImage:'images/nav_x.png',
		width:21,
		height:20,
		top:20,
		left:20
	});
	
	var closeAnimation = function (){
		bg.animate(Ti.UI.createAnimation({
			duration: 250,
			transform: Ti.UI.create2DMatrix().scale(0),
			opacity:0
		}));
		
		bgShade.animate(Ti.UI.createAnimation({
			duration: 250,
			//transform: Ti.UI.create2DMatrix().scale(0),
			opacity:0
		}));
		
		setTimeout(function (){
			win.close();
		}, 300);
	};
	
	
	closeButton.addEventListener('click', function (){
		closeAnimation();
	});
	
	
	var groove = PopUp.groove(54);
	
	
	var title = Ti.UI.createLabel({
		color: 'white',
		font: {fontSize: 20, fontWeight: 'bold'},
		top: 15,
		text: 'Faces from iPhoto'
	});
	
	
	var subTitle1 = Ti.UI.createLabel({
		color: 'white',
		font:  {fontSize: 15, fontWeight: 'regular'},
		textAlign: 'center',
		top: 35,
		text: 'Link to a Faces Album'
	});
	
	
	//TableView
	var tableData = [];

	var createRow = function (data){
		
		var row = Ti.UI.createTableViewRow({
			height: 50,
			data: data,
			selected: data.name === person.facesAlbumID ? true : false
		});
		
		var img = Ti.UI.createImageView({
			left: 5,
			height: 48,
			width: 48,
			image:data.posterImage,
			//borderRadius: 5,
			hires: true,
			touchEnabled: false
		});
		
		var title = Ti.UI.createLabel({
			color: 'white',
			width:200,
			font: {fontSize: 17,fontWeight: 'bold'},
			left: 60,
			text: data.name,
			touchEnabled: false
		});
		
		var number = Ti.UI.createLabel({
			color: 'white',
			font: {fontSize: 15,fontWeight: 'regular'},
			right: 15,
			text: '('+data.numberOfAssets+')',
			touchEnabled: false
		});
		
		var checkMark = Ti.UI.createView({
			backgroundImage: 'images/check_mark_yellow.png',
			width: 29,
			height: 29,
			right: 15,
			opacity: (data.name === person.facesAlbumID) ? 1 : 0,
			touchEnabled: false
			//visible: selected
		});
		
		row.add(img);
		row.add(title);
		//row.add(number);
		row.add(checkMark);
		
		row.checkMark = checkMark;
		
		return row;
	};
	
	
	for (var i = 0; i < dataArray.length; i++){
		tableData.push(createRow(dataArray[i]));
	};
	
	var tableView = Ti.UI.createTableView({
		backgroundColor: 'transparent',
		width: 296,
		height: 440-24-10,
		top:56,
		data: tableData
	});
	
	tableView.addEventListener('click', function(e){
		var rows = tableView.data[0].rows;
		for(var i = 0; i < rows.length; i ++){
			rows[i].checkMark.opacity = 0;
		};
		
		if(e.row.selected === true){
			e.row.checkMark.opacity = 0;
			e.row.selected = false;
			var DataPersons = require('model/DataPersons');
			DataPersons.linkFacesAlbumToPerson(person,null);
		}
		else if (e.row.selected === false){
			e.row.checkMark.opacity = 1;
			e.row.selected = true;
			var DataPersons = require('model/DataPersons');
			DataPersons.linkFacesAlbumToPerson(person,e.rowData.data);
		};
	});
	
	
	//Assemble Everything
	bg.add(closeButton);
	bg.add(groove);
	bg.add(title);
	bg.add(subTitle1);
	bg.add(tableView);
	
	win.add(bgShade);
	win.add(bg);
	
	win.addEventListener( 'close', function (){
		bg.remove(closeButton);
		bg.remove(groove);
		bg.remove(title);
		bg.remove(subTitle1);
		bg.remove(tableView);
		closeButton = null;
		groove = null;
		title = null;
		subTitle1 = null;
		tableView = null;
		
		win.remove(bgShade);
		win.remove (bg);
		bgShade = null;
		bg = null;
	});
	
	win.open();
	
	bg.animate(Ti.UI.createAnimation({
		duration: 250,
		transform: Ti.UI.create2DMatrix().scale(1),
		opacity:1
	}));
	
	bgShade.animate(Ti.UI.createAnimation({
		duration: 250,
		opacity:.5
	}));
};