exports.loadWin = function (personID){
	var assetslibrary = require('ti.assetslibrary');
	var Window = require('ui/components/Window');
	var TopNavigation = require('ui/components/TopNavigation');
	var PicturesList = require('ui/windows/PicturesList');
	var NavigationController = require('ui/NavigationController');
	var DataPersons = require('model/DataPersons');
	var MiscComponents = require('ui/components/MiscComponents');
	
	//Get initial Data
	var dataIn;
	
	//Create Window
	var win = Window.createWindow({
		tabBarHidden: true,
		tabbedBarHidden: true
	});
	win.clickable = true;
	
	
	//Set Navigation
	var backButton = TopNavigation.backButton(win);
	win.leftNavButton = backButton;
	
	
	var editButton = TopNavigation.editButton('edit', 40);
	editButton.label.text = 'edit';
	editButton.left = null;
	editButton.right = 5;
	//editButton.label.text = 'Edit';
	
	editButton.addEventListener('click', function (){
		if (globals.tutorialModeOn === true){
			var PopUp = require('ui/components/PopUp');
			var instructions = PopUp.instructionsPopUp(I("Wait"),I("Let's_finish_the_tutorial"));	
			instructions.open();
		}else{
			var PersonEdit = require('ui/windows/PersonEdit');
			var win2 = PersonEdit.loadWin(personID);
			globals.currentStack.open(win2,{animated:true});
		}
	});

	win.rightNavButton  = editButton;


	var centerTitle = TopNavigation.centerSingleLabel('-', 'white');// TopNavigation.centerDoubleLabel('', '', 'white');
	win.centerTitle = centerTitle;
	win.titleControl = centerTitle;
	
	
	//Create Tableview
	var tableData = [];
	
	//Thumb Picture Row
	var thumbHeaderRow = MiscComponents.createThumbHeaderRow('person');
	thumbHeaderRow.clickable = true; 
	
	var thumbHeaderRowClick = function(e){
		if (globals.tutorialModeOn === true){
			var PopUp = require('ui/components/PopUp');
			var instructions = PopUp.instructionsPopUp(I("Wait"),I("Let's_finish_the_tutorial"));	
			instructions.open();
		}
		else{
			if(dataIn.allPictures.length > 0){
				var opts = {
				  cancel: 2,
				  options: [I('Pick_a_specific_photo'), 
				  I('Randomly_rotate_photo'), 
				  //'Latest Photo',
				  'Cancel'],
				  selectedIndex: 2,
				  //destructive: 0,
				  title: I('Select_Cover_Photo')
				};
				
				var dialog = Ti.UI.createOptionDialog(opts)
				
				dialog.addEventListener('click', function (e){
					if (e.index === 0){
						globals.currentStack.open(PicturesList.loadWin('allPictures','person',personID,true),{animated:true});
					}
					else if (e.index === 1){
						function callback(){
							updatePersonDetail({getAlsoThumbNail: true});
						};
						DataPersons.setThumbForPerson(personID,null,'random',callback);
					}
				});
				
				dialog.show();
			}
			else{
				alert(I('Please_tag_some_pictures_before')+centerTitle.label.text)
			};
		};	
	};
		
	thumbHeaderRow.addEventListener('click', function (e){
		if (win.clickable === true){
			thumbHeaderRowClick(e);
			win.clickable = false; 
			var clickTimeout = setTimeout(function(){
				win.clickable = true; 
			},
			1500);
		};
	});
	
	
	//Favorites Row
	var favoritesRow = createRow(I('Favorites'),'-');
	favoritesRow.clickable = true;
	
	favoritesRow.add(Ti.UI.createView({
		backgroundImage: 'images/favorites_detail.png',
		width: 30,
		height: 28,
		left: 17
	}));
	
	var favoritesRowClick = function(e){
		if (globals.tutorialModeOn === true){
			var PopUp = require('ui/components/PopUp');
			var instructions = PopUp.instructionsPopUp(I("Wait"),I("Let's_finish_the_tutorial"));	
			instructions.open();
		}
		else{
			var win1 = PicturesList.loadWin('favorites','person', personID);
			globals.currentStack.open(win1,{animated:true});
			/*
			function openFavoritesPictures (){
				globals.currentStack.open(win1,{animated:true});
				Ti.App.removeEventListener('pictureListReady',openFavoritesPictures);	
			};
			
			Ti.App.addEventListener('pictureListReady',openFavoritesPictures);
			*/
		};	
	};
	
	favoritesRow.addEventListener('touchend', function (e){
		if (win.clickable === true){
			if (globals.tutorialModeOn === true){
				var PopUp = require('ui/components/PopUp');
				var instructions = PopUp.instructionsPopUp(I("Wait"),I("Let's_finish_the_tutorial"));	
				instructions.open();
			}
			else{
				if(favoritesRow.enabled === true){
					favoritesRowClick(e);
					win.clickable = false; 
					var clickTimeout = setTimeout(function(){
						win.clickable = true;
					},
					1500);
				}
				else{
					alert(I('Press_the_star_button_while'));
				};
			};	
		};
	});
	
	
	
	//Together Row
	var togetherRow = createRow(I('Together_with_you'),'-');
	togetherRow.clickable = true; 
	
	togetherRow.add(Ti.UI.createView({
		backgroundImage: 'images/together.png',
		width: 30,
		height: 24,
		left: 15
	}));
	
	var togetherRowClick = function(e){
			var win1 = PicturesList.loadWin('together', 'person', personID);
			globals.currentStack.open(win1,{animated:true});
			/*
			function openTogetherPictures (){
				globals.currentStack.open(win1,{animated:true});
				Ti.App.removeEventListener('pictureListReady',openTogetherPictures);	
			};
			
			Ti.App.addEventListener('pictureListReady',openTogetherPictures);
			*/
	};
	
	togetherRow.addEventListener('click', function (e){
		if (globals.tutorialModeOn === true){
			var PopUp = require('ui/components/PopUp');
			var instructions = PopUp.instructionsPopUp(I("Wait"),I("Let's_finish_the_tutorial"));	
			instructions.open();
		}
		else{
			if (win.clickable === true){
				if (togetherRow.enabled === true){
					togetherRowClick(e);
					win.clickable = false; 
				}
				else{
					var you = globals.col.persons.find({isMyself: true});
					if (!you[0] && win.clickable === true){
						win.clickable = false;
						Ti.App.fireEvent('addContact',{pickingYourself: true});
						setTimeout(function (){
							globals.currentStack.navGroup.close(win);
						},250);
					}
					else{
							alert(I('Tag_pictures_with_both_of_you')); 
					};
				};
				var clickTimeout = setTimeout(function(){
					win.clickable = true; 
				},
				1500);
			};
		};	
	});
	
	
	
	//All Pictures Row
	var allPicturesRow = createRow(I('All_Pictures'),'-');
	allPicturesRow.clickable = true; 
	
	allPicturesRow.add(Ti.UI.createView({
		backgroundImage: 'images/polaroids.png',
		width: 31,
		height: 31,
		left: 15
	}));
	
	var allPicturesClick = function(e){
		var win1 = PicturesList.loadWin('allPictures', 'person', personID);
		globals.currentStack.open(win1,{animated:true});
		/*
		function openAllPictures (){
			globals.currentStack.open(win1,{animated:true});
			Ti.App.removeEventListener('pictureListReady',openAllPictures);	
		};
		
		Ti.App.addEventListener('pictureListReady',openAllPictures);
		*/
	};
			
	allPicturesRow.addEventListener('click', function (e){
		if (win.clickable === true){
			if (globals.tutorialModeOn === true){
				var PopUp = require('ui/components/PopUp');
				var instructions = PopUp.instructionsPopUp(I("Wait"),I("Let's_finish_the_tutorial"));	
				instructions.open();
			}
			else{
				if (allPicturesRow.enabled === true){
					allPicturesClick(e);
					win.clickable = false; 
					var clickTimeout = setTimeout(function(){
						win.clickable = true; 
					},
					1500);
				}
				else{
					alert(I('Tag_photos_with_this_Person'));
					//tagMoreRowClick(e);
					win.clickable = true;
				};
			};	
		};
	});
	
	
	//Add Photos Row
	var tagMoreRow = MiscComponents.createAddPhotosRow();
	tagMoreRow.clickable = true; 
	
	var tagMoreRowClick = function(e){
		var LibraryPicturesPicker = require('/ui/windows/LibraryPicturesPicker');
		LibraryPicturesPicker.loadWin('person',personID);
	};
	
	tagMoreRow.addEventListener('click', function (e){
		if (win.clickable === true){
			tagMoreRowClick(e);
			win.clickable = false; 
			var clickTimeout = setTimeout(function(){
				win.clickable = true; 
			},
			1500);
		};
	});
	
	
	var tableView = MiscComponents.standardTableView();
	tableView.showVerticalScrollIndicator = false;
	tableView.data = tableData;
	
	
	//Assemble Window
	win.add(tableView);
	
	
	//Methods
	var updatePersonDetail = _.throttle(function (e1){
		var getAlsoThumbNail = false;
		if (e1 && e1.getAlsoThumbNail){
			getAlsoThumbNail = e1.getAlsoThumbNail;
		};
	
		function callback (e){
			if (getAlsoThumbNail === true){
				thumbHeaderRow.update(e);
				Ti.App.fireEvent('updatePersonsList');
			};
		};
		
		//alert(getAlsoThumbNail)
		dataIn = DataPersons.loadPersonDetail(personID, callback, getAlsoThumbNail);
		
		if (!dataIn.person.firstName && dataIn.person.lastName){
			centerTitle.label.text = dataIn.person.lastName;
		}
		else if (dataIn.person.firstName && !dataIn.person.lastName){
			centerTitle.label.text = dataIn.person.firstName;
		}
		else{
			centerTitle.label.text = dataIn.person.firstName +' '+dataIn.person.lastName ;
		};
		
		//tagMoreRow.update(centerTitle.label.text);
		thumbHeaderRow.thumbnailTypeLabel.text = dataIn.person.thumbnailType === 'normal' ? '' : dataIn.person.thumbnailType;
		favoritesRow.update(dataIn.favorites.length);
		togetherRow.update(dataIn.together.length);
		allPicturesRow.update(dataIn.allPictures.length);
		
		
		tableView.headerView = thumbHeaderRow;
		tableData = [];
		tableView.data = [];
		
		tableData.push(allPicturesRow);
		tableData.push(favoritesRow);
		if (dataIn.person.isMyself === false){
			tableData.push(togetherRow);
		};
		tableData.push(tagMoreRow);
		
		tableView.data = tableData;
	},250);
	
	
	function preClean(){
		allPicturesRow.clean();
		favoritesRow.clean();
		tagMoreRow.clean();
		thumbHeaderRow.clean();
		//editButton.clean();
		//backButton.clean();
		//centerTitle.clean();
		/*
		editButton = null;
		backButton = null;
		centerTitle = null;
		
		allPicturesRow = null;
		togetherRow = null;
		favoritesRow = null;
		tagMoreRow = null;
		thumbHeaderRow = null;
		*/
		Ti.API.debug("Cleaned PersonDetail");
	};
	
	//Expose
	win.preClean = preClean;
	
	//Listeners
	Ti.App.addEventListener('updatePersonDetail', updatePersonDetail);
	
	win.addEventListener('close', function (){
		Ti.App.removeEventListener('updatePersonDetail', updatePersonDetail);
	});
	
	win.addEventListener('focus', function (){
		tableView.reset();
	});
	
	
	//Initialize
	updatePersonDetail({getAlsoThumbNail: true});
	
	
	//Tutorial 
	if (globals.tutorialModeOn === true){
		setTimeout(function(){
			var PopUp = require('ui/components/PopUp');
			var instructions = PopUp.instructionsPopUp(I("Taggin_photos_with_Quick_Tag"),I("Great_now_let's_add_some_photos"));	
			instructions.open();
		},500);
	};
	
	
	return win;
};


var createRow = function (titleText,count){
	var Labels = require('ui/components/Labels');
	
	var row = Ti.UI.createTableViewRow({
		backgroundColor:'transparent',
		className: 'personDetailRow',
		height: 50,
		editable: false,
		enabled: false
		//leftImage: data.image
	});
	
	var groove = Ti.UI.createView({
		backgroundImage: 'images/groove_white.png',
		width: 320,
		height:2,
		top:row.height-2
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: titleText,
		color: globals.fontColors[2],
		font: {fontWeight: 'bold', fontSize: 17},
		minimumFontSize: 14,
		shadowColor: '#fff',
		shadowOffset:{x:1,y:1},
		width: 175,
		left: 75,
		height:20
	});
	
	var countLabel = Ti.UI.createLabel({
		text: '('+count+')',
		color: globals.fontColors[0],
		right: 15,
		textAlign: 'right',
		width: 50,
		height:20
	});
	
	var selectionOverlay = Ti.UI.createView({
		backgroundImage: 'images/row_selected.png',
		width:320,
		height:row.height,
		opacity: 0.02,
	});
	
	selectionOverlay.addEventListener('touchstart', function (e){
		if(row.enabled === true){
			selectionOverlay.opacity = 1;
			groove.visible = false;
		}
	});
	
	function update(newCount){
		countLabel.text = '('+newCount+')';
		if(newCount === 0){
			titleLabel.color = globals.fontColors[0];
			row.enabled = false;
		}
		else{
			titleLabel.color = globals.fontColors[2];
			row.enabled = true;
		};
	};
	
	update(count);
	
	function clean(){
		var children = row.children;
		for(var i = children.length-1 ; i >= 0;  i--){
			row.remove(children[i]);
			children[i] = null;
		};
		Ti.API.debug('Cleaned Row');
	};
	
	row.add(groove);
	row.add(titleLabel);
	row.add(countLabel);
	row.add(selectionOverlay);
	
	row.titleLabel = titleLabel;
	row.countLabel = countLabel;
	row.selectionOverlay = selectionOverlay;
	row.groove = groove;
	row.update = update;
	row.clean = clean;
	
	return row;
};



var infoFromContact = function (id){
	var person = Ti.Contacts.getPersonByID(id);
	
	var info = {};
	info.firstName = person.firstName;
	info.lastName = person.lastName;
	//info.created = person.created;
	//info.modified = person.modified;

	info.addressBookID = person.recordId	
	
	return info;
};




var personEmptyThumb = function (){
	var view = Ti.UI.createView({
		backgroundImage: 'images/cover_empty.png',
		//top:2,
		width:80,
		height:80
	}).toImage();
	
	return view;
};