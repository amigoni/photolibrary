exports.loadWin = function (personID){
	var assetslibrary = require('ti.assetslibrary');
	var Window = require('ui/components/Window');
	var TopNavigation = require('ui/components/TopNavigation');
	var PicturesList = require('ui/windows/PicturesList');
	var NavigationController = require('ui/NavigationController');
	var DataPersons = require('model/DataPersons');
	var MiscComponents = require('ui/components/MiscComponents')
	
	//Get initial Data
	var dataIn = DataPersons.loadPersonDetail(personID);
	
	var personFullName;
	
	
	//Create Window
	var win = Window.createWindow({
		tabBarHidden: true,
		tabbedBarHidden: true
	});
	
	
	//Set Navigation
	var doneButton = TopNavigation.editButton();
	doneButton.left = null;
	doneButton.right = 10;
	doneButton.label.text = 'done';
	
	doneButton.addEventListener('click', function (){
		globals.currentStack.close(win);
	});

	//win.rightNavButton = doneButton;
	win.leftNavButton = TopNavigation.backButton(win);
	
	var centerTitle = TopNavigation.centerSingleLabel('-','white');
	win.centerTitle = centerTitle;
	win.titleControl = centerTitle;
	
	
	//Create Tableview
	var tableData = [];
	
	
	//Link to Contact Row
	var linkToContactRow = createRow(
		I('Link_to_Contact'),
		I('Linked_to ')+personFullName,
		I('Tap_to_link_this_person_to_another_contact'),
		100
	);
	
	/*
	linkToContactRow.add(Ti.UI.createView({
		backgroundImage: 'images/photos_app_dark.png',
		width: 33,
		height: 32,
		left: 14,
		top: 10
	}));
	*/
	
	linkToContactRow.addEventListener('click', function (){
		tableView.reset();
		Titanium.Contacts.showContacts({
			selectedPerson: function(e){
				// *** ADD FILTER FOR CONTACTS KIND TO ALLOW ONLY PERSON.	
				var person = Ti.Contacts.getPersonByID(e.person.recordId);
	
				var info = {};
				info.firstName = person.firstName;
				info.lastName = person.lastName;
				info.addressBookID = person.recordId	

				
				//Check to see if the person already exists. 
				if(!DataPersons.loadPersonForAddressBookID(info.addressBookID) || DataPersons.loadPersonForAddressBookID(info.addressBookID).$id === personID ){
					//Match with faces Callback. 
					var addPersonCB = function (e){
						if(e){
							//var PopUp = require('ui/components/PopUp');
							//PopUp.facesMatch(e.name, newPersonID, e.group);
						};
					};
					
					DataPersons.updatePersonAddressBookLink(personID,info);
				}
				else{
					alert(I('This_person_is_already_in_your_list'));
				};
			}
		});	 
	});
	
	
	
	//Link to Faces Row
	var linkToFacesRow = createRow(
		I('Link_to_iPhoto_Faces'),
		I('Linked to ')+(dataIn.person.firstName+' '+(dataIn.person.lastName ? dataIn.person.lastName : '')),
		I('Photos_from_linked_Faces_person')
	);
	
	linkToFacesRow.linkedFacesAlbum = null;
	
	linkToFacesRow.add(Ti.UI.createView({
		backgroundImage: 'images/photos_app_dark.png',
		width: 33,
		height: 32,
		left: 14,
		top: 10
	}));
	
	
	linkToFacesRow.addEventListener('click', function (){
		tableView.reset();
		var facesAlbumID = 1;
	
		var callBack = function (groups){
			
			Ti.API.info('Got ' + groups.length + ' groups');
			
			if (groups.length === 0){
				var PopUp = require('ui/components/PopUp');
				PopUp.alertDialog(I('No_Faces_Albums'),I('There_are_no_Faces_Albums_in_your_Photo_Library'));	
			}
			else if(groups.length > 0){
				var LinkToFacesAlbum = require('ui/windows/LinkToFacesAlbum');
				LinkToFacesAlbum.loadWin(facesAlbumID,groups,dataIn.person);
			};
		};
		
		DataPersons.getFacesGroups(callBack);
	});
	
	
	//isYou Row
	var isYouRowText;
	if (dataIn.person.isMyself === true){
		isYouRowText = I('This_is_you')
	}
	else{
		isYouRowText = I('Set_as_yourself')
	};
	
	var isYouRow = createRow(isYouRowText,null,I('Tap_to_set_this_person_as_yourself'),65);
	//isYouRow.titleLabel.left = null;
	//isYouRow.titleLabel.textAlign = 'center';
	//isYouRow.titleLabel.top = null;
	//isYouRow.titleLabel.color = '#999999';
	//isYouRow.titleLabel.font = {fontSize: 15, fontWeight: 'bold'};
	isYouRow.isThisYou = dataIn.person.isMyself;
	isYouRow.thirdLabel.top = 30;
	
	if(dataIn.person.isMyself === false){
		isYouRow.checkBox = Ti.UI.createView({
			backgroundImage:  'images/grey_checkbox.png',
			width: 18,
			height: 18,
			right: 20,
			top: 10
		});
		
		isYouRow.checkMark = Ti.UI.createView({
			backgroundImage:  'images/grey_checkmark.png',
			width: 26,
			height: 22,
			right: isYouRow.checkBox.right -4,
			top: 13,
			visible: isYouRow.isThisYou
		});
		
		isYouRow.add(isYouRow.checkBox);
		isYouRow.add(isYouRow.checkMark);
	};
	
	
	isYouRow.addEventListener('click', function (){
		tableView.reset();
		if( isYouRow.isThisYou == true){
			//isYouRow.isThisYou = false;
			//isYouRow.checkMark.visible= false;
			alert(I('To_change_yourself_check_this_box_on_the_person_that_is_yourself'))
		}
		else{
			isYouRow.isThisYou = true;
			isYouRow.titleLabel.text =I('This_is_you')
			isYouRow.checkBox.visible = false;
			isYouRow.checkMark.visible= false;
			var DataPersons = require('model/DataPersons');
			DataPersons.changeMyselfPersonStatus(personID);
			//tableView.deleteRow(1,{animated: true});
		};
	} );
	
	
	tableData.push(linkToContactRow);
	//if(globals.hasFaces === true){tableData.push(linkToFacesRow);};
	tableData.push(isYouRow);

	
	var tableView = MiscComponents.standardTableView();
	tableView.height = 480-42;
	tableView.data = tableData;
	
	
	//Assemble Window
	win.add(tableView);
	
	
	//Methods
	var updatePersonEdit = function (){
		//Load Data
		dataIn = DataPersons.loadPersonDetail(personID);
		
		if (!dataIn.person.firstName && dataIn.person.lastName){
			centerTitle.label.text = dataIn.person.lastName;
		}
		else if (dataIn.person.firstName && !dataIn.person.lastName){
			centerTitle.label.text = dataIn.person.firstName;
		}
		else{
			centerTitle.label.text = dataIn.person.firstName +' '+dataIn.person.lastName ;
		};

		personFullName = centerTitle.label.text;
		linkToContactRow.subTitleLabel.text = I('Linked_to')+personFullName;
		linkToFacesRow.linkedFacesAlbum = dataIn.linkedFacesAlbum;
		linkToFacesRow.subTitleLabel.text = I('Linked_to')+dataIn.linkedFacesAlbum;
		isYouRow.isThisYou = dataIn.person.isMyself;
		isMyself = isYouRow.isThisYou;
	};
	
	function preClean (){
		Ti.API.debug('Cleaned Person Edit');
	};
	
	
	//Expose
	win.preClean = preClean;
	
	//Listeners
	Ti.App.addEventListener('updatePersonEdit', updatePersonEdit);
	
	win.addEventListener('focus', function (){
		//tableView.reset();
	});
	
	win.addEventListener('close', function (){
		Ti.App.removeEventListener('updatePersonEdit', updatePersonEdit);
	});
	
	
	//Initialiaze
	updatePersonEdit();
	
	
	return win;
};


var createRow = function (titleText, subTitleText, thirdLabelText, rowHeight){
	var Labels = require('ui/components/Labels');
	
	var row = Ti.UI.createTableViewRow({
		backgroundColor:'transparent',
		className: 'personDetailRow',
		height: rowHeight ? rowHeight : 90,
		editable: false
		//leftImage: data.image
	});
	
	var groove = Ti.UI.createView({
		backgroundImage: 'images/groove_white.png',
		width: 320,
		height:2,
		top: row.height-2
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: titleText,
		textAlign: 'left',
		color: globals.fontColors[2],
		font: {fontWeight: 'bold', fontSize: 17},
		minimumFontSize: 14,
		shadowColor: '#fff',
		shadowOffset:{x:1,y:1},
		width: 175,
		left: 15,
		top:8,
		height:20
	});
	
	var subTitleLabel = Ti.UI.createLabel({
		text: subTitleText,
		font: {fontSize: 14},
		color: globals.fontColors[0],
		left: 15,
		textAlign: 'left',
		width: 310-75,
		height:20,
		top: 25,
	});
	
	var thirdLabel = Ti.UI.createLabel({
		text: thirdLabelText,
		font: {fontSize: 12},
		shadowColor: 'white',
		shadowOffset:{x:0,y:1},
		color: globals.fontColors[0],
		textAlign: 'left',
		left: 15,
		width: 300,
		height: Ti.UI.SIZE,
		top: 50
	});
	
	var selectionOverlay = Ti.UI.createView({
		backgroundImage: 'images/row_selected.png',
		width:320,
		height:row.height,
		opacity: 0.02,
	});
	
	selectionOverlay.addEventListener('touchstart', function (e){
		selectionOverlay.opacity = 1;
		groove.visible = false;
	});
	
	
	row.add(groove);
	row.add(titleLabel);
	row.add(subTitleLabel);
	row.add(thirdLabel);
	row.add(selectionOverlay);
	
	row.titleLabel = titleLabel;
	row.subTitleLabel = subTitleLabel;
	row.thirdLabel = thirdLabel;
	row.selectionOverlay = selectionOverlay;
	row.groove = groove;
	
	return row;
};
