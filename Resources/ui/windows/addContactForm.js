exports.loadWin = function (textTyped,baseWin){
	var Window = require('ui/components/Window');
	
	var win = Ti.UI.createWindow({
		backgroundImage: 'images/stripedBg.png',
		title: 'New Contact',
		width: '100%',
		height: '100%',
		modal: baseWin ? false : true,
		tabBarHidden: true,
		navBarHidden: false
	});
	
	
	var cancelButton = Ti.UI.createButton({
		title: 'Cancel'
	});
	
	cancelButton.addEventListener('click', function (){
		if (baseWin){
			baseWin.close()
			setTimeout(function (){
				baseWin.clean()},
			250);
		}
		else{
			win.close();
		}
	})
	
	win.leftNavButton = cancelButton;
	
	var doneButton = Ti.UI.createButton({
		title: 'Done'
	});
	
	doneButton.addEventListener('click', function (){
		if ( firstNameRow.textField.value || lastNameRow.textField.value ){
			var DataPersons = require('model/DataPersons');
			var newPerson = DataPersons.addNewAddressBookContact(firstNameRow.textField.value, lastNameRow.textField.value);
			win.fireEvent('addedNewContact',{value: newPerson});
			
			if (baseWin){
				baseWin.close()
			}
			else{
				win.close();
			}
		}
		else {
			alert(I('Please_enter_at_least_a_first_name_or_a_last_name'));
		}
	});
	
	win.rightNavButton = doneButton;
	
	
	///TableView
	function createRow (hintText, initialText){
		var row = Ti.UI.createTableViewRow({
			width: 320,
			height: 45
		});
		
		var textField = Ti.UI.createTextField({
			value: initialText,
			//backgroundColor: 'red',
			left: 10,
			width: row.width -35,
			hintText: hintText,
			autocorrect: false,
			clearButtonMode:Ti.UI.INPUT_BUTTONMODE_ONFOCUS 
		});
		
		row.add(textField);
		
		row.textField = textField;
		
		return row;
	};
	
	
	var tableHeader = Ti.UI.createLabel({
		text: I('Please_enter_First_and_Last_name'),
		textAlign: 'center',
		shadowColor: 'white',
		width: 290,
		top:0,
		shadowOffset: {x:1,y:1},
		font: {fontSize: 15},
		color: '#54637C'
	});
	
	var firstNameRow = createRow('First',textTyped);
	var lastNameRow = createRow('Last');
	
	var tableView = Ti.UI.createTableView({
		style: Titanium.UI.iPhone.TableViewStyle.GROUPED,
		width: '100%',
		height: Ti.Platform.displayCaps.getPlatformHeight-44,
		//top: 60,
		//backgroundColor: 'transparent',
		footerView: tableHeader,
		data: [firstNameRow, lastNameRow]
	});
	tableView.setContentInsets({top:5},{animated:false});
	
	//win.add(tableHeader)
	win.add(tableView);
	
	
	//Methods
	function clean (){
		var children = win.children;
		
		for(var i = children.length - 1 ; i >= 0;  i--){
			win.remove(children[i]);
			children[i] = null;
		};
		
		Ti.API.debug('Cleaned Add Contact window');
	};
	
	
	//Expose
	win.clean = clean;
	
	
	return win; 
};
