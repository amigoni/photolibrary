require('ti.viewshadow');

exports.loadWin = function (data, demoMode){
	demoMode = demoMode? demoMode : false
	
	var win = Ti.UI.createWindow({
		width: '100%',
		height: '100%',
		tabbedBarHidden: true,
		navBarHidden: true,
		tabBarHidden: true,
		//backgroundColor : '#3A3D3B',
		backgroundGradient : { type:'linear', colors:['#535353', '#232323'], startPoint:{ x:0, y:0 }, endPoint:{ x:0, y:480 }, backFillStart:false },
		//barColor: '#000',
		opacity: 1,
		//transform:Ti.UI.create2DMatrix().scale(1.2),
		//translucent : true,
		orientationModes: [Titanium.UI.PORTRAIT],
		transform: Titanium.UI.create2DMatrix().translate(0,480),
		kind: 'posterDetail'
	});
	
	globals.currentModal = win;
	
	///// NavBar
	var navBar = Ti.UI.createView({
		width: 320,
		height: 44,
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundImage:'images/nav_x.png',
		//backgroundColor: 'red',
		width: 72,
		height: 44,
		top: 0,
		left: 0
	}); //TopNavigation.backButton(win,'white');
	
	closeButton.add(Ti.UI.createView({
		backgroundImage:'images/nav_x.png',
		width: 23,
		height: 23,
		top: 10,
		left: 10
	})); //TopNavigation.backButton(win,'white');
	
	closeButton.addEventListener('click', function (){
		closingAnimation();
	});

	var ShareButton = require('ui/components/ShareButton');

	var shareButton = ShareButton.shareButton(['email','facebook','twitter','contact'],null,win);
	shareButton.top = 10;
	shareButton.right = 10;
	
	shareButton.clickFunction = function (e){
		if (demoMode === false){
			var optionDialog = ShareButton.createPopUp(posterView.toImage());
			optionDialog.open();
		}
		else{
			alert('Purchase this feature to enable sharing');
		};
	};
	
	navBar.add(closeButton);
	navBar.add(shareButton);
/////
	
	
	// Main Paper Area
	var scrollView = Ti.UI.createScrollView({
		width:  Ti.Platform.displayCaps.platformWidth,
		height: Ti.Platform.displayCaps.platformHeight,
		contentWidth: 320,
		contentHeight: 'auto',
		showVerticalScrollIndicator: false,
		//layout: 'vertical'
	});
	
	
	var posterView =  Ti.UI.createView({
		backgroundImage: 'images/newspaper_bg.png',
		backgroundTopCap: 20,
		backgroundLeftCap: 20,
		width: 318*2,
		left: 2,
		top: 50,
		height: Ti.UI.SIZE,
		anchorPoint: {x: 0, y: 0},
		transform: Ti.UI.create2DMatrix().scale(.5)
		//layout: 'vertical'
	});
	
	posterView.addEventListener('click', function (){
		overlay.blurAll();
	});
	
	
	var Helper = require('lib/Helper');
	var newSize = Helper.imageResizeFill(313*2, 309*2, data.image.width, data.image.height);
	
	var containerScroll = Ti.UI.createScrollView({
		width: newSize.width,
		height: newSize.height,
		contentWidth: newSize.width,
		contentHeight: newSize.height,
		//borderColor:'#999999',
		top: 2*2,
		left: 2*2,
		maxZoomScale: 2,
		minZoomScale: 1
	});
	
	var image = Ti.UI.createView({
	//var image =  filterableimageview.createFilterableImageView({	
		//backgroundImage: blob,
		backgroundImage: data.image,
		//borderColor:'#999999',
		//backgroundColor: 'gray',
		width: newSize.width,
		height: newSize.height,
		//top: 2*2,
		//left: 2*2,//(310-newSize.width)/2+3,
		filter: [{name:'grayscale'}]
	});
	
	image.addEventListener('postlayout', function (){
		posterView.height = newSize.height+6*2
	})
	
	containerScroll.add(image);
	
	posterView.add(containerScroll);
	posterView.add(Ti.UI.createView({top: newSize.height, height:10*2}));
	
	
	var overlayLayer = Ti.UI.createView({
		top:2,
		width: posterView.width,
		height: Ti.UI.SIZE
	});
	
	posterView.add(overlayLayer);
	var overlay;
	
	function updatePosterView (type){
	
		function foundOverlay(){
			var overlay1;
			for(var i =0; i < overlayLayer.children.length; i++){
				if (overlayLayer.children[i].kind === type){
					overlay1 = overlayLayer.children[i];
					Ti.API.debug('FOUND ONE OF THE TYPE')
					break
				}
			};
			
			return overlay1;
		};
		
		function createOverlay (args){
			if(args.type === 'national'){
				overlay = nationalGeographic({
					width: args.width,
					height: args.height
				});
			}
			else if (args.type === 'time'){
				overlay =  timeMagazine({
					width: args.width,
					height: args.height
				});
			}		
			else if (args.type === 'vogue'){
				overlay =  vogue({
					width: args.width,
					height: args.height
				});
			
			}	
			else if (args.type === 'rollingStones'){
				overlay =  rollingStones({
					width: args.width,
					height: args.height
				});
			}		
			
			return overlay;
		};
		/*
		if (overlay){
			posterView.remove(overlay);
		}
		*/
		
		overlay = foundOverlay()
		
		//Hide all overlays
		for(var i =0; i < overlayLayer.children.length; i++){
			overlayLayer.children[i].visible = false;
		};
			
		//Make visible or Add new overlay	
		if (overlay){
			overlay.visible = true;
		}	
		else{
			overlay = createOverlay({
				type: type,
				width: newSize.width,
				height: newSize.height
			})
			overlayLayer.add(overlay);
			Ti.API.debug("Createting new Overlay")
		};			
	};
	
	updatePosterView('national');
	
	//Switch Bar
	var switchBar = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 45,
		top: newSize.height/2+50+17,
		layout:'horizontal'
	});
	
	var stretchButton = Ti.UI.createButton({
		backgroundImage: 'images/move_button.png',
		width:35,
		height:35,
		//title: 'Stretch',
	});
	
	stretchButton.addEventListener('click', function (e){
		overlayLayer.visible = false;
	});
	
	var textButton = Ti.UI.createButton({
		backgroundImage: 'images/frame_button.png',
		left:35,
		width: 35,
		height: 35,
		//title: 'Text'
	});
	
	textButton.addEventListener('click', function(e){
		overlayLayer.visible = true;
	});
	
	switchBar.add(stretchButton);
	switchBar.add(textButton);
	
	
	//Poster seletor
	var posterSelector = Ti.UI.createScrollView({
		//backgroundColor: 'red',
		height: 90,
		contentHeight: 90,
		contentWidth: 'auto',
		widht: 320,
		top: newSize.height/2 + 50 + 50+ 35,
		layout: 'horizontal'
	});
	
	posterSelector.addEventListener('click', function (e){
		updatePosterView(e.source.kind);
	});
	
	var thumbSize = Helper.imageResizeFill(200, 85, data.image.width, data.image.height);
	
	function createThumb(type){
		var view = Ti.UI.createView({
			backgroundImage: 'images/newspaper_bg.png',
			backgroundTopCap: 20,
			backgroundLeftCap: 20,
			width: 90,
			height: 90,
			left: 10,
			height: Ti.UI.SIZE,
			kind: type
		});
		
		var container = Ti.UI.createView({
			width: 85,
			height: 85,
			touchEnabled: false
		});
		
		var image1 = Ti.UI.createView({
			//backgroundColor: 'black',
			backgroundImage: data.image,
			width: thumbSize.width,
			height: thumbSize.height,
			touchEnabled: false
		});
		
		container.add(image1);
		
		var overlayImage;
			
		if(type === 'national'){
			overlayImage = 'images/poster_thumb_national.png'
		}
		else if (type === 'time'){
			overlayImage = 'images/poster_thumb_time.png'
		}
		else if (type === 'vogue'){
			overlayImage = 'images/poster_thumb_vogue.png'
		}
		else if (type === 'rollingStones'){
			overlayImage = 'images/poster_thumb_rolling_stones.png'
		}

		
		var overlay1 = Ti.UI.createView({
			backgroundImage: overlayImage,
			//backgroundTopCap: 20,
			//backgroundLeftCap: 20,
			width: 85,
			height: 85,
			touchEnabled: false
		});
		
		view.add(container);
		view.add(overlay1);
		
		return view;
	};
	
	posterSelector.add(createThumb('national'));
	posterSelector.add(createThumb('time'));
	posterSelector.add(createThumb('vogue'));
	posterSelector.add(createThumb('rollingStones'));
	
	
	scrollView.contentHeight = newSize.height/2+50+50+20+100
	//Assemble
	scrollView.add(navBar);
	scrollView.add(posterView);
	scrollView.add(switchBar);
	scrollView.add(posterSelector);
	
	win.add(scrollView)
	

	//Methods
	function closingAnimation (){	//WARNING Don't name closeAnimation. it causes crashes'		
		overlay.blurAll();
		win.animate(Ti.UI.createAnimation({
			duration: 250,
			//opacity: 0,
			transform: Titanium.UI.create2DMatrix().translate(0,480)
		}));
		
		setTimeout(function(){
			clean();
			globals.currentModal = null;
			win.close();
		},250);
	};

	function openAnimation (){
		win.animate(Ti.UI.createAnimation({
			duration:  250,
			//opacity: 1,
			transform: Titanium.UI.create2DMatrix().translate(0,0)
		}));
	};
	
	function clean (){
		if(win.preClean){win.preClean()};
		
		var navBarChildren = navBar.children;
		for(var i = navBarChildren.length - 1 ; i >= 0;  i--){
			navBar.remove(navBarChildren[i]);
			navBarChildren[i] = null;
		};
		navBarChildren = null;
		
		var posterViewChildren = posterView.children;
		for(var i = posterViewChildren.length - 1 ; i >= 0;  i--){
			posterView.remove(posterViewChildren[i]);
			posterViewChildren[i] = null;
		};
		posterViewChildren = null;
		
		var scrollChildren = scrollView.children;
		for(var i = scrollChildren.length - 1 ; i >= 0;  i--){
			scrollView.remove(scrollChildren[i]);
			scrollChildren[i] = null;
		};
		scrollChildren = null;
	
		var winChildren = win.children;
		for(var i = winChildren.length - 1 ; i >= 0;  i--){
			win.remove(winChildren[i]);
			winChildren[i] = null;
		};
		
		//globals.currentModal  = null
		Ti.API.debug('Cleaned Picture of the Week Detail');
	};
	
	
	//Expose
	win.closingAnimation = closingAnimation;
	win.clean = clean;
	

	//Listeners
	win.addEventListener('open', function (){
		openAnimation();
	});
	
	
	return win;
};


//Components
function textField (args){
	var label = Ti.UI.createLabel({
		backgroundColor: 'transparent',
		text: args.value,
		width: args.width,//'94%',
		height: args.height,
		color: args.color,
		top: args.top,
		bottom: args.bottom,
		textAlign:'center',
		font: args.font,
		shadowColor: 'black',
		shadowOffset:{x:1,y:1}
	});
	
	var textField = Ti.UI.createTextField({
		backgroundColor: 'transparent',
		value:args.value,
		width: args.width,//'94%',
		height: args.height,
		color: args.color,
		top: args.top,
		bottom:args.bottom,
		appearance:Titanium.UI.KEYBOARD_APPEARANCE_ALERT,
		autocorrect:false,
		suppressReturn: true,
		textAlign:'center',
		font: args.font,
		hintText: args.hintText,
		kind: 'textfield',
		opacity:1
	});
	
	//Could use label for shadow
	label.add(textField);
	
	label.addEventListener('click', function (e){
		//alert(e)
		//textField.focus();
	});
	
	label.textField = textField;
	
	return label;
};


function textArea(args){
	var textField = Ti.UI.createTextArea({
		backgroundColor: 'transparent',
		value: args.value,
		width: args.width,//'94%',
		bottom:args.bottom,
		top:args.top,
		height: args.height,
		color: args.color,
		left: args.left? args.left : null,
		//top:46,
		appearance: Titanium.UI.KEYBOARD_APPEARANCE_ALERT,
		autocorrect: false,
		suppressReturn: true,
		textAlign: args.textAlign? args.textAlign : 'center',
		font: args.font,
		hintText: args.hintText,
		kind: 'textfield',
	});
	
	return textField
};


function nationalGeographic (args){	
	var view = Ti.UI.createView({
		width: args.width,
		top: 0,
		left: 5, 
		height: args.height +2,
		kind:'national'
	});
	
	var overlay = Ti.UI.createView({
		backgroundImage:'images/poster_overlay_national.png',
		//backgroundTopCap: 50,
		//backgroundLeftCap: 50,
		width:args.width,
		height: view.height
	});
	
	var title1 = textArea({ 
		font: {fontFamily: 'TimesNewRomanMT-Condensed', fontSize: 30*2},
		//minimumFontSize: 24*2,
		width: 270*2,
		height: 30*2,
		top: 25*2,
		color: 'white',
		hintText: 'NATIONAL\n',
		value: 'NATIONAL\n'
	});
	
	title1.addEventListener('change', function(e){
		title1.value = e.value.toUpperCase();
		
		Ti.API.debug(e.value);
	});
	
	title1.addEventListener('return', function(e){
		title1.value = e.value+'\n';
	});
	
	var title2 = textArea({ 
		font: {fontFamily: 'TimesNewRomanMT-Condensed', fontSize: 30*2},
		minimumFontSize: 24*2,
		width: 270*2,
		height: 30*2,
		top: 50*2,
		color: 'white',
		hintText: 'GEOGRAPHIC\n',
		value: 'GEOGRAPHIC\n'
	});
	
	title2.addEventListener('change', function(e){
		title2.value = e.value.toUpperCase();
	});
	
	title2.addEventListener('return', function(e){
		title2.value = e.value+'\n';
	});
	
	
	var subTitle = textField({ 
		font: {fontFamily: 'Arial', fontSize: 15*2},
		minimumFontSize: 12*2,
		width: 270*2,
		height: 60*2,
		bottom: 15*2,
		color: 'white',
		value: 'Ciao Mamma guarda come mi diverto'//I('type_text_here')
	});

	view.add(overlay);
	view.add(title1);
	view.add(title2);
	view.add(subTitle);
	
	function clean (){
		view.remove(overlay);
		view.remove(title1);
		view.remove(title2);
		view.remove(subTitle);
	};
	
	function blurAll(){
		title1.blur();
		title2.blur();
		subTitle.blur();
	};
	
	view.blurAll = blurAll;
	view.clean = clean;
	view.title1 = title1;
	view.title2 = title2;
	view.subTitle = subTitle;
	
	return view;
};


function timeMagazine(args){
	var view = Ti.UI.createView({
		width: args.width,
		height: args.height +2,
		top: 0,
		left: 5, 
		kind:'time'
	});
	
	var overlay = Ti.UI.createView({
		backgroundImage:'images/poster_overlay_time.png',
		//backgroundTopCap: 50,
		//backgroundLeftCap: 50,
		width: args.widht,
		height: view.height
	});
	
	var title = textField({ 
		font: {fontFamily: 'Times New Roman', fontSize: 50*2},
		minimumFontSize: 24*2,
		width: 270*2,
		height: 60*2,
		top: 0,
		color: 'red',
		hintText: 'TIME',
		value: 'TIME'
	});
	
	
	
	title.addEventListener('change', function(e){
		title.value = e.value.toUpperCase();
	});
	
	var subTitle = textField({ 
		font: {fontFamily: 'News Gothic', fontWeight: 'bold', fontSize: 20*2},
		minimumFontSize: 12*2,
		width: 270*2,
		height: 25*2,
		bottom: 35*2,
		color: 'white',
		value: 'SHOKING NEWS'//I('type_text_here')
	});
	
	var paragraph = textArea({ 
		font: {fontFamily: 'Arial', fontSize: 13*2},
		minimumFontSize: 12*2,
		width: 270*2,
		height: 40*2,
		bottom: 0,
		color: 'white',
		value: 'Ciao Mamma guarda come mi diverto'//I('type_text_here')
	});
	
	view.add(overlay);
	view.add(title);
	view.add(subTitle);
	view.add(paragraph);
	
	function clean (){
		view.remove(overlay);
		view.remove(title);
		view.remove(subTitle);
		view.remove(paragraph);
	};
	
	function blurAll(){
		title.textField.blur();
		subTitle.textField.blur();
	};
	
	view.blurAll = blurAll;
	view.clean = clean;
	view.title = title;
	view.subTitle = subTitle;
	
	return view;
};


function rollingStones(args){
	var view = Ti.UI.createView({
		width: args.width,
		height: args.height,
		kind: 'rollingStones'
	});
	
	var title = textField({ 
		font: {fontFamily: 'Royal Acid', fontSize: 50*2},
		minimumFontSize: 24*2,
		width: 300*2,
		height: 60*2,
		top: 0,
		color: 'red',
		hintText: 'RollingStones',
		value: 'RollingStones'
	});
	
	var subTitle = textArea({ 
		font: {font: 'Arial', fontSize: 15*2},
		minimumFontSize: 12*2,
		height: 60*2,
		color: 'white',
		value: I('type_text_here')
	});
	
	view.add(title);
	view.add(subTitle);
	
	function clean (){
		view.remove(title);
		view.remove(subTitle);
	};
	
	function blurAll(){
		title.blur();
		subTitle.blur();
	};
	
	view.blurAll = blurAll;
	view.clean = clean;
	view.title = title;
	view.subTitle = subTitle;
	
	return view;
}


function vogue(args){
	var view = Ti.UI.createView({
		width: args.width,
		height: args.height,
		kind: 'vogue'
	});	
	
	var title = textField({ 
		font: {fontFamily: 'DubielPlain', fontSize: 80*2},
		minimumFontSize: 60*2,
		width: 300*2,
		height: 80*2,
		top: 0,
		color: 'pink',
		hintText: 'VOGUE',
		value: 'VOGUE'
	});
	
	var subTitle = textArea({ 
		font: {font: 'Arial Narrow', fontSize: 15*2},
		minimumFontSize: 12*2,
		width: 150,
		left: 10,
		bottom: 10,
		height: 60*2,
		color: 'pink',
		textAlign: 'left',
		value: "TOO SEXY FOR THIS APP"
	});
	
	view.add(title);
	view.add(subTitle);
	
	function clean (){
		view.remove(title);
		view.remove(subTitle);
	};
	
	function blurAll(){
		title.blur();
		subTitle.blur();
	};
	
	view.blurAll = blurAll;
	view.clean = clean;
	view.title = title;
	view.subTitle = subTitle;
	
	return view;
};


function motivational (args){
	var view = Ti.UI.createView({
		width: args.width,
		height: args.height
	});
	
	var title = textField({ 
		font: {font: 'Times New Roman', fontSize: 40*2},
		minimumFontSize: 34*2,
		height: 40*2,
		color: 'white',
		hintText: 'National\nGeografic'
	});
	
	var subTitle = textArea({ 
		font: {font: 'Arial', fontSize: 15*2},
		minimumFontSize: 12*2,
		height: 60*2,
		color: 'white',
		value: I('type_text_here')
	});
	
	view.add(overlay);
	view.add(title);
	view.add(subTitle);
	
	function clean (){
		view.remove(overlay);
		view.remove(title);
		view.remove(subTitle);
	};
	
	function blurAll(){
		title.blur();
		subTitle.blur();
	};
	
	view.blurAll = blurAll;
	view.clean = clean;
	view.title = title;
	view.subTitle = subTitle;
	
	return view;
}
