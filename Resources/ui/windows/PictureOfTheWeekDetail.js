exports.loadWin = function (data){
	
	var win = Ti.UI.createWindow({
		width: '100%',
		height: '100%',
		tabbedBarHidden: true,
		navBarHidden: true,
		tabBarHidden: true,
		//backgroundColor : '#3A3D3B',
		backgroundGradient : { type:'linear', colors:['#535353', '#232323'], startPoint:{ x:0, y:0 }, endPoint:{ x:0, y:480 }, backFillStart:false },
		//barColor: '#000',
		opacity: 1,
		//transform:Ti.UI.create2DMatrix().scale(1.2),
		//translucent : true,
		orientationModes: [Titanium.UI.PORTRAIT],
		transform: Titanium.UI.create2DMatrix().translate(0,480),
		kind: 'pictureOfTheWeekDetail'
	});
	
	globals.currentModal = win;
	
	///// NavBar
	var navBar = Ti.UI.createView({
		width: 320,
		height: 44
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundImage:'images/nav_x.png',
		//backgroundColor: 'red',
		width: 72,
		height: 44,
		top: 0,
		left: 0
	}); //TopNavigation.backButton(win,'white');
	
	closeButton.add(Ti.UI.createView({
		backgroundImage:'images/nav_x.png',
		width: 23,
		height: 23,
		top: 10,
		left: 10
	})); //TopNavigation.backButton(win,'white');
	
	closeButton.addEventListener('click', function (){
		closingAnimation();
	});

	
	navBar.add(closeButton);
	/////
	
	
	// Main Paper Area
	var scrollView = Ti.UI.createScrollView({
		height: Ti.Platform.displayCaps.platformHeight,
		width:  320,
		contentWidth: 300,
		contentHeight: 'auto',
		showVerticalScrollIndicator: false,
		layout: 'vertical'
	});
	
	var paperBg = Ti.UI.createView({
		backgroundImage: 'images/newspaper_bg.png',
		backgroundTopCap: 20,
		backgroundLeftCap: 20,
		width: 318,
		left: 2,
		height: Ti.UI.SIZE,
		top: 10,
		layout: 'vertical'
	});
	
	
	//Header Area
	var header = Ti.UI.createView({
		width: 320,
		height: 75,
	});
	
	header.add(Ti.UI.createLabel({
		text: I('Picture_of_the_Week'),
		font: {fontSize: 34, fontFamily: 'Old English Text MT'},
		color: globals.fontColors[2],
		top: 10,
	}));
	
	header.add(Ti.UI.createView({
		backgroundColor: '#3A3D3B',
		width: 280,
		height: 1,
		top: 53
	}))
	
	header.add(Ti.UI.createView({
		backgroundColor: '#3A3D3B',
		width: 280,
		height: 1,
		top: 74
	}));
	
	header.add(Ti.UI.createLabel({
		text: I('Week_of')+moment(data.data.date).format("MMM Do, YYYY"),
		font: {fontSize: 16, fontFamily: 'Georgia'},
		color: globals.fontColors[2],
		top: 53,
	}));
	//
	
	
	var Helper = require('lib/Helper');
	var newSize = Helper.imageResizeFill(270, 300, data.image.width, data.image.height);
	
	var containerScroll = Ti.UI.createScrollView({
		width: newSize.width,
		height: newSize.height,
		contentWidth: newSize.width,
		contentHeight: newSize.height,
		borderColor:'#999999',
		top:10,
		left:(310-newSize.width)/2+3,
		maxZoomScale:2,
		minZoomScale:1
	});
	
	var filterableimageview = require('jp.msmc.filterableimageview');
	var grey = new filterableimageview.Filter(function(){
        this.saturation(-1);
   });
    var normal = new filterableimageview.Filter(function(){
        this.saturation(0);
    });
	//var image = Ti.UI.createView({	
	var image =  filterableimageview.createFilterableImageView({	
		//backgroundImage: blob,
		image: data.image,
		//borderColor:'#999999',
		//backgroundColor: 'gray',
		width: newSize.width,
		height: newSize.height,
		//top: 10,
		//left: (310-newSize.width)/2+3,
		filter: grey
	});
	
	containerScroll.add(image);


	var captionLabel = Ti.UI.createLabel({
		text: data.data.awardCaption,
		textAlign: 'center',
		width: 260,
		height: Ti.UI.SIZE,
		top: 5,
		bottom: 10,
		//minimumFontSize: 12,
		font:{ fontFamily: 'Georgia', fontSize: 14}
	});
	
	
	paperBg.add(header);
	paperBg.add(containerScroll);
	paperBg.add(captionLabel);
	paperBg.add(Ti.UI.createView({height: 10}));
	////////
	
	var filterArea = Ti.UI.createView({
		height: 60,
		width:Ti.UI.SIZE,
		layout: 'horizontal'
	});
	
	function switchButtons(mode){
		if (mode=== 'b&w'){
			blackAndWhiteButton.selected = true;
			colorButton.selected = false;
			blackAndWhiteButton.backgroundImage = 'images/POW_B&W_button_sel.png';
			colorButton.backgroundImage = 'images/POW_Color_button.png';
			image.filter = grey;
		}
		else{
			blackAndWhiteButton.selected = false;
			colorButton.selected = true;
			blackAndWhiteButton.backgroundImage = 'images/POW_B&W_button.png';
			colorButton.backgroundImage = 'images/POW_Color_button_sel.png';
			image.filter = normal;
		}
	};
	
	var blackAndWhiteButton = Ti.UI.createView({
		backgroundImage: 'images/POW_B&W_button_sel.png',
		width:47,
		height:42,
		left:-2,
		selected: true
	});
	
	blackAndWhiteButton.addEventListener('touchstart', function(){
		if (blackAndWhiteButton.selected === false){
			switchButtons('b&w');
		};
	})
	
	var colorButton = Ti.UI.createView({
		backgroundImage: 'images/POW_Color_button.png',
		//backgroundColor: 'red',
		width:47,
		height:42,
		selected: false
	});
	
	colorButton.addEventListener('touchstart', function(){
		if (colorButton.selected === false){
			switchButtons('color');
		};
	});
	
	filterArea.add(colorButton);
	filterArea.add(blackAndWhiteButton);
	
	
	//Share Area
	var ShareButton = require('ui/components/ShareButton');
	var shareView = Ti.UI.createView({
		//backgroundColor: 'red',
		height: 80,
	});
	
	
	//Must use this to give time to assemble the picture
	//setTimeout( function (){
	
	var facebookButton = ShareButton.facebookButton();
	facebookButton.left = 10;
	
	facebookButton.addEventListener('click', function (){
		//var PopUp = require('ui/components/PopUp');
		//PopUp.loadingPopUp('Loading poster...');
		ShareButton.postToFacebookPhoto(createPosterImage(data),facebookButton);
		//Ti.App.fireEvent('closeLoadingPopUp');
	});
	
	var twitterButton = ShareButton.twitterButton();
	
	twitterButton.addEventListener('click', function(e) {
		//var PopUp = require('ui/components/PopUp');
		//PopUp.loadingPopUp('Loading poster...');
		
		var Twitter = require('com.0x82.twitter');
		var composer = Twitter.createTweetComposerView();
		composer.addImage(createPosterImage(data)); // must be a TiBlob
		composer.open();
		 
	  	//Ti.App.fireEvent('closeLoadingPopUp');
	});
	
	
	var emailButton = ShareButton.emailButton();
	
	emailButton.addEventListener('click',function (){
		//var PopUp = require('ui/components/PopUp');
		//PopUp.loadingPopUp('Loading poster...');
		//data.image = blob;
		data.image = image.filteredImage();
		ShareButton.emailPhoto(createPosterImage(data));
		
		
		//Ti.App.fireEvent('closeLoadingPopUp');
	});

	shareView.add(facebookButton);
	shareView.add(emailButton);
	shareView.add(twitterButton);
	//}, 100);
	///
	
	
	scrollView.add(navBar);
	scrollView.add(paperBg);
	scrollView.add(filterArea);
	scrollView.add(shareView);
	
	
	//Assemble
	win.add(scrollView);
	

	//Methods
	function closingAnimation (){	//WARNING Don't name closeAnimation. it causes crashes'		
		win.animate(Ti.UI.createAnimation({
			duration: 250,
			//opacity: 0,
			transform: Titanium.UI.create2DMatrix().translate(0,480)
		}));
		
		setTimeout(function(){
			clean();
			globals.currentModal = null;
			win.close();
		},250);
	};

	function openAnimation (){
		win.animate(Ti.UI.createAnimation({
			duration:  250,
			//opacity: 1,
			transform: Titanium.UI.create2DMatrix().translate(0,0)
		}));
	};
	
	function clean (){
		if(win.preClean){win.preClean()};
		
		var navBarChildren = navBar.children;
		for(var i = navBarChildren.length - 1 ; i >= 0;  i--){
			navBar.remove(navBarChildren[i]);
			navBarChildren[i] = null;
		};
		navBarChildren = null;
		
		var headerChildren = header.children;
		for(var i = headerChildren.length - 1 ; i >= 0;  i--){
			header.remove(headerChildren[i]);
			headerChildren[i] = null;
		};
		headerChildren = null;
		
		var pagerBgChildren = paperBg.children;
		for(var i = pagerBgChildren.length - 1 ; i >= 0;  i--){
			paperBg.remove(pagerBgChildren[i]);
			pagerBgChildren[i] = null;
		};
		pagerBgChildren = null;
		
		var shareViewChildren = shareView.children;
		for(var i = shareViewChildren.length - 1 ; i >= 0;  i--){
			shareView.remove(shareViewChildren[i]);
			shareViewChildren[i] = null;
		};
		shareViewChildren = null;
		
		var scrollChildren = scrollView.children;
		for(var i = scrollChildren.length - 1 ; i >= 0;  i--){
			scrollView.remove(scrollChildren[i]);
			scrollChildren[i] = null;
		};
		scrollChildren = null;
	
		var winChildren = win.children;
		for(var i = winChildren.length - 1 ; i >= 0;  i--){
			win.remove(winChildren[i]);
			winChildren[i] = null;
		};
		
		//globals.currentModal  = null
		Ti.API.debug('Cleaned Picture of the Week Detail');
	};
	
	
	//Expose
	win.closingAnimation = closingAnimation;
	win.clean = clean;
	

	//Listeners
	win.addEventListener('open', function (){
		openAnimation();
	});
	
	
	return win;
};


function filterImage(imageData){
	var filterableimageview = require('jp.msmc.filterableimageview');
	
	var win = Ti.UI.createWindow({
		width: 2000,
		height: 2000,
		navBarHidden: true,
		tabBarHidden: true,
		//left:-2000
	});
	
	var imageF = filterableimageview.createFilterableImageView({		
		image: imageData,
		//width: 100,
		//height: 100,
		filter:[{name: 'grayscale'}]
	});
	
	win.add(imageF);
	win.open();
	
	setTimeout(function (){
		win.remove(imageF);
		imageF = null;
		win.close();
		win = null;
		Ti.API.debug('Cleaned Picture of The Week filtered Image');
	},500);
	
	return imageF.filteredImage();
};


function createPosterImage(data){
	var scale = 960/320;
	
	var win = Ti.UI.createWindow({
		width: 2000,
		height: 2000,
		navBarHidden: true,
		tabBarHidden: true,
		left:-2000
	});
	
	var view = Ti.UI.createWindow({
		//backgroundImage: 'images/newspaper_bg.png',
		backgroundColor: 'white',
		backgroundTopCap: 20,
		backgroundLeftCap: 20,
		width: 318*scale,
		left: 2*scale,
		height: Ti.UI.SIZE,
		top: 10*scale,
		layout: 'vertical',
		opacity:1
	});
	
	
	//Header Area
	var header = Ti.UI.createView({
		width: 320*scale,
		height: 75*scale,
	});
	
	header.add(Ti.UI.createLabel({
		text: I('Picture_of_the_Week'),
		font: {fontSize: 34*scale, fontFamily: 'Old English Text MT'},
		color: globals.fontColors[2],
		top: 10*scale,
	}));
	
	header.add(Ti.UI.createView({
		backgroundColor: '#3A3D3B',
		width: 280*scale,
		height: 1*scale,
		top: 53*scale
	}))
	
	header.add(Ti.UI.createView({
		backgroundColor: '#3A3D3B',
		width: 280*scale,
		height: 1*scale,
		top: 74*scale
	}));
	
	header.add(Ti.UI.createLabel({
		text: I('Week_of')+moment(data.data.date).format("MMM Do, YYYY"),
		font: {fontSize: 16*scale, fontFamily: 'Georgia'},
		color: globals.fontColors[2],
		top: 53*scale,
	}));
	//
	
	
	var Helper = require('lib/Helper');
	var newSize = Helper.imageResizeFill(280*scale, 300*scale, data.image.width, data.image.height);
	
	var filterableimageview = require('jp.msmc.filterableimageview');
	
	//var image =  filterableimageview.createFilterableImageView({	
	var image = Ti.UI.createView({
		backgroundImage: data.image,
		//image:data.image,
		borderColor:'#999999',
		//backgroundColor: 'gray',
		width: newSize.width,
		height: newSize.height,
		top: 10*scale,
		left: (310*scale-newSize.width)/2+3*scale,
	});

	var captionLabel = Ti.UI.createLabel({
		text: data.data.awardCaption,
		textAlign: 'center',
		width: 260*scale,
		height: Ti.UI.SIZE,
		top: 5*scale,
		bottom: 10*scale,
		//minimumFontSize: 12,
		font:{ fontFamily: 'Georgia', fontSize: 14*scale}
	});
	
	
	view.add(header);
	view.add(image);
	view.add(captionLabel);
	view.add(Ti.UI.createView({height: 30}));
	
	win.add(view);
	win.open();
	
	setTimeout(function (){
		newSize = null;
		view.remove(header);
		view.remove(image);
		view.remove(captionLabel);
		win.remove(view);
		header = null;
		image = null;
		captionLabel = null;
		view = null;
		win.close();
		win = null;
		Ti.API.debug('Cleaned Picture of The Week Poster');
	},200);

	//win.transform = Ti.UI.create2DMatrix().scale(.3)
	
	/*
	if (view.width > 960){
		var ratio = view.height/posterImage.width;
		view.width = 960;
		view.height = ratio * 960;
	};
	*/
	
	return view.toImage();
};
