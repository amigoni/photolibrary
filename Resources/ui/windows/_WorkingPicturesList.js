exports.loadWin = function (listType,tagType,tagID){	
	var Window = require('ui/components/Window');
	var TopNavigation = require('ui/components/TopNavigation');
	var DataMedia = require('/model/DataMedia');
	var DataPersons = require('model/DataPersons');
	var MiscComponents = require('ui/components/MiscComponents');

	//Load Data
	var dataIn;
	
	function getData(){
		//Get the new assets
		if (tagType === 'person'){
			dataIn = DataPersons.loadPictureListData(listType,tagType,tagID);
		}
		else if  (tagType === 'label'){
			var DataLabels = require('model/DataLabels');
			dataIn = DataLabels.loadPictureListData(listType,tagType,tagID);
		};
		
		if (dataIn.assets.length >  0 ){
			var PopUp = require('ui/components/PopUp');
			//PopUp.loadingPopUp('Loading...')
		};	
	};
	
	getData();
	
	//Create Window
	var win = Window.createWindow({
		tabBarHidden:true,
		tabbedBarHidden:true,
	});
	
	var centerTitle = TopNavigation.centerDoubleLabel(dataIn.title, dataIn.subTitle);
	win.centerTitle = centerTitle;
	win.title = dataIn.title;
	win.subTitle = dataIn.subTitle;
	//globals.tabGroup.navBar.update(win);
	
	
	//Set Navigations	
	var addButton = TopNavigation.addButton();
	
	addButton.addEventListener('click', function (){
		var LibraryPicturesPicker = require('/ui/windows/LibraryPicturesPicker');
		 LibraryPicturesPicker.loadWin(tagType,tagID);
		//win1.open();
		//globals.tabGroup.getActiveTab().open(win1,{animated:true});
	});
	
	win.rightNavButton = addButton;
	win.leftNavButton = TopNavigation.backButton(win);
	
	
	//ScrollView
	var PicturesList = require('ui/windows/PicturesList');
	var scrollView = PicturesList.picturesScrollView(dataIn, DataPersons.clickFunction, MiscComponents.createStandardThumbnail,win);	
	
	
	function update(){
		DataMedia.loadPictureListMedia(dataIn.assets,scrollView.refreshScrollView);
	};
	
	update();
	
	
	var refreshData = function (){
		getData();
		update();
	};
	
	win.add(scrollView);
	
	//Event listeners and Cleanup 
	Ti.App.addEventListener('updatePicturesList', refreshData);
	
	function preClean (){
		scrollView.clean();
		Ti.App.removeEventListener('updatePicturesList', refreshData);
	};
	
	win.preClean = preClean;
	
	return win;
};



exports.picturesScrollView = function (dataIn,clickFunction,createThumbnailFunction,win){
	var images = dataIn;
	
	var scrollView = Ti.UI.createScrollView({
		width: 320,
		height: 480-44,
		top:44,
		contentHeight: 460-42+1,
		contentWidth: 320,
		showVerticalScrollIndicator: true,
		win:win
	});
	
	//Cleans up the mess. Safeguard.
	var clean = function (){
		var thumbs = contentContainer.children;
		if (thumbs){
			for (var i =0; i < thumbs.length;i++){
				contentContainer.remove(thumbs[i]);
				thumbs[i] = null;
			};
		};
		
		var children = scrollView.children;
		if (children){
			for (var i =0; i < children.length;i++){
				scrollView.remove(children[i]);
			};
		};
		
		Ti.API.debug('Cleaned ScrollView')
	};
	
	//Makes it scrollView even if it's too small
	scrollView.addEventListener('postlayout', function(){
		if (scrollView.contentHeight < scrollView.height){
			scrollView.contentHeight = scrollView.height+1;
		};
	});
	
	scrollView.addEventListener('click', function (e){
		clickFunction(scrollView,e.source);
	});
	
	//Layout Calculations	
	var hSpacing = 5;
	var vSpacing = 5; 
	var numberOfColumns = 3;
	var thumbWidth = (320-(hSpacing*(numberOfColumns+1)))/numberOfColumns;
	
	var columns = []; //Array of columns to calculate the tallest
	
	var column1 = {};
	column1.left = hSpacing;
	column1.height = 0;
	
	var column2 = {};
	column2.left = column1.left +hSpacing+thumbWidth;
	column2.height = 0;
	
	var column3 = {};
	column3.left = column2.left+hSpacing+thumbWidth;
	column3.height = 0;
	
	columns.push(column1);
	columns.push(column2);
	columns.push(column3);
	
	var currentColumn = column1;
	
	//Function that adds item to the scrollView. 
	var currentColumnNumber = 0;
	
	var contentContainer = Ti.UI.createView({
		width: 320,
		height: 5,
		top: 0
	});
	
	function addItem (item){
		currentColumn = columns[currentColumnNumber];
		item.top = currentColumn.height+vSpacing;
		item.left = currentColumn.left
		currentColumn.height +=  item.height +vSpacing;
	
		contentContainer.add(item);
		//Sort the columns by the highest and set the content height to the highest.
		function sortTallest(ob1,ob2) {
	    	var n1 = ob1.height;
	    	var n2 = ob2.height;
	    	if (n1 < n2) {return 1}
			else if (n1 > n2){return -1}
		    else { return 0}//nothing to split
		};
		
		columns.sort(sortTallest);
		contentContainer.height = columns[0].height;
		
		//currentColumn = columns[numberOfColumns-1];
		currentColumnNumber = (currentColumnNumber < numberOfColumns-1) ? (currentColumnNumber = currentColumnNumber+1) :(currentColumnNumber = 0) ;
	};
	
	
	var refreshScrollView = function(data2){
		//alert(data2)
		if(contentContainer.children){
			var children = contentContainer.children;
			
			for (var i =0; i < children.length; i++){
				contentContainer.remove(children[i]);
				column1.height = 0;
				column2.height = 0;
				column3.height = 0;
				currentColumn = column1;
				currentColumnNumber = 0;
			};
		};
		
		//Update the Data Array
		images = data2;
		scrollView.images = images;
		
		//Load Initial 
		loadInitialData(images);	
	};
	
	
	// Scrolling
	var updating = false;
	var lastDistance = 0;
	
	scrollView.addEventListener('scroll',function(e){
		var offset = e.source.contentOffset.y;
		var height = e.source.height;
		var total = offset + height;
		var theEnd = contentContainer.size.height;
		var distance = theEnd - total;
	
		// going down is the only time we dynamically load,
		// going up we can safely ignore -- note here that
		// the values will be negative so we do the opposite
		if (distance < lastDistance){
			// adjust the % of rows scrolled before we decide to start fetching
			var nearEnd = theEnd * .75;
	
			if (updating== false && total >= nearEnd&& (scrollView.lastLoadedIndex < images.length)){
				//beginUpdate();
			};
		};
		lastDistance = distance;
	});
	
	
	function beginUpdate(){
		Ti.API.debug('Began Update');
		updating = true;
		
		//Add 100 items
		var currentIndex = scrollView.lastLoadedIndex;
		for (var i = currentIndex; i < (images.length < currentIndex +100 ? images.length : currentIndex +100); i++){
			Ti.API.debug('Total Images: '+images.length+' CurrentImage:'+scrollView.lastLoadedIndex);
			scrollView.addItem(createThumbNailFunction(i,images[scrollView.lastLoadedIndex]));
			scrollView.lastLoadedIndex++;
		};
		
		endUpdate();
	};
	
	
	function endUpdate(){
		updating = false;
		//scrollView.contentContainer.height =Ti.UI.SIZE;
		scrollView.contentHeight = contentContainer.height;
		Ti.API.debug('endUpdate')
	};
	
	
	function loadInitialData (imagesData){
		//Load the first 40 pictures	
		var startTime = new Date().getTime();
		
		for (var i = 0; i < (imagesData.length > 10000 ? 10000: imagesData.length); i++){
			scrollView.addItem(createThumbnailFunction(i,imagesData[i],images));
			scrollView.lastLoadedIndex = i;
		};
		
		var endTime = new Date().getTime();
		Ti.API.info('Loaded  '+imagesData.length+' in Picture List in: ' + (endTime - startTime) + 'ms');
		
		scrollView.contentHeight = contentContainer.height;
		
		globals.popUpDelaySwitch = false; 
		Ti.App.fireEvent('closeLoadingPopUp');
	};
	
	scrollView.add(contentContainer);
	
	//Expose Properties and Functions
	//scrollView.contentContainer = contentContainer;
	scrollView.addItem = addItem;
	scrollView.refreshScrollView  = refreshScrollView;
	scrollView.images = images;
	scrollView.clean = clean;
	
	return scrollView;	
};