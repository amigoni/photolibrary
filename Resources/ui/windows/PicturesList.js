exports.loadWin = function (listType, tagType, tagID, isThumbRow){	
	var Window = require('ui/components/Window');
	var TopNavigation = require('ui/components/TopNavigation');
	var DataMedia = require('/model/DataMedia');
	var DataPersons = require('model/DataPersons');
	var DataLabels = require('model/DataLabels');
	var MiscComponents = require('ui/components/MiscComponents');
	var PicturesScrollView = require('ui/components/PicturesScrollView');

	//Load Data
	function getData(){
		var data;
		//Get the new assets
		if (tagType === 'person'){
			data = DataPersons.loadPictureListData(listType,tagType,tagID);
		}
		else if  (tagType === 'label'){
			var DataLabels = require('model/DataLabels');
			data = DataLabels.loadPictureListData(listType,tagType,tagID);
		};
		
		if (data.assets.length >  0 ){
			var PopUp = require('ui/components/PopUp');
			//PopUp.loadingPopUp('Loading...')
		};	
		
		return data;
	};
	
	var dataIn = getData();
	
	
	//Create Window
	var win = Window.createWindow({
		//navBarHidden: true,
		tabBarHidden: true,
		tabbedBarHidden: true,
		selectionMode: 'single'
	});
	
	
	
	//Set Navigations	
	win.leftNavButton = TopNavigation.backButton(win);
	
	var shareButton = TopNavigation.shareButton();

	shareButton.addEventListener('click', function (){
		toggleSelectMode();
	});
	
	win.rightNavButton = shareButton;
	
	var centerTitle = TopNavigation.centerDoubleLabel(dataIn.title, dataIn.subTitle, 'white');
	win.centerTitle = centerTitle;
	win.titleControl = centerTitle;
	
	win.title = dataIn.title;
	win.subTitle = dataIn.subTitle;
	
	/*
	var addButton = TopNavigation.addButton();
	
	addButton.addEventListener('click', function (){
		var LibraryPicturesPicker = require('/ui/windows/LibraryPicturesPicker');
		 LibraryPicturesPicker.loadWin(tagType,tagID);
		//win1.open();
		//globals.tabGroup.getActiveTab().open(win1,{animated:true});
	});
	
	//win.rightNavButton = addButton;
	*/
	
	
	
	//HintText
	var hintText = Ti.UI.createLabel({
		text: createHintText(listType,tagType,dataIn.title),
		width: 300,
		height: Ti.UI.SIZE,
		bottom: 10,
		font: {fontSize: 14},
		textAlign: 'center',
		color: globals.fontColors[2],
		shadowColor: 'white',
		shadowOffset: {x:0,y:1},
		visible: dataIn.assets.length > 9 ? false : true
	});	
	
	
	var emptyListLabel = Ti.UI.createLabel({
		text: 'There are no pictures in this list',
		width: 300,
		height: Ti.UI.SIZE,
		top: 55,
		font: {fontSize: 14},
		textAlign: 'center',
		color: globals.fontColors[2],
		shadowColor: 'white',
		shadowOffset: {x:0,y:1},
		visible: dataIn.assets.length > 0 ? false : true
	});	
	
	
	//ScrollView
	var scrollView = new PicturesScrollView(dataIn.assets.length, MiscComponents.createStandardThumbnail,win);	
	scrollView.clickable = true; 
	
	scrollView.addEventListener('loadMoreData', function(e){
		DataMedia.loadPictureListMedia(dataIn.assets, scrollView.loadData, e.startIndex, e.endIndex);
	});
	
	
	var scrollViewClick = function(e){
		var thumb = e.source; 

		if (win.selectionMode === 'single'){
			if (isThumbRow === true){
				
				//For selecting Cover picture
				function closeWin (){
					globals.currentStack.navGroup.close(win);
					Ti.App.removeEventListener('thumbHeaderRowLoaded', closeWin);
				};
							
				Ti.App.addEventListener('thumbHeaderRowLoaded', closeWin);
				///
				
				if (tagType === 'person'){
					var DataPersons = require('model/DataPersons');
					DataPersons.setThumbForPerson(tagID, e.source.backgroundImage, e.source.URL);
					Ti.App.fireEvent('updatePersonDetail',{getAlsoThumbNail: true});
				}
				else if(tagType === 'label'){
					var DataLabels = require('model/DataLabels');
					DataLabels.setThumbForLabel(tagID,e.source.backgroundImage,e.source.URL);
					Ti.App.fireEvent('updateLabelDetail',{getAlsoThumbNail: true});
				};
				
				//globals.currentStack.navGroup.close(win);
			}
			else{
				//scrollView.images = scrollView.imagesDataArray;
				
				//clickFunction(scrollView,e.source);
				var dictionary = {};
				dictionary.winTitle =scrollView.win.title;
				dictionary.winSubtitle = scrollView.win.subTitle;
				
				dictionary.currentPageIndex = thumb.index;
				dictionary.numberOfAssets = scrollView.numberOfAssets;
				dictionary.imagesDataArray = scrollView.imagesDataArray;
				
			
				var DataLabels = require('model/DataLabels');
				dictionary.requestLabelDataFunction = DataLabels.queryLabelsData;
				
				var PicturesGallery = require('ui/windows/PicturesGallery');
				var win1 = PicturesGallery.loadWin(dictionary,tagType);
				
				win1.addEventListener('loadMoreData', function (e){
					function updateData(newImagesData){
						win1.imagesDataArray = win1.imagesDataArray.concat(newImagesData);
						Ti.API.debug('ImageDataArray Length '+win1.imagesDataArray.length);
						
						win1.fireEvent('finishedLoadingMoreData');
						win1.loadingMoreData = false;
					};
					
					DataMedia.loadPictureListMedia(dataIn.assets,updateData,e.startIndex,e.endIndex);
				});
				
				win1.open();
			};
		};
	};	
	
	scrollView.addEventListener('click', function(e){
		if (scrollView.clickable === true){
			scrollViewClick(e);
			scrollView.clickable = false; 
			var clickTimeout = setTimeout(function(){
				scrollView.clickable = true; 
			},
			2000);
		};
	});	
	
	
	
	//Assemble
	win.add(emptyListLabel);
	win.add(hintText);
	win.add(scrollView);
	
	
	
	//Weird Way of doing this. Asking for data Twice. Need to Simplify. 
	//This was done to make it consistant with the other method so that I can use just one scrollView. 
	DataMedia.loadPictureListMedia(dataIn.assets,scrollView.refreshScrollView,0,29);
	
	
	//Methods
	var refreshData = function (e){
		length = e.length ? e.length : 99; 
		
		var newData = getData();
		DataMedia.loadPictureListMedia(newData.assets, scrollView.refreshScrollView, 0, length);
		
		if (newData.assets.length > 9){
			hintText.visible = false;
		}
		else{
			hintText.visible = true;
		};
		scrollView.selectionEnabled = true; 
		scrollView.selectedThumb = null;
	};
		
	
	function toggleSelectMode (){
		if (win.selectionMode === 'multiple'){
			win.selectionMode = 'single';
			scrollView.clearSelectedThumbs();
		}
		else {
			var SelectMultipleOverlay = require('ui/components/SelectMultipleOverlay');
			SelectMultipleOverlay.appear(win, scrollView);
		};
	};
	
	
	function checkIfDataHasChanged (){
		var newData = getData();
		
		function arraysEqual(arr1, arr2) {
		    if(arr1.assets.length !== arr2.assets.length)
		        return false;
		    for(var i = arr1.assets.length; i--;) {
		        if(arr1.assets[i] !== arr2.assets[i])
		            return false;
		    };
		    return true;
		};
		
		if (arraysEqual(newData,dataIn) === false){
				refreshData(newData);
		};	
	};
	
	function updatePictureListCount(){
		var updateAssetsList = getData();
		scrollView.numberOfAssets = updateAssetsList.assets.length;
		scrollView.checkIfStillLoading();
	};
	
	
	function preClean (){
		scrollView.clean();
		Ti.App.removeEventListener('updatePicturesList', refreshData);
		Ti.App.removeEventListener('checkIfDataHasChanged', checkIfDataHasChanged);
		Ti.App.removeEventListener('pictureListUpdateListCount', updatePictureListCount);
	};
	
	
	//Expose
	win.preClean = preClean;
	win.scrollView = scrollView;
	win.toggleSelectMode = toggleSelectMode;
	
	//Listeners
	Ti.App.addEventListener('checkIfDataHasChanged', checkIfDataHasChanged);
	Ti.App.addEventListener('updatePicturesList', refreshData);
	Ti.App.addEventListener('pictureListUpdateListCount', updatePictureListCount);
	
	
	return win;
};


function createHintText (listType,tagType,variableName){
	var labelText; 
	
	if (listType === 'allPictures'){
		var pronoun = tagType === 'person' ? 'Tag pictures with ' : 'Label pictures with '
		labelText = pronoun+'\n'+variableName+' to add them to this list.' 
	}
	else if (listType === 'favorites'){
		var pronoun = tagType === 'person' ? 'of ' : 'labeled with '
		labelText = 'Press the star button on pictures '+pronoun+'\n'+variableName+' to add them to this list.' 

	}
	else if (listType === 'together'){
		//var pronoun = tagType === 'person' ? 'with ' : 'labeled with '
		labelText = 'Tag pictures with yourself and\n'+variableName+' to add them to this list.' 
	}
	
	return labelText
};