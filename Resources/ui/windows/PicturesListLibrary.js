exports.loadWin = function (group, isRootWindow){
	isRootWindow = isRootWindow ? isRootWindow : false;
	
	var DataMedia = require('/model/DataMedia');
	var DataPersons = require('model/DataPersons');
	var TopNavigation = require('ui/components/TopNavigation');
	var MiscComponents = require('ui/components/MiscComponents');
	var Window = require('ui/components/Window');
	var PicturesScrollView = require('ui/components/PicturesScrollView');
	
	//Create Window
	var win = Window.createWindow({
		tabbedBarHidden:true,
		selectionMode: 'single'
	});
	
	win.leftNavButton = isRootWindow === false ? TopNavigation.backButton(win) : TopNavigation.menuButton();
	
	var shareButton = TopNavigation.shareButton();

	shareButton.addEventListener('click', function (){
		toggleSelectMode();
	});
	
	win.rightNavButton = shareButton;
	
	var centerTitle = TopNavigation.centerSingleLabel(group.name,'white');
	win.centerTitle = centerTitle;
	win.titleControl = centerTitle;
	win.title = group.name;
	win.subTitle = '';
	////
	
	
	var emptyListLabel = Ti.UI.createLabel({
		text: 'There are no pictures in this list',
		width: 300,
		height: Ti.UI.SIZE,
		top: 55,
		font: {fontSize: 14},
		textAlign: 'center',
		color: globals.fontColors[2],
		shadowColor: 'white',
		shadowOffset: {x:0,y:1},
		visible: false//numberOfAssets > 0 ? false : true
	});	
	
	
	var scrollView = new PicturesScrollView(group.numberOfAssets, MiscComponents.createStandardThumbnail,win);
	scrollView.clickable = true;
	
	scrollView.addEventListener('loadMoreData', function(e){
		DataMedia.getGroupAssets(group, scrollView.loadData, e.startIndex, e.endIndex);
	});
	
	var scrollViewClick = function(e){
		var thumb = e.source; 
	
		if (win.selectionMode === 'single'){
			//scrollView.images = scrollView.imagesDataArray;
			//clickFunction(scrollView,e.source);
			var dictionary = {};
			dictionary.winTitle = scrollView.win.title;
			dictionary.winSubtitle = scrollView.win.subTitle;
			
			dictionary.currentPageIndex = thumb.index;
			dictionary.numberOfAssets = scrollView.numberOfAssets;
			dictionary.imagesDataArray = scrollView.imagesDataArray;
			
			var DataLabels = require('model/DataLabels');
			dictionary.requestLabelDataFunction = DataLabels.queryLabelsData;
			
			var PicturesGallery = require('ui/windows/PicturesGallery');
			var win1 = PicturesGallery.loadWin(dictionary);
			
			win1.addEventListener('loadMoreData', function (e){
				function updateData(newImagesData){
					win1.imagesDataArray = win1.imagesDataArray.concat(newImagesData);
					Ti.API.debug('ImageDataArray '+win1.imagesDataArray);
					
					win1.fireEvent('finishedLoadingMoreData');
					win1.loadingMoreData = false;
				};
				
				DataMedia.getGroupAssets(group,updateData,e.startIndex,e.endIndex);
			});
			
			win1.open();
		};
	};
	
	scrollView.addEventListener('click', function(e){
		if (scrollView.clickable === true){
			scrollViewClick(e);
			scrollView.clickable = false; 
			var clickTimeout = setTimeout(function(){
				scrollView.clickable = true; 
			},
			2000);
		};
	});	
	
	
	win.add(emptyListLabel);
	
	//Callback for the loading data function
	var update = function (data, assetsCount){
		scrollView.numberOfAssets = assetsCount;
		scrollView.refreshScrollView(data);
		win.add(scrollView);
		scrollView.selectionEnabled = true; 
		scrollView.selectedThumb = null;
		if(scrollView.numberOfAssets === 0){emptyListLabel.visible = true}
		else{emptyListLabel.visible = false}
	};
	
	function refreshData (){
		DataMedia.getGroupAssets(group,update,0,29);
	};
	
	refreshData();
	
	function toggleSelectMode (){
		if (win.selectionMode === 'multiple'){
			win.selectionMode = 'single';
			scrollView.clearSelectedThumbs();
		}
		else {
			var SelectMultipleOverlay = require('ui/components/SelectMultipleOverlay');
			SelectMultipleOverlay.appear(win, scrollView);
		};
	};
	
	
	//Event listeners and Cleanup 
	Ti.App.addEventListener('updatePicturesList', refreshData);
	
	function preClean (){
		scrollView.clean();
		win.remove(scrollView);
		scrollView = null;
		Ti.App.removeEventListener('updatePicturesList', refreshData);
	};
	
	win.preClean = preClean;
	win.scrollView = scrollView;
	win.toggleSelectMode = toggleSelectMode;
	
	win.addEventListener('focus', function(){
		if (scrollView.selectedThumb){
			scrollView.selectedThumb.toggleOverlay();
			scrollView.selectedThumb = null;
			scrollView.selectionEnabled = true; 
		};
	});
	
	return win;
};