exports.loadWin = function (){
	var Window = require('ui/components/Window');
	var TopNavigation = require('ui/components/TopNavigation');
	var MiscComponents = require('ui/components/MiscComponents');
	
	
	//Load Data
	var DataPicturesOfTheWeek = require('model/DataPictureOfTheWeek');
	var dataIn = DataPicturesOfTheWeek.loadPictureOfTheWeekListData();
	
	
	//Create Window
	var win = Window.createWindow({
		tabBarHidden: true,
		tabbedBarHidden: false,
		kind: 'pictureOfTheWeekList'
	});
	
	globals.currentNavGroupWindow = win;
	
	
	//Set Navigation		
	var menuButton = TopNavigation.menuButton();
	
	var centerTitle = TopNavigation.centerSingleLabel(I('Picture_of_the_Week'),'white');
	
	win.leftNavButton = menuButton;
	win.centerTitle = centerTitle;
	win.titleControl = centerTitle;
	
	
	//Create Tableview
	var header = Ti.UI.createView({
		width: 320,
		height: 70,
	});
		
	header.add(Ti.UI.createLabel({
		text: I('Picture_of_the_Week'),
		font: {fontSize: 36, fontFamily: 'Old English Text MT'},
		color: globals.fontColors[2]
	}));
	
	var tableData = [];
	
	var tableView = MiscComponents.standardTableView();
	tableView.headerView = header;
	tableView.clickable = true;	
	
	
	function openGallery (kind){
		var dictionary = {};
		dictionary.winTitle = I('Picture_of_the_Week');
		dictionary.currentPageIndex = 0;
		dictionary.numberOfAssets = kind === 'lastWeekRow' ?  dataPickRows.lastWeekPictures.length : dataPickRows.thisWeekPictures.length ;
		dictionary.winSubtitle = '';
		dictionary.imagesDataArray = kind === 'lastWeekRow' ?  dataPickRows.lastWeekPictures : dataPickRows.thisWeekPictures;
		
		var PicturesGallery = require('ui/windows/PicturesGallery');
		var win1 = PicturesGallery.loadWin(dictionary, 'person' , kind);
		
		/*
		win1.addEventListener('loadMoreData', function (e){
			function updateData(newImagesData){
				win1.imagesDataArray = win1.imagesDataArray.concat(newImagesData);
				Ti.API.info('ImageDataArray Length '+win1.imagesDataArray.length);
				
				win1.fireEvent('finishedLoadingMoreData');
				win1.loadingMoreData = false;
			};
			
			DataMedia.loadPictureListMedia(dataIn.assets,updateData,e.startIndex,e.endIndex);
		});
		*/
		
		win1.open();
		setTimeout(function (){
			tableView.reset();
		},1000);
	};
	
		
	var tableViewClick = _.throttle(function(e){
		if (e.row.kind === 'lastWeekRow'){
			if (e.row.isActive === true){
				openGallery('lastWeekRow');
			}
			else{
				tableView.reset()
				alert(e.row.messageLabel.text);
			}
		}
		else if ( e.row.kind === 'thisWeekRow'){
			if (e.row.isActive === true){
					openGallery('thisWeekRow');
			}
			else{
				alert(I('Taken_no_pictures'));
				setTimeout(function (){
					tableView.reset();
				},100);
			};
		}
		else {
			var DataPictureOfTheWeek = require('model/DataPictureOfTheWeek');
			var powDetailData = DataPictureOfTheWeek.loadPictureOfTheWeekDetail(e.row.data.URL);
			
			var PictureOfTheWeekDetail = require('ui/windows/PictureOfTheWeekDetail');
			var win2 = PictureOfTheWeekDetail.loadWin(powDetailData);
			win2.open();
			setTimeout(function (){
				tableView.reset();
			},1000);
		};
		
	},2000);	
	
	tableView.addEventListener('click', function (e){
		if (tableView.clickable === true){
			tableViewClick(e);
			tableView.clickable = false; 
			var clickTimeout = setTimeout(function(){
				tableView.clickable = true; 
			},
			2000);
		};
	});
	
	
	//Assemble
	win.add(tableView);
	
	
	
	//Methods
	var setData = _.throttle(function (value){
		dataIn = value;
		tableData = [];
		
		tableData.push(createLoadingRow());
	
		for (var i = 0; i < dataIn.pastPicturesOfTheWeek.length; i++){	
			var row = createPastRow(dataIn.pastPicturesOfTheWeek[i]);
			tableData.push(row);
		};
		
		tableView.data = tableData;
		
		if (dataIn.pastPicturesOfTheWeek.length <= 0){
			setTimeout(function (){
				var PopUp = require('ui/components/PopUp');
				var titleText = I('Picture_of_the_Week');
				var subTitleText = I('Every_Monday_you_get_to_pick');
				var instructions = PopUp.instructionsPopUp(titleText,subTitleText);
				instructions.open();	
			},500)
		};
	}, 250);
	
	
	
	var dataPickRows;
	
	function setPickRows(data){
		dataPickRows = data;
		//Remove Loading Row
		tableData.splice(0,1);
		
		//1. Find out current Date
		var today  = moment().day();
		
		// If it's the first day of the week don't add the currentWeekRow add the pickRow.
		if (today != globals.properties.firstWorkingDay || data.pickedLastWeek === true){
			var firstRow = createThisWeekRow(data);
			tableData.splice(0,0,firstRow);
		} ;
		
		// If it's the second day of the week then I can't pick but I will display the row saying that you missed the picking day. 
		// If I have already picked last week then I don't add this row. 
		if ((today === globals.properties.firstWorkingDay && data.pickedLastWeek === false ) || dataIn.pastPicturesOfTheWeek.length === 0 ){
			tableData.splice(0,0,createPickRow(true, data))
		}		
		else if  (today > globals.properties.firstWorkingDay && data.pickedLastWeek === false) {
			tableData.splice(0,0,createPickRow(false, data))
		};
		
		tableView.data = tableData;
	};
	
	
	function loadPictureData(){
		DataPicturesOfTheWeek.getLastWeekMedia(setPickRows)
	};
	
	
	function resetData(){
		Ti.API.debug('POW: Reset Data');
		
		tableView.data = [];
		dataIn = DataPicturesOfTheWeek.loadPictureOfTheWeekListData()
		setData(dataIn);
		loadPictureData();
	};
	
	
	function preClean(){
		globals.currentNavGroupWindow = null;
		Ti.API.debug('Cleaned Picture of the Week List');
	};


	
	//Expose
	win.resetData = resetData;
	win.preClean = preClean;

	
	//Listeners	
	Ti.App.addEventListener('updatePictureOfTheWeekList', resetData);
	//Since the Navigation control seems to keep a copy of this window and not close it. 
	//i am forced to remove and add the listeners on blur and focus. Otherwise the data is requested every time the 
	//navigation group is opened. 
	win.addEventListener('focus', function (){
		Ti.App.addEventListener('mainViewIsOpen', loadPictureData);
	});
	
	win.addEventListener('close', function (){
		Ti.App.removeEventListener('mainViewIsOpen', loadPictureData);
		Ti.App.removeEventListener('updatePictureOfTheWeekList', resetData);
	});
	
	win.addEventListener('blur', function (){
		Ti.App.removeEventListener('mainViewIsOpen', loadPictureData);
	});
	
	
	//Initialize
	setData(dataIn);
	//PickRow Data is loaded by the mainViewIsOpen Listener;
	
	
	return win;
};



function createStandardRow (image, title, subtitle, height){
	height = height? height : 50
	var row = Ti.UI.createTableViewRow({
		backgroundSelectedImage:'images/row_selected.png',
		backgroundFocusedImage: 'images/row_selected.png',
		className: 'standardPOW',
		hasArrow: false,
		//data: data,
		height: height,
		editable: false,
	});
	
	var bottomGroove = Ti.UI.createView({width:row.width,height:1, top:row.height-1,backgroundColor:'black',opacity:.05});
	var topGroove = Ti.UI.createView({width:row.width,height:1,top:0, backgroundColor:'white'});
	
	var img = Ti.UI.createView({
		backgroundImage: image,
		left: 10,
		top: 4,
		height: 40,
		width: 40,
		touchEnabled: false
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: title,
		textAlign: 'left',
		font: {fontSize: 17, fontFamily: 'Georgia'},
		color: globals.fontColors[2],
		shadowColor: '#fff',
		shadowOffset: {x:1,y:1},
		width: 200,
		top: 5,
		left: 60,
		height: 20,
		touchEnabled: false
	});
	
	var subTitleLabel = Ti.UI.createLabel({
		text: subtitle,
		textAlign: 'left',
		font: {fontSize: 13, fontFamily: 'Georgia'},
		color: globals.fontColors[1],
		shadowColor: '#fff',
		shadowOffset: {x:1,y:1},
		width: 200,
		left: 60,
		top: 20,
		height: 20,
		touchEnabled: false
	});
	
	
	var selectionOverlay = Ti.UI.createView({
		backgroundImage: 'images/row_selected.png',
		width: 320,
		height: row.height,
		opacity: 0.02,
	});
	
	var longClick = false;
	selectionOverlay.addEventListener('touchstart', function (e){
		selectionOverlay.opacity = 1;
		//groove.visible = false;
		setTimeout(function(){
			longClick = true;
		},1500);
	});

	
	row.add(topGroove);
	row.add(bottomGroove);
	row.add(img);
	row.add(titleLabel);
	row.add(subTitleLabel);
	
	row.add(selectionOverlay);
	
	row.selectionOverlay = selectionOverlay;
	row.groove = bottomGroove;
	
	
	return row;
};


function createLoadingRow(){
	var row = Ti.UI.createTableViewRow({
		//backgroundSelectedImage:'images/row_selected.png',
		//backgroundFocusedImage: 'images/row_selected.png',
		//className: 'standardPOW',
		//hasArrow: false,
		//data: data,
		width: 320,
		height: 50,
		editable: false,
		touchEnabled: false
	});
	
	var bottomGroove = Ti.UI.createView({width:row.width,height:1, top:row.height-1,backgroundColor:'black',opacity:.05});
	var topGroove = Ti.UI.createView({width:row.width,height:1,top:0, backgroundColor:'white'});
	
	var activityIndicator = Ti.UI.createActivityIndicator({
      color: globals.fontColors[2],
      font: {fontFamily:'Georgia', fontSize:18},
      message: I('Loading_last_week_photos'),
      style:  Titanium.UI.iPhone.ActivityIndicatorStyle.DARK ,
      //top:10,
      //left:10,
      height: 25,
      width: 200
    });
	
	activityIndicator.show();
	row.add(topGroove);
	row.add(bottomGroove);
	row.add(activityIndicator);
	
	return row;
};


function createPastRow (POWData){
	var date = moment(POWData.date);
	var image = POWData.thumbnail;
	var title = date.format("MMM Do, YYYY");
	var subTitle = I('Week_of');
	
	var row = createStandardRow( image, title, subTitle);
	row.data = POWData;
	
	return row;
};


function createPickRow (todayIsPickDay, data){
	var moment = require('lib/Moment');
	var Labels = require('ui/components/Labels');
	
	var image = 'images/cover_empty.png';
	var title =I('Last_Week');
	var subTitle = I('Week_of')+data.lastWeekFirstWorkingDay.format("MMM Do, YYYY");
	
	var row = createStandardRow( image, title, subTitle,100);
	row.kind = 'lastWeekRow';
	
	if (todayIsPickDay && data.lastWeekPictures.length > 0){
		row.isActive = true; 
	}
	else{
		row.isActive = false;
	};
	
	
	var countLabel = Labels.label({
		text: '('+data.lastWeekPictures.length+')',
		color: globals.fontColors[0],
		right: 15,
		height: 20,
		top: 0,
		touchEnabled: false
	});
	
	if (todayIsPickDay === true){
		if (data.lastWeekPictures.length > 0){
			var pleasePickLabel = Ti.UI.createLabel({
				text:I( 'Please_pick_a_Picture'),
				textAlign: 'center',
				font: {fontSize: 17, fontFamily: 'Georgia'},
				color: globals.fontColors[2],
				shadowColor: '#fff',
				shadowOffset: {x:1,y:1},
				width: 200,
				top: 50,
				left: 60,
				height: 20,
				touchEnabled: false
			});
			
			row.add(countLabel);
			row.countLabel = countLabel;
			
			var now = moment();
			var endOfDay = moment().endOf('day');
			var timeLeft = endOfDay.diff(now);	
			timeLeft = moment.duration(timeLeft);
			timeLeft = moment(timeLeft.asMilliseconds()).format('HH:mm')
		
			
			var timeLeftLabel = Ti.UI.createLabel({
				text: I('Time_left_to_pick')+' '+timeLeft,
				textAlign: 'center',
				font: {fontSize: 14, fontFamily: 'Georgia'},
				color: 'red',
				shadowColor: '#fff',
				shadowOffset: {x:1,y:1},
				width: 200,
				top: pleasePickLabel.top+20,
				left: 60,
				height: 20,
				touchEnabled: false
			});
			
			row.add(pleasePickLabel);
			row.add(timeLeftLabel);
		}
		else{
			var messageLabel = Ti.UI.createLabel({
				text: I('Sorry_No_pictures_of_last_week'),
				textAlign: 'center',
				font: {fontSize: 14, fontFamily: 'Georgia'},
				color: globals.fontColors[2],
				shadowColor: '#fff',
				shadowOffset: {x:1,y:1},
				width: 310,
				top: 45,
				//left: 60,
				height: Ti.UI.SIZE,
				touchEnabled: false
			});
			
			row.messageLabel = messageLabel;
			
			row.add(messageLabel);
		};
	}
	else{
		var messageLabel = Ti.UI.createLabel({
			text: I('Sorry_you_missed_this_week'),
			textAlign: 'center',
			font: {fontSize: 14, fontFamily: 'Georgia'},
			color: globals.fontColors[2],
			shadowColor: '#fff',
			shadowOffset: {x:1,y:1},
			width: 310,
			top: 45,
			//left: 60,
			height: Ti.UI.SIZE,
			touchEnabled: false
		});
		
		row.messageLabel = messageLabel;
		
		row.add(messageLabel)
	};
	
	
	return row;
};


function createThisWeekRow(data){
	var Labels = require('ui/components/Labels');
	
	var image = 'images/cover_empty.png';
	var title = I('This_Week');
	var subTitle = I('Week_of')+data.thisWeekFirstWorkingDay.format("MMM Do, YYYY");
	
	var row = createStandardRow( image, title, subTitle);
	row.isActive = data.thisWeekPictures.length > 0 ? true : false;
	row.kind = 'thisWeekRow';

	var countLabel = Labels.label({
		text: '('+data.thisWeekPictures.length+')',
		color: globals.fontColors[0],
		right: 15,
		height: 20,
		touchEnabled: false
	});
	
	row.add(countLabel);
	row.countLabel = countLabel;
	
	return row;
};
