exports.NavigationController = function() {
	this.windowStack = [];
};

exports.NavigationController.prototype.open = function(/*Ti.UI.Window*/windowToOpen) {
	//add the window to the stack of windows managed by the controller
	this.windowStack.push(windowToOpen);
	
	//grab a copy of the current nav controller for use in the callback
	//var that = this;
	windowToOpen.addEventListener('close', function() {
		Ti.API.debug(globals.currentStack.windowStack);
		globals.currentStack.windowStack.pop();
		
		//This cause some issues. 
		//that.windowStack.pop();
	});

	//hack - setting this property ensures the window is "heavyweight" (associated with an Android activity)
	windowToOpen.navBarHidden = windowToOpen.navBarHidden || false;
	
	//This is the first window
	//Ti.API.debug(this.windowStack);
	
	if(this.windowStack.length === 1) {
		if(Ti.Platform.osname === 'android') {
			windowToOpen.exitOnClose = true;
			windowToOpen.open();
		} else {
			this.navGroup = Ti.UI.iPhone.createNavigationGroup({
				window : windowToOpen,
				width: Ti.Platform.displayCaps.platformWidth
			});
			
			//Add Top Icons
			//this.navGroup.add(Navigation.topIcons());
			//var Navigation = require('/ui/windows/Navigation');
			//this.navGroup.add(Navigation.navBadge());
		
			this.containerWindow = Ti.UI.createWindow({
				width:Ti.Platform.displayCaps.platformWidth,
				backgroundImage: 'images/bg_white.png',
				visible: true,
				orientationModes:[Ti.UI.PORTRAIT]
			});
			
			
			this.containerWindow.backToPortrait = function (){
				this.orientationModes =[Ti.UI.PORTRAIT];
				this.width = Ti.Platform.displayCaps.platformWidth;
				this.height = Ti.Platform.displayCaps.platformHeight;
			};
			
			this.containerWindow.add(this.navGroup);
			this.containerWindow.open();
			
			globals.tabGroup.darken = function (){
				darkShadowOver = Ti.UI.createView({
					backgroundColor: 'black',
					width: 320,
					height: 480,
					opacity: 0,
				});
				
				globals.tabGroup.containerWindow.add(darkShadowOver); 
				
				darkShadowOver.animate(Ti.UI.createAnimation({
					duration: 500,
					opacity:.25
				}));
			};
			
			globals.tabGroup.lighten = function (){
				
				darkShadowOver.animate(Ti.UI.createAnimation({
					duration: 250,
					opacity:0
				}));
				
				setTimeout(function (){
					globals.tabGroup.containerWindow.remove(darkShadowOver);
				}, 300)
			};
		}
	}
	//All subsequent windows
	else {
		if(Ti.Platform.osname === 'android') {
			windowToOpen.open();
		} else {
			this.navGroup.open(windowToOpen);
		}
	}
};

//go back to the initial window of the NavigationController
exports.NavigationController.prototype.home = function() {
	//store a copy of all the current windows on the stack
	var windows = this.windowStack.concat([]);
	
	for(var i = windows.length, l = 0 ; i <= l; i--) {
		if (windows[i]!= null){
			(this.navGroup) ? this.navGroup.close(windows[i]) : windows[i].close();
		}
	}
	
	this.windowStack = [this.windowStack[0]]; //reset stack
};


exports.NavigationController.prototype.close = function (win){
	this.navGroup.close(win);
	this.windowStack.pop();
} ;