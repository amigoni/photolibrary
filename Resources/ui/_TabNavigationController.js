//Controls the top level navigation of the app. 
exports.loadMe = function (){
	
	// create tab group
	globals.tabGroup = Titanium.UI.createTabGroup({
		backgroundImage: 'images/bg_white.png',
		activeTab:0,
	}); //Must be before PersonsList
	
	/*
	var bg = Ti.UI.createView({
		backgroundImage: 'images/bg_white.png',
		width: '100%',
		height: 470,
		top: 10
	});
	*/
	
	globals.currentTabGroup = globals.tabGroup; //This is here for the navigation buttons. To recognize which is the current tabGroup this or the modal one. 
	
	globals.tabGroup.GetController = function (){
		return globals.tabGroup;
	};
	
	globals.tabGroup.orientationModes = [
		Titanium.UI.LANDSCAPE_LEFT,
		Titanium.UI.LANDSCAPE_RIGHT,
		Titanium.UI.PORTRAIT,
		Titanium.UI.UPSIDE_PORTRAIT
	];
	
	//Dark shadow below popup and above tabGroup.
	//Had to put it here. Can't seem to put it in the popup tabGroup. '
	var darkShadowOver;
	
	globals.tabGroup.darken = function (){
		darkShadowOver = Ti.UI.createView({
			backgroundColor: 'black',
			width: 320,
			height: 480,
			opacity: 0,
		});
		
		globals.tabGroup.add(darkShadowOver); 
		
		darkShadowOver.animate(Ti.UI.createAnimation({
			duration: 500,
			opacity:.25
		}));
	};
	
	globals.tabGroup.lighten = function (){
		
		darkShadowOver.animate(Ti.UI.createAnimation({
			duration: 250,
			opacity:0
		}));
		
		setTimeout(function (){
			globals.tabGroup.remove(darkShadowOver);
		}, 300)
	};
	
	//Made this in to a function because of asynch. 
	var createTabs = function (albums,events,faces){
		
		//Tab 0
		var AlbumsList = require('ui/windows/AlbumsList');
		
		var win0 = AlbumsList.loadWin(albums);
		
		//Must use this method to fix a Ti. Error Bug. 
		globals.tabGroup.tab0 = Titanium.UI.createTab({  
		    //icon:'KS_nav_views.png',
		    title:'Albums',
		    window:win0
		});
		
		globals.tabGroup.addTab(globals.tabGroup.tab0);  
		
		//Tab 1
		var win1;
		var tab1;
		var hasEvents = false;
		
		if (events.length >0){
			hasEvents = true;
			
			var EventsList = require('ui/windows/EventsList');
			win1 = EventsList.loadWin(events)
		};
		
		globals.tabGroup.tab1 = Titanium.UI.createTab({  
		    //icon:'KS_nav_views.png',
		    title:'Events',
		    window:win1
		});
		
		globals.tabGroup.addTab(globals.tabGroup.tab1);  
		
		//Tab 2
		var PersonsList = require('ui/windows/PersonsList');
		var win2 = PersonsList.loadWin();
	
	
		globals.tabGroup.tab2 = Titanium.UI.createTab({  
		    //icon:'KS_nav_views.png',
		    title:'People',
		    window:win2
		});
		globals.tabGroup.addTab(globals.tabGroup.tab2);  
	
		//globals.tabGroup.navBar.update(win2);
		
		//Tab 3
		var LabelsList = require('ui/windows/LabelsList');
		var win3 = LabelsList.loadWin();
		
		globals.tabGroup.tab3 = Titanium.UI.createTab({  
		    //icon:'KS_nav_views.png',
		    title:'Labels',
		    window:win3
		});
		
		globals.tabGroup.addTab(globals.tabGroup.tab3);  
	
	
		var TopNavigation = require('ui/components/TopNavigation');
		globals.tabGroup.navBar = TopNavigation.createNavBar();
	
		var TabbedBar = require('ui/components/TabbedBar');
		globals.tabGroup.tabbedBar = new TabbedBar(hasEvents);
	
		globals.tabGroup.add(globals.tabGroup.navBar);
		globals.tabGroup.add(globals.tabGroup.tabbedBar);
		globals.tabGroup.open();	
		globals.tabGroup.setActiveTab(globals.tabGroup.tab0);
	};
	
	var DataMedia = require('model/DataMedia');
	DataMedia.getGroupTypes(createTabs);
};
