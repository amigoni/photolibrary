exports.loadNav = function (){	
	var NavigationController = require('/ui/NavigationController').NavigationController;
	globals.tabGroup = new NavigationController();
	
	var darkOver;
	globals.tabGroup.goBehind = function (){
		sideBarWindow.visible = false;
		darkOver = Ti.UI.createView({
			backgroundColor: 'black',
			opacity: 0,
			width: '100%',
			height: '100%',
		})
		globals.tabGroup.containerWindow.add(darkOver);
		
		globals.tabGroup.containerWindow.animate(Ti.UI.createAnimation({
			duration: 350,
			//opacity: 1,
			transform: Ti.UI.create2DMatrix().scale(.90),
			curve: Ti.UI.ANIMATION_CURVE_EASE_OUT
		}));
		
		darkOver.animate(Ti.UI.createAnimation({
			duration: 350,
			opacity: .7,
			curve: Ti.UI.ANIMATION_CURVE_EASE_OUT
		}));
		
	};
	
	globals.tabGroup.comeUpfront = function (){
		globals.tabGroup.orientationModes = [
			Titanium.UI.PORTRAIT,
		];
		
		globals.tabGroup.containerWindow.animate(Ti.UI.createAnimation({
			duration: 250,
			transform: Ti.UI.create2DMatrix().scale(1),
			curve: Ti.UI.ANIMATION_CURVE_EASE_OUT
		}));
		
		darkOver.animate(Ti.UI.createAnimation({
			duration: 250,
			opacity: 0,
			curve: Ti.UI.ANIMATION_CURVE_EASE_OUT
		}));
		
		setTimeout(function (){
			globals.tabGroup.containerWindow.remove(darkOver);
			sideBarWindow.visible = true;
		},250);
	};
	
	globals.tabGroup.disappear = function (){
		globals.tabGroup.containerWindow.opacity = 0;
		sideBarWindow.opacity = 0;
	};
	
	//Used when you don't have permissions enabled to run the app and you are resuming'
	globals.tabGroup.turnOff = function (){
		globals.tabGroup.containerWindow.animate(Ti.UI.createAnimation({
				duration: 2000,
				opacity: 0
		}));
		
		sideBarWindow.visible = false;
	};
	
	globals.tabGroup.appear = function (){
		globals.tabGroup.containerWindow.opacity = 1;
		sideBarWindow.opacity = 1;
	};
	
	globals.currentStack = globals.tabGroup;
	
	
	// This event listener opens the hidden window.
	var visible = false; 
	Ti.App.addEventListener('toggleSideBar', function(e) {
	    sideBarWindow.visible = true; //Assures is visible after hiding from Gallery Win animation.
	    
	    if(visible === false) {
	         sideBarWindow.visible = true; //Assures is visible after hiding from Gallery Win animation.
	        var a = Ti.UI.createAnimation({
	            duration: 200,
	            left: 260,
	            curve: Ti.UI.ANIMATION_CURVE_EASE_OUT
	        });
	        
	        a.addEventListener('complete',function (e){
	        	
	        	Ti.App.fireEvent('mainViewIsClosed');
	        });
	      
	        globals.tabGroup.containerWindow.animate(a);
	        
	        sideBarWindow.appearAnimation();
	        visible = true;
	        hiddenButton.touchEnabled = true;
	    } 
	    else {
	        var b = Ti.UI.createAnimation({
	            duration: 200,
	            left: 0,
	            curve: Ti.UI.ANIMATION_CURVE_EASE_OUT
	        });
	      	
	      	b.addEventListener('complete',function (e){
	        	Ti.App.fireEvent('mainViewIsOpen');
	        	sideBarWindow.visible = false;
	        });
	      
	    	globals.tabGroup.containerWindow.animate(b);
	    	
	    	sideBarWindow.disappearAnimation();
	        visible = false; 
	        hiddenButton.touchEnabled = false;
	    };
	});		
		
		
	//Hidden Button to Close Window over the open tabGroup	
	var hiddenButton =  Ti.UI.createWindow({
			width: 60,
			height:'100%',
			right: 0,
			touchEnabled: false
		});
		
	hiddenButton.addEventListener('click', function (){
		Ti.App.fireEvent('toggleSideBar')
	});
	
	
	//Load Sidebar
	var sideBarWindow
	function initialLoadSidebar(albums, events, faces, photoStreamGroup, cameraRollGroup, photoLibraryGroup){
		//Covers at startup to make it seem seemless.
		var coverUp =  Ti.UI.createWindow({
			backgroundImage:  Ti.Platform.displayCaps.platformHeight === 568 ? '/iphone/Default-568h@2x.png' : '/iphone/Default.png'
		});
		coverUp.open();
		
		//Load Sidebar
		sideBarWindow = loadLeftNav(albums, events, faces , photoStreamGroup, cameraRollGroup, photoLibraryGroup);
		
		//Loading Person List at Start
		var PersonsList = require('/ui/windows/PersonsList');
		globals.tabGroup.open(PersonsList.loadWin());
		
		
		//Load Navbar
		var TopNavigation = require('ui/components/TopNavigation');
		//globals.tabGroup.navBar = TopNavigation.createNavBar();
		//globals.tabGroup.navBar.currentBG = 'images/navbar_grey.png';
		//globals.tabGroup.containerWindow.add(globals.tabGroup.navBar);
		
		hiddenButton.open();// Opens the Button since it's a window; '
		
		sideBarWindow.visible = true;
		
		//Close and remove CoverUP win;
		coverUp.close();	
		coverUp = null;
	};
	
	var DataMedia = require('model/DataMedia');
	DataMedia.getGroupTypes(initialLoadSidebar);
	
};



var loadLeftNav = function (albums, events, faces, photoStreamGroup, cameraRollGroup, photoLibraryGroup){		
	
	var win = Ti.UI.createWindow({
		backgroundImage: '/images/sidebar_bg.png',
		width: Ti.Platform.displayCaps.platformWidth,
		height: Ti.Platform.displayCaps.platformHeight,
		backgroundColor:'transparent',
		left: 0,
		visible: false
	});
	
	var shadow = Ti.UI.createView({
		backgroundImage:'/images/sidebar_shadow.png',
		backgroundLeftCap: 25,
		backgroundTopCap: 25,
		width: 265,
		height: Ti.Platform.displayCaps.platformHeight,
		left:-1,
		touchEnabled: false
	});


	//Header	
	var headerView = Ti.UI.createView({
		//backgroundImage:'/images/Navigation/OmbraNavigation.png',
		width:250,
		height:70,
		left:0,
		top:0
	});
	
	headerView.addEventListener('click', function(){
		changeStack('MyProfile',PersonDetail.loadWin(null,true));
		Ti.App.fireEvent('toggleSideBar')
	});
	
	var topLine = Ti.UI.createView({
		width:250,
		height:1,
		left:0,
		bottom: 0,
		backgroundColor: 'white',
		opacity: .1
	});
		
	var bottomLine = Ti.UI.createView({
		width:250,
		height:1,
		left:0,
		bottom: 1,
		backgroundColor: '#000000' //Lighter is '#232324'
	});
	
	/*
	var picture = Ti.UI.createView({
		backgroundImage:'/images/Navigation/Placeholder_pic.png',
		backgroundSelectedImage:'/images/Navigation/Placeholder_pic_sel.png',
		backgroundFocusedImage:'/images/Navigation/Placeholder_pic_sel.png',
		width:50,
		height:50,
		left: 10
	});
	*/
	
	var headerTitle = Ti.UI.createLabel({
		text:'Tag my Photos',
		color: '#FCFFF4',
		font:{fontWeight: 'bold', fontSize: 17},
		left: 70,
		top: 20
	});
		
	var headerSubTitle =  Ti.UI.createLabel({
		//text:'Ciao Ciao',
		color: '#FCFFF4',
		left: 70,
		top: headerTitle.top + 20
	});

	
	headerView.add(bottomLine)
	headerView.add(topLine)
	//headerView.add(picture)
	headerView.add(headerTitle)
	headerView.add(headerSubTitle)		
	
	
	
	//TableView	
	var peopleWin = createRow('People');
	peopleWin.add( Ti.UI.createView({
		backgroundImage:'/images/sidebar_people_icon.png',
		width: 38,
		height: 25,
		left: 10
	}));
	
	var PersonList;
	peopleWin.action = function (){
		var PersonsList = require('/ui/windows/PersonsList');
		changeStack(PersonsList.loadWin());
	};
	
	
	
	var labelsWin = createRow('Labels');
	labelsWin.add( Ti.UI.createView({
		backgroundImage:'/images/sidebar_labels_icon.png',
		width: 30,
		height: 30,
		left: 15
	}));
	
	labelsWin.action = function (){
		var LabelsList = require('/ui/windows/LabelsList');
		changeStack(LabelsList.loadWin());
	};
	
	
	var cameraRollWin = createRow(I('Camera_Roll'));
	cameraRollWin.add( Ti.UI.createView({
		backgroundImage:'/images/sidebar_camera_roll_icon.png',
		width: 45,
		height: 31,
		left: 10,
		transform: Ti.UI.create2DMatrix().scale(.85)
	}));
	
	cameraRollWin.action = function (){
		var PicturesListLibrary = require('/ui/windows/PicturesListLibrary');
		var DataMedia = require('/model/DataMedia');
		//This assures the camera Roll To be updated.
		function updateCameraRoll(group){
			changeStack(PicturesListLibrary.loadWin(group,true));
		};
		
		DataMedia.getCameraRollGroup(updateCameraRoll)
	};
	
	
	var photoStreamWin = createRow(I('Photo_Stream'));
	photoStreamWin.add( Ti.UI.createView({
		backgroundImage:'/images/sidebar_photostream_icon.png',
		width: 39,
		height: 21,
		left: 10,
		tranform: Ti.UI.create2DMatrix().scale(.8)
	}));
	
	photoStreamWin.action = function (){
		var PicturesListLibrary = require('/ui/windows/PicturesListLibrary');
		changeStack(PicturesListLibrary.loadWin(photoStreamGroup,true));
	};
	
	
	var photoLibraryWin = createRow(I('Photo_Library'));
	photoLibraryWin.add( Ti.UI.createView({
		backgroundImage:'/images/sidebar_photolibrary_icon.png',
		width: 28,
		height: 28,
		left: 15,
		tranform: Ti.UI.create2DMatrix().scale(.8)
	}));
	
	photoLibraryWin.action = function (){
		var PicturesListLibrary = require('/ui/windows/PicturesListLibrary');
		changeStack(PicturesListLibrary.loadWin(photoLibraryGroup, true));
	};
	
	
	var albumsWin = createRow(I('Albums'));
	albumsWin.add( Ti.UI.createView({
		backgroundImage:'/images/sidebar_albums_icon.png',
		width: 25,
		height: 32,
		left: 17
	}));
	
	albumsWin.action = function (){
		var AlbumsList = require('/ui/windows/AlbumsList');
		changeStack(AlbumsList.loadWin(albums));
	};
	
	
	
	var eventsWin = createRow(I('Events'));
	eventsWin.add( Ti.UI.createView({
		backgroundImage:'/images/sidebar_events_icon.png',
		width: 29,
		height: 28,
		left: 15
	}));
	
	eventsWin.action = function (){
		var EventsList = require('/ui/windows/EventsList');
		changeStack(EventsList.loadWin(events));
	};
	
	
	
	var pictureOfTheWeekWin = createRow(I('Picture_of_the_Week'));
	pictureOfTheWeekWin.add( Ti.UI.createView({
		backgroundImage:'/images/sidebar_pictureOfTheWeek_icon.png',
		width: 29,
		height: 30,
		left: 15
	}));
	
	pictureOfTheWeekWin.action = function (){
		var PictureOfTheWeekList = require('/ui/windows/PictureOfTheWeekList');
		changeStack(PictureOfTheWeekList.loadWin());
	};
	
	
	var theShopWin = createRow(I('The_Shop'));
	theShopWin.add(Ti.UI.createView({
		backgroundImage:'/images/sidebar_shop_icon.png',
		width: 36,
		height: 29,
		left: 14
	}));
	
	theShopWin.action = function (){
		var TheShop = require('/ui/windows/TheShop');
		changeStack(TheShop.loadWin());
	};
	
	
	var feedbackWin = createRow(I('Feedback_and_Support'));
	feedbackWin.add( Ti.UI.createView({
		backgroundImage:'/images/sidebar_support.png',
		width: 31,
		height: 31,
		left: 14
	}));
	
	feedbackWin.action = function (){
		var Feedback = require('/ui/windows/Feedback');
		changeStack(Feedback.loadWin());
	};
	
	
	
	var newsWin = createRow(I('News_and_Updates'));
	newsWin.add( Ti.UI.createView({
		backgroundImage:'/images/sidebar_news.png',
		width: 32,
		height: 29,
		left: 14
	}));
	
	var unreadNews = globals.col.news.count({opened:false});
	var allNews = globals.col.news.getAll();
	
	var number = Ti.UI.createView({
		backgroundImage: 'images/sidebar_news_number_bg.png',
		width: 30,
		height: 30,
		right: 15,
		top: 11,
		visible: unreadNews > 0 ? true : false
	});
	
	var numberLabel = Ti.UI.createLabel({
		text: unreadNews,
		font: {fontSize: 17, fontWeight: 'bold'},
		color: '#FCFFF4',
		shadowColor: 'black',
		shadowOffset:{x:1,y:1},
		textAlign: 'center',
		top:3,
	})
	
	number.add(numberLabel );
	newsWin.add(number);
	
	newsWin.action = function (){
		var NewsList = require('/ui/windows/NewsList');
		changeStack(NewsList.loadWin());
	};
	
	
	
	var rateMe = createRow(I('Rate_me'));
	rateMe.add( Ti.UI.createView({
		backgroundImage:'/images/favorites_white.png',
		width: 33,
		height: 31,
		left: 17
	}));
	
	rateMe.action = function (){
		var PopUp = require('ui/components/PopUp');
		var rateMePopUp = PopUp.rateMePopUp();
		rateMePopUp.open();
	};
	
	
	var tableData = [];
	
	function updateTableData (albums, events, faces, photoStreamGroup, cameraRollGroup, photoLibraryGroup){
		tableData = [];
		
		unreadNews = globals.col.news.count({opened:false})
		if (unreadNews > 0 ){
			number.visible = true;
			numberLabel.text = unreadNews;
			tableData.push(newsWin);
		};
		
		tableData.push(createDividerRow());
		tableData.push(peopleWin);
		tableData.push(labelsWin);
		tableData.push(pictureOfTheWeekWin);
		tableData.push(createDividerRow());
		tableData.push(cameraRollWin);
		
		
		if (photoStreamGroup && photoStreamGroup.numberOfAssets > 0){
			tableData.push(photoStreamWin);
		};
		
		if (photoLibraryGroup && photoLibraryGroup.numberOfAssets > 0){
			tableData.push(photoLibraryWin);
		};
		
		tableData.push(albumsWin);
		if (events.length >0){
			tableData.push(eventsWin);
		};		
		
		tableData.push(createDividerRow());
		
		//tableData.push(theShopWin);
		tableData.push(feedbackWin);
		
		if (unreadNews === 0 && allNews.length > 0){
			number.visible = false;
			numberLabel.text = unreadNews;
			tableData.push(newsWin);
		};
		
		if (!globals.properties.hasRatedApp || globals.properties.hasRatedApp === false){
			tableData.push(rateMe);
		};
		
		tableView.data = tableData;
	};
	
		
	var tableHeader = Ti.UI.createView({
		width: 260,
		height: 1,
		left:0,
		backgroundColor: '#000000'
	});	
	
	var tableFooter = Ti.UI.createView({
		width: 260,
		height: Ti.UI.SIZE,
		left:0,
	});	
	
	tableFooter.add(Ti.UI.createView({
		width: 260,
		height: 1,
		top: 0,
		backgroundColor: 'white',
		opacity: .1
	}))
	
	tableFooter.add(Ti.UI.createView({
		backgroundImage: 'images/inventedByMozzarello.png',
		top: 70,
		width: 196,
		height: 51
	}));
	
	tableFooter.addEventListener('click', function (){
		Ti.Platform.openURL('http://mozzarello.com');
	});
	
	var tableView = Ti.UI.createTableView({
		backgroundColor: 'transparent',
		//separatorColor: '#666666',
		width:260,
		left:0,
		separatorStyle: false,
		//data: tableData,
		top: -1,//68,
		footerView: tableFooter,
		headerView: tableHeader,
		showVerticalScrollIndicator: false,
		selectedRow: null,
		selectionStyle: false,
		clickable: true
	});
	
	
	function refreshTableData (){
		var DataMedia = require('model/DataMedia');
		DataMedia.getGroupTypes(updateTableData);
	};
	
	
	
	function tableViewClick (e){
		//Deselect all rows except for the one clicked. 
		if (e.row.kind != 'divider'){
			for(var i = 0; i < tableData.length; i++){
				if (i != e.index && tableData[i].kind != 'divider'){
					tableData[i].selectedState.visible = false;
					tableData[i].unselectedState.visible = true;
				};
			};
			
			
			if (e.row != tableView.selectedRow){
				tableView.selectedRow = e.row;
				e.rowData.action();
			};
			
			setTimeout(function(){
				Ti.App.fireEvent('toggleSideBar')
			},500)
		};
	};
	
	
	tableView.addEventListener('click', function (e){
		if (tableView.clickable === true){
			tableViewClick(e);
			tableView.clickable = false; 
			var clickTimeout = setTimeout(function(){
				tableView.clickable = true; 
			},
			500);
		};
	});	
	
	
	var darkCover = Ti.UI.createView({
		backgroundColor: 'black',
		opacity: .7,
		width: win.width,
		height: win.height,
	});
	
	//Methods
	var changeStack = function (newWin){
		globals.tabGroup.home();
		globals.tabGroup.open(newWin);
		//globals.tabGroup.resetAndOpen(newWin);
	};
	
	
	function appearAnimation (){
		darkCover.animate(Ti.UI.createAnimation({
			opacity: 0,
			delay: 50,
			duration: 500,
			curve: Ti.UI.ANIMATION_CURVE_EASE_OUT
		}))
	};
	
	function disappearAnimation(){
		darkCover.animate(Ti.UI.createAnimation({
			opacity: .7,
			delay: 0,
			duration: 300,
			curve: Ti.UI.ANIMATION_CURVE_EASE_IN
		}));
	};
	
	
	//Assemble
	win.add(tableView);
	//win.add(headerView);
	win.add(shadow);
	win.add(darkCover);
	
	win.open();
	
	//Expose
	win.appearAnimation = appearAnimation;
	win.disappearAnimation = disappearAnimation;
	
	//Listeners
	Ti.App.addEventListener('updateSideBar', refreshTableData);
	
	//Initialize
	updateTableData(albums, events, faces, photoStreamGroup, cameraRollGroup, photoLibraryGroup);
	
	return win;
};		



var createRow = function (title,icon){
	var row = Ti.UI.createTableViewRow({
		height: 50,
		//color:'white' ,
		width: 260,
		changeStackStyle:false,
		editable: false
	});
	
	row.addEventListener('click', function (){
		row.selectedState.visible = true;
		row.unselectedState.visible = false;
	});
	
	var selectedState = Ti.UI.createView({
		backgroundImage:'/images/sidebar_row_sel.png',
		width: 260,
		height: 50,
		visible: false
	});
	
	var selectedIcon = Ti.UI.createView({
		backgroundImage:'/images/Navigation/'+icon+'_sel.png',
		backgroundSelectedImage:'/images/Navigation/'+icon+'_sel.png',
		width:35,
		height:35,
		left: 10
	});
		
		//var selectedLabel = text.font4(title);
		//selectedLabel.left = 55;
	
	//selectedState.add(selectedIcon);
	//selectedState.add(selectedLabel);
	
	
	
	var unselectedState = Ti.UI.createView({
		backgroundImage:'/images/sidebar_row.png',
		width: 260,
		height: 50,
	});
	
	var unselectedLabel = Ti.UI.createLabel({
		text: title,
		textAlign: 'left',
		font: {fontSize: 17, fontWeight: 'bold'},
		color: '#FCFFF4',
		shadowColor: 'black',
		shadowOffset:{x:1,y:1},
		left: 60
	});
	
	
	//Assemble
	row.add(selectedState);
	row.add(unselectedState);
	row.add(unselectedLabel)
	
	
	//Expose
	row.selectedState = selectedState;
	row.unselectedState = unselectedState;
	
	
	return row;
};


var createDividerRow = function (){
	var row = Ti.UI.createTableViewRow({
		height: 20,
		touchEnabled: false,
		editable: false,
		kind: 'divider'
	});
	
	row.add(Ti.UI.createView({
		backgroundImage:'/images/sidebar_row.png',
		backgroundTopCap: 5,
		//backgroundColor: 'black',
		//opacity: .3
	}))
	
	row.add(Ti.UI.createView({
		backgroundTopCap: 5,
		backgroundColor: 'white',
		opacity: .1
	}))
	
	return row;
};
