exports.addBubble = function (bubbleData){
	
	var objectToReturn = {};//This is to prevent polution of BubbleData;
	
	//1. Find if the bubble already exists. If it doesn't add it. '
	var bubbles = globals.col.bubbles.find({tagType: bubbleData.tagType, tagID: bubbleData.tagID, mediaID : bubbleData.mediaID});
	
	if (!bubbles[0]){
		globals.col.bubbles.save({
			x: bubbleData.x,
			y: bubbleData.y,
			hasSpot: bubbleData.hasSpot,
			mediaID: bubbleData.mediaID,
			tagType: bubbleData.tagType,
			tagID: bubbleData.tagID
		});
			
		globals.col.bubbles.commit();
		
		var $id = globals.col.bubbles.getLastInsertId();
		var bubble = globals.col.bubbles.find({$id:$id});
		
		objectToReturn.bubbleData = bubble[0];
		objectToReturn.isNewBubble = true
		
		Ti.API.info('Added New Bubble: '+$id);
		bubbleData.$id = $id;
		
		Ti.App.fireEvent('addedNewBubble', bubbleData);
	}
	else{
		Ti.API.info('Found Existing Bubble');
		objectToReturn.bubbleData = bubbles[0];
		objectToReturn.isNewBubble = false;
	};
	
	return objectToReturn; //Returns the added Object to give info back to the bubble to create it. 
};

//Usually called from removeTagFromMedia
exports.deleteBubble = function (bubbleID){
	globals.col.bubbles.remove({$id:bubbleID});
	globals.col.bubbles.commit();
	Ti.API.info('Deleted Bubble: '+bubbleID);
};


//**********************
//NEEED TO REVISE THIS. JUST USE REMOVE TAG FROM MEDIA
//Used when you delete bubble from the picture.
exports.deleteBubbleWithX = function(bubbleID){
	//1. Get the Bubble 
	var bubble = globals.col.bubbles.find({$id:bubbleID});

	if (bubble[0]){
		//1. Get the Media URL
		var DataMedia = require('model/DataMedia');
		var media = globals.col.media.find({$id:bubble[0].mediaID});
		//alert(bubble[0])
		if (media[0]){
			//2. Remove Tag From Media
			DataMedia.removeTagFromMedia(media[0].URL,bubble[0].tagType,bubble[0].tagID);
		};
	};
};


//UPDATES THE POSITION OF THE BUBBLE
exports.updateBubble = function (bubbleData){
	globals.col.bubbles.update({$id:bubbleData.$id},{$set:{x:bubbleData.x}},{},true); 
	globals.col.bubbles.update({$id:bubbleData.$id},{$set:{y:bubbleData.y}},{},true); 
	globals.col.bubbles.update({$id:bubbleData.$id},{$set:{hasSpot:true}},{},true); 
	
	globals.col.bubbles.commit();
	
	Ti.API.info('Saved new Bubble Position '+bubbleData.x+' '+bubbleData.y+' for bubble: '+bubbleData.$id);
};


//Used to Load Bubbles for a Media in Pictures Gallery
exports.getBubblesForMedia = function (mediaURL){
	var startTime = new Date().getTime();

	var DataMedia = require('model/DataMedia');
	var arrayToReturn = [];
	
	//1. Get the Media to get the array of bubbles
	var media = DataMedia.loadMediaFromURL(mediaURL);
	
	//2. If the media exists. Loop through the bubbles array to get the info of the bubble and the info of the 
	// tag attached to the bubble. 
	if(media){
		var bubblesIDArray = media.bubbles;

		for (var i=0; i < bubblesIDArray.length; i++){
			var bubble = globals.col.bubbles.find({$id: bubblesIDArray[i]});
			
			if (bubble[0]){
				var objectToReturn = bubble[0]; //Create this not to pollute database. 
				
				if (objectToReturn.tagType === 'person'){	
					var person = globals.col.persons.find({$id: objectToReturn.tagID});
					if(person[0]){ 
						objectToReturn.title = (person[0].firstName ? person[0].firstName :'')+' '+(person[0].lastName?person[0].lastName:'');
					}
					else{
						Ti.API.debug('Abandoned Bubble?')
					};
				};
				/*
				else if (objectToReturn.tagType === 'label'){
					var label = globals.col.labels.find({$id: objectToReturn.tagID});
					if (label[0]){
						objectToReturn.title =  label[0].name;
					};
					
				};
				*/
				arrayToReturn.push(objectToReturn);
			};
		};
	};
	
	var endTime = new Date().getTime();
	Ti.API.info('Got bubbles in ' + (endTime - startTime) + 'ms');

	return arrayToReturn; 
};