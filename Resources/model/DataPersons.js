///////////////////////////////////////////////////////////////////////////////
//CRUD METHODS
exports.loadPerson = function (personID){
	var data = globals.col.persons.find({$id:personID});
	
	return data[0];
};


exports.loadPersonForAddressBookID = function (addressBookID){
	var data = globals.col.persons.find({addressBookID:addressBookID});
	
	return data[0];
};


exports.loadAllContactsFromAddressBook = function (){
	//2 Get all the AdressBook Contacts
	var iphoneContacts = Titanium.Contacts.getAllPeople();
	
	
	//Array that contains all the contact objects
	globals.addressBookContacts = [];

	//Create Contact object to push to each row from the Address Book Data. 
	for(var i =0; i < iphoneContacts.length; i++){
		var contact = {};
		contact.name = iphoneContacts[i].fullName;
		contact.id = iphoneContacts[i].recordId;
		globals.addressBookContacts.push(contact);
	};
	
	function sortAZ(ob1,ob2) {
    	var n1 = ob1.name.toLowerCase()
    	var n2 = ob2.name.toLowerCase()
    	if (n1 > n2) {return 1}
		else if (n1 < n2){return -1}
	    else { return 0}//nothing to split
	};
	
	globals.addressBookContacts.sort(sortAZ);
	
	return globals.addressBookContacts;
};


exports.addNewAddressBookContact = function (firstName,lastName){
	var newPerson = Ti.Contacts.createPerson({
	  firstName: firstName,
	  lastName: lastName
	});
	
	//Refresh the person list
	var DataPersons = require('model/DataPersons');
	DataPersons.loadAllContactsFromAddressBook();
	
	return {id:newPerson.recordId , name: newPerson.fullName};
};


exports.addPerson = function(data,callBack) {
	if (!callBack){
		callBack = function (){
			Ti.API.info('Adding Contact and automatching to faces. This is from Gallery View Probably')
		};
	};
	
	var DataPersons = require('model/DataPersons');
	
	globals.col.persons.save({
		firstName: data.firstName ,
		lastName: data.lastName,
		addressBookID: data.addressBookID,
		facesAlbumID: null, //data.firstName+' '+data.lastName
		isMyself: data.isMyself ? data.isMyself : false ,
		media: [],
		mediaURLs:[],
		thumbnailPictureURL:null,
		thumbnailType:'random'
		//createdDate:data.createdDate,
	});	
	
	globals.col.persons.commit();
	
	
	//Change hasBeenSetup status
	if (data.isMyself === true){
		var properties = globals.col.properties.getAll();
		properties[0].hasBeenSetup = true;
		globals.col.properties.commit();
	};
	
	var personID = globals.col.persons.getLastInsertId();
	var person = globals.col.persons.find({$id:personID});
	
	/*
	TEMPORARILY DISABLED This is the faces match. 
	var personObject = globals.col.persons.find({$id:personID});
	
	DataPersons.findMatchForPersonToFaces(personObject[0].facesAlbumID,callBack);
	*/
	
	return person[0];
};


exports.updatePersonAddressBookLink = function (personID, data){
	globals.col.persons.update({$id: personID},{$set: {firstName: data.firstName}},{},true); 
	globals.col.persons.update({$id: personID},{$set: {lastName: data.lastName}},{},true);
	globals.col.persons.update({$id: personID},{$set: {addressBookID: data.addressBookID}},{},true); 

	globals.col.persons.commit();
	
	Ti.App.fireEvent('updatePersonEdit');
	Ti.App.fireEvent('updatePersonDetail');
	Ti.App.fireEvent('updatePersonsList');
	
	Ti.API.debug('updatePersonAddressBookLink');
};


exports.addPersonFromAddressBookID = function (addressBookID){
	var DataPersons = require('model/DataPersons');
	
	var person = globals.col.persons.find({addressBookID: addressBookID});
	
	//1 get the person from Contacts
	if(!person[0]){
		var contact = Ti.Contacts.getPersonByID(addressBookID)
		
		person = DataPersons.addPerson({
			firstName: contact.firstName,
			lastName: contact.lastName,
			addressBookID: contact.recordId
		});
	}
	else{
		person = person[0];
	};
	
	return person;
};


exports.deletePerson = function (personID){
	var DataPersons = require('model/DataPersons');
	var DataMedia = require('model/DataMedia');
	
	var startTime = new Date().getTime();
	
	//1 Get Person Data to get addressBookID
	var person = DataPersons.loadPerson(personID);

	
	//2 Remove Person From Media Array
	var media = person.media;
	for (var i = 0; i < media.length; i++){
		var bubbles = globals.col.bubbles.find({mediaID: media[i], tagType:'person', tagID: personID});
		var medium = globals.col.media.find({$id:media[i]});
		medium[0].people.splice(medium[0].people.indexOf(personID),1);
		medium[0].bubbles.splice(medium[0].bubbles.indexOf(bubbles[0].$id),1);
	};
	
	globals.col.media.commit();
	
	//3 Remove Bubbles
	globals.col.bubbles.remove({tagID:personID});
	globals.col.bubbles.commit();
	
	
	/*
	//2 Remove Media
	var media = person.media;
	
	for (var i = media.length-1; i >= 0; i--){
		var medium = globals.col.media.find({$id:media[i]});
		if(medium[0]){
			DataMedia.removeTagFromMedia(medium[0].URL,'person',person.$id, false);
		};
	};
	*/
	
	globals.col.persons.remove({$id:personID});
	globals.col.persons.commit();
	
	var endTime = new Date().getTime();
	Ti.API.info('Deleted Person in: ' + (endTime - startTime) + 'ms');
	
	Ti.App.fireEvent('closePopUp');
};



///////////////////////////////////////////////////////////////////////////////
//LOAD DATA METHODS FOR VARIOUS WINDOWS

//LOADS DATA FOR PERSON LIST WINDOW
exports.loadPersonsList = function (){
	var DataMedia = require('model/DataMedia');
	
	var data = globals.col.persons.getAll();
	
	//Get Thumbnails
	for (var i =0; i < data.length; i++){
		var file = Titanium.Filesystem.getFile(globals.thumbsDir.nativePath, data[i].$id+'.jpg');
		data[i].thumbnail =  file.read();
	};
	
	
	function sortByFirstName(obj1,obj2) {
		// Handles case they're both equal (or both missing)
	    if (obj1 == obj2) {return 0}
	    
	    // Handle case firstName is missing
	    if (obj2.firstName == null || obj2.firstName == "") {return 1}
	    if (obj1.firstName == null || obj1.firstName == "") {return -1}
	    
    	var n1 = obj1.firstName.toLowerCase()
    	var n2 = obj2.firstName.toLowerCase()
    	if (n1 > n2) {return 1}
		else if (n1 < n2){return -1}
	    else { return 0}//nothing to split
	};
	
	function sortByLastName(obj1,obj2) {
		// Handles case they're both equal (or both missing)
	    if (obj1 == obj2) {return 0}
	    
	    // Handle case firstName is missing
	    if (obj2.lastName == null || obj2.lastName == "") {return 1}
	    if (obj1.lastName == null || obj1.lastName == "") {return -1}
	    
    	var n1 = obj1.lastName.toLowerCase()
    	var n2 = obj2.lastName.toLowerCase()
    	if (n1 > n2) {return 1}
		else if (n1 < n2){return -1}
	    else { return 0}//nothing to split
	};
	
	if (globals.properties.sortPeopleBy === 'firstName'){
		data.sort(sortByFirstName);
	}
	else if (globals.properties.sortPeopleBy === 'lastName'){
		data.sort(sortByLastName);
	};
	
	
	return data;
};


//LOADS DATA FOR PERSON DETAIL WINDOW
exports.loadPersonDetail = function (personID, callback, getAlsoThumbnail){
	getAlsoThumbnail = getAlsoThumbnail === false ? false : true; 
	
	var startTime = new Date().getTime();
	
	var DataPersons = require('model/DataPersons');
	var DataMedia = require('model/DataMedia');
	
	var returnObject = {};
	
	//1. Get the person info 
	returnObject.person = DataPersons.loadPerson(personID);
	
	//2. Get Favorites List
	returnObject.favorites = globals.col.media.find({archived: false, people: personID, isFavorite: true},{$sort:{originalDate:-1}});
	
	//3. Get All Pictures List
	returnObject.allPictures = globals.col.media.find({archived: false, people: personID},{$sort:{originalDate:-1}});
	
	//4. Get Together Pictures List
	var currentMyself = globals.col.persons.find({isMyself: true});
	//Here to return an empty array so that the Person Detail doesn't crash. '
	returnObject.together = [];
	
	if(currentMyself[0] && personID != currentMyself[0].$id){
	//There should be a way to do this automatically in the JSONDB but It seems not to work. 
	//This is a workaround that might be slower. 
		
		//1. Take the two arrays. 
		var personMedia = returnObject.person.media; 
		var mySelfMedia  = currentMyself[0].media;
		
		//2. Intersect the Arrays to find the common ones. 
		var arrayIntersection = function (a,b){
			var newArray = [];
			var bigArray;
			if (a.length >= b.length){
				bigArray = a;
				smallArray = b;
			}
			else{
				bigArray =b;
				smallArray = a;
			};
			
			for (var i =0; i< bigArray.length;i++){
				if (smallArray.indexOf(bigArray[i]) != -1){
					newArray.push(bigArray[i]);
				};
			};
			
			return newArray;
		};
		
		var intersection =  arrayIntersection(mySelfMedia,personMedia);
		
		//3. Get each media and put it into the return array. 
		//This is also a workaround for JSONDB. Look below. 
		for(var i =0; i < intersection.length; i++){
			var medium =  globals.col.media.find({archived: false,$id: intersection[i]})
			if (medium[0].people.indexOf(currentMyself[0].$id)!= -1){
				returnObject.together.push(medium[0]);
			};
		};
		
		//This method should be working but doesn't. TODO: Contact Json DB with a sample case. '
		//returnObject.together = globals.col.media.find({$id:'4ff85b706be1341a6106d0e3'});
	};
	
	
	//5. Get Thumbnail
	if (getAlsoThumbnail === true){
		DataPersons.getThumbForPerson(personID, callback);
	};
	
	
	var endTime = new Date().getTime();
	Ti.API.info('Got Person Detail Arrays in: ' + (endTime - startTime) + 'ms');
	
	return returnObject;
};


//LOADS DATA FOR PICTURE LIST WINDOW
exports.loadPictureListData = function (listType, tagType, tagID){
	
	var returnObject = {};
	
	if (tagType === 'person'){
		var DataPersons = require('model/DataPersons');
		var data = DataPersons.loadPersonDetail(tagID, null, false);
		
		returnObject.person = data.person; 
		returnObject.title = (data.person.firstName? data.person.firstName : '')+(data.person.lastName?(' '+data.person.lastName):'');
		
		if(listType === 'favorites'){
			returnObject.assets = data.favorites;
			returnObject.subTitle = I('favorites');
		}
		else if (listType === 'together'){
			returnObject.assets = data.together;
			returnObject.subTitle = I('together');
		}
		else if (listType === 'allPictures'){
			returnObject.assets = data.allPictures;
			returnObject.subTitle = I('all_Pictures');
		};
	}
	else if (tagType === 'label'){
		
	};
	
	return returnObject;
};


//SETS THUMBNAIL FOR A PERSON
exports.setThumbForPerson = function(personID, blob, thumbnailURL,callback){
	var DataMedia = require('model/DataMedia');
	var thumbnailType = 'normal';
		
	function successCB(e){
		blob = e.asset.thumbnail; 
		globals.col.persons.update({$id:personID}, {$set:{thumbnailURL:thumbnailURL}}, {}, true);
		globals.col.persons.update({$id:personID}, {$set:{thumbnailType:thumbnailType}}, {}, true);
		globals.col.persons.commit();	
		
		var file = Titanium.Filesystem.getFile(globals.thumbsDir.nativePath, personID+'.jpg');
	 	file.write(blob);
	 	
	 	if(callback){
	 		callback(thumbnailURL);
	 	};
	};	
	
		
	if(thumbnailURL === 'random'){
		thumbnailType = 'random';
		var person = globals.col.persons.find({$id:personID});
		
		if(person[0] && person[0].mediaURLs.length > 0){
			var media = person[0].mediaURLs;
			var thumbnailURL = media[Math.floor(Math.random() * media.length)];
			
			var tryCounter = 0;
			function errorCB (){
				tryCounter++;
				if (tryCounter <=3){
					Ti.API.debug("ASSET NOT FOUND TRY #: "+tryCounter)
					var person1 = globals.col.persons.find({$id:personID});
					var media1 = person1[0].mediaURLs;
					var newThumbnailURL = media1[Math.floor(Math.random() * media1.length)];
					DataMedia.getSingleAssetFromURL(newThumbnailURL, successCB, errorCB);
				}
				else{
					//Delete ThumbNail
					var file = Titanium.Filesystem.getFile(globals.thumbsDir.nativePath, personID+'.jpg');
			 		file.deleteFile();
			 		
			 		callback();
				};
			};
			
			DataMedia.getSingleAssetFromURL(thumbnailURL, successCB, errorCB);
		}
		else{
			var file = Titanium.Filesystem.getFile(globals.thumbsDir.nativePath, person.$id+'.jpg');
	 		file.deleteFile();
	 		
			globals.col.persons.update({$id:personID}, {$set:{thumbnailType:thumbnailType}}, {}, true);
			globals.col.persons.commit();	
		};
	}
	else if (thumbnailURL === 'latest'){
		thumbnailType = 'latest';
	}
	else {
		globals.col.persons.update({$id:personID}, {$set:{thumbnailURL:thumbnailURL}}, {}, true);
		globals.col.persons.update({$id:personID}, {$set:{thumbnailType:thumbnailType}}, {}, true);
		globals.col.persons.commit();	
		
		var file = Titanium.Filesystem.getFile(globals.thumbsDir.nativePath, personID+'.jpg');
	 	file.write(blob);
	};
};


exports.getThumbForPerson = function (personID, callback){
	var DataPersons = require('model/DataPersons');
	var DataMedia = require('model/DataMedia');
	
	var person = globals.col.persons.find({$id:personID});
	person = person[0];
	
	function successCB (e){
		if (callback){
			callback(e.asset.defaultRepresentation.fullScreenImage);
		}
		else{
			Ti.App.fireEvent('thumbHeaderRowLoaded');
		};
	};
	
	
	if(person.thumbnailType === 'random'){
		if(person.media.length > 0){
			function callback1 (thumbNailURL){
				DataMedia.getSingleAssetFromURL(thumbNailURL, successCB);
			};
			
			DataPersons.setThumbForPerson(personID, null, 'random' ,callback1);
		}
		else{ //Set to random but don't have any pics '
			if (callback){
				callback('images/cover_empty_big.png');
			}
			else{
				Ti.App.fireEvent('thumbHeaderRowLoaded');
			}
		};
	}
	else{
		function errorCB (e){
			var file = Titanium.Filesystem.getFile(globals.thumbsDir.nativePath, person.$id+'.jpg');
	 		file.deleteFile();
	 		
			if (callback){
				callback('images/cover_empty_big.png');
			};
			
			Ti.App.fireEvent('thumbHeaderRowLoaded');
			Ti.API.debug("ASSET IS MISSING SETTING THUMB TO NULL")
		};
		
		DataMedia.getSingleAssetFromURL(person.thumbnailURL, successCB, errorCB);
	};
};


//CHANGES YOURSELF FROM ONE PERSON TO ANOTHER
exports.changeMyselfPersonStatus = function (personID){
	//1 Find the current myself
	var currentMyself = globals.col.persons.find({isMyself: true});
	
	//Set the current self as not myself
	if (currentMyself.length > 0){
		globals.col.persons.update({$id:currentMyself[0].$id},{$set:{isMyself:false}},{},true); 
		globals.col.persons.commit();	
	};
	
	//Set the new person as myself
	globals.col.persons.update({$id:personID},{$set:{isMyself:true}},{},true); 
	globals.col.persons.commit();	
	
	Ti.API.debug('Changed mySelf to '+personID);
};


//USED TO FILL THE TABLE WHEN ADDING A PERSON FROM PICTURE DOUBLE TAP
exports.queryAddressBookContacts = function (value){
	var data = globals.addressBookContacts;

	function sortInputFirst(input, data) {
	  	
	    var first = [];
	    var others = [];
	    for (var i = 0; i < data.length; i++) {
	        if (data[i].name.toLowerCase().indexOf(input) == 0) {
	            first.push(data[i]);
	        } 
	        else if (data[i].name.toLowerCase().indexOf(input) > 0) {
	            others.push(data[i]);
	        }
	    }
	    first.sort();
	    others.sort();
	    
	    return (first.concat(others));
	};

	var results = sortInputFirst(value.toLowerCase(), data);
	
	return results;
};


///////////////////////////////////////////////////////////////////////////////
//iPHOTO FACES METHODS BELOW

exports.getFacesGroups = function (callBack){
	var assetslibrary = require('ti.assetslibrary');
	
	var successCb = function(e) {
			var groups = e.groups;
			
			function sortAZ(ob1,ob2) {
		    	var n1 = ob1.name.toLowerCase()
		    	var n2 = ob2.name.toLowerCase()
		    	if (n1 > n2) {return 1}
				else if (n1 < n2){return -1}
			    else { return 0}//nothing to split
			};
			
			groups =  groups.sort(sortAZ);
			
			callBack(groups)
	};

	var errorCb = function(e) {
		Ti.API.error('There was an error getting loading Faces albums: ' + e.error);
	};

	//Get data to create the rows. 
	var groups = assetslibrary.getGroups([assetslibrary.AssetsGroupTypeFaces], successCb, errorCb);	
};



//Checks the Faces Album and then adds or remove the assets from contact. 
exports.checkFacesAlbumForPersonID = function (personID, addOrRemove, group){
	var DataPersons = require('model/DataPersons');
	var DataMedia = require('model/DataMedia');

	//1 Load the person
	var person = DataPersons.loadPerson(personID);
	
	//1 Get all the assets from the faces group
	var startTime = new Date().getTime();
	
	group.getAssets(function(e) {
		var assetsList = e.assets;
		
		//2 For each Media Load it. 
		for (var i = 0; i < assetsList.assetsCount; i++){
			//Add the media. (if it already exists it won't be added twice)
			var asset = assetsList.getAssetAtIndex(i);
			
			//Get all the media asset. To get the URL
			function update (media1){
				var bubbleData = {};
				bubbleData.hasSpot = false;
				bubbleData.x = 0;
				bubbleData.y = 0;
				bubbleData.mediaID = media1.$id;
				bubbleData.tagType = 'person';
				bubbleData.tagID = person.addressBookID;
				
				//Tag the media with the person
				if(addOrRemove === 'add'){
					DataMedia.addTagToMedia(media1.URL,'person',person.addressBookID,bubbleData);
				}
				else if (addOrRemove === 'remove') {
					DataMedia.removeTagFromMedia(media1.URL,'person',person.addressBookID);
				};
			};
			
			var media = globals.col.media.find({URL:asset.defaultRepresentation.url});
			media = media[0];
			
			if(media){
				update(media);
			}
			else{
				DataMedia.addMedia({URL:asset.defaultRepresentation.url},update);
			};
		};	
		var endTime = new Date().getTime();
		Ti.API.info(addOrRemove+' Faces Assets ' + (endTime - startTime) + 'ms');
		
		Ti.App.fireEvent('updatedFacesLink');
		Ti.App.fireEvent('updatePersonDetail');
		Ti.App.fireEvent('updatePersonsList');
		Ti.API.debug("checkFacesAlbumForPersonID")
	});		
};


//Find if there is a Faces match and returns the group. 
exports.findMatchForPersonToFaces = function (facesAlbumID,callBack){
	var assetslibrary = require('ti.assetslibrary');

	var successCb = function(e) {
		var groups = e.groups;
		 
		 var returnObject;
		 for (var i = 0; i < groups.length; i++){
		 	
		 	Ti.API.debug(groups[i].name+' '+facesAlbumID);
		 	
		 	if (groups[i].name === facesAlbumID){
		 		returnObject = {name: groups[i].name, group: groups[i]};
		 	};
		 	
		 	
		 	callBack(returnObject);
		 	//TODO: Ask if they want to match it manually. 
		 }; 
	};
	
	var errorCb = function(e) {
		Ti.API.error('error: ' + e.error);
	};
	
	var faces = assetslibrary.getGroups([assetslibrary.AssetsGroupTypeFaces], successCb, errorCb);
};


exports.linkFacesAlbumToPerson = function (personInfo,newGroup){
	var DataPersons = require('model/DataPersons');

	var callBackRemove = function (e){
		//There is a match. So I need to remvove the old links
		if (e && e.group){
			Ti.App.addEventListener('updatedFacesLink', update );
			DataPersons.checkFacesAlbumForPersonID(personInfo.$id,'remove',e.group);
		} 
		//There is no current match so I only have to add the new ones.
		else{
			update();
		};
	};
	
	function update (){
		//Change the linked group before adding the new assets
		var newFacesAlbumID = newGroup ? newGroup.name : (personInfo.firstName+' '+personInfo.lastName);
		//If there is a groupName use that otherwise set it to the person full name;
		
		//Tag all the pictures from the new group
		globals.col.persons.update({$id:personInfo.$id},{$set:{facesAlbumID:newFacesAlbumID}},{},true); 
		globals.col.persons.commit();
		
		
		function callBackAdd (e){
			Ti.API.debug(e);
			DataPersons.checkFacesAlbumForPersonID(personInfo.$id,'add',e.group);
		};
		
		//Get the group relative to the facesAlbumID.
		DataPersons.findMatchForPersonToFaces(newFacesAlbumID,callBackAdd);
		
		Ti.App.removeEventListener('updatedFacesLink', update )
	};
	
	//Get the group relative to the facesAlbumID.
	DataPersons.findMatchForPersonToFaces(personInfo.facesAlbumID,callBackRemove);
};