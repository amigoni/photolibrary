exports.addLabel = function(data) {
	var label; 
	
	if (data.name.length > 0){
		function capitaliseFirstLetter(string){
		    return string.charAt(0).toUpperCase() + string.slice(1);
		};
		
		data.name = capitaliseFirstLetter(data.name);
		
		label = globals.col.labels.find({name:data.name});
	
		if (label.length === 0){
			globals.col.labels.save({
				name:data.name,
				media: [],
				mediaURLs: [],
				thumbnailType: 'random'
			});	
			
			globals.col.labels.commit();
			
			label = globals.col.labels.find({$id: globals.col.labels.getLastInsertId()});
			label = label[0];
		}
		else {
			label = label[0];
		};
	};
	
	Ti.App.fireEvent('updateLabelDetail');
	Ti.App.fireEvent('updateLabelsList');
	
	return label;
};


exports.updateLabelName = function (labelID,labelName){
	globals.col.labels.update({$id:labelID}, {$set:{name:labelName}}, {}, true);
	globals.col.labels.commit();
	
	Ti.App.fireEvent('updateLabelDetail');
	Ti.App.fireEvent('updateLabelsList');
};


exports.loadLabel = function (labelID){
	var data = globals.col.labels.find({$id:labelID});

	return data[0];
};


exports.loadLabelsForMediaURL = function (mediaURL){
	var DataMedia = require('model/DataMedia');
	var DataLabels = require('model/DataLabels');
	
	var arrayToReturn = [];
	
	var medium = DataMedia.loadMediaFromURL(mediaURL);
	if (medium && medium.labels){
		for (var i =0; i< medium.labels.length; i++){
			var label = DataLabels.loadLabel(medium.labels[i]);
			arrayToReturn.push(label);
		};
	};
	
	return arrayToReturn;
};


exports.deleteLabel = function (labelID){
	var DataLabels = require('model/DataLabels');
	var DataMedia = require('model/DataMedia');
	
	//1 Get Label Data 
	var label = DataLabels.loadLabel(labelID);
	
	var media = label.media;	
	//2 Remove Media
	for (var i = media.length-1; i >= 0; i--){
		var medium = globals.col.media.find({$id:media[i]});
		if(medium[0]){
			DataMedia.removeTagFromMedia(medium[0].URL,'label',labelID);
		};
	};
	
	globals.col.labels.remove({$id:labelID});
	globals.col.labels.commit();
};



//LOADS DATA FOR LABEL LIST WINDOW
exports.loadLabelsList = function (){
	
	var data = globals.col.labels.getAll();
	
	//Get Thumbnails
	for (var i =0; i < data.length; i++){
		var file = Titanium.Filesystem.getFile(globals.thumbsDir.nativePath, data[i].$id+'.jpg');
		data[i].thumbnail =  file.read();
	};
	
	function sortAZ(ob1,ob2) {
    	var n1 = ob1.name.toLowerCase()
    	var n2 = ob2.name.toLowerCase()
    	if (n1 > n2) {return 1}
		else if (n1 < n2){return -1}
	    else { return 0}//nothing to split
	};
	
	data.sort(sortAZ);
	
	return data;
};


//LOADS DATA FOR LABEL DETAIL WINDOW
exports.loadLabelDetail = function (labelID, callback, getAlsoThumbnail){
	getAlsoThumbnail = getAlsoThumbnail === false ? false : true; 
	
	var startTime = new Date().getTime();
	var DataLabels = require('model/DataLabels');
	var DataMedia = require('model/DataMedia');
	
	var returnObject = {};
	
	//1. Get the label info 
	returnObject.label = DataLabels.loadLabel(labelID);
	
	//2. Get Favorites List
	returnObject.favorites = globals.col.media.find({archived: false, labels: labelID, isFavorite: true},{$sort:{originalDate:-1}});
	
	//3. Get All Pictures List
	returnObject.allPictures = globals.col.media.find({archived: false, labels: labelID},{$sort:{originalDate:-1}});	
	
	//4. Get Thumbnail
	if (getAlsoThumbnail === true ){
		DataLabels.getThumbForLabel(labelID, callback);
		/*
		if(returnObject.label.thumbnailType === 'random'){
			if(returnObject.label.media.length > 0){
				DataLabels.setThumbForLabel(labelID, null,'random',callback);
			}
			else{ //Set to random but don't have any pics '
				if (callback){
					callback();
				};
			};
		}
		else{	
			function successCB (e){
				if (callback){
					callback(e.asset.defaultRepresentation.fullScreenImage);
				};
			};
			
			DataMedia.getSingleAssetFromURL(returnObject.label.thumbnailURL, successCB);
		};
		*/
	};
	
		
	var endTime = new Date().getTime();
	Ti.API.info('Got Label Detail Arrays in: ' + (endTime - startTime) + 'ms');
	
	return returnObject;
};


//LOADS DATA FOR THE PICTURE LIST WINDOW
exports.loadPictureListData = function (listType,tagType,tagID){
	var DataLabels = require('model/DataLabels');
	
	var returnObject = {};
	
	var data = DataLabels.loadLabelDetail(tagID, null, false);
	
	returnObject.label = data.label; 
	returnObject.title = data.label.name;
	
	if(listType === 'favorites'){
		returnObject.assets = data.favorites;
		returnObject.subTitle = I('favorites');
	}
	else if (listType === 'allPictures'){
		returnObject.assets = data.allPictures;
		returnObject.subTitle = I('all Pictures');
	};
	
	return returnObject;
};


//SETS THE THUMBNAIL FOR A SPECIFIC LABEL
exports.setThumbForLabel = function(labelID, blob, thumbnailURL, callback){
	var DataMedia = require('model/DataMedia');
	var thumbnailType = 'normal';
	
	function successCB(e){
		blob = e.asset.thumbnail; 
		globals.col.labels.update({$id:labelID}, {$set:{thumbnailURL:thumbnailURL}}, {}, true);
		globals.col.labels.update({$id:labelID}, {$set:{thumbnailType:thumbnailType}}, {}, true);
		globals.col.labels.commit();	
		
		var file = Titanium.Filesystem.getFile(globals.thumbsDir.nativePath, labelID+'.jpg');
	 	file.write(blob);
	 	
	 	if (callback){
	 		callback(thumbnailURL);
	 	};
	 	
	};
	
	if(thumbnailURL === 'random'){
		thumbnailType = 'random';
		var label = globals.col.labels.find({$id:labelID});
		
		if(label[0] && label[0].mediaURLs.length >0){
			var media = label[0].mediaURLs;
			var thumbnailURL = media[Math.floor(Math.random() * media.length)];
			var tryCounter = 0;
			
			function errorCB (){
				tryCounter++;
				if (tryCounter <=3){
					Ti.API.debug("ASSET NOT FOUND TRY #: "+tryCounter)
					var label1 = globals.col.labels.find({$id:labelID});
					var media1 = label1[0].mediaURLs;
					var newThumbnailURL = media1[Math.floor(Math.random() * media1.length)];
					DataMedia.getSingleAssetFromURL(newThumbnailURL, successCB, errorCB);
				}
				else{
					//Delete ThumbNail
					var file = Titanium.Filesystem.getFile(globals.thumbsDir.nativePath, labelID+'.jpg');
			 		file.deleteFile();
			 		
			 		callback();
				};
			};
			
			DataMedia.getSingleAssetFromURL(thumbnailURL, successCB, errorCB);
		};
	}
	else if (thumbnailURL === 'latest'){
		thumbnailType = 'latest';
	}
	else {
		globals.col.labels.update({$id:labelID}, {$set:{thumbnailURL:thumbnailURL}}, {}, true);
		globals.col.labels.update({$id:labelID}, {$set:{thumbnailType:thumbnailType}}, {}, true);
		globals.col.labels.commit();	
		
		var file = Titanium.Filesystem.getFile(globals.thumbsDir.nativePath, labelID+'.jpg');
	 	file.write(blob);
	};
};


exports.getThumbForLabel = function (labelID, callback){
	var DataLabels = require('model/DataLabels');
	var DataMedia = require('model/DataMedia');
	
	var label = globals.col.labels.find({$id:labelID});
	label = label[0];
	
	function successCB (e){
		if (callback){
			callback(e.asset.defaultRepresentation.fullScreenImage);
		}
		else{
			Ti.App.fireEvent('thumbHeaderRowLoaded');
		};
	};
	
	
	if(label.thumbnailType === 'random'){
		if(label.media.length > 0){
			function callback1 (thumbNailURL){
				DataMedia.getSingleAssetFromURL(thumbNailURL, successCB);
			};
			
			DataLabels.setThumbForLabel(labelID, null, 'random' ,callback1);
		}
		else{ //Set to random but don't have any pics '
			if (callback){
				callback('images/cover_empty_big.png');
			}
			else{
				Ti.App.fireEvent('thumbHeaderRowLoaded');
			}
		};
	}
	else{
		function errorCB (e){
			var file = Titanium.Filesystem.getFile(globals.thumbsDir.nativePath, label.$id+'.jpg');
	 		file.deleteFile();
	 		
			if (callback){
				callback('images/cover_empty_big.png');
			};
			
			Ti.App.fireEvent('thumbHeaderRowLoaded');
			Ti.API.debug("ASSET IS MISSING SETTING THUMB TO NULL")
		};
		
		DataMedia.getSingleAssetFromURL(label.thumbnailURL, successCB, errorCB);
	};
};


//USED TO POPULATE THE TABLE WHEN YOU ARE ADDING A LABEL
exports.queryLabelsData = function (value){
	var data = globals.col.labels.find({ name : new RegExp(value,"i")});
	
	function sortInputFirst(input, data) {
  	
	    var first = [];
	    var others = [];
	    for (var i = 0; i < data.length; i++) {
	        if (data[i].name.toLowerCase().indexOf(input) == 0) {
	            first.push(data[i]);
	        } 
	        else if (data[i].name.toLowerCase().indexOf(input) > 0) {
	            others.push(data[i]);
	        }
	    }
	    
	    function sortName(ob1,ob2) {
	    	var n1 = ob1.name.toLowerCase()
	    	var n2 = ob2.name.toLowerCase()
	    	if (n1 > n2) {return 1}
			else if (n1 < n2){return -1}
		    else { return 0}//nothing to split
		};
					
	    first.sort(sortName);
	    others.sort(sortName);
	    
	    return (first.concat(others));
	};

	var results = sortInputFirst(value.toLowerCase(), data);
	
	return results;
};


//Stock Labels
var stockLabel = [
	{name: 'Happiness' ,symbol: ':-)'},
	{name: 'Dead', symbol:'X-P'},
	{name: 'Sad', symbol:':-('},
	{name: 'Funny', symbol: ':-D'},
	{name: 'Pranked', symbol: ':-P'},
	{name: 'Cool', symbol: 'B-)'},
	{name: 'Surprise', symbol:':-o'},
	{name: 'Upps', symbol:':-X'},
	{name: 'Furious', symbol:'>:-o'},
	{name: 'Crying', symbol: ":'("},
	{name: 'Naughty', symbol: '>:-)'},
	{name: 'Angel', symbol: 'o:-)'},
	{name: 'Blushing', symbol:':*)' },
	{name: 'Romance', symbol:':-*'},
];
