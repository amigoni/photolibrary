exports.checkForNews = function (){
	Ti.API.info('CHECKING NEWS');
	
	var client = Ti.Network.createHTTPClient({
	     onload : function(e) {
	        var newss= JSON.parse(this.responseText);
	   
			//Parse through news and add the new ones to the db.	   
	        for (var i = 0; i < newss.items.length; i++){
	        	var news = newss.items[i];
	        	var matchingNews = globals.col.news.find({guid: news.guid});
	        	
	        	if(!matchingNews[0]){
	        		globals.col.news.save({
	        			title: news.title,
	        			description: news.description,
	        			link: news.link,
	        			guid: news.guid,
	        			pubDate: news.pubDate,
	        			opened: false
	        		});
	        		
	        		globals.col.news.commit();
	        	};
	        };
	        
	        if (globals.col.news.count({opened: false})> 0){
	        	Ti.App.fireEvent('updateSideBar'); 
	        };
	     },
	     onerror : function(e) {
	        Ti.API.debug("Error checking News: "+e.error);
	     },
	     timeout : 5000 
	 });

	 client.open("GET", "http://mammamiaapps.com/latestnews.json");
	 client.send(); 
};

//Keep news file here and update to stringify & upload.
var newsFile = {
	items: [
		{	
			title:"Version 1.1",
			description: "Version 1.1 Released",
			link:"http://mozzarello.com",
			guid: 1,
			pubDate: "Thu, 15 Nov 2009 12:00:00 +0000",
		}
	]
};

//Ti.API.debug(JSON.stringify(newsFile))
