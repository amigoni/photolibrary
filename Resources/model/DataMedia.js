
//Query the Alasset library for what types of groups are present.
//To find if there are Faces, Events etc.
//This method is used at startup to create the tabgroup
exports.getGroupTypes = function (callback){
	//Gets the groups of each Type;
	var getGroupType = function (groupTypeIn, callback1){
		var assetslibrary = require('ti.assetslibrary');
		
		var successCb = function(e) {
			var groups = e.groups;
			callback1(groups);
		};	
		
		var errorCb = function(e) {
			Ti.API.error('error: ' + e.error);
			callback1();
		};
		
		//Create the data for the rows. 
		var groupT = assetslibrary.getGroups([groupTypeIn], successCb, errorCb);
	};

	
	var assetslibrary = require('ti.assetslibrary');
	//1. Find what groups you have. 
	//Have to use this callback method becaus of asynch module
	var events = [];
	var checkEvents = function (dataIn){
		if(dataIn[0]){
			globals.hasEvents = true;
		}
		else{
			globals.hasEvents = false;
		};
		events = dataIn.reverse(); //Reverse the order to get the latest up top.
	};
	
	getGroupType(assetslibrary.AssetsGroupTypeEvent,checkEvents);
	
	var faces = [];
	var checkFaces = function (dataIn){
		if(dataIn[0]){
			globals.hasFaces = true;
		}
		else{
			globals.hasFaces = false;
		};
		
		faces = dataIn;
	};
	getGroupType(assetslibrary.AssetsGroupTypeFaces,checkFaces);
	
	
	//For Albums I need to combine, Regular Albums with Camera Roll and PhotoStream
	var albums=[];
	
	
	
	//Warning. These are nested. 
	var photoLibraryGroup; 
	var photoLibrary = function(dataIn){
		photoLibraryGroup = dataIn[0];
		callback(albums, events, faces, photoStreamGroup, cameraRollGroup, photoLibraryGroup);//To allow faces and events to come back too. 
	};
	
	var cameraRollGroup;
	var cameraRoll = function(dataIn){
		cameraRollGroup = dataIn[0];
		//if(dataIn[0]){albums.unshift(dataIn[0])};
		//callback(albums, events, faces, photoStreamGroup, cameraRollGroup);//To allow faces and events to come back too. 
		getGroupType(assetslibrary.AssetsGroupTypeLibrary, photoLibrary);
	};
	
	var photoStreamGroup;
	var photoStream = function (dataIn){
		photoStreamGroup = dataIn[0];
		//if (dataIn[0]){albums.unshift(dataIn[0])};
		getGroupType(assetslibrary.AssetsGroupTypeSavedPhotos,cameraRoll);
	};
	
	var checkAlbums = function (dataIn){
		albums = dataIn.reverse(); //Should get me the latest First.
		getGroupType(assetslibrary.AssetsGroupTypePhotoStream,photoStream);
	};
	
	getGroupType(assetslibrary.AssetsGroupTypeAlbum,checkAlbums);
};


exports.loadAllMedia = function (group,callback,tagType,tagID){
	var startTime = new Date().getTime();
	var DataMedia = require('model/DataMedia');
	var assetslibrary = require('ti.assetslibrary');
	var assetsToReturn = []; //Array of Asset Object to return
	
	group.assetsFilter = assetslibrary.AssetsFilterPhotos;
	group.getAssets(function(e) {
		var endTime = new Date().getTime();
		Ti.API.info('Got assets in ' + (endTime - startTime) + 'ms');
		
		var assetsList = e.assets;

		for (var i = 0; i < assetsList.assetsCount; i++) {
			var asset = assetsList.getAssetAtIndex(i);
			
			var rep = asset.defaultRepresentation;
			var url = rep.url;
			
			// Createing a simplified object to return. Might not need this for performance. 
			var assetToReturn = {};
			assetToReturn.URL = url;
			assetToReturn.thumbnail = asset.thumbnail;
			assetToReturn.aspectRatioThumbnail = asset.aspectRatioThumbnail;
			
			//Check if the Medium is Tagged or not. 
			var medium = DataMedia.loadMediaFromURL(assetToReturn.URL);
			assetToReturn.selected = false;
			
			if(tagType === 'person'){
				if(medium && medium.people.indexOf(tagID) != -1){
					assetToReturn.selected = true;
				};
			}
			else if(tagType === 'label'){
				if(medium && medium.labels.indexOf(tagID) != -1){
					assetToReturn.selected = true;
				};
			};
			
			assetsToReturn.push(assetToReturn);
		};
		var endTime2 = new Date().getTime();
		Ti.API.info('Processed assets in ' + (endTime2 - endTime) + 'ms');
		
		callback(assetsToReturn);
	});		
};


exports.getSingleGroupType = function (groupTypeIn, callback){
	var assetslibrary = require('ti.assetslibrary');
		
	var successCb = function(e) {
		var groups = e.groups;
		callback(groups);
	};	
	
	var errorCb = function(e) {
		Ti.API.error('error: ' + e.error);
	};
	
	//Create the data for the rows. 
	assetslibrary.getGroups([groupTypeIn], successCb, errorCb);
};


exports.getCameraRollGroup = function (callback){
	var assetslibrary = require('ti.assetslibrary');
	var PicturesListLibrary = require('/ui/windows/PicturesListLibrary');
	
	var successCb = function(e){
		callback(e.groups[0]);
	};	
	
	var errorCb = function(e) {
		Ti.API.error('error: ' + e.error);
	};
	
	//Create the data for the rows. 
	assetslibrary.getGroups([assetslibrary.AssetsGroupTypeSavedPhotos], successCb, errorCb);
};


//When opening the library tab if I open a group I need to get data in a format that the gallery Win can read it. 
//similar to the loadAllMedia function but with no tagTyp and tagID. Additionally I need to add the assets to my library that are not in there. 
exports.getGroupAssets = function (group, callback, startIndex, endIndex){ 
	var assetslibrary = require('ti.assetslibrary');
	group.assetsFilter = assetslibrary.AssetsFilterPhotos;
	group.getAssets(
		 function(e) {
			var DataMedia = require('model/DataMedia');
			var assetsToReturn = []; //Array of Asset Object to return
			if(endIndex >= group.numberOfAssets){endIndex = group.numberOfAssets-1};
			var startTime = new Date().getTime();
			var assetsList = e.assets;
			var endTime1 = new Date().getTime();
			Ti.API.info('Library Response in ' + (endTime1 - startTime) + 'ms');
			
			//Using reverse order to get the latest pictures First.
			for (var i = (assetsList.assetsCount-1)-startIndex; i >= (assetsList.assetsCount-1)-endIndex; i--){
				var asset = assetsList.getAssetAtIndex(i);
				// Createing a simplified object to return. Might not need this for performance. 
				var assetToReturn = {};
				assetToReturn.URL = asset.defaultRepresentation.url;
				assetToReturn.thumbnail = asset.thumbnail;
				assetToReturn.aspectRatioThumbnail = asset.aspectRatioThumbnail;
				assetToReturn.date = asset.date;
				assetsToReturn.push(assetToReturn);
			};
			
			var endTime = new Date().getTime();
			Ti.API.info('Got assets from Allasset Library in ' + (endTime - startTime) + 'ms');
		
			callback(assetsToReturn, assetsList.assetsCount);
		}
	);
};


exports.loadPictureListMedia = function (assetsArray, callBack, startIndex, endIndex){
	var DataMedia = require('model/DataMedia');
	var assetslibrary = require('ti.assetslibrary');
	var startTime = new Date().getTime();
	
	if(endIndex >= assetsArray.length){endIndex =  assetsArray.length-1};
	
	//Array to put media in
	var mediaToReturn = [];
	
	//Function to get media and thumb
	function loadMediaWithThumb (mediaID,isLastOne){
		var medium = globals.col.media.find({$id:mediaID});
		
		var errorCB = function (e) {
			Ti.API.error('No such asset. Returning empty thumb picture' + e.error);
			//if (medium[0]){DataMedia.deleteMedia(medium[0].$id, false)};
			if (medium[0]){DataMedia.archiveMedia(medium[0].URL, false)};
		};
		
		var successCB = function(e) {
			if(e.asset.defaultRepresentation.url){
				var assetToReturn = {};
				assetToReturn.URL = medium[0].URL;
				assetToReturn.aspectRatioThumbnail = e.asset.aspectRatioThumbnail;
				assetToReturn.thumbnail = e.asset.thumbnail;
				//assetToReturn.isFavorite = medium[0].isFavorite;
				
				mediaToReturn.push(assetToReturn);
			}
			else{ //The media was remove from library
				Ti.API.debug("DELETING MEDIA FROM LIBRARY")
				
				//DataMedia.deleteMedia(mediaID);
				DataMedia.archiveMedia(medium[0].URL);
				Ti.App.fireEvent('pictureListUpdateListCount')
			};
			
			if (isLastOne === true){
				callBack(mediaToReturn);
				var endTime = new Date().getTime();
				Ti.API.info('Got Picture List Media of '+assetsArray.length+' in: ' + (endTime - startTime) + 'ms');		
			};
		};
		
		assetslibrary.getAsset(medium[0].URL, successCB, errorCB);
	};
	
	//Loop to get all the media from that person
	var isLastOne = false;

	for(var i = startIndex; i <= endIndex; i++){
		if (i === endIndex){
			isLastOne = true;
		};
		loadMediaWithThumb(assetsArray[i].$id,isLastOne);
	};	
};



exports.getGroupAssetsQuickTag = function (group,callback,startIndex,endIndex,tagType,tagID){ 
	var assetslibrary = require('ti.assetslibrary');
	group.assetsFilter = assetslibrary.AssetsFilterPhotos;
	group.getAssets(
		 function(e) {
			var DataMedia = require('model/DataMedia');
			
			var tagCurrentAssets;
			if(tagType === 'person'){
				var DataPersons = require('model/DataPersons');
				tagCurrentAssets = DataPersons.loadPerson(tagID).mediaURLs;
				
			}
			else if(tagType === 'label'){
				var DataLabels = require('model/DataLabels');
				tagCurrentAssets = DataLabels.loadLabel(tagID).mediaURLs;
			};
	
			
			var assetsToReturn = []; //Array of Asset Object to return
			if(endIndex >= group.numberOfAssets){endIndex = group.numberOfAssets-1};
			
			var startTime = new Date().getTime();
			var assetsList = e.assets;
			
			//Reverse order for latest up top.
			for (var i = (assetsList.assetsCount-1)-startIndex; i >= (assetsList.assetsCount-1)-endIndex; i--) {
				var asset = assetsList.getAssetAtIndex(i);
				
				var rep = asset.defaultRepresentation;
				var url = rep.url;
				
				var selected = false;
				
				if(tagCurrentAssets.indexOf(url)!= -1){
					selected = true;
				};
								
				// Createing a simplified object to return. Might not need this for performance. 
				var assetToReturn = {};
				assetToReturn.URL = url;
				assetToReturn.thumbnail = asset.thumbnail;
				assetToReturn.aspectRatioThumbnail = asset.aspectRatioThumbnail;
				assetToReturn.selected = selected;
				
				assetsToReturn.push(assetToReturn);
			};
			
			var endTime = new Date().getTime();
			Ti.API.info('Got assets from Allasset Library in ' + (endTime - startTime) + 'ms');
		
			callback(assetsToReturn);
		}
	);
};


exports.getGroupAssetsBasicArray = function (group,callback){ 
	var assetslibrary = require('ti.assetslibrary');
	group.assetsFilter = assetslibrary.AssetsFilterPhotos;
	group.getAssets(
		 function(e) {
			var DataMedia = require('model/DataMedia');
			var assetsToReturn = []; //Array of Asset Object to return

			var startTime = new Date().getTime();
			var assetsList = e.assets;
			
			for (var i = 0; i < assetsList.assetsCount; i++) {
				var asset = assetsList.getAssetAtIndex(i);
				
				var rep = asset.defaultRepresentation;
				var url = rep.url;
				
				// Createing a simplified object to return. Might not need this for performance. 
				var assetToReturn = {};
				assetToReturn.URL = url;
				assetsToReturn.push(assetToReturn);
			};
			
			var endTime = new Date().getTime();
			Ti.API.info('Got Array of Allasset Library with URLS in ' + (endTime - startTime) + 'ms');
		
			callback(assetsToReturn);
		}
	);
};


exports.getSingleAssetFromURL = function (URL, callbackS, callbackE){
	var assetslibrary = require('ti.assetslibrary');
	var DataMedia = require('model/DataMedia');
	
	function errorCB (e){
		Ti.API.error('No such asset. Returning empty thumb picture' + e.error);
		var media = globals.col.media.find({archived: false, URL:URL});
		if (media[0]){DataMedia.archiveMedia(URL)};
		
		//Return the thumb through the succ
		callbackE()
	};
	
	assetslibrary.getAsset(URL, callbackS , errorCB);
};



//Loads all the thumbnails from the Library marking already the ones that are selected. 
exports.loadLibraryPickerDataWithSelections = function (tagOrPersonID,type,callback){
	var assetslibrary = require('ti.assetslibrary');
	var assetsToReturn = []; //Array of Asset Object to return
	
	var successCb = function(e) {
		
		var groups = e.groups;
		for (var i =0; i < groups.length; i++){
			if (groups[i].type === 16){
				group = groups[i];	
			};
		};
		
		group.assetsFilter = assetslibrary.AssetsFilterPhotos;
		group.getAssets(function(e) {
			var assetsList = e.assets;
	
			for (var i = 0; i < assetsList.assetsCount; i++) {
				var asset = assetsList.getAssetAtIndex(i);
				
				// Createing a simplified object to return. Might not need this for performance. 
				var assetToReturn = {};
				assetToReturn.URL = asset.URL;
				assetToReturn.thumbnail = asset.thumbnail;
				assetToReturn.aspectRatioThumbnail = asset.aspectRatioThumbnail;
				
				//Go Find the media in the dataBase. If found see if it has this tag. 
				assetsToReturn.push(assetToReturn);
			};
			callback(assetsToReturn);
		});		
	};
	
	
	var errorCb = function(e) {
		Ti.API.error('error: ' + e.error);
	};
	
	var groups = assetslibrary.getGroups([assetslibrary.AssetsGroupTypeAll], successCb, errorCb);
};



//Loads the Media Image to be displayed.  This method must be used for performance. 
exports.loadMediaAssetImage = function (image,newIndex,callback){
	var startTime = new Date().getTime();
	var assetslibrary = require('ti.assetslibrary');
	
	var errorCB = function (e) {
		Ti.API.error('No such asset. Returning empty thumb picture' + e.error);
		var media = globals.col.media.find({URL:image.URL});
		//if (media[0]){DataMedia.deleteMedia(media[0].$id)};
		if (media[0]){DataMedia.archiveMedia(media[0].URL)};
	};
	
	var successCB = function(e) {
		//if (image.index === newIndex){//This makes sure that the image is still the same.
			//image.backgroundImage = e.asset.defaultRepresentation.fullScreenImage;
			//image.hasFullImage = true;
			
			callback(newIndex,e.asset.defaultRepresentation.fullScreenImage);
		//};
		var endTime = new Date().getTime();
		Ti.API.info('Got Full Asset in ' + (endTime - startTime) + 'ms');
	};
	
	//if (image.index === newIndex){//This makes sure that the image is still the same.
	assetslibrary.getAsset(image.URL, successCB, errorCB);	
	//};
};


exports.loadMediaFromURL = function (URL, hash){
	var DataMedia = require('model/DataMedia');
	
	var media = globals.col.media.find({archived: false, URL: URL});
	
	//If no media check for archive look in the archives
	if(!media[0] && hash){
		media = globals.col.media.find({hash: hash});
		
		if(media[0]){
			DataMedia.unarchiveMedia(URL, hash);
		};
	};
	
	return media[0];
};


exports.unarchiveMedia = function (URL, hash){
	
	var oldMedia = globals.col.media.find({hash:hash});
	
	globals.col.media.update({hash:hash},{$set:{URL: URL}},{},true); 
	globals.col.media.update({hash:hash},{$set:{archived: false}},{},true); 
	globals.col.media.commit();
	
	var newMedia = globals.col.media.find({URL: URL});
	newMedia = newMedia[0];
	
	//Tag People
	//1. Add Media From Person
	for (var i = 0; i < newMedia.people.length ; i++){
		var person = globals.col.persons.find({$id:newMedia.people[i]});
		person[0].media.push(newMedia.$id);
		person[0].mediaURLs.push(newMedia.URL);
	};
	globals.col.persons.commit();
	
	//2. Add Media From Labels
	for (var i = 0; i < newMedia.labels.length ; i++){
		var label = globals.col.labels.find({$id:newMedia.labels[i]});
		label[0].media.push(newMedia.$id,1);
		label[0].mediaURLs.push(newMedia.URL,1);
	};
	globals.col.labels.commit();
	
	
	//Tag Labels
	Ti.API.debug('Unarchived Media with Hash: '+hash+' set new URL: '+URL);
};


//Returns the strings of People Names and Tag names of a media for the Gallery View.
exports.loadMediaPeopleNamesAndTagNames = function (mediaID){
	var assetslibrary = require('ti.assetslibrary');
	
	var DataPersons = require('model/DataPersons');
	var DataLabels = require('model/DataLabels');
	
	//1. Find the media
	var medium = globals.col.media.find({$id:mediaID});
	
	//Create a dicitonary to return the data to. 
	var returnObject = {};
	returnObject.peopleNames = '';
	returnObject.labelsNames = '';
	
	//Construct the People String
	if (medium[0] && medium[0].people){
		for (var i =0; i < medium[0].people.length;i++){
			var person = DataPersons.loadPerson(medium[0].people[i]);
			var firstName = person.firstName? person.firstName : '';
			var lastName = person.lastName? person.lastName : '';
			
			if(i < medium[0].people.length-1){
				returnObject.peopleNames += firstName+' '+lastName+', ';
			}
			else{
				returnObject.peopleNames += firstName+' '+lastName;
			}
			
			if (returnObject.peopleNames === ''){
				returnObject.peopleNames == 'no people tagged';
			}
		};
	}
	else{
		returnObject.peopleNames == 'no people tagged';
	};
	
	
	//Construct the Label String
	if (medium[0] && medium[0].people){
		for (var i =0; i < medium[0].labels.length;i++){
			var label = DataLabels.loadLabel(medium[0].labels[i]);
		
			if(i < medium[0].labels.length-1){
				returnObject.labelsNames += label.name+', ';
			}
			else{
				returnObject.labelsNames +=  label.name;
			};
			
			if (returnObject.labelsNames === ''){
				returnObject.labelsNames == 'no labels tagged';
			};
		};
	}	
	else{
		returnObject.labelsNames == 'no people tagged';
	};	
	
	return returnObject;
};


exports.addMedia = function (data, callback){
	
	var DataMedia = require('model/DataMedia');
	//1 Check the Media doesn't already exist
	//Need a unique identifier
	var media = globals.col.media.find({URL: data.URL});
	var medium;
	
	//If it Doesn't exist add it'
	if(media.length === 0){
		//Want to get some data for the asset
		function callback1(e){
			var pictureDate = DataMedia.getMediaDate(e.asset);
			var location;
			if (e.asset.location) {
				location = {
					lat: e.asset.location.latitude,
					lng: e.asset.location.longitude,
					alt: e.asset.location.altitude
				};	
			}
			else{
				location = false;
			};	
			
			globals.col.media.save({
				archived: false,
				URL: data.URL,
				hash: data.hash,
				metadata: data.metadata,
				people:[],
				labels:[],
				bubbles: [],
				isFavorite: false,
				originalDate: pictureDate,
				type: e.asset.type,
				duration: e.asset.duration,
				orientation: e.asset.orientation,
				location: location
			});
			
			globals.col.media.commit();
			
			
			media = globals.col.media.find({$id:globals.col.media.getLastInsertId()});
			Ti.API.debug(media)
			medium = media[0];
			if (callback){callback(medium)};
		};
		
		DataMedia.getSingleAssetFromURL(data.URL,callback1);
	}
	else{
		medium = media[0];
	};	
	
	return medium
};



exports.getMediaDate = function (asset){
	var pictureDate;
	var rep = asset.defaultRepresentation;
	
	if(rep.metadata){ //NOTE: Videos don't seem to have Exif?'
		Ti.API.debug('Metadata exists');
		var exif = rep.metadata["{Exif}"];
		
		if (exif){
			Ti.API.debug('Exif exists');
			if(exif.DateTimeOriginal){
				Ti.API.debug('Exif Original Date');
				pictureDate = exif.DateTimeOriginal;
			}
			else if(!exif.DateTimeOriginal && exif.DateTimeDigitized){
				Ti.API.debug('Exif Digitized Date');
				pictureDate = exif.DateTimeDigitized;
			}
			else{
				Ti.API.debug('Exif no Date: using file date');
				pictureDate = asset.date;
			};
		}
		else{
			Ti.API.debug('No exif: using file date');
			pictureDate = asset.date;
		};
	}
	else{
		Ti.API.debug('No metadata: using file date');
		pictureDate = asset.date;
	};
	
	if(Object.prototype.toString.call(pictureDate) === "[object Date]"){
		Ti.API.debug('Date is a Date Object')
	}
	else if (Object.prototype.toString.call(pictureDate) === "[object String]"){
		Ti.API.debug('Date is a String Object')
		require('date');
		var parsedDate = Date.parse(pictureDate);
		
		if (!parsedDate){
			Ti.API.debug("Data.js couldn't Parse so Parsing Manually");
			var year = pictureDate.substr(0,4);
			var month = pictureDate.substr(5,2);
			var day = pictureDate.substr(8,2);
			var hours = pictureDate.substr(11,2);
			var minutes = pictureDate.substr(14,2);
			var seconds = pictureDate.substr(17,2);
			var milliseconds = 0;
			var d = new Date(year, month, day, hours, minutes, seconds, milliseconds);
			pictureDate = d;
		}
		else{
			pictureDate = parsedDate;
		};
	};
	
	pictureDate = pictureDate.getTime();	
	
	return pictureDate;
};



//This method has been replaced by archive media which retains the info. 
//Preserve it just in case. 
exports.deleteMedia = function (mediaID, fireEvents){
	fireEvents = fireEvents ? fireEvents : true;
	
	var media = globals.col.media.find({$id:mediaID});
	media = media[0];
	
	//1. Remove Media From Person
	for (var i = 0; i < media.people.length ; i++){
		var person = globals.col.persons.find({$id:media.people[i]});
		var index = person[0].media.indexOf(mediaID);
		person[0].media.splice(index,1);
		var indexURL = person[0].mediaURLs.indexOf(media.URL);
		person[0].mediaURLs.splice(indexURL,1);
	};
	globals.col.persons.commit();
	
	//2. Remove Media From Labels
	for (var i = 0; i < media.labels.length ; i++){
		var label = globals.col.labels.find({$id:media.labels[i]});
		var index = label[0].media.indexOf(mediaID);
		label[0].media.splice(index,1);
		var indexURL = label[0].mediaURLs.indexOf(media.URL);
		label[0].mediaURLs.splice(indexURL,1);
	};
	globals.col.labels.commit();
	
	//3. Remove Bubbles
	for (var i = 0; i < media.bubbles.length ; i++){
		globals.col.bubbles.remove({$id:media.bubbles[i]});
	};
	globals.col.bubbles.commit();
	
	//4. Delete Media
	globals.col.media.remove({$id:mediaID});
	globals.col.media.commit();
	
	if (fireEvents === true){
		Ti.App.fireEvent('updatePersonDetail');
		Ti.App.fireEvent('updatePersonsList');
		Ti.App.fireEvent('updateLabelDetail');
		Ti.App.fireEvent('updateLabelsList');
	};
	
};


exports.archiveMedia = function (mediaURL, fireEvents){
	fireEvents = fireEvents ? fireEvents : true;
	
	var media = globals.col.media.find({archived: false, URL:mediaURL});
	media = media[0];
	var mediaID = media.$id;
	
	//1. Remove Media From Person
	for (var i = 0; i < media.people.length ; i++){
		var person = globals.col.persons.find({$id:media.people[i]});
		var index = person[0].media.indexOf(mediaID);
		person[0].media.splice(index,1);
		var indexURL = person[0].mediaURLs.indexOf(media.URL);
		person[0].mediaURLs.splice(indexURL,1);
	};
	globals.col.persons.commit();
	
	//2. Remove Media From Labels
	for (var i = 0; i < media.labels.length ; i++){
		var label = globals.col.labels.find({$id:media.labels[i]});
		var index = label[0].media.indexOf(mediaID);
		label[0].media.splice(index,1);
		var indexURL = label[0].mediaURLs.indexOf(media.URL);
		label[0].mediaURLs.splice(indexURL,1);
	};
	globals.col.labels.commit();
	
	//4. Archive Media
	globals.col.media.update({URL:mediaURL},{$set:{archived:true}},{},true); 
	globals.col.media.commit();
	
	if (fireEvents === true){
		Ti.App.fireEvent('updatePersonDetail');
		Ti.App.fireEvent('updatePersonsList');
		Ti.App.fireEvent('updateLabelDetail');
		Ti.App.fireEvent('updateLabelsList');
	};
	
	Ti.API.debug('Archived Media: '+mediaURL);
};


exports.addTagToMediaFromLibraryPicker = function (pictureData, tagType, tagID){
	var DataPersons = require('model/DataPersons');
	var DataLabels = require('model/DataLabels');
	var DataMedia = require('model/DataMedia');
	var DataBubbles = require('model/DataBubbles');
	
	var startTime = new Date().getTime();
	
	var mediaURL = pictureData.URL;
	var hash = Ti.Utils.md5HexDigest(pictureData.backgroundImage);

	function tagMedia(media){
		if(tagType === 'person'){
			//Get or add the Person
			var person = DataPersons.addPersonFromAddressBookID(tagID);
			tagID = person.$id// Use the Person $id instead of the addresssBookID.
			
			if(person.media.indexOf(media.$id) === -1){
				person.media.push(media.$id);
				
				//Adding the media URL because it's used as the identifier in the ALAsset,
				//should think of switching fully to this instead of mediaID
				person.mediaURLs.push(mediaURL); 
			};
			
			//Add the Person to the Media
			if(media.people.indexOf(person.$id) === -1){
				media.people.push(person.$id);
			};		
			
			
			//add Bubble
			var bubbleData = {
				x: 0,
				y: 0,
				hasSpot: false,
				mediaID: media.$id,
				tagType: tagType,
				tagID: tagID
			};
			
			var newBubble = DataBubbles.addBubble(bubbleData);
			
			if(newBubble.isNewBubble === true){
				if(media.bubbles.indexOf(newBubble.bubbleData.$id) === -1){
					media.bubbles.push(newBubble.bubbleData.$id);
				};
			};
			
			globals.col.persons.commit();
			globals.col.bubbles.commit();
			globals.col.media.commit();
		}
		else if (tagType === 'label'){
			
			var label = globals.col.labels.find({$id: tagID});
			label = label[0];
			
			if(label.media.indexOf(media.$id) === -1){
				label.media.push(media.$id);
				
				//Adding the media URL because it's used as the identifier in the ALAsset,
				//should think of switching fully to this instead of mediaID
				label.mediaURLs.push(mediaURL); 
			};
			
			//Add the Person to the Media
			if(media.labels.indexOf(label.$id) === -1){
				media.labels.push(label.$id);
			};		
			
			globals.col.labels.commit();
			globals.col.media.commit();
			
			var endTime = new Date().getTime();
			Ti.API.info('Added Tag to Media in: ' + (endTime - startTime) + 'ms');
		};
	};
	
	
	//1. Find if media already exists if not add it. 
	var media = DataMedia.loadMediaFromURL(mediaURL,hash);
	
	if (media){
		tagMedia(media);
	}
	else{
		media = DataMedia.addMedia({URL: mediaURL, hash: hash}, tagMedia);
	};
};


exports.addTagToMedia = function (pictureData, tagType, tagID, bubbleData, fireEvents){
	fireEvents = fireEvents ? fireEvents : false; //To Limit firing update events from Library Picker
	//Need to use addressBookID as an input incase the person is not yet in the DB. 
	var DataPersons = require('model/DataPersons');
	var DataLabels = require('model/DataLabels');
	var DataMedia = require('model/DataMedia');
	var DataBubbles = require('model/DataBubbles');
	
	var mediaURL = pictureData.URL;
	var hash = Ti.Utils.md5HexDigest(pictureData.backgroundImage);
	
	var startTime = new Date().getTime();
	
	//Need this as a function, as it has to be called possibly as a callback.
	var newBubble;
	
	function tagMedia(media){
		var mediaID = media.$id;

		//Add Media ID to the bubbleData
		if(bubbleData){
			bubbleData.mediaID = mediaID;
		}
		else{
			bubbleData = {};
			bubbleData.x = 0;
			bubbleData.y = 0;
			bubbleData.hasSpot = false;
			//bubbleData.title = dataForBubble.name;
			bubbleData.mediaID = mediaID;
			bubbleData.tagType = tagType;	
			bubbleData.tagID = tagID;//This gets generated in the DataMedia.addTagToMedia method
		};
		
		
		//2. add the tag respectively. 
		if(tagType === 'person'){
			//tagID must be addressBookID
			//2.1.1 Find the person with the addressBookID
			var	person = DataPersons.addPersonFromAddressBookID(tagID);
			
			
			//2.1.2 Add Media to Person
			if(person.media.indexOf(mediaID) === -1){
				person.media.push(mediaID);
				
				//Adding the media URL because it's used as the identifier in the ALAsset,
				//should think of switching fully to this instead of mediaID
				person.mediaURLs.push(mediaURL); 
				
				if(person.media.length === 1){
					globals.col.persons.update({$id:person.$id}, {$set:{thumbnailURL: mediaURL}}, {}, true);
					globals.col.persons.commit();	
					
					var file = Titanium.Filesystem.getFile(globals.thumbsDir.nativePath, person.$id+'.jpg');
				 	file.write(pictureData.backgroundImage);
				}
			};
			
			//2.1.3 Add Person to Media	
			if(media.people.indexOf(person.$id) === -1){
				media.people.push(person.$id);
			};		
			
			//2.1.4 Add Person ID to bubble
			bubbleData.tagID = person.$id
			
			//3 Add the bubble
			newBubble = DataBubbles.addBubble(bubbleData);
			
			if(newBubble.isNewBubble === true){
				if(media.bubbles.indexOf(newBubble.bubbleData.$id) === -1){
					media.bubbles.push(newBubble.bubbleData.$id);
				};
			};	
			
			globals.col.persons.commit(); //This shouldb e an update function. Check the get media.
			globals.col.bubbles.commit(); //This shouldb e an update function. Check the get media.
			globals.col.media.commit(); //This shouldb e an update function. Check the get media.
		}
		else if (tagType === 'label'){
			//2.2.1 Get Label
			var label = DataLabels.loadLabel(tagID);
			
			if (label){
				//2.2.2 Add Media to Label
				if(label.media.indexOf(mediaID) === -1){
					label.media.push(mediaID);
					
					//Adding the media URL because it's used as the identifier in the ALAsset,
					//should think of switching fully to this instead of mediaID
					label.mediaURLs.push(mediaURL); 
					
					if(label.media.length === 1){
						globals.col.labels.update({$id: label.$id}, {$set:{thumbnailURL: mediaURL}}, {}, true);
						globals.col.labels.commit();	
						
						var file = Titanium.Filesystem.getFile(globals.thumbsDir.nativePath, label.$id+'.jpg');
					 	file.write(pictureData.backgroundImage);
					}
				};
				
		
				//2.2.3 Add Label to Media	
				if(media.labels.indexOf(label.$id) === -1){
					media.labels.push(label.$id);
				};	
				
				globals.col.labels.commit();
				globals.col.media.commit(); //This shouldb e an update function. Check the get media.
			}
			else {
				Ti.API.debug('No Label Found when adding tag to media')
			};
		};
		
		//*** This causes significant delay and might not be necessary here.
		//Check if you can just refresh when you focus on these windows, instead
		//or just create a different update that is cheaper. 
		/*
		setTimeout(function(){
			if (tagType === 'person' && fireEvents === true){
				Ti.App.fireEvent('updatePersonDetail');
				Ti.App.fireEvent('updatePersonsList');
			}
			else if (tagType === 'label'  && fireEvents === true){
				Ti.App.fireEvent('updateLabelDetail');
				Ti.App.fireEvent('updateLabelsList');
			};
		},10);
		*/
		
		var endTime = new Date().getTime();
		Ti.API.info('Added Media From Picture in ' + (endTime - startTime) + 'ms');
		
		return newBubble; // Return this value to trigger the bubble creation or not.
	};
	
	//1. Find if media already exists if not add it. 
	var media1 = DataMedia.loadMediaFromURL(mediaURL,hash);
	
	if (media1){
		tagMedia(media1);
	}
	else{
		media1 = DataMedia.addMedia({URL: mediaURL, hash: hash},tagMedia);
	};
};


exports.removeTagFromMedia = function (mediaURL, tagType, tagID , fireEvents){
	fireEvents = fireEvents? fireEvents : false; //Prevents firing many updates if it's a batch remove.
	
	var DataMedia = require('model/DataMedia');
	var DataBubbles = require('model/DataBubbles');
	
	//Ti.API.debug(mediaURL+' '+tagType+' '+tagID);
	
	//1. Find if media already exists. 
	var media = DataMedia.loadMediaFromURL(mediaURL);
	var mediaID; 
	
	if (media){
		mediaID = media.$id;
		
		//In this case the tagID is the $id of the person
		if(tagType === 'person'){
			var DataPersons = require('model/DataPersons');
			
			//1 Remove media from Person
			var person = globals.col.persons.find({$id: tagID});//DataPersons.loadPerson(tagID);
			person = person[0];
			
			var mediaIndex = person.media.indexOf(mediaID);	
		
			if(mediaIndex != -1){
				person.media.splice(mediaIndex,1);
				
				//Remove the  URL from the array
				var urlIndex = person.mediaURLs.indexOf(mediaURL);
				if(urlIndex != -1){
					person.mediaURLs.splice(urlIndex,1);
				};
			};

			globals.col.persons.commit();
			
			//2 Remove person from Media
			var personIndex = media.people.indexOf(person.$id);	
			
			if(personIndex != -1){
				media.people.splice(personIndex,1);
			};
			
			//Remove Bubbles
			//1. find the bubble with mediaID
			var bubbles = globals.col.bubbles.find({mediaID:mediaID,tagType:tagType,tagID:tagID});
			
			if (bubbles[0]){
				var bubble = bubbles[0];
				//1. Delete Bubble
				DataBubbles.deleteBubble(bubble.$id);
				
				//2. remove Bubble Id from Media. 
				var bubbleIndex = media.bubbles.indexOf(bubble.$id)
				if (bubbleIndex != -1){
					media.bubbles.splice(bubbleIndex,1);
				};
			};
			
			globals.col.media.commit();
			
			if(fireEvents === true){
				Ti.App.fireEvent('updatePersonDetail');
				Ti.App.fireEvent('updatePersonsList');
			};	
		}
		else if(tagType === 'label'){
			var DataLabels = require('model/DataLabels');
			
			//1 Remove media from label
			var label = DataLabels.loadLabel(tagID);
			
			var mediaIndex = label.media.indexOf(mediaID);	
			
			if(mediaIndex != -1){
				label.media.splice(mediaIndex,1);
				
				//Remove the  URL from the array
				var urlIndex = label.mediaURLs.indexOf(mediaURL)
				if(urlIndex != -1){
					label.mediaURLs.splice(urlIndex,1);
				};
			};
			
			globals.col.labels.commit();
			
			//2 Remove label from Media
			var labelIndex = media.labels.indexOf(tagID);	
		
			if(labelIndex != -1){
				media.labels.splice(labelIndex,1);
			};
			
			globals.col.media.commit();
			
			if(fireEvents === true){
				Ti.App.fireEvent('updateLabelDetail');
				Ti.App.fireEvent('updateLabelsList');
			};	
		};
		
		Ti.App.fireEvent('checkIfDataHasChanged');
		Ti.API.debug('Removed '+tagID+' From Media');
	}
	else{
		
		Ti.API.debug('Error: No Media for that URL(Remove Tag From Media)');
		
		return;
	};
};


//Changes the Favorite Status of a media.
exports.changeFavoriteStatusForMedia = function (pictureData){
	var DataMedia = require('model/DataMedia');
	
	var mediaURL = pictureData.URL;
	
	var hash = Ti.Utils.md5HexDigest(pictureData.backgroundImage);
	
	function toggleFavorite (medium){
		var makeFavorite;
		if(medium){
			if (!medium.isFavorite || medium.isFavorite == false){
				makeFavorite = true;
			}
			else if (medium.isFavorite == true){
				makeFavorite = false;
			};
		};
		
		Ti.API.info('Set Favorite to '+makeFavorite+' for '+mediaURL);
		
		globals.col.media.update({URL: mediaURL},{$set:{isFavorite:makeFavorite}},{},true); 
		
		globals.col.media.commit();
		
		Ti.App.fireEvent('updatePersonDetail');
		Ti.App.fireEvent('updatePersonsList');
		Ti.App.fireEvent('updateLabelDetail');
		Ti.App.fireEvent('updateLabelsList');
	}
	
	var media = DataMedia.loadMediaFromURL(mediaURL,hash);
	
	
	if (media){
		toggleFavorite(media);
	}
	else{
		DataMedia.addMedia({URL: mediaURL, hash: hash}, toggleFavorite);
	};
};