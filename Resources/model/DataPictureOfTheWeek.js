exports.loadPictureOfTheWeekListData = function (){
	globals.properties.firstWorkingDay = 1; //i.e. 1 Monday
	
	var returnObject = {}
	returnObject.pastPicturesOfTheWeek  = globals.col.pictureOfTheWeek.getAll().reverse();
	
	for (var i = 0; i < returnObject.pastPicturesOfTheWeek.length; i++){
		var file = Titanium.Filesystem.getFile(globals.pictureOfTheWeekDir.nativePath, returnObject.pastPicturesOfTheWeek[i].$id+'_thumb.jpg');
		returnObject.pastPicturesOfTheWeek[i].thumbnail =  file.read();
	};
	
	return returnObject;
};


function getDateRange (){
	var moment = require('lib/Moment');
	
	globals.properties.firstWorkingDay = 1; //i.e. 1 Monday
	
	//1. Figure out all the dates.
	var today = moment().day();
	var thisWeekFirstWorkingDay;
	if (today === 0 ){ //IF it's sunday need to go back one week.
		thisWeekFirstWorkingDay = moment().day(globals.properties.firstWorkingDay-7).startOf('day');
	}
	else{
		thisWeekFirstWorkingDay = moment().day(globals.properties.firstWorkingDay).startOf('day');
	};
	
	var lastWeekFirstWorkingDay = moment(thisWeekFirstWorkingDay).day(globals.properties.firstWorkingDay-7);
	
	return {thisWeekFirstWorkingDay: thisWeekFirstWorkingDay, lastWeekFirstWorkingDay:lastWeekFirstWorkingDay}
}


exports.getLastWeekMedia = function (mainCallBack){
	var startTime = new Date().getTime();
	
	var DataMedia = require('model/DataMedia');
	
	var dateRange = getDateRange();
	
	var returnObject = {};
	returnObject.thisWeekFirstWorkingDay = dateRange.thisWeekFirstWorkingDay;
	returnObject.lastWeekFirstWorkingDay = dateRange.lastWeekFirstWorkingDay;
	
	var lastWeekPictures = [];
	var thisWeekPictures = [];
	returnObject.lastWeekPictures = lastWeekPictures;
	returnObject.thisWeekPictures = thisWeekPictures;
	
	
	// Find if I picked last week
	var lastWeek = globals.col.pictureOfTheWeek.find({date: dateRange.lastWeekFirstWorkingDay.valueOf()});
	returnObject.pickedLastWeek = lastWeek.length > 0 ? true : false;
	
	function mediaCallback(albums, faces, events, photoStreamGroup, cameraRollGroup, photoLibraryGroup){
		function getAssets(group, nextGroup){
			group.getAssets(function (e){
				for (var i = e.assets.assetsCount-1; i >= 0 ; i--){
					var asset = e.assets.getAssetAtIndex(i);
					
					var date = asset.date.getTime();
					
					if (date >  dateRange.lastWeekFirstWorkingDay.valueOf()){
						var DataMedia = require('model/DataMedia');
						var dateRep = date;
						//var dateRep = DataMedia.getMediaDate(asset);
						Ti.API.debug('ASSET DATE : '+date+' EXIF DATE: '+dateRep);
	
						if (dateRep >= dateRange.thisWeekFirstWorkingDay.valueOf()){
							var assetToReturn = {};
							assetToReturn.URL = asset.defaultRepresentation.url;
							assetToReturn.thumbnail = asset.thumbnail;
							assetToReturn.aspectRatioThumbnail = asset.aspectRatioThumbnail;
							assetToReturn.date = asset.date;
							
							thisWeekPictures.push(assetToReturn);	
							Ti.API.debug(asset.date)
						}
						else if (dateRep < dateRange.thisWeekFirstWorkingDay.valueOf()){
							var assetToReturn = {};
							assetToReturn.URL = asset.defaultRepresentation.url;
							assetToReturn.thumbnail = asset.thumbnail;
							assetToReturn.aspectRatioThumbnail = asset.aspectRatioThumbnail;
							assetToReturn.date = asset.date;
							
							lastWeekPictures.push(assetToReturn);	
							Ti.API.debug(asset.date)
						};
					}
					else{
						Ti.API.debug('Too Old: '+asset.date);
						//Exit the loop when you encounter the first old picture.
						//Pictures in PhotoLibrary and Camera roll are in chronological order
						break
					};
				};
				
				if( isLastGroup === false && nextGroup){
					nextGroup()
				}
				else if (isLastGroup === true && mainCallBack) {
					mainCallBack(returnObject)
					
					var endTime = new Date().getTime();
					Ti.API.info('Processed POW in: ' + (endTime - startTime) + 'ms');	
				};
			});	
		};
		
		//Going through all the possible groups to find the last pictures. 
		var isLastGroup = false;
		/*	
		if (albums && albums[0]){
			//Dunno if Albums are sorted by date. 
			for (var i = 0; i < albums.length; i++){
				getAssets(albums[i]);
			};
			
			getPhotoLibrary();
			Ti.API.debug('POW: GOT ALBUMS ASSETS');
		}
		else{
			getPhotoLibrary();
		};
		
		function getPhotoLibrary (){
			if (photoLibraryGroup){
				getAssets(photoLibraryGroup, getPhotoStream);
				Ti.API.debug('POW: GOT LIBRARY ASSETS');
			}
			else{
				getPhotoStream();
			};
		}
		
		function getPhotoStream(){
			if(photoStreamGroup){
				getAssets(photoStreamGroup, getCameraRoll);
				Ti.API.debug('POW: GOT PHOTOSTREAM ASSETS');
			}
			else{
				getCameraRoll();
			};
		}
		*/
		function getCameraRoll (){
			if(cameraRollGroup){
				isLastGroup = true;
				getAssets(cameraRollGroup);
				Ti.API.debug('POW: GOT CAMERA ASSETS');
			};
		};
		
		getCameraRoll();
	};
	
	
	function errorCb (){
		alert(I("No_library_access"));
	};
	
	
	DataMedia.getGroupTypes(mediaCallback);
};



exports.loadPictureOfTheWeekDetail = function (mediaURL) {

	var pow = globals.col.pictureOfTheWeek.find({URL: mediaURL});
	var file = Titanium.Filesystem.getFile(globals.pictureOfTheWeekDir.nativePath, pow[0].$id+'_photo.jpg');	
	
	return {
		image: file.read(),
		data: pow[0]
	};
};



exports.addPictureOfTheWeek = function (mediaURL, awardCaption){
	var moment = require('lib/Moment');
	
	//1. Get Picture
	var thisWeekFirstWorkingDay = moment().day(globals.properties.firstWorkingDay).startOf('day').valueOf();
	var lastWeekFirstWorkingDay = moment(thisWeekFirstWorkingDay).day(globals.properties.firstWorkingDay-7);
	
	var pastPOW = globals.col.pictureOfTheWeek.find({date:lastWeekFirstWorkingDay.valueOf()});
	if (pastPOW.length === 0){
		globals.col.pictureOfTheWeek.save({
			date: lastWeekFirstWorkingDay.valueOf(),
			URL: mediaURL,
			awardCaption: awardCaption
		});
		
		globals.col.pictureOfTheWeek.commit();
		
		//Set Thumbnail
		function callbackAsset(e){
			var thumbFile = Titanium.Filesystem.getFile(globals.pictureOfTheWeekDir.nativePath, globals.col.pictureOfTheWeek.getLastInsertId()+'_thumb.jpg');
			thumbFile.write(e.asset.thumbnail);
			
			var photoFile = Titanium.Filesystem.getFile(globals.pictureOfTheWeekDir.nativePath, globals.col.pictureOfTheWeek.getLastInsertId()+'_photo.jpg');
			photoFile.write(e.asset.defaultRepresentation.fullScreenImage);
			Ti.App.fireEvent('updatePictureOfTheWeekList');
		};
		
		 var DataMedia = require('model/DataMedia');
		 DataMedia.getSingleAssetFromURL(mediaURL, callbackAsset);
	}
	else{
		alert(I("already_picked_a_Picture_of_the_Week"));
	};
};