// subscribe to events send with postMessage
worker.addEventListener('message',function(event){
	
	var DataMedia = require('model/DataMedia');

	var startTime = new Date().getTime();
	
	var assetsToReturn = []; //Array of Asset Object to return
		 
	event.data.group.getAssets(function(e) {
		var assetsList = e.assets;
	
		for (var i = event.data.message; i < event.data.group.numberOfAssets; i++) {
			var asset = assetsList.getAssetAtIndex(i);
			
			var rep = asset.defaultRepresentation;
			var url = rep.url;
			
			// Createing a simplified object to return. Might not need this for performance. 
			var assetToReturn = {};
			assetToReturn.URL = url;
			
			
			//Had to localize this function. it's a different context. '
			var addMedia = function (data){
			//1 Check the Media doesn't already exist
				//Need a unique identifier
				var mySK = 'mySecretKey'
				var jsondb = require("com.irlgaming.jsondb");
				
				var mediaDB = jsondb.factory('media',mySK);
				var media =mediaDB.find({URL:data.URL});
				
				//If it Doesn't exist add it'
				if(media.length === 0){
					globals.col.media.save({
						URL: data.URL,
						metadata: data.metadata,
						people:[],
						labels:[],
						bubbles: [],
						isFavorite: false
						//createdDate:data.createdDate,
					});
					
					globals.col.media.commit();
					
					media = globals.col.media.find({$id:globals.col.media.getLastInsertId()});
					media = media[0];
				}
				else{
					media = media[0];
				};	
					
				return media;
			};
			
			var newMedia = addMedia(assetToReturn);
			
			
			assetToReturn.thumbnail = asset.thumbnail;
			assetToReturn.aspectRatioThumbnail = asset.aspectRatioThumbnail;
			assetToReturn.$id = newMedia.$id;
			assetToReturn.isFavorite = newMedia.isFavorite;

			assetsToReturn.push(assetToReturn);
			
			Ti.App.fireEvent('gotAsset', {data:assetsToReturn});
			
			// send data back to any subscribers
   			// pull data from the event from the data property
		};
		
		var endTime = new Date().getTime();
		Ti.API.info('Processed assets from Library in ' + (endTime - startTime) + 'ms');
	});
});


