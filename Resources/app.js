Titanium.UI.iPhone.hideStatusBar();
Titanium.UI.iPhone.statusBarStyle = Titanium.UI.iPhone.StatusBar.TRANSLUCENT_BLACK;

Ti.API.info(Ti.Utils.sha1("testing"));
Ti.API.info(Ti.Filesystem.applicationDataDirectory);

Titanium.include('lib/json.i18n.js');
//var I = require('/lib/i18nhack');
var _ = require('/lib/underscore');
var Loader = require('Loader');
var assetslibrary = require('ti.assetslibrary');

assetslibrary.addEventListener('libraryChanged', function(e) {
	Ti.API.info("ASSETS LIBRARY HAS CHANGED");
	Ti.API.info(e);
	
	//Closes any modal window
	if (globals.currentModal && globals.currentModal.kind === 'pictureGallery'){
		globals.currentModal.winCloseAnimation();
	};
	
	//Update Picture List
	Ti.App.fireEvent('updatePicturesList',{length:29});
});


//var worker = require('ti.worker');

///////////////////////////////////////////////////////////////////////////////
//TestFlight Setup
//var testflight = require('com.0x82.testflight');

// WARNING: ONLY USE THIS ON DEVELOPMENT! DON'T GO TO THE APP STORE WITH THIS LINE!!
//testflight.setDeviceIdenifier(Ti.Platform.id);

//testflight.takeOff('7d979545243ad638851dc8f69b99a19a_MTAzNzIyMDExLTEwLTE5IDAzOjM1OjA4Ljg5NjEwOA');
///////////////////////////////////////////////////////////////////////////////

var globals = {};
globals.version = '1.3.1';

Ti.API.debug('Contact Authorization UNKNOWN: '+Ti.Contacts.AUTHORIZATION_UNKNOWN)
Ti.API.debug('Contact Authorization AUTHORIZED: '+Ti.Contacts.AUTHORIZATION_AUTHORIZED)
Ti.API.debug('Contact Authorization DENIED: '+Ti.Contacts.AUTHORIZATION_DENIED)
Ti.API.debug('Contact Authorization RESTRICTED: '+Ti.Contacts.AUTHORIZATION_RESTRICTED);
Ti.API.debug('Current Contact Authorization: '+Ti.Contacts.contactsAuthorization)

//Check Location Services authorization for picture access and then contact authorization
if (Titanium.Geolocation.locationServicesAuthorization ==  Ti.Geolocation.AUTHORIZATION_DENIED || Titanium.Geolocation.locationServicesAuthorization == Ti.Geolocation.AUTHORIZATION_RESTRICTED ){
	alert("Sorry there is no way to access your photos without Location Services access.\nPlease enable location services for this app in your Settings")
}
else{
	if (Ti.Contacts.contactsAuthorization == Ti.Contacts.AUTHORIZATION_UNKNOWN){
		Ti.Contacts.requestAuthorization(function(e){
	        if (e.success) {
	        	Loader.loadMe();
	        } 
	        else {
	        	alert("There was an error accessing your contacts. Please check your Settings.")
	        }
	    });
	}
	else if (Ti.Contacts.contactsAuthorization == Ti.Contacts.AUTHORIZATION_DENIED || Ti.Contacts.contactsAuthorization == Ti.Contacts.AUTHORIZATION_RESTRICTED ){
		alert("Sorry there is no way to tag your photos without accessing your contacts.\nPlease enable access to your contacts in your Privacy Settings")
	}
	else{
		Loader.loadMe();
	};
};	


Ti.App.addEventListener('resume', function(){
	
	//Checking for permissions
	if (Titanium.Geolocation.locationServicesAuthorization ==  Ti.Geolocation.AUTHORIZATION_DENIED || Titanium.Geolocation.locationServicesAuthorization == Ti.Geolocation.AUTHORIZATION_RESTRICTED ){
		var notify = require('bencoding.localnotify');
		notify.cancelAllLocalNotifications();
		if (globals.tabGroup){globals.tabGroup.turnOff()}
		alert("Sorry there is no way to access your photos without Location Services access.\nPlease close this app and  enable location services for this app in your Settings")
	}
	else if (Ti.Contacts.contactsAuthorization == Ti.Contacts.AUTHORIZATION_DENIED || Ti.Contacts.contactsAuthorization == Ti.Contacts.AUTHORIZATION_RESTRICTED ){
		var notify = require('bencoding.localnotify');
		notify.cancelAllLocalNotifications();
		if (globals.tabGroup){globals.tabGroup.turnOff()}
		alert("Sorry there is no way to tag your photos without accessing your contacts.\nPlease close this app and enable access to your contacts in your Privacy Settings")
	}
	else{
		var DataPersons = require('model/DataPersons');
		DataPersons.loadAllContactsFromAddressBook();
		
		var Notifications = require('Notifications');
		Notifications.checkNotifications();
		setTimeout(function (){
			Ti.App.iOS.removeEventListener('notification', notificationCallback);
		},3000);
		
		
		//Picture of the Week. In case it was open when you paused the app. 
		if (globals.currentNavGroupWindow && globals.currentNavGroupWindow.kind === 'pictureOfTheWeekList'){
			globals.currentNavGroupWindow.resetData();
		};
		
		//Check for news
		var DataNews = require('model/DataNews');
		DataNews.checkForNews();
		
		//Changes in library
		var dataHasChanged = false;
		if (dataHasChanged === true){
			if (globals.currentModal && globals.currentModal.kind === 'pictureGallery'){
				globals.currentModal.winCloseAnimation();
			};
			Ti.App.fireEvent('updatePicturesList',{length:29});
		};
	};
}); 


Ti.App.addEventListener('pause', function(){
	Ti.App.iOS.addEventListener('notification', notificationCallback);
	
	//Close the Picture of the Week Detail picture
	if (globals.currentModal && globals.currentModal.kind === 'pictureOfTheWeekDetail'){
		globals.currentModal.closeAnimation();
		globals.currentModal = null;
	};
}); 


function notificationCallback(e){
	if (e.userInfo.type === 'pictureOfTheWeek'){
		Ti.App.fireEvent('goToPictureOfTheWeek');
	};
};	
	