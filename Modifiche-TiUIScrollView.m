-(void)setPagingEnabled_:(id)args
{
    ENSURE_SINGLE_ARG(args, NSNumber);
    ENSURE_UI_THREAD(setPagingEnabled_, args);
    [scrollView setPagingEnabled:[args boolValue]];
}

-(void)setScrollEnabled_:(id)args
{
    ENSURE_SINGLE_ARG(args, NSNumber);
    ENSURE_UI_THREAD(setScrollEnabled_, args);
    [scrollView setScrollEnabled:[args boolValue]];
}